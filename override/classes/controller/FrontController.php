<?php
/**
 * Page Cache Ultimate, Page Cache standard and Speed pack are powered by Jpresta (jpresta . com)
 *
 *    @author    Jpresta
 *    @copyright Jpresta
 *    @license   You are just allowed to modify this copy for your own use. You must not redistribute it. License
 *               is permitted for one Prestashop instance only but you can install it on your test instances.
 */
use Symfony\Component\Debug\Debug;
class FrontController extends FrontControllerCore
{
    /**
     * Initializes front controller: sets smarty variables,
     * class properties, redirects depending on context, etc.
     *
     * @global bool     $useSSL           SSL connection flag
     * @global Cookie   $cookie           Visitor's cookie
     * @global Smarty   $smarty
     * @global Cart     $cart             Visitor's cart
     * @global string   $iso              Language ISO
     * @global Country  $defaultCountry   Visitor's country object
     * @global string   $protocol_link
     * @global string   $protocol_content
     * @global Link     $link
     * @global array    $css_files
     * @global array    $js_files
     * @global Currency $currency         Visitor's selected currency
     *
     * @throws PrestaShopException
     */
    public function init()
    {
        Hook::exec(
            'actionFrontControllerInitBefore',
            [
                'controller' => $this,
            ]
        );

        /*
         * Globals are DEPRECATED as of version 1.5.0.1
         * Use the Context object to access objects instead.
         * Example: $this->context->cart
         */
        global $useSSL, $cookie, $smarty, $cart, $iso, $defaultCountry, $protocol_link, $protocol_content, $link, $css_files, $js_files, $currency;

        if (self::$initialized) {
            return;
        }

        self::$initialized = true;

        Controller::init();

        // enable Symfony error handler if debug mode enabled
        $this->initDebugguer();

		// Réinitialisation du delivery mode ---------------------------------------------------
		if(Tools::getValue('reinit_delivery_mode')) {
			$this->context->updateDeliveryMode(0);
            Tools::redirect('/index.php');
		}

		// Delivery mode sur page produit ------------------------------------------------------
		if(Tools::getValue('delivery_mode') && Tools::getValue('set_product_delivery_mode')) {
			$this->context->updateDeliveryMode((int) Tools::getValue('delivery_mode'));
            if(Tools::getValue('redirect')) {
				// On rafraîchit la page et on ajoute au panier --------------------------------
	            $redirect 				= urldecode(Tools::getValue('redirect'));
	            $quantity_wanted		= urldecode(Tools::getValue('quantity_wanted'));
	            $id_product				= urldecode(Tools::getValue('id_product'));
	            $redirect 				= str_replace("&add-to-cart=1", "", $redirect);
	            $redirect 				= str_replace(".html", ".html?add-to-cart=1&quantity_wanted=".$quantity_wanted."&id_product=".$id_product."&page=produit", $redirect);
	            Tools::redirect($redirect);
	        }
	        else
	            Tools::redirect('/index.php');
		}

		// Delivery mode sur page d'accueil ----------------------------------------------
		if(Tools::getValue('delivery_mode') && Tools::getValue('set_home_delivery_mode')) {
			$this->context->updateDeliveryMode((int) Tools::getValue('delivery_mode'));
            $redirect 					= $this->context->link->getCategoryLink(Configuration::get('PS_HOME_CATEGORY'));
            Tools::redirect($redirect);
		}

		// Delivery mode sur page category ----------------------------------------------
		if(Tools::getValue('delivery_mode') && Tools::getValue('set_categ_delivery_mode')) {
			$this->context->updateDeliveryMode((int) Tools::getValue('delivery_mode'));
			$root_category 				= Category::getRootCategory();
            $redirect 					= $this->context->link->getCategoryLink($root_category);
			if(Tools::getIsset("add_cart")) {
				// On rafraîchit la page et on ajoute au panier -------------------------
	            $quantity_wanted		= urldecode(Tools::getValue('quantity_wanted'));
	            $id_product				= urldecode(Tools::getValue('id_product'));
	            $redirect 			   .= "?add-to-cart=1&quantity_wanted=".$quantity_wanted."&id_product=".$id_product."&page=categorie";
	            Tools::redirect($redirect);
			}
			else {
				// On rafraîchit la page catégorie --------------------------------------
	            Tools::redirect($redirect);
			}
		}

		// Delivery mode sur page commande -----------------------------------------------
		if(Tools::getValue('delivery_mode') && Tools::getValue('set_checkout_delivery_mode')) {
			$this->context->updateDeliveryMode((int) Tools::getValue('delivery_mode'));
            if(Tools::getValue('redirect')) {
	            $redirect 				= urldecode(Tools::getValue('redirect'));
	            Tools::redirect($redirect);
	        }
	        else
	            Tools::redirect('/index.php?controller=order');
		}

		// Delivery mode sur page historique de commande ---------------------------------
		if(Tools::getValue('delivery_mode') && Tools::getValue('set_reorder_delivery_mode')) {
			$this->context->updateDeliveryMode((int) Tools::getValue('delivery_mode'));
            $redirect 			= urldecode(Tools::getValue('redirect'));
            Tools::redirect($redirect);
		}
		// Vérification du delivery mode -------------------------------------------------
		if(Tools::getValue('check_delivery_mode')) {
			if($this->context->cookie->__get('delivery_mode')) {
	            $this->ajaxRender(Tools::jsonEncode([
	                'deliveryMode_ok' => 1
	            ]));
			}
			else {
	            $this->ajaxRender(Tools::jsonEncode([
	                'deliveryMode_ok' => 0
	            ]));
			}
			die();
		}
		if($this->context->cookie->__get('delivery_mode')) {
			$this->context->delivery_mode = $this->context->cookie->__get('delivery_mode');
		}

        // If current URL use SSL, set it true (used a lot for module redirect)
        if (Tools::usingSecureMode()) {
            $useSSL = true;
        }

        // For compatibility with globals, DEPRECATED as of version 1.5.0.1
        $css_files = $this->css_files;
        $js_files = $this->js_files;

        $this->sslRedirection();

        if ($this->ajax) {
            $this->display_header = false;
            $this->display_footer = false;
        }

        // If account created with the 2 steps register process, remove 'account_created' from cookie
        if (isset($this->context->cookie->account_created)) {
            unset($this->context->cookie->account_created);
        }

        ob_start();

        $protocol_link = (Configuration::get('PS_SSL_ENABLED') || Tools::usingSecureMode()) ? 'https://' : 'http://';
        $useSSL = ((isset($this->ssl) && $this->ssl && Configuration::get('PS_SSL_ENABLED')) || Tools::usingSecureMode()) ? true : false;
        $protocol_content = ($useSSL) ? 'https://' : 'http://';
        $link = new Link($protocol_link, $protocol_content);
        $this->context->link = $link;

        if ($id_cart = (int) $this->recoverCart()) {
            $this->context->cookie->id_cart = (int) $id_cart;
        }

        if ($this->auth && !$this->context->customer->isLogged($this->guestAllowed)) {
            Tools::redirect('index.php?controller=authentication' . ($this->authRedirection ? '&back=' . $this->authRedirection : ''));
        }

        /* Theme is missing */
        if (!is_dir(_PS_THEME_DIR_)) {
            throw new PrestaShopException($this->trans('Current theme is unavailable. Please check your theme\'s directory name ("%s") and permissions.', [basename(rtrim(_PS_THEME_DIR_, '/\\'))], 'Admin.Design.Notification'));
        }

        if (Configuration::get('PS_GEOLOCATION_ENABLED')) {
            if (($new_default = $this->geolocationManagement($this->context->country)) && Validate::isLoadedObject($new_default)) {
                $this->context->country = $new_default;
            }
        } elseif (Configuration::get('PS_DETECT_COUNTRY')) {
            $has_currency = isset($this->context->cookie->id_currency) && (int) $this->context->cookie->id_currency;
            $has_country = isset($this->context->cookie->iso_code_country) && $this->context->cookie->iso_code_country;
            $has_address_type = false;

            if ((int) $this->context->cookie->id_cart && ($cart = new Cart($this->context->cookie->id_cart)) && Validate::isLoadedObject($cart)) {
                $has_address_type = isset($cart->{Configuration::get('PS_TAX_ADDRESS_TYPE')}) && $cart->{Configuration::get('PS_TAX_ADDRESS_TYPE')};
            }

            if ((!$has_currency || $has_country) && !$has_address_type) {
                $id_country = $has_country && !Validate::isLanguageIsoCode($this->context->cookie->iso_code_country) ?
                    (int) Country::getByIso(strtoupper($this->context->cookie->iso_code_country)) : (int) Tools::getCountry();

                $country = new Country($id_country, (int) $this->context->cookie->id_lang);

                if (!$has_currency && validate::isLoadedObject($country) && $this->context->country->id !== $country->id) {
                    $this->context->country = $country;
                    $this->context->cookie->id_currency = (int) Currency::getCurrencyInstance($country->id_currency ? (int) $country->id_currency : (int) Configuration::get('PS_CURRENCY_DEFAULT'))->id;
                    $this->context->cookie->iso_code_country = strtoupper($country->iso_code);
                }
            }
        }

        $currency = Tools::setCurrency($this->context->cookie);

        if (isset($_GET['logout']) || ($this->context->customer->logged && Customer::isBanned($this->context->customer->id))) {
            $this->context->customer->logout();

            Tools::redirect(isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : null);
        } elseif (isset($_GET['mylogout'])) {
            $this->context->customer->mylogout();
            Tools::redirect(isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : null);
        }

        /* Cart already exists */
        if ((int) $this->context->cookie->id_cart) {
            if (!isset($cart)) {
                $cart = new Cart($this->context->cookie->id_cart);
            }

            if (Validate::isLoadedObject($cart) && $cart->orderExists()) {
                PrestaShopLogger::addLog('Frontcontroller::init - Cart cannot be loaded or an order has already been placed using this cart', 1, null, 'Cart', (int) $this->context->cookie->id_cart, true);
                unset($this->context->cookie->id_cart, $cart, $this->context->cookie->checkedTOS);
                $this->context->cookie->check_cgv = false;
            } elseif ((int) (Configuration::get('PS_GEOLOCATION_ENABLED'))
                && !in_array(strtoupper($this->context->cookie->iso_code_country), explode(';', Configuration::get('PS_ALLOWED_COUNTRIES')))
                && $cart->nbProducts()
                && (int) (Configuration::get('PS_GEOLOCATION_NA_BEHAVIOR')) != -1
                && !FrontController::isInWhitelistForGeolocation()
                && !in_array($_SERVER['SERVER_NAME'], ['localhost', '127.0.0.1', '::1'])
            ) {
                /* Delete product of cart, if user can't make an order from his country */
                PrestaShopLogger::addLog('Frontcontroller::init - GEOLOCATION is deleting a cart', 1, null, 'Cart', (int) $this->context->cookie->id_cart, true);
                unset($this->context->cookie->id_cart, $cart);
            } elseif ($this->context->cookie->id_customer != $cart->id_customer || $this->context->cookie->id_lang != $cart->id_lang || $currency->id != $cart->id_currency) {
                // update cart values
                if ($this->context->cookie->id_customer) {
                    $cart->id_customer = (int) $this->context->cookie->id_customer;
                }
                $cart->id_lang = (int) $this->context->cookie->id_lang;
                $cart->id_currency = (int) $currency->id;
                $cart->update();
            }
            /* Select an address if not set */
            if (isset($cart) && (!isset($cart->id_address_delivery) || $cart->id_address_delivery == 0 ||
                !isset($cart->id_address_invoice) || $cart->id_address_invoice == 0) && $this->context->cookie->id_customer) {
                $to_update = false;
                if (!isset($cart->id_address_delivery) || $cart->id_address_delivery == 0) {
                    $to_update = true;
                    $cart->id_address_delivery = (int) Address::getFirstCustomerAddressId($cart->id_customer);
                }
                if (!isset($cart->id_address_invoice) || $cart->id_address_invoice == 0) {
                    $to_update = true;
                    $cart->id_address_invoice = (int) Address::getFirstCustomerAddressId($cart->id_customer);
                }
                if ($to_update) {
                    $cart->update();
                }
            }
        }

        if (!isset($cart) || !$cart->id) {
            $cart = new Cart();
            $cart->id_lang = (int) $this->context->cookie->id_lang;
            $cart->id_currency = (int) $this->context->cookie->id_currency;
            $cart->id_guest = (int) $this->context->cookie->id_guest;
            $cart->id_shop_group = (int) $this->context->shop->id_shop_group;
            $cart->id_shop = $this->context->shop->id;
            if ($this->context->cookie->id_customer) {
                $cart->id_customer = (int) $this->context->cookie->id_customer;
                $cart->id_address_delivery = (int) Address::getFirstCustomerAddressId($cart->id_customer);
                $cart->id_address_invoice = (int) $cart->id_address_delivery;
            } else {
                $cart->id_address_delivery = 0;
                $cart->id_address_invoice = 0;
            }

            // Needed if the merchant want to give a free product to every visitors
            $this->context->cart = $cart;
            CartRule::autoAddToCart($this->context);
        } else {
            $this->context->cart = $cart;
        }

    	$this->context->ordered_products			= [];
		if($this->context->cookie->id_customer)
	        $this->context->ordered_products		= Order::getAllOrderedProducts($this->context->cookie->id_customer);

    	$this->context->cart_products				= [];
        if($this->context->cart->id)
        	$this->context->cart_products			= Cart::getCartProductIDs($this->context->cart->id);		

        $this->context->cart->checkAndUpdateAddresses();

        $this->context->smarty->assign('request_uri', Tools::safeOutput(urldecode($_SERVER['REQUEST_URI'])));

        // Automatically redirect to the canonical URL if needed
        if (!empty($this->php_self) && !Tools::getValue('ajax')) {
            $this->canonicalRedirection($this->context->link->getPageLink($this->php_self, $this->ssl, $this->context->language->id));
        }

        Product::initPricesComputation();

        $display_tax_label = $this->context->country->display_tax_label;
        if (isset($cart->{Configuration::get('PS_TAX_ADDRESS_TYPE')}) && $cart->{Configuration::get('PS_TAX_ADDRESS_TYPE')}) {
            $infos = Address::getCountryAndState((int) $cart->{Configuration::get('PS_TAX_ADDRESS_TYPE')});
            $country = new Country((int) $infos['id_country']);
            $this->context->country = $country;
            if (Validate::isLoadedObject($country)) {
                $display_tax_label = $country->display_tax_label;
            }
        }

        /*
         * These shortcuts are DEPRECATED as of version 1.5.0.1
         * Use the Context to access objects instead.
         * Example: $this->context->cart
         */
        self::$cookie = $this->context->cookie;
        self::$cart = $cart;
        self::$smarty = $this->context->smarty;
        self::$link = $link;
        $defaultCountry = $this->context->country;

        $this->displayMaintenancePage();

        if (Country::GEOLOC_FORBIDDEN == $this->restrictedCountry) {
            $this->displayRestrictedCountryPage();
        }

        $this->iso = $iso;
        $this->context->cart = $cart;
        $this->context->currency = $currency;

        Hook::exec(
            'actionFrontControllerInitAfter',
            [
                'controller' => $this,
            ]
        );
    }

    /**
     * Compiles and outputs full page content.
     *
     * @return bool
     *
     * @throws Exception
     * @throws SmartyException
     */
    public function display()
    {
		$time_start = microtime(true);
        $this->context->smarty->assign([
            'layout' => $this->getLayout(),
            'stylesheets' => $this->getStylesheets(),
            'javascript' => $this->getJavascript(),
            'js_custom_vars' => Media::getJsDef(),
            'notifications' => $this->prepareNotifications(),
        ]);

        $this->smartyOutputContent($this->template);

		$time_end = microtime(true);
		$diff  = ($time_end-$time_start)*1000;
	
		//die("getProductSearchVariables processing time = $diff ms<br>");

        return true;
    }

    /*
    * module: pagecache
    * date: 2020-12-14 11:19:09
    * version: 7.0.2
    */
    protected function displayAjax()
    {
        if (!Tools::getIsset('page_cache_dynamics_mods')) {
            if (is_callable('parent::displayAjax')) {
                return parent::displayAjax();
            }
            else {
                return;
            }
        }
        $this->initHeader();
        $this->assignGeneralPurposeVariables();
        require_once _PS_MODULE_DIR_ . 'pagecache/pagecache.php';
        $result = PageCache::execDynamicHooks($this);
        if (Tools::version_compare(_PS_VERSION_,'1.6','>')) {
            $this->context->smarty->assign(array(
                'js_def' => PageCache::getJsDef($this),
            ));
            $result['js'] = $this->context->smarty->fetch(_PS_ALL_THEMES_DIR_.'javascript.tpl');
        }
        $this->context->cookie->write();
        header('Content-Type: text/html');
        header('Cache-Control: no-cache');
        header('X-Robots-Tag: noindex');
        die(Tools::jsonEncode($result));
    }
    /*
    * module: pagecache
    * date: 2020-12-14 11:19:09
    * version: 7.0.2
    */
    public function isRestrictedCountry()
    {
        return $this->restrictedCountry;
    }

    protected function initDebugguer()
    {
        if (true === _PS_MODE_DEV_) {
            Debug::enable();
        }
    }

    protected function assignGeneralPurposeVariables()
    {
		// Les stores et les shops doivent avoir le même ID !!! ----------------------------------
        $store = Store::getStoreAddress($this->context->shop->id);

        $templateVars = [
            'cart' => $this->cart_presenter->present($this->context->cart),
            'currency' => $this->getTemplateVarCurrency(),
            'customer' => $this->getTemplateVarCustomer(),
            'language' => $this->objectPresenter->present($this->context->language),
            'page' => $this->getTemplateVarPage(),
            'shop' => $this->getTemplateVarShop(),
            'urls' => $this->getTemplateVarUrls(),
            'configuration' => $this->getTemplateVarConfiguration(),
            'field_required' => $this->context->customer->validateFieldsRequiredDatabase(),
            'breadcrumb' => $this->getBreadcrumb(),
            'link' => $this->context->link,
            'time' => time(),
            'static_token' => Tools::getToken(false),
            'token' => Tools::getToken(),
            'debug' => _PS_MODE_DEV_,
            'shop_is_open' => Store::isOpen(),
            'shop_phone' => $store['phone'],
            'open_hours' => Store::getDisplayOpenHours(),
            'google_api_key' => Configuration::get("LLW_BASTA_GOOGLE_MAP_API_KEY"),
            'delivery_mode' => $this->context->delivery_mode,
            'taux_reduc_emporter' => SpecificPriceRule::getShopTakeAwayReductionRate(),
            'footer_before' => Hook::exec('displayFooterBefore'),
            'controller' => $this->context->controller->php_self,
            'store_name' => Store::getStoreName($this->context->shop->id),
            'auto_add_to_cart' => false,
            'time' => time()
        ];

		// Gestion de l'ajout automatique du produit au panier ---------------------------
        if(Tools::getValue('add-to-cart')) {
	        $templateVars ['auto_add_quantity_wanted']		= urldecode(Tools::getValue('quantity_wanted'));
            $templateVars ['auto_add_id_product']			= urldecode(Tools::getValue('id_product'));
            $templateVars ['auto_add_page']					= urldecode(Tools::getValue('page'));
            $templateVars ['auto_add_to_cart']				= 1;
        }

        $modulesVariables = Hook::exec(
            'actionFrontControllerSetVariables',
            [
                'templateVars' => &$templateVars,
            ],
            null,
            true
        );

        if (is_array($modulesVariables)) {
            foreach ($modulesVariables as $moduleName => $variables) {
                $templateVars['modules'][$moduleName] = $variables;
            }
        }

        $this->context->smarty->assign($templateVars);

        Media::addJsDef([
            'prestashop' => $this->buildFrontEndObject($templateVars),
        ]);
    }

    public function getTemplateVarCustomer($customer = null)
    {
        if (Validate::isLoadedObject($customer)) {
            $cust = $this->objectPresenter->present($customer);
        } else {
            $cust = $this->objectPresenter->present($this->context->customer);
        }

        unset(
            $cust['secure_key'],
            $cust['passwd'],
            $cust['show_public_prices'],
            $cust['deleted'],
            $cust['id_lang']
        );

        $cust['is_logged'] = $this->context->customer->isLogged(true);

        $cust['gender'] = $this->objectPresenter->present(new Gender($cust['id_gender']));
        unset($cust['id_gender']);

        $cust['risk'] = $this->objectPresenter->present(new Risk($cust['id_risk']));
        unset($cust['id_risk']);

        $addresses = $this->context->customer->getSimpleAddresses();
        foreach ($addresses as &$a) {
            $a['formatted'] = AddressFormat::generateAddress(new Address($a['id']), [], '<br>');
        }
        $cust['addresses'] = $addresses;
        $cust['last_delivery_address'] = '';
                
        if($cust['is_logged']) {
			$customer_orders 					= Order::getCustomerOrders($this->context->customer->id);
			if(is_array($customer_orders) && count(($customer_orders))) {
				$last_order 					= $customer_orders [0];
	            $order 							= new Order((int) $last_order['id_order']);
				$last_delivery_address			= new Address((int) $last_order['id_address_delivery']);
				$cust['last_delivery_address']	= $last_delivery_address->address1.' '.$last_delivery_address->postcode.' '.$last_delivery_address->city;
			}

        }

        return $cust;
    }

    public function setMedia()
    {
        parent::setMedia();

        $this->registerStylesheet('theme-stylish-select', '/assets/css/select2.css', ['media' => 'all', 'priority' => 1000]);
        $this->registerStylesheet('theme-aos', '/assets/js/aos/aos.css', ['media' => 'all', 'priority' => 1000]);
        $this->registerStylesheet('theme-animate', '/assets/css/animate.min.css', ['media' => 'all', 'priority' => 1000]);
        $this->registerStylesheet('theme-button', '/assets/css/button.css', ['media' => 'all', 'priority' => 1000]);
        $this->registerStylesheet('theme-strengthify', '/assets/js/password-strength-meter-2.1.0/password.css', ['media' => 'all', 'priority' => 1000]);

        $this->registerJavascript('google-api-places', 'https://maps.googleapis.com/maps/api/js?key='.Configuration::get("LLW_KYO_GOOGLE_MAP_API_KEY").'&libraries=places', ['position' => 'bottom', 'priority' => 1000, 'server' => 'remote']);
        //$this->registerJavascript('theme-bootstrap', '/assets/js/bootstrap.min.js', ['position' => 'bottom', 'priority' => 1100]);
        $this->registerJavascript('theme-jquery-stylish-select', '/assets/js/select2.js', ['position' => 'bottom', 'priority' => 1200]);
        $this->registerJavascript('theme-aos-js', '/assets/js/aos/aos.js', ['position' => 'bottom', 'priority' => 1300]);
        $this->registerJavascript('theme-strengthify', '/assets/js/password-strength-meter-2.1.0/password.js', ['position' => 'bottom', 'priority' => 1400]);
        $this->registerJavascript('theme-bootbox', '/assets/js/bootbox/bootbox.min.js', ['position' => 'bottom', 'priority' => 1500]);
        $this->registerJavascript('theme-bootbox-locale', '/assets/js/bootbox/bootbox.locales.min.js', ['position' => 'bottom', 'priority' => 1500]);
        
        return true;
    }

    protected function getCategoryPath($category)
    {
        return array(
            'title' => $category->name,
            'url' => $this->context->link->getCategoryLink($category),
        );
    }

    protected function getCurrentCategoryPath($category)
    {
        return array(
            'title' => $category->name,
            'url' => '',
        );
    }

    /**
     * Adds jQuery UI component(s) to queued JS file list.
     *
     * @param string|array $component
     * @param string $theme
     * @param bool $check_dependencies
     */
    public function addJqueryUI($component, $theme = 'base', $check_dependencies = true)
    {
        $css_theme_path = '/js/jquery/ui/themes/' . $theme . '/minified/jquery.ui.theme.min.css';
        $css_path = '/js/jquery/ui/themes/' . $theme . '/minified/jquery-ui.min.css';
        $js_path = '/js/jquery/ui/jquery-ui.min.js';

        $this->registerStylesheet('jquery-ui-theme', $css_theme_path, ['media' => 'all', 'priority' => 95]);
        $this->registerStylesheet('jquery-ui', $css_path, ['media' => 'all', 'priority' => 90]);
        $this->registerJavascript('jquery-ui', $js_path, ['position' => 'bottom', 'priority' => 90]);
    }

}
