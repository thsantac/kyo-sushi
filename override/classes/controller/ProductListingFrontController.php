<?php
/**
 * Page Cache Ultimate, Page Cache standard and Speed pack are powered by Jpresta (jpresta . com)
 *
 *    @author    Jpresta
 *    @copyright Jpresta
 *    @license   You are just allowed to modify this copy for your own use. You must not redistribute it. License
 *               is permitted for one Prestashop instance only but you can install it on your test instances.
 */
use PrestaShop\PrestaShop\Core\Product\Search\SortOrder;
abstract class ProductListingFrontController extends ProductListingFrontControllerCore
{
    /*
    * module: pagecache
    * date: 2020-12-14 11:19:09
    * version: 7.0.2
    */
    protected function doProductSearch($template, $params = array(), $locale = null)
    {
        if (!Tools::getIsset('page_cache_dynamics_mods')) {
            return parent::doProductSearch($template, $params, $locale);
        }
    }

    /**
     * This method is the heart of the search provider delegation
     * mechanism.
     *
     * It executes the `productSearchProvider` hook (array style),
     * and returns the first one encountered.
     *
     * This provides a well specified way for modules to execute
     * the search query instead of the core.
     *
     * The hook is called with the $query argument, which allows
     * modules to decide if they can manage the query.
     *
     * For instance, if two search modules are installed and
     * one module knows how to search by category but not by manufacturer,
     * then "ManufacturerController" will use one module to do the query while
     * "CategoryController" will use another module to do the query.
     *
     * If no module can perform the query then null is returned.
     *
     * @param ProductSearchQuery $query
     *
     * @return ProductSearchProviderInterface or null
     */
    private function getProductSearchProviderFromModules($query)
    {
        $providers = Hook::exec(
            'productSearchProvider',
            ['query' => $query],
            null,
            true
        );

        if (!is_array($providers)) {
            $providers = [];
        }

        foreach ($providers as $provider) {
            if ($provider instanceof ProductSearchProviderInterface) {
                return $provider;
            }
        }
    }

    /**
     * This returns all template variables needed for rendering
     * the product list, the facets, the pagination and the sort orders.
     *
     * @return array variables ready for templating
     */
    protected function getProductSearchVariables()
    {
        /*
         * To render the page we need to find something (a ProductSearchProviderInterface)
         * that knows how to query products.
         */

        // the search provider will need a context (language, shop...) to do its job
        $context = $this->getProductSearchContext();

        // the controller generates the query...
        $query = $this->getProductSearchQuery();

        // ...modules decide if they can handle it (first one that can is used)
        $provider = $this->getProductSearchProviderFromModules($query);

        // if no module wants to do the query, then the core feature is used
        if (null === $provider) {
            $provider = $this->getDefaultProductSearchProvider();
        }

        $resultsPerPage = (int) Tools::getValue('resultsPerPage');
        if ($resultsPerPage <= 0) {
            $resultsPerPage = Configuration::get('PS_PRODUCTS_PER_PAGE');
        }

        // we need to set a few parameters from back-end preferences
        $query
            ->setResultsPerPage($resultsPerPage)
            ->setPage(max((int) Tools::getValue('page'), 1))
        ;

        // set the sort order if provided in the URL
        if (($encodedSortOrder = Tools::getValue('order'))) {
            $query->setSortOrder(SortOrder::newFromString(
                $encodedSortOrder
            ));
        }

        $query->setSortOrder(SortOrder::newFromString('product.categpos.asc'));

        // get the parameters containing the encoded facets from the URL
        $encodedFacets = Tools::getValue('q');

        /*
         * The controller is agnostic of facets.
         * It's up to the search module to use /define them.
         *
         * Facets are encoded in the "q" URL parameter, which is passed
         * to the search provider through the query's "$encodedFacets" property.
         */

        $query->setEncodedFacets($encodedFacets);

        // We're ready to run the actual query!

        /** @var ProductSearchResult $result */
        $result = $provider->runQuery(
            $context,
            $query
        );

        if (Configuration::get('PS_CATALOG_MODE') && !Configuration::get('PS_CATALOG_MODE_WITH_PRICES')) {
            $this->disablePriceControls($result);
        }

        // sort order is useful for template,
        // add it if undefined - it should be the same one
        // as for the query anyway
        if (!$result->getCurrentSortOrder()) {
            $result->setCurrentSortOrder($query->getSortOrder());
        }

        // prepare the products
        $products 							= $result->getProducts();
        
        /*foreach($products as $product) {
	        echo "product : ".$product['name']." dans catégorie : ".$product['category_default']."<br>";
        }
        die();*/
        
		$time_start = microtime(true);

        $product_features 					= [];
		$excluded_feature 					= Configuration::get("LLW_KYO_EXCLUDE_FEATURE");
		$prev_category_default 				= $products[0]['id_category_default'];
		$nb_products 						= 0;
		$nb_products_by_category 			= [];
        foreach($products as &$product) {
	        $data_features					= '';
			foreach($product['features'] as $product_feature) {
				if($product_feature['id_feature'] == $excluded_feature) {
					$data_features 		   .= $product_feature['id_feature_value']."-";
				}
			}
			$data_features 					= rtrim($data_features, "-");
			$product['data_features'] 		= $data_features;
			if($product['id_category_default'] != $prev_category_default) {
				$nb_products_by_category [$prev_category_default]	= $nb_products;
				$prev_category_default		= $product['id_category_default'];
				$nb_products 				= 0;
			}
			$nb_products++;
        }
		$nb_products_by_category [$prev_category_default]	= $nb_products;

        $products = $this->prepareMultipleProductsForTemplate(
            $products
        );

        // render the facets
        if ($provider instanceof FacetsRendererInterface) {
            // with the provider if it wants to
            $rendered_facets = $provider->renderFacets(
                $context,
                $result
            );
            $rendered_active_filters = $provider->renderActiveFilters(
                $context,
                $result
            );
        } else {
            // with the core
            $rendered_facets = $this->renderFacets(
                $result
            );
            $rendered_active_filters = $this->renderActiveFilters(
                $result
            );
        }

        $pagination = $this->getTemplateVarPagination(
            $query,
            $result
        );

        // prepare the sort orders
        // note that, again, the product controller is sort-orders
        // agnostic
        // a module can easily add specific sort orders that it needs
        // to support (e.g. sort by "energy efficiency")
        $sort_orders = $this->getTemplateVarSortOrders(
            $result->getAvailableSortOrders(),
            $query->getSortOrder()->toString()
        );

        $sort_selected = false;
        if (!empty($sort_orders)) {
            foreach ($sort_orders as $order) {
                if (isset($order['current']) && true === $order['current']) {
                    $sort_selected = $order['label'];

                    break;
                }
            }
        }
        
        $searchVariables = [
            'result' => $result,
            'label' => $this->getListingLabel(),
            'products' => $products,
            'sort_orders' => $sort_orders,
            'sort_selected' => $sort_selected,
            'pagination' => $pagination,
            'rendered_facets' => $rendered_facets,
            'rendered_active_filters' => $rendered_active_filters,
            'js_enabled' => $this->ajax,
            'current_url' => $this->updateQueryString([
                'q' => $result->getEncodedFacets(),
            ]),
            'features_to_exclude' => $features_to_exclude,
            'nb_products_by_category' => $nb_products_by_category
        ];

        Hook::exec('filterProductSearch', ['searchVariables' => &$searchVariables]);
        Hook::exec('actionProductSearchAfter', $searchVariables);

        return $searchVariables;
    }

    protected function getProductSearchVariablesAlt()
    {
		// On affiche tous les produits de la boutique ------------------------------------------------
		$context 						= Context::getContext();
		$root_category					= Category::getRootCategory();
		$category 						= new Category($root_category->id, $context->language->id, $context->shop->id);
        $assembler 						= new ProductAssembler($context);
        $presenterFactory 				= new ProductPresenterFactory($context);
        $presentationSettings 			= $presenterFactory->getPresentationSettings();
        $presenter 						= new PrestaShop\PrestaShop\Core\Product\ProductListingPresenter(
            new PrestaShop\PrestaShop\Adapter\Image\ImageRetriever(
                $context->link
            ),
            $context->link,
            new PrestaShop\PrestaShop\Adapter\Product\PriceFormatter(),
            new PrestaShop\PrestaShop\Adapter\Product\ProductColorsRetriever(),
            $context->getTranslator()
        );

		$raw_products 					= Product::getProducts($context->language->id, 0, 1000, 'position', 'ASC', $root_category->id, true, $context);
        $products_for_template 			= array();
        foreach ($raw_products as &$item) {
	        $item['data_features']		= '';
            $products_for_template[] 	= $presenter->present(
                $presentationSettings,
                $assembler->assembleProduct($item),
                $context->language
            );
        }
		
		$nb_products					= count($raw_products);

        /*foreach($products as $product) {
	        echo "product : ".$product['name']." dans catégorie : ".$product['category_default']."<br>";
        }
        die();*/
        
        $product_features 					= [];
		$excluded_feature 					= Configuration::get("LLW_KYO_EXCLUDE_FEATURE");
		$prev_category_default 				= $products_for_template[0]['id_category_default'];
		$nb_products 						= 0;
		$nb_products_by_category 			= [];
        foreach($products_for_template as &$product) {
	        $data_features					= '';
			foreach($product['features'] as $product_feature) {
				if($product_feature['id_feature'] == $excluded_feature) {
					$data_features 		   .= $product_feature['id_feature_value']."-";
				}
			}
			$data_features 					= rtrim($data_features, "-");
			$product['data_features'] 		= $data_features;
			if($product['id_category_default'] != $prev_category_default) {
				$nb_products_by_category [$prev_category_default]	= $nb_products;
				$prev_category_default		= $product['id_category_default'];
				$nb_products 				= 0;
			}
			$nb_products++;
        }
		$nb_products_by_category [$prev_category_default]	= $nb_products;

        $products = $products_for_template;

        $searchVariables = [
            'result' => [],
            'label' => $this->getListingLabel(),
            'products' => $products,
            'sort_orders' => [],
            'sort_selected' => null,
            'pagination' => [],
            'rendered_facets' => [],
            'rendered_active_filters' => [],
            'js_enabled' => $this->ajax,
            'current_url' => null,
            'features_to_exclude' => $features_to_exclude,
            'nb_products_by_category' => $nb_products_by_category
        ];

        return $searchVariables;
    }
}
