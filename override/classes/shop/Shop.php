<?php
/**
 * 2007-2019 PrestaShop and Contributors
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/OSL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2019 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
 * International Registered Trademark & Property of PrestaShop SA
 */
use PrestaShop\PrestaShop\Core\Addon\Theme\Theme;

/**
 * @since 1.5.0
 */
class Shop extends ShopCore
{
	public $mt_min_order = 0;
	public $frais_par_zone = 1;
    public $taux_reduc_emporter;
    public $id_solumag;

    const TO_BE_DELIVERED = 1;
    const TO_GO = 2;

    protected static $debug_message = '';

    public function __construct($id = null, $id_lang = null, $id_shop = null)
    {
		self::$definition['fields']['mt_min_order'] = ['type' => self::TYPE_INT];
		self::$definition['fields']['frais_par_zone'] = ['type' => self::TYPE_INT];
		self::$definition['fields']['taux_reduc_emporter'] = ['type' => self::TYPE_INT, 'required' => true];
		self::$definition['fields']['id_solumag'] = ['type' => self::TYPE_INT];

	    self::$debug_message = '';
	    
		ShopCore::__construct($id, $id_lang, $id_shop);
	}

    protected static function init()
    {
        Shop::$id_shop_default_tables = ['product', 'category'];

        $asso_tables = [
            'carrier' => ['type' => 'shop'],
            'carrier_lang' => ['type' => 'fk_shop'],
            'category' => ['type' => 'shop'],
            'category_lang' => ['type' => 'fk_shop'],
            'cms' => ['type' => 'shop'],
            'cms_lang' => ['type' => 'fk_shop'],
            'cms_category' => ['type' => 'shop'],
            'cms_category_lang' => ['type' => 'fk_shop'],
            'contact' => ['type' => 'shop'],
            'country' => ['type' => 'shop'],
            'currency' => ['type' => 'shop'],
            'employee' => ['type' => 'shop'],
            'hook_module' => ['type' => 'fk_shop'],
            'hook_module_exceptions' => ['type' => 'fk_shop', 'primary' => 'id_hook_module_exceptions'],
            'image' => ['type' => 'shop'],
            'lang' => ['type' => 'shop'],
            'meta_lang' => ['type' => 'fk_shop'],
            'module' => ['type' => 'shop'],
            'module_currency' => ['type' => 'fk_shop'],
            'module_country' => ['type' => 'fk_shop'],
            'module_group' => ['type' => 'fk_shop'],
            'product' => ['type' => 'shop'],
            'product_attribute' => ['type' => 'shop'],
            'product_lang' => ['type' => 'fk_shop'],
            'referrer' => ['type' => 'shop'],
            'store' => ['type' => 'shop'],
            'webservice_account' => ['type' => 'shop'],
            'warehouse' => ['type' => 'shop'],
            'stock_available' => ['type' => 'fk_shop', 'primary' => 'id_stock_available'],
            'carrier_tax_rules_group_shop' => ['type' => 'fk_shop'],
            'attribute' => ['type' => 'shop'],
            'feature' => ['type' => 'shop'],
            'group' => ['type' => 'shop'],
            'attribute_group' => ['type' => 'shop'],
            'tax_rules_group' => ['type' => 'shop'],
            'zone' => ['type' => 'shop'],
            'manufacturer' => ['type' => 'shop'],
            'supplier' => ['type' => 'shop'],
            'calendar_llw' => ['type' => 'shop'],
        ];

        foreach ($asso_tables as $table_name => $table_details) {
            Shop::addTableAssociation($table_name, $table_details);
        }

        Shop::$initialized = true;
    }

    public static function debugDelivery() {
		$context 					= Context::getContext();
	    $debug_delivery				= false;
	    if($context->customer->email=="thierry.santacana@gmail.com"
	    || $context->customer->email=="pymercury@kyosushi.com"
	    || $context->customer->email=="thierry@llw.fr"
	    || $context->customer->email=="isabelle@llw.fr"
	    ) {
		    $debug_delivery			= true;
	    }
	    return $debug_delivery;
    }

    public static function getShopName($id)
    {
		$sql = '
			SELECT `name`
			FROM `'._DB_PREFIX_.'shop`
			WHERE `id_shop` = '.$id
		;
		return Db::getInstance()->getValue($sql);
    }

    public static function getMinimumCommande($id_shop='')
    {
		if(empty($id_shop)) {
			$id_shop 	= Context::getContext()->shop->id;
		}
	    $sql 			= '
	    					SELECT MIN(bksm.mt_min_order)
	    					FROM `'._DB_PREFIX_.'bestkit_shippingmatrix` bksm 
							JOIN `'._DB_PREFIX_.'bestkit_shippingmatrix_shop` bksms ON bksm.id_bestkit_shippingmatrix = bksms.id_bestkit_shippingmatrix
							WHERE bksms.id_shop = '.$id_shop.'
	    					';
	    $mt_min_order	= Db::getInstance()->getValue($sql);

        $context 						= Context::getContext();
		if($context->cart->id_address_delivery && $context->delivery_mode==Context::TO_BE_DELIVERED) {

			$_address 					= new Address($context->cart->id_address_delivery);
	        $customer 					= new Customer((int)$context->cart->id_customer);
	        $ids_group 					= [$customer->id_default_group];
			if($context->shop->frais_par_zone) {
				$_km 						= Cart::computeDistance($_address);
				if($_km >= 0 && $_km < 1000) {
					if($_km==0)			$_km = 1;
					$sql 					= '
						SELECT * FROM `' . _DB_PREFIX_ . 'bestkit_shippingmatrix_shop` bss
						INNER JOIN `' . _DB_PREFIX_ . 'bestkit_shippingmatrix` bs
						ON bss.`id_bestkit_shippingmatrix` = bs.`id_bestkit_shippingmatrix`
						AND ' . (float)$_km . ' >= bs.`km_from`
						AND ' . (float)$_km . ' < bs.`km_to`
						WHERE bss.`id_shop` =' . $id_shop. '
					';
			        if ($ids_group) {
			            $sql				   .= ' 
				            AND EXISTS (SELECT 1 FROM '._DB_PREFIX_.'bestkit_shippingmatrix_group bsg
							WHERE bsg.id_bestkit_shippingmatrix = bs.id_bestkit_shippingmatrix
							AND bsg.id_group IN ('.implode(',', array_map('intval', $ids_group)).')) 
						';
			        }
					$sql 					   .= '
						ORDER BY bs.`price_from` ASC
					';
					$shipping_rates 				= Db::getInstance()->ExecuteS($sql);
					return $shipping_rates[0]['mt_min_order'];
				}
			}
			else {
				if($_address->secteur) {
					$sql 							= '
						SELECT * FROM `' . _DB_PREFIX_ . 'bestkit_shippingmatrix` bs
						INNER JOIN `' . _DB_PREFIX_ . 'bestkit_shippingmatrix_shop` bss
						ON bss.`id_bestkit_shippingmatrix` = bs.`id_bestkit_shippingmatrix` AND bss.`id_shop` =' . $id_shop .'
						WHERE bs.`zone_name` = \''.pSQL($_address->secteur).'\'
					';
			        if ($ids_group) {
			            $sql				   .= ' 
				            AND EXISTS (SELECT 1 FROM '._DB_PREFIX_.'bestkit_shippingmatrix_group bsg
							WHERE bsg.id_bestkit_shippingmatrix = bs.id_bestkit_shippingmatrix
							AND bsg.id_group IN ('.implode(',', array_map('intval', $ids_group)).')) 
						';
			        }
					$sql 					   .= '
						ORDER BY bs.`price_from` ASC
					';
					$shipping_rates 				= Db::getInstance()->ExecuteS($sql);
					if(isset($shipping_rates[0]['mt_min_order']))
						return $shipping_rates[0]['mt_min_order'];
					else
						return 0;
				}
			}			
		}

		return $mt_min_order;
    }

    public static function getFraisParZone($id)
    {
		$sql = '
			SELECT `frais_par_zone`
			FROM `'._DB_PREFIX_.'shop`
			WHERE `id_shop` = '.$id
		;
		return Db::getInstance()->getValue($sql);
    }

    /**
     * @return Address the current shop address
     */
    public function getAddress()
    {
        $storeAddress 				= Store::getStoreAddress();
        if (!isset($this->address)) {
            $address 				= new Address();
            $address->company 		= Context::getContext()->shop->name;
            $address->id_country 	= $storeAddress['id_country'];
            $address->id_state 		= '';
            $address->address1 		= $storeAddress['address1'];
            $address->address2 		= $storeAddress['address2'];
            $address->postcode 		= $storeAddress['postcode'];
            $address->city 			= $storeAddress['city'];

            $this->address 			= $address;
        }

        return $this->address;
    }

	// ============================================================================================
	// Prochaine date et heure de livraison
	// ============================================================================================
	public static function getNextDeliveryDateAndHour() 
	{
		$return 									= [
			'today'									=> '',
			'day' 									=> '',
			'hour' 									=> ''
		];

		$day 										= date("Y-m-d");
		$today 										= date("Y-m-d");
		$notFound 									= true;
		$add 										= 0;
		$hours 										= Store::getStoreHours();
		while($notFound) {
			$weekDayNumber 							= date("N", mktime(0, 0, 0, substr($day, 5, 2), substr($day, 8, 2), substr($day, 0, 4)));
			if(CalendarLLW::isFerie($day) || empty($hours[$weekDayNumber])) {
				$add++;
				if($add > 7)
					die("Shop::getNextDeliveryDateAndHour - Impossible de déterminer la prochaine date !!!");
				$day 								= date("Y-m-d", mktime(0, 0, 0, date("m"), date("d")+$add, date("Y")));
			}
			else {
				$notFound							= false;
				$nextHour 							= self::getNextDeliveryHour($day);	
				$return ['day']						= date("d/m/Y", mktime(0, 0, 0, substr($day, 5, 2), substr($day, 8, 2), substr($day, 0, 4)));			
				$return ['today']					= false;
				if($day==$today) {
					$return ['today']				= true;
					$return ['hour']				= str_replace(":", "h", substr($nextHour, 11, 5));
				}
				else {
					$return ['hour']				= str_replace(":", "h", $nextHour);
				}
			}
		}

		return $return;
		
	}
	
	// ============================================================================================
	// Prochaine heure de livraison pour une journée donnée
	// ============================================================================================
	public static function getNextDeliveryHour($day) 
	{
		$today 										= date("Y-m-d");
		if($day==$today) {
			$heures_possibles						= self::getAvailableTimes($day);
			if(isset($heures_possibles ['availableTimeSlots'][0]))
				return $heures_possibles ['availableTimeSlots'][0];
			else
				return "???";
		}
		else {
			$hours 									= Store::getDayOpenHours($day);
			if($hours[0]) {
				return $hours[0][0];
			}
			else {
				return $hours[1][0];
			}
		}
		
	}
	
	// =========================================================================================================
	// Recherche des heures d'ouverture d'une journée donnée, ainsi que les heures mini et maxi
	// La journée donnée est réputée ouverte !
	// =========================================================================================================
	public static function getStoreOpenDayHours($day=null) 
	{
		if($day == null)
			$day 					= date("Y-m-d");

		$times 						= [];
		$today 						= date("Y-m-d");
		$current_hour 				= date("H:i");
		
        $delai_mini_livraison 		= Configuration::get('LLW_KYO_DELAI_MINI_LIVRAISON');
        $delai_mini_emporter 		= Configuration::get('LLW_KYO_DELAI_MINI_EMPORTER');

        $open_hours					= Store::getDayOpenHours($day);

		$context 					= Context::getContext();

		if($context->delivery_mode == Context::TO_BE_DELIVERED)
			$delai_mini				= $delai_mini_livraison;
		else
			$delai_mini				= $delai_mini_emporter;

		// Calcul de l'heure mini par rapport à l'heure actuelle et à la prochaine heure d'ouverture -------------------------
        $heure_mini					= '';
        $heure_maxi					= '';

        if($day == $today) {
	        if(count($open_hours[0])) {
		        if($current_hour < $open_hours[0][0]) { 	// Heures d'ouverture le matin
					$heure_mini		= $open_hours[0][0];
				}
		        if($current_hour >= $open_hours[0][0] && $current_hour <= $open_hours[0][1]) { 	// Heures d'ouverture le matin
					$heure_mini		= $current_hour;
				}
		        if($current_hour > $open_hours[0][1] && $current_hour < $open_hours[1][0]) { 	// Entre heures d'ouverture du matin et de l'après-midi
					$heure_mini		= $open_hours[1][0];
				}
		        if($current_hour >= $open_hours[1][0] && $current_hour <= $open_hours[1][1]) { 	// Heures d'ouverture l'après-midi
					$heure_mini		= $current_hour;
				}
			}
			else {
		        if($current_hour < $open_hours[1][0]) { 	// Heures d'ouverture l'après-midi
					$heure_mini		= $open_hours[1][0];
				}
		        if($current_hour >= $open_hours[1][0] && $current_hour <= $open_hours[1][1]) { 	// Heures d'ouverture l'après-midi
					$heure_mini		= $current_hour;
				}
			}
        }
        else {
	        if(count($open_hours[0])) {
				$heure_mini			= $open_hours[0][0];
			}
			else {
				$heure_mini			= $open_hours[1][0];
			}
        }
        
		// Calcul de l'heure maxi par rapport à la prochaine heure de fermeture ---------------------------------------------
        if(count($open_hours[0])) { 	// Heures d'ouverture le matin
	        $heure_maxi				= $open_hours[0][1];
        }
        if(count($open_hours[1])) { 	// Heures d'ouverture l'après-midi
	        $heure_maxi				= $open_hours[1][1];
        }
        
        if(empty($heure_mini)) {
            $heure_mini				= '00:00';
        }
        if(empty($heure_maxi)) {
            $heure_maxi				= '00:00';
        }
        
        return [
	        'open_hours'	=> $open_hours,
	        'heure_mini'	=> $heure_mini,
	        'heure_maxi'	=> $heure_maxi,
        ];
    }    
       
	// =========================================================================================================
	// Recherche des tranches horaires disponibles pour une journée donnée
	// La journée donnée est réputée ouverte !
	// =========================================================================================================
	public static function getAvailableTimes($day=null) 
	{
		if($day == null)
			$day 					= date("Y-m-d");

		$today 						= date("Y-m-d");

		$debug_delivery 					= false;
        if(self::debugDelivery()) {
			$debug_delivery 				= true;
		}

        $data 						= self::getStoreOpenDayHours($day);
        $open_hours					= $data['open_hours'];
        $heure_mini					= $data['heure_mini'];
        $heure_maxi					= $data['heure_maxi'];
        if(self::debugDelivery()) {
			ob_start();
			print_r($data);
			$buffer 				= ob_get_contents();
			ob_end_clean();
			self::$debug_message   .= "Horaires du jour = ".$buffer."<br>";
		}
        
        $nb_minutes_tranche  		= Configuration::get('LLW_KYO_NB_MINUTES_TRANCHE');
        $delai_mini_livraison 		= Configuration::get('LLW_KYO_DELAI_MINI_LIVRAISON');
        $delai_mini_emporter 		= Configuration::get('LLW_KYO_DELAI_MINI_EMPORTER');

		// Heure de départ : heure actuelle pour aujourd'hui, 00:00 pour les jours suivants --------------------------
		$context 					= Context::getContext();

        if(self::debugDelivery()) {
			self::$debug_message	   .= "Heure mini : ".$heure_mini.":00<br>";
		}
		if($day == $today) {
			$from_hour 				= date("Y-m-d H:i:s");
	        if(self::debugDelivery()) {
				self::$debug_message	   .= "Heure actuelle = ".$from_hour."<br>";
			}
			$time 					= new DateTime($from_hour);
			if($context->delivery_mode == Context::TO_BE_DELIVERED)
				$time->add(new DateInterval('PT' . $delai_mini_livraison . 'M'));
			if($context->delivery_mode == Context::TO_GO)
				$time->add(new DateInterval('PT' . $delai_mini_emporter . 'M'));
			$from_hour				= $time->format('Y-m-d H:i:s');
	        if(self::debugDelivery()) {
				self::$debug_message	   .= "1-Heure devient ".$from_hour."<br>";
			}
		}
		else
			$from_hour 				= date("Y-m-d H:i:s", mktime(0, 0, 0, substr($day, 5, 2), substr($day, 8, 2), substr($day, 0, 4)));
			
		//echo "1-from_hour=$from_hour<br>";
		// Si heure de départ < heure minimum alors on se positionne sur la prochaine heure en fonction des délais -----
		if($from_hour < $day.' '.$heure_mini.":00") {
			$new_from_hour 		= $day.' '.$heure_mini.":00";
			$start_date 		= new DateTime($from_hour);
			$since_start 		= $start_date->diff(new DateTime($new_from_hour));
			// Nombre de minutes séparant l'heure de départ de l'heure minimum ----------------------------
			$nbDiffMinutes 		= ($since_start->days * 24 * 60) + ($since_start->h * 60) + $since_start->i;
	        if(self::debugDelivery()) {
				self::$debug_message	   .= "Différence de minutes = ".$nbDiffMinutes."<br>";
			}
			//echo "delivery_mode=".$context->delivery_mode."<br>";
			//echo "nbDiffMinutes=$nbDiffMinutes, delai_mini_livraison=$delai_mini_livraison, delai_mini_emporter=$delai_mini_emporter<br>";
			if($nbDiffMinutes < $delai_mini_livraison && $context->delivery_mode == Context::TO_BE_DELIVERED) {
				$time 			= new DateTime($from_hour);
				$time->add(new DateInterval('PT' . $delai_mini_livraison . 'M'));
				$new_from_hour	= $time->format('Y-m-d H:i:s');
				if($new_from_hour < $day.' '.$heure_mini.":00") {
					$new_from_hour		= $day.' '.$heure_mini.":00";
					$from_hour 			= $new_from_hour;
				}
			}
			else
			if($nbDiffMinutes < $delai_mini_emporter && $context->delivery_mode == Context::TO_GO) {
				$time 			= new DateTime($from_hour);
				$time->add(new DateInterval('PT' . $delai_mini_emporter . 'M'));
				$new_from_hour	= $time->format('Y-m-d H:i:s');
				if($new_from_hour < $day.' '.$heure_mini.":00") {
					$new_from_hour		= $day.' '.$heure_mini.":00";
					$from_hour 			= $new_from_hour;
				}
			}
	        if(self::debugDelivery()) {
				self::$debug_message	   .= "2-Heure devient ".$from_hour."<br>";
			}
		}
		
		// Si heure de départ dépasse l'heure maxi du jour alors aucun créneau disponible ----------------
		if($from_hour > $day.' '.$heure_maxi.':00') {
			return [
				'select' 					=> '',
				'availableTimeSlots'		=> '',
				'debug_message' 			=> str_replace("<br>", "\r\n", self::$debug_message),
				'debug_delivery'			=> $debug_delivery
			];
		}

		// On se positionne sur des créneaux fixes toutes les n minutes ----------------------------------		
		$today 						= date('Y-m-d H:i:s');
		$nbTranches 				= 60 / $nb_minutes_tranche;
		$from_hour_min 				= substr($from_hour, 14, 2);
		for($i = $nbTranches; $i > 0; $i--) {
			if($from_hour_min <= ($nb_minutes_tranche * $i) && $from_hour_min > ($nb_minutes_tranche * ($i-1))) {
				$from_hour_min		= $nb_minutes_tranche * $i;
				$from_hour 			= substr($from_hour, 0, 14).$from_hour_min.':00';
				//echo "3-from_hour=$from_hour<br>";
				if($from_hour_min==60) {
					$from_hour 		= date("Y-m-d H:i:s", mktime(substr($from_hour, 11, 2)+1, 0, 0, substr($day, 5, 2), substr($day, 8, 2), substr($day, 0, 4)));
				}
			}
		}
        if(self::debugDelivery()) {
			self::$debug_message	   .= "3-Heure devient ".$from_hour."<br>";
		}

		// Recherche des créneaux disponibles -----------------------------------------------------------
		// 2020-08-03 15:12:00
		$last_dayhour 						= $day.' '.$heure_maxi.':00';
		$from_dayhour						= $from_hour;
		$availableTime						= false;

        if(count($open_hours[1]))
        	$last_dayhour					= $day.' '.$open_hours[1][1].':00';
        else
        	$last_dayhour					= $day.' '.$open_hours[0][1].':00';

        $select 							= '';
        $availableTimeSlots					= [];

        if(self::debugDelivery()) {
			self::$debug_message		   .= "Dernière heure possible ".$last_dayhour."<br>";
		}
		while($from_dayhour <= $last_dayhour) {
			$hm 							= substr($from_dayhour, 11, 5);
			$heure_possible 				= false;
			if(count($open_hours[0]) && $open_hours[0][0] && count($open_hours[1]) && $open_hours[1][0]) {
				if(($hm >= $open_hours[0][0] && $hm <= $open_hours[0][1]) 
				|| ($hm >= $open_hours[1][0] && $hm <= $open_hours[1][1]))
					$heure_possible			= true;
			}
			else
			if(count($open_hours[1]) && $open_hours[1][0]) {
				if($hm >= $open_hours[1][0] && $hm <= $open_hours[1][1])
					$heure_possible			= true;
			}
			if($heure_possible) {
				if(!$select) {
					$selected 				= '';
					$select 			  	= '<option value="">Heures possibles</option>';
				}
				$select 				   .= '<option value="'.$from_dayhour.'"'.$selected.'>'.substr($from_dayhour, 11, 5).'</option>';
				$availableTime				= true;
				$availableTimeSlots[]		= $from_dayhour;
			}
			$time 							= new DateTime($from_dayhour);
			$time->add(new DateInterval('PT' . $nb_minutes_tranche . 'M'));
			$from_dayhour					= $time->format('Y-m-d H:i:s');
		}
		
		return [
			'select' 						=> $select,
			'availableTimeSlots'			=> $availableTimeSlots,
			'debug_message' 				=> str_replace("<br>", "\r\n", self::$debug_message),
			'debug_delivery'				=> $debug_delivery
		];
	}
	
}
