<?php
/**
 * Page Cache Ultimate, Page Cache standard and Speed pack are powered by Jpresta (jpresta . com)
 *
 * @author    Jpresta
 * @copyright Jpresta
 * @license   You are just allowed to modify this copy for your own use. You must not redistribute it. License
 *               is permitted for one Prestashop instance only but you can install it on your test instances.
 */
class Context extends ContextCore
{
    public $delivery_mode = 0;
	public $ordered_products;
	public $cart_products;

    /** @var int */
    const TO_BE_DELIVERED = 1;
    const TO_GO = 2;

	public static $debug = true;

    public static function logAction($str) {
	
		if(!self::$debug)	return;
	
		if(!is_dir($_SERVER['DOCUMENT_ROOT']."/debug")) {
			$oldmask 						= umask(0);
			mkdir($_SERVER['DOCUMENT_ROOT']."/debug", 0777);
			umask($oldmask);
			$fp								= fopen($_SERVER['DOCUMENT_ROOT']."/debug/log.log", "w");
		}
		else
	    	$fp								= fopen($_SERVER['DOCUMENT_ROOT']."/debug/log.log", "a");
	    
	    $context 							= self::getContext();
	    if($context->cart->id) {
		    $str 							= "Cart : ".$context->cart->id." - ".$str;
	    }
	    fwrite($fp, date("Y-m-d H:i:s")." ==> ".$str."\n");
	    //echo(date("Y-m-d H:i:s")." ==> ".$str."<br>");
	    fclose($fp);
    
    }

    /*
    * module: pagecache
    * date: 2020-12-14 11:19:10
    * version: 7.0.2
    */
    public function getMobileDetect()
    {
        if ($this->mobile_detect === null) {
            if (!Module::isEnabled('pagecache')) {
                return parent::getMobileDetect();
            } else {
                require_once _PS_MODULE_DIR_ . 'pagecache/pagecache.php';
                if ($this->mobile_detect === null) {
                    if (PageCache::isCacheWarmer()) {
                        $this->mobile_detect = new JprestaUtilsMobileDetect();
                    } else {
                        return parent::getMobileDetect();
                    }
                }
            }
        }
        return $this->mobile_detect;
    }

    /**
     * Update context after customer login.
     *
     * @param Customer $customer Created customer
     */
    public function updateCustomer(Customer $customer)
    {
        $this->customer = $customer;
        $this->cookie->id_customer = (int) $customer->id;
        $this->cookie->customer_lastname = $customer->lastname;
        $this->cookie->customer_firstname = $customer->firstname;
        $this->cookie->passwd = $customer->passwd;
        $this->cookie->logged = 1;
        $customer->logged = 1;
        $this->cookie->email = $customer->email;
        $this->cookie->is_guest = $customer->isGuest();

        if (Configuration::get('PS_CART_FOLLOWING') && (empty($this->cookie->id_cart) || Cart::getNbProducts($this->cookie->id_cart) == 0) && $idCart = (int) Cart::lastNoneOrderedCart($this->customer->id)) {
            $this->cart = new Cart($idCart);
            $this->cart->secure_key = $customer->secure_key;
        } else {
            $idCarrier = (int) $this->cart->id_carrier;
            $this->cart->secure_key = $customer->secure_key;
            $this->cart->delivery_mode = 0;
			$delivery_mode = (int) Context::getContext()->cookie->__get('delivery_mode');
			if(isset(Context::getContext()->cart->delivery_mode) && Context::getContext()->cart->delivery_mode)
				$this->cart->delivery_mode = $delivery_mode;
            $this->cart->id_carrier = 0;
            $this->cart->setDeliveryOption(null);
            $this->cart->updateAddressId($this->cart->id_address_delivery, (int) Address::getFirstCustomerAddressId((int) ($customer->id)));
            $this->cart->id_address_delivery = (int) Address::getFirstCustomerAddressId((int) ($customer->id));
            $this->cart->id_address_invoice = (int) Address::getFirstCustomerAddressId((int) ($customer->id));
        }
        $this->cart->id_customer = (int) $customer->id;

        if(empty($idCarrier))
        	$idCarrier						= Carrier::getCarrierForDeliveryMode();

        if (isset($idCarrier) && $idCarrier) {
            $deliveryOption = [$this->cart->id_address_delivery => $idCarrier . ','];
            $this->cart->setDeliveryOption($deliveryOption);
            $this->cart->id_carrier = $idCarrier;
        }

        $this->ordered_products			= Order::getAllOrderedProducts($customer->id);
        $this->cart_products			= Cart::getCartProductIDs($this->cart->id);

        $this->cart->save();
        $this->cookie->id_cart = (int) $this->cart->id;
        $this->cart->autosetProductAddress();

        $this->cookie->registerSession(new CustomerSession());

        $this->cookie->write();
        
        Address::updateAddressShop((int) ($customer->id), $this->shop->id);
    }

    public function updateDeliveryMode($delivery_mode)
    {
	    $this->context 				= self::getContext();
	    $cart 						= '';
    	$idCarrier					= Carrier::getCarrierForDeliveryMode();
        if ((int) $this->context->cookie->id_cart) {
            $cart 					= new Cart($this->context->cookie->id_cart);
			$cart->delivery_mode 	= $delivery_mode;
	        if ($idCarrier) {
	            $cart->id_carrier 	= $idCarrier;
	        }
			$cart->update();
		}
        $this->delivery_mode 		= $delivery_mode;
        $this->cookie->__set('delivery_mode', $delivery_mode);
        $this->cookie->write();

        $this->smarty->assign('delivery_mode', $this->delivery_mode);
        
		if($cart) {
	        Hook::exec('actionChangeDeliveryMode', array('cart' => $this->cart));
	    }
    }

}
