<?php
/**
 * 2007-2019 PrestaShop and Contributors
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/OSL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2019 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
 * International Registered Trademark & Property of PrestaShop SA
 */
use PrestaShop\PrestaShop\Adapter\Product\PriceFormatter;

class CheckoutDeliveryStep extends CheckoutDeliveryStepCore
{
    public $delivery_form_has_no_continue_button = false;

    public function handleRequest(array $requestParams = array())
    {
        if(isset($requestParams['delivery_option'])) {
            $this->setComplete(false);
            $this->getCheckoutSession()->setDeliveryOption(
                $requestParams['delivery_option']
            );
            $this->getCheckoutSession()->setRecyclable(
                isset($requestParams['recyclable']) ? $requestParams['recyclable'] : false
            );

            $useGift = isset($requestParams['gift']) ? $requestParams['gift'] : false;
            $this->getCheckoutSession()->setGift(
                $useGift,
                ($useGift && isset($requestParams['gift_message'])) ? $requestParams['gift_message'] : ''
            );
        }

        if (isset($requestParams['delivery_message'])) {
            $this->getCheckoutSession()->setMessage($requestParams['delivery_message']);
        }
        
        if (isset($requestParams['delivery_date'])) {
            $this->getCheckoutSession()->setDeliveryDate($requestParams['delivery_date']);
        }
        
        if (isset($requestParams['delivery_hour'])) {
            $this->getCheckoutSession()->setDeliveryHour($requestParams['delivery_hour']);
        }
        
		$error 										= false;
        $cart_delivery								= $this->getCheckoutSession()->getCartDelivery();
        $cart_delivery_date_hour					= '';
        if(isset($cart_delivery[0]['delivery_time']))
			$cart_delivery_date_hour				= $cart_delivery[0]['delivery_time'];

		$now 										= date("Y-m-d H:i:s");
        if($cart_delivery_date_hour && $cart_delivery_date_hour < $now && !$error) {
			if($this->isCurrent()) {
	            $this->context->controller->errors		= [];
	            $this->context->controller->errors[]	= "La date et l'heure que vous avez précédemment choisies (".$cart_delivery_date_hour.") sont dépassées. Merci de choisir une heure ultérieure pour votre commande.";
				$cart_delivery_date_hour				= '';
				$error 									= true;
	        	$this->getCheckoutProcess()->setHasErrors(true);
            }
		}

        if ($this->isReachable() && isset($requestParams['confirmDeliveryOption'])) {
            // we're done if
            // - the step was reached (= all previous steps complete)
            // - user has clicked on "continue"
            // - there are delivery options
            // - the is a selected delivery option
            // - the module associated to the delivery option confirms
            $deliveryOptions = $this->getCheckoutSession()->getDeliveryOptions();
            $this->setNextStepAsCurrent();
            $this->setComplete(
                !empty($deliveryOptions)
                && $this->getCheckoutSession()->getSelectedDeliveryOption()
                && $this->getCheckoutSession()->getSelectedDeliverydate()
                && $this->getCheckoutSession()->getSelectedDeliveryHour()
                && $this->isModuleComplete($requestParams)
                && !$error
            );
        }

		if($error && $this->isCurrent()) {
            $this->delivery_form_has_no_continue_button = true;
            $this->setCurrent(true);
            $this->setComplete(false);
        }

        $this->setTitle($this->getTranslator()->trans('Livraison / Retrait', array(), 'Shop.Theme.Checkout'));

        Hook::exec('actionCarrierProcess', array('cart' => $this->getCheckoutSession()->getCart()));
    }

    public function render(array $extraParams = array())
    {
        return $this->renderTemplate(
            $this->getTemplate(),
            $extraParams,
            $this->getTemplateParameters()
        );
    }

    public function getTemplateParameters()
    {
        // Heure de livraison -------------------------------------------------------------------
		$error 							= false;
		$errors 						= [];
        $cart 							= $this->getCheckoutSession()->getCart();
        $cart_delivery					= $this->getCheckoutSession()->getCartDelivery();
        $cart_delivery_date_hour		= '';
        if(isset($cart_delivery[0]['delivery_time']))
			$cart_delivery_date_hour	= $cart_delivery[0]['delivery_time'];

		$this->getCheckoutSession()->delivery_hour = $cart_delivery_date_hour;

		$delai_mini 					= '';
        $delai_mini_livraison 			= Configuration::get('LLW_KYO_DELAI_MINI_LIVRAISON');
        $delai_mini_emporter 			= Configuration::get('LLW_KYO_DELAI_MINI_EMPORTER');
		if($this->context->delivery_mode == Context::TO_BE_DELIVERED)
			$delai_mini 				= $delai_mini_livraison . ' mn';
		if($this->context->delivery_mode == Context::TO_GO)
			$delai_mini 				= $delai_mini_emporter . ' mn';

        $params = array(
            'hookDisplayBeforeCarrier' => Hook::exec('displayBeforeCarrier', array('cart' => $this->getCheckoutSession()->getCart())),
            'hookDisplayAfterCarrier' => Hook::exec('displayAfterCarrier', array('cart' => $this->getCheckoutSession()->getCart())),
            'id_address' => $this->getCheckoutSession()->getIdAddressDelivery(),
            'delivery_options' => $this->getCheckoutSession()->getDeliveryOptions(),
            'delivery_option' => $this->getCheckoutSession()->getSelectedDeliveryOption(),
            'recyclable' => $this->getCheckoutSession()->isRecyclable(),
            'recyclablePackAllowed' => $this->isRecyclablePackAllowed(),
            'delivery_message' => $this->getCheckoutSession()->getMessage(),
            'gift' => array(
                'allowed' => $this->isGiftAllowed(),
                'isGift' => $this->getCheckoutSession()->getGift()['isGift'],
                'label' => $this->getTranslator()->trans(
                    'I would like my order to be gift wrapped %cost%',
                    array('%cost%' => $this->getGiftCostForLabel()),
                    'Shop.Theme.Checkout'
                ),
                'message' => $this->getCheckoutSession()->getGift()['message'],
            ),
            'delivery_form_has_no_continue_button' => $this->delivery_form_has_no_continue_button,
            'cart_delivery_date' => substr($cart_delivery_date_hour, 0, 8),
            'cart_delivery_hour' => substr($cart_delivery_date_hour, 11, 5),
			'delai_mini' => $delai_mini,
        );

		// Dates possibles pour la livraison / retrait ------------------------------------------
		$params							= $this->assignDeliveryDate($params);
		if($params['delivery_dates']=='' || (is_array($params['delivery_dates']) && count($params['delivery_dates'])==0)) {
            $errors[] 					= "Désolé, il n'y a plus de créneaux horaires disponibles pour votre votre commande sur les 5 prochains jours ouverts. Merci de nous appeler au <a href='tel:".Configuration::get('PS_SHOP_PHONE')."'>".Configuration::get('PS_SHOP_PHONE')."</a> pour traiter votre commande.";
			$error 						= true;
		}

		if($error) {
            $this->delivery_form_has_no_continue_button = true;
        }
        $params['delivery_form_has_no_continue_button'] = $this->delivery_form_has_no_continue_button;
        $params['errors'] = $errors;

        return $params;
    }

	// ============================================================================================
	// Affectation d'une date et d'une heure de livraison
	// ============================================================================================
    public function assignDeliveryDate($params)
    {
		$delivery_dates								= $this->getNext5Days();
		$current_day_week_number					= date("N");
		$current_hour								= date("H:i");
		
        $cart_delivery								= $this->getCheckoutSession()->getCartDelivery();
        $cart_delivery_date							= '';
        $cart_delivery_date_hour					= '';
        if(isset($cart_delivery[0]['delivery_time'])) {
	        $cart_delivery_date_hour				= $cart_delivery[0]['delivery_time'];
	        $delivery_time 							= explode(" ", $cart_delivery[0]['delivery_time']);
	        $cart_delivery_date						= $delivery_time[0];
        }

		$now 										= date("Y-m-d H:i:s");
        if($cart_delivery_date_hour < $now) {
			$cart_delivery_date						= '';
		}

        $params['delivery_dates']					= $delivery_dates;
        if($cart_delivery_date) {
	        if(date("Y-m-d", mktime(0, 0, 0, substr($cart_delivery_date, 5, 2), substr($cart_delivery_date, 8, 2), substr($cart_delivery_date, 0, 4))) == date("Y-m-d"))
	        	$params['delivery_dates_selected']	= "Aujourd'hui";
	        else
	        	$params['delivery_dates_selected']	= "Le ".substr($cart_delivery_date, 8, 2)."/".substr($cart_delivery_date, 5, 2)."/".substr($cart_delivery_date, 0, 4);
	    }
		else
	        $params['delivery_dates_selected']		= '';
        $params['now']								= date("H:i");
		return $params;
    }

	// ==========================================================================================================
	// Recherche des 5 prochains jours ouvrés
	// ==========================================================================================================
	public function getNext5Days()
	{
		$shop_open_days 				= CalendarLLW::getOpenDays();

		$next_day						= '';
		$current_day_week_number 		= date("N");
		$i 								= $current_day_week_number;

		$current_date 					= date("Y-m-d");
		$today 							= date("Y-m-d");
		$n 								= 0;
		$j 								= 0;
		$next_days						= array();

		$wn 							= date("N", mktime(0, 0, 0, substr($current_date, 5, 2), substr($current_date, 8, 2), substr($current_date, 0, 4)));

		while($j <= 4) {
			if($shop_open_days[$wn]) {
				if($current_date==$today) {
					$next_days[$j]		= "Aujourd'hui";
				}
				else {
					$next_days[$j]		= "Le ".date("d/m/Y", mktime(0, 0, 0, date("m"), date("d")+$n, date("Y")));
				}
				$j++;
			}
			$n++;
			$current_date				= date("Y-m-d", mktime(0, 0, 0, date("m"), date("d")+$n, date("Y")));
			$wn							= date("N", mktime(0, 0, 0, date("m"), date("d")+$n, date("Y")));
		}

		return $next_days;
	}

    protected function isModuleComplete($requestParams)
    {
        $deliveryOptions = $this->getCheckoutSession()->getDeliveryOptions();
        $currentDeliveryOption = $deliveryOptions[$this->getCheckoutSession()->getSelectedDeliveryOption()];
        if (!$currentDeliveryOption['is_module']) {
            return true;
        }

        $isComplete = true;
        Hook::exec(
            'actionValidateStepComplete',
            array(
                'step_name' => 'delivery',
                'request_params' => $requestParams,
                'completed' => &$isComplete,
            ),
            Module::getModuleIdByName($currentDeliveryOption['external_module_name'])
        );

        return $isComplete;
    }
}
