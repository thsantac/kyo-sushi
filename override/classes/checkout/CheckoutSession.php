<?php
/**
 * 2007-2019 PrestaShop and Contributors
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/OSL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2019 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
 * International Registered Trademark & Property of PrestaShop SA
 */
class CheckoutSession extends CheckoutSessionCore
{
	public $delivery_hour;
	public $delivery_date;
	
    public function customerHasLoggedIn()
    {
        return $this->context->customer->isLogged(true);
    }

    public function setDeliveryDate($delivery_date)
    {
        return $this->_updateDeliveryDate(Tools::safeOutput($delivery_date));
    }

    public function setDeliveryHour($delivery_hour)
    {
        return $this->_updateDeliveryHour(Tools::safeOutput($delivery_hour));
    }

    private function _updateDeliveryDate($delivery_date)
    {
		$this->delivery_date 			= $delivery_date;
        return true;
    }

    private function _updateDeliveryHour($delivery_hour)
    {
        $delai_mini_livraison 			= Configuration::get('LLW_KYO_DELAI_MINI_LIVRAISON');
        $delai_mini_emporter 			= Configuration::get('LLW_KYO_DELAI_MINI_EMPORTER');
        if ($delivery_hour) {
	        if ($this->delivery_date == "Le ".date("d/m/Y")) {
		        $now  						= date("Y-m-d H:i:s", mktime(date("H"), date("i"), date("s"), substr($delivery_hour, 5, 2), substr($delivery_hour, 8, 2), substr($delivery_hour, 0, 4)));
				// Est-ce que l'horaire choisi est toujours possible ? ----------------------------------------------------------
				$start_date 				= new DateTime($now);
				$since_start 				= $start_date->diff(new DateTime($delivery_hour));
				$nbDiffMinutes 				= ($since_start->days * 24 * 60) + ($since_start->h * 60) + $since_start->i;
				if($nbDiffMinutes < $delai_mini_livraison && $this->context->delivery_mode == Context::TO_BE_DELIVERED) {
		            $this->context->controller->errors[] = "Désolé, nous ne pouvons plus fournir votre commande à livrer pour l'horaire désiré (délai de préparation). Merci de choisir une heure ultérieure pour votre commande.";
					$this->delivery_hour 	= '';
					return false;
				}
				else
				if($nbDiffMinutes < $delai_mini_emporter && $this->context->delivery_mode == Context::TO_GO) {
		            $this->context->controller->errors[] = "Désolé, nous ne pouvons plus fournir votre commande à emporter pour l'horaire désiré (délai de préparation). Merci de choisir une heure ultérieure pour votre commande.";
					$this->delivery_hour 	= '';
					return false;
				}
			}
			$this->context->cart->updateDeliveryHour($delivery_hour);
			$this->delivery_hour 		= $delivery_hour;
        }

        return true;
    }

    public function getSelectedDeliveryHour()
    {
        return $this->delivery_hour;
    }

    public function getSelectedDeliveryDate()
    {
        return $this->delivery_date;
    }

    public function setIdAddressDelivery($id_address)
    {
        $delivery_ok 								= true;
        $error 										= '';
		$return 									= Cart::getDeliveryByZoneReturn($this->context->cart->getDeliveryByZone($id_address));

        if($return['delivery_ok']) {
	        $this->context->cart->updateAddressId($this->context->cart->id_address_delivery, $id_address);
	        $this->context->cart->id_address_delivery = $id_address;
	        $this->context->cart->save();
	
	        return [
	        	'delivery_ok' 	=> true,
	        	'error'			=> ''
	        ];
        }
        else
	        return [
	        	'delivery_ok' 	=> false,
	        	'error'			=> $return['error']
	        ];
    }

    public function getIdAddressDelivery()
    {
        return $this->context->cart->id_address_delivery;
    }

    public function getCartDelivery()
    {
        return $this->context->cart->getCartDelivery();
    }

}
