<?php
use Symfony\Component\Translation\TranslatorInterface;

class CheckoutAddressesStep extends CheckoutAddressesStepCore
{
    protected $addressForm;
    protected $use_same_address = true;
    protected $show_delivery_address_form = false;
    protected $show_invoice_address_form = false;

	public $has_bad_address = false;
    protected $form_has_no_continue_button = false;
    public $context;
	
    public function __construct(
        Context $context,
        TranslatorInterface $translator,
        CustomerAddressForm $addressForm
    ) {
	    $this->context = $context;
        AbstractCheckoutStep::__construct($this->context, $translator);
        $this->addressForm = $addressForm;
    }

    public function handleRequest(array $requestParams = array())
    {
	    $delivery_mode = $this->context->delivery_mode;
	    
	    $canSkip = true;

        $this->addressForm->setAction($this->getCheckoutSession()->getCheckoutURL());

        if (array_key_exists('use_same_address', $requestParams)) {
            $this->use_same_address = (bool) $requestParams['use_same_address'];
            if (!$this->use_same_address) {
				$canSkip = false;
                $this->setCurrent(true);
            }
        }

        if (isset($requestParams['cancelAddress'])) {
            if ($requestParams['cancelAddress'] === 'invoice') {
                if ($this->getCheckoutSession()->getCustomerAddressesCount() < 2) {
                    $this->use_same_address = true;
                }
            }
            $this->setCurrent(true);
        }

        $this->addressForm->fillWith(array(
            'firstname' => $this->getCheckoutSession()->getCustomer()->firstname,
            'lastname' => $this->getCheckoutSession()->getCustomer()->lastname,
        ));
        
        $error = false;

        if (isset($requestParams['saveAddress'])) {
		    $canSkip = false;
            $saved = $this->addressForm->fillWith($requestParams)->submit();
            if (!$saved) {
                $this->setCurrent(true);
                $this->getCheckoutProcess()->setHasErrors(true);
                if ($requestParams['saveAddress'] === 'delivery') {
                    $this->show_delivery_address_form = true;
                } else {
                    $this->show_invoice_address_form = true;
                }
            } else {
                if ($requestParams['saveAddress'] === 'delivery') {
                    $this->use_same_address = isset($requestParams['use_same_address']);
                }
	            if ($delivery_mode==2) {
		            $canSkip						= true;
		            $this->use_same_address 		= true;	
		        }
                $id_address = $this->addressForm->getAddress()->id;
                if ($requestParams['saveAddress'] === 'delivery') {

                    $return  				= $this->getCheckoutSession()->setIdAddressDelivery($id_address);

                    if(!$return['delivery_ok'] && $delivery_mode==1) {
			            $this->getCheckoutProcess()->setHasErrors(true);
			            $this->context->controller->errors[] = $return['error'];
						$error = true;

			            /*$addressPersister = new CustomerAddressPersister(
			                $this->context->customer,
			                $this->context->cart,
			                Tools::getToken(true, $this->context)
			            );
			
			            $deletionResult = (bool) $addressPersister->delete(
			                new Address((int) $id_address, $this->context->language->id),
			                Tools::getValue('token')
			            );*/
                    }
                    if ($this->use_same_address && !$error) {
                        $this->getCheckoutSession()->setIdAddressInvoice($id_address);
                    }
                } else {
                    $this->getCheckoutSession()->setIdAddressInvoice($id_address);
                }
            }
        } 
        elseif (isset($requestParams['newAddress'])) {
		    $canSkip = false;
            $this->setCurrent(true);
            if ($requestParams['newAddress'] === 'delivery') {
                $this->show_delivery_address_form = true;
            } else {
                $this->show_invoice_address_form = true;
            }
            $this->addressForm->fillWith($requestParams);
            $this->form_has_no_continue_button = !$this->use_same_address;
        } 
        elseif (isset($requestParams['editAddress'])) {
		    $canSkip = false;
            $this->setCurrent(true);
            if ($requestParams['editAddress'] === 'delivery') {
                $this->show_delivery_address_form = true;
            } else {
                $this->show_invoice_address_form = true;
            }
            $this->addressForm->loadAddressById($requestParams['id_address']);
            $this->form_has_no_continue_button = true;
        } 
        elseif (isset($requestParams['deleteAddress'])) {
		    $canSkip = false;
            $addressPersister = new CustomerAddressPersister(
                $this->context->customer,
                $this->context->cart,
                Tools::getToken(true, $this->context)
            );

            $deletionResult = (bool) $addressPersister->delete(
                new Address((int) Tools::getValue('id_address'), $this->context->language->id),
                Tools::getValue('token')
            );
            if ($deletionResult) {
                $this->context->controller->success[] = $this->getTranslator()->trans(
                    'Address successfully deleted!',
                    array(),
                    'Shop.Notifications.Success'
                );
                $this->context->controller->redirectWithNotifications(
                    $this->getCheckoutSession()->getCheckoutURL()
                );
            } else {
                $this->getCheckoutProcess()->setHasErrors(true);
                $this->context->controller->errors[] = $this->getTranslator()->trans(
                    'Could not delete address.',
                    array(),
                    'Shop.Notifications.Error'
                );
            }
        }
        elseif (isset($requestParams['confirm-addresses'])) {
		    $canSkip = false;
            if (isset($requestParams['id_address_delivery'])) {
                $id_address = $requestParams['id_address_delivery'];

                if (!Customer::customerHasAddress($this->getCheckoutSession()->getCustomer()->id, $id_address)) {
                    $this->getCheckoutProcess()->setHasErrors(true);
                } else {
                    if ($this->getCheckoutSession()->getIdAddressDelivery() != $id_address) {
                        $this->setCurrent(true);
                        $this->getCheckoutProcess()->invalidateAllStepsAfterCurrent();
                    }

                    $return 				= $this->getCheckoutSession()->setIdAddressDelivery($id_address);
                    if(!$return['delivery_ok'] && $delivery_mode==1) {
			            $this->getCheckoutProcess()->setHasErrors(true);
			            $this->context->controller->errors[] = $return['error'];
                    }
                    if ($this->use_same_address) {
                        $this->getCheckoutSession()->setIdAddressInvoice($id_address);
                    }
                }
            }

            if (isset($requestParams['id_address_invoice'])) {
                $id_address = $requestParams['id_address_invoice'];
                if (!Customer::customerHasAddress($this->getCheckoutSession()->getCustomer()->id, $id_address)) {
                    $this->getCheckoutProcess()->setHasErrors(true);
                } else {
                    $this->getCheckoutSession()->setIdAddressInvoice($id_address);
                }
            }

            if (!$this->getCheckoutProcess()->hasErrors()) {
                $this->setNextStepAsCurrent();
                $this->setComplete(
                    $this->getCheckoutSession()->getIdAddressInvoice() &&
                    $this->getCheckoutSession()->getIdAddressDelivery()
                );
            }
        }
		else {
			if ($delivery_mode==1) {
	            $id_address 			= $this->getCheckoutSession()->getIdAddressDelivery();
	            $return 				= $this->getCheckoutSession()->setIdAddressDelivery($id_address);
	            if(!$return['delivery_ok'] && $delivery_mode==1) {
				    $canSkip 			= false;
		            $this->getCheckoutProcess()->setHasErrors(true);
		            $this->context->controller->errors[] = $return['error'];
	            }
			}
		}

        $addresses_count = $this->getCheckoutSession()->getCustomerAddressesCount();

	    if($delivery_mode==2 && $addresses_count > 0) {
		    if (!isset($requestParams['deleteAddress']) && !isset($requestParams['editAddress']) && !isset($requestParams['newAddress'])) {
			    $canSkip		= true;
		    }
		}

	    if($delivery_mode==2 && $canSkip) {
			if ($addresses_count > 0) {
	            $this->setComplete(true);
	            $this->use_same_address = true;
				$this->form_has_no_continue_button = false;
				$this->show_delivery_address_form = false;
				$this->show_invoice_address_form = false;
				$this->setTitle($this->getTranslator()->trans('Addresses', array(), 'Shop.Theme.Checkout'));
				return $this;
			}
	    }

        if ($addresses_count === 0) {
            $this->show_delivery_address_form = true;
            $this->form_has_no_continue_button = true;
            //echo "1<br>";
        } elseif (!$this->use_same_address && $delivery_mode==1) {
			$this->show_delivery_address_form = false;
            $this->show_invoice_address_form = true;
            $this->setComplete(false);
            $this->setCurrent(true);
            $this->form_has_no_continue_button = true;
			$canSkip = false;
        }

        if ($this->show_invoice_address_form) {
            $this->form_has_no_continue_button = true;
            //echo "3<br>";
        } elseif ($this->show_delivery_address_form && $delivery_mode==1) {
            if ($this->use_same_address || $addresses_count < 2) {
	            //$this->setCurrent(true); // Si à true on ne plus plus passer par la phase d'identification
	            $this->setComplete(false);
				$this->form_has_no_continue_button = true;
				//echo "4<br>";
            }
        }

        if (Module::isEnabled('quantitydiscountpro')) {
            include_once(_PS_MODULE_DIR_.'quantitydiscountpro/quantitydiscountpro.php');
            $quantityDiscount = new QuantityDiscountRule();
            $quantityDiscount->createAndRemoveRules();
        }

		if($delivery_mode==1 && $this->getCheckoutSession()->getIdAddressDelivery() && $canSkip) {
            $this->setCurrent(false);
            $this->setComplete(true);
			//echo "5<br>";
		}

		if($error && $delivery_mode==1) {
			//echo "6<br>";
            $this->form_has_no_continue_button = true;
            $this->has_bad_address = true;
            $this->setCurrent(true);
            $this->setComplete(false);
        }

		if(!$error && $delivery_mode==1 && $addresses_count > 0 && $this->getCheckoutSession()->getIdAddressDelivery() && !isset($requestParams['newAddress'])) {
	        $return 				= $this->getCheckoutSession()->setIdAddressDelivery($this->getCheckoutSession()->getIdAddressDelivery());
            if(!$return['delivery_ok']) {
				//echo "7<br>";
	            $this->form_has_no_continue_button = true;
				$this->setCurrent(true);
				if(!isset($requestParams['editAddress'])) {
					$this->has_bad_address = true;
		            $this->context->controller->errors	= $return['error'];
				}
            }
        }

        $this->setTitle($this->getTranslator()->trans('Addresses', array(), 'Shop.Theme.Checkout'));

        return $this;
    }

    public function getTemplateParameters()
    {
        $errors 					= [];
	    $delivery_mode 				= $this->context->delivery_mode;
	    $delivery_address_error		= '';

        $idAddressDelivery 			= (int) $this->getCheckoutSession()->getIdAddressDelivery();
        if($idAddressDelivery && $delivery_mode==1) {
	        $return 				= $this->getCheckoutSession()->setIdAddressDelivery($idAddressDelivery);
	        if(!$return['delivery_ok']) {
	            $this->getCheckoutProcess()->setHasErrors(true);
	            $errors['delivery_address_error']['id_address']	= $idAddressDelivery;
	            $errors['delivery_address_error']['exception'] 	= $return['error'];
	            $this->context->controller->errors	= [];
				$this->form_has_no_continue_button 	= true;
	            $this->has_bad_address 				= true;
	        }
		}
	    
        $idAddressInvoice = (int) $this->getCheckoutSession()->getIdAddressInvoice();
        $params = array(
            'address_form' => $this->addressForm->getProxy(),
            'use_same_address' => $this->use_same_address,
            'use_different_address_url' => $this->context->link->getPageLink(
                'order',
                true,
                null,
                array('use_same_address' => 0)
            ),
            'new_address_delivery_url' => $this->context->link->getPageLink(
                'order',
                true,
                null,
                array('newAddress' => 'delivery')
            ),
            'new_address_invoice_url' => $this->context->link->getPageLink(
                'order',
                true,
                null,
                array('newAddress' => 'invoice')
            ),
            'id_address' => (int) Tools::getValue('id_address'),
            'id_address_delivery' => $idAddressDelivery,
            'id_address_invoice' => $idAddressInvoice,
            'show_delivery_address_form' => $this->show_delivery_address_form,
            'show_invoice_address_form' => $this->show_invoice_address_form,
            'form_has_no_continue_button' => $this->form_has_no_continue_button,
            'has_bad_address' => $this->has_bad_address,
            'delivery_mode' => $delivery_mode,
            'delivery_address_error' => $delivery_address_error,
        );

        $controller 								= $this->context->controller;
        if (isset($controller)) {
            $warnings 								= $controller->checkoutWarning;
            $addressWarning = isset($warnings['address'])
                ? $warnings['address']
                : false;
            $invalidAddresses = isset($warnings['invalid_addresses'])
                ? $warnings['invalid_addresses']
                : array();

            if (in_array($idAddressDelivery, $invalidAddresses)) {
                $errors['delivery_address_error'] 	= $addressWarning;
	            $params = array_replace(
	                $params,
	                array(
	                    'not_valid_addresses' => implode(',', $invalidAddresses),
	                ),
	                $errors
	            );
            }

            if (in_array($idAddressInvoice, $invalidAddresses)) {
                $errors['invoice_address_error'] 	= $addressWarning;
	            $params = array_replace(
	                $params,
	                array(
	                    'not_valid_addresses' => implode(',', $invalidAddresses),
	                ),
	                $errors
	            );
            }

            if ($delivery_mode == 1 && $this->show_invoice_address_form
                || $idAddressInvoice != $idAddressDelivery
                || !empty($errors['invoice_address_error'])
            ) {
                $this->use_same_address 			= false;
	            $params = array_replace(
	                $params,
	                array(
	                    'use_same_address' => $this->use_same_address,
	                )
	            );
            }

        }

        return $params;
    }

}
