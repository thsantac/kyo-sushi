<?php
/**
 * Copyright since 2007 PrestaShop SA and Contributors
 * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.md.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/OSL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://devdocs.prestashop.com/ for more information.
 *
 * @author    PrestaShop SA and Contributors <contact@prestashop.com>
 * @copyright Since 2007 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
 */
use PrestaShop\PrestaShop\Adapter\ServiceLocator;

class Carrier extends CarrierCore
{
    /** @var bool A emporter */
    public $delivery_mode;

    /** @var int */
    const TO_BE_DELIVERED = 1;
    const TO_GO = 2;

    public function __construct($id = null, $id_lang = null) {
		self::$definition['fields']['delivery_mode'] = ['type' => self::TYPE_INT];
		CarrierCore::__construct($id, $id_lang);
	}

    /**
     * Get all carriers in a given language.
     *
     * @param int $id_lang Language id
     * @param int $modules_filters Possible values:
     *                             - PS_CARRIERS_ONLY
     *                             - CARRIERS_MODULE
     *                             - CARRIERS_MODULE_NEED_RANGE
     *                             - PS_CARRIERS_AND_CARRIER_MODULES_NEED_RANGE
     *                             - ALL_CARRIERS
     * @param bool $active Returns only active carriers when true
     *
     * @return array Carriers
     */
    public static function getCarriers($id_lang, $active = false, $delete = false, $id_zone = false, $ids_group = null, $modules_filters = self::PS_CARRIERS_ONLY)
    {
        // Filter by groups and no groups => return empty array
        if ($ids_group && (!is_array($ids_group) || !count($ids_group))) {
            return [];
        }

		// #TS - 20160910 -----------------------------------------------------------------------------
        $context 							= Context::getContext();
        $delivery_mode						= 0;
		if($context->delivery_mode) {
			if($context->delivery_mode==1) {
		        $delivery_mode				= $context->delivery_mode;
	    	}
	    }

        $sql = '
		SELECT c.*, cl.delay
		FROM `' . _DB_PREFIX_ . 'carrier` c
		LEFT JOIN `' . _DB_PREFIX_ . 'carrier_lang` cl ON (c.`id_carrier` = cl.`id_carrier` AND cl.`id_lang` = ' . (int) $id_lang . Shop::addSqlRestrictionOnLang('cl') . ')
		LEFT JOIN `' . _DB_PREFIX_ . 'carrier_zone` cz ON (cz.`id_carrier` = c.`id_carrier`)' .
        ($id_zone ? 'LEFT JOIN `' . _DB_PREFIX_ . 'zone` z ON (z.`id_zone` = ' . (int) $id_zone . ')' : '') . '
		' . Shop::addSqlAssociation('carrier', 'c') . '
		WHERE c.`deleted` = ' . ($delete ? '1' : '0');
        if ($active) {
            $sql .= ' AND c.`active` = 1 ';
        }
        if ($id_zone) {
            $sql .= ' AND cz.`id_zone` = ' . (int) $id_zone . ' AND z.`active` = 1 ';
        }
        if ($ids_group) {
            $sql .= ' AND EXISTS (SELECT 1 FROM ' . _DB_PREFIX_ . 'carrier_group
									WHERE ' . _DB_PREFIX_ . 'carrier_group.id_carrier = c.id_carrier
									AND id_group IN (' . implode(',', array_map('intval', $ids_group)) . ')) ';
        }

        switch ($modules_filters) {
            case 1:
                $sql .= ' AND c.is_module = 0 ';

                break;
            case 2:
                $sql .= ' AND c.is_module = 1 ';

                break;
            case 3:
                $sql .= ' AND c.is_module = 1 AND c.need_range = 1 ';

                break;
            case 4:
                $sql .= ' AND (c.is_module = 0 OR c.need_range = 1) ';

                break;
        }

		// #TS - 20160910 -----------------------------------------------------------------------------
		if($delivery_mode) $sql .= ' AND c.`delivery_mode` = '.$delivery_mode;
		
        $sql .= ' GROUP BY c.`id_carrier` ORDER BY c.`position` ASC';

        $cache_id = 'Carrier::getCarriers_' . md5($sql);
        if (!Cache::isStored($cache_id)) {
            $carriers = Db::getInstance()->executeS($sql);
            Cache::store($cache_id, $carriers);
        } else {
            $carriers = Cache::retrieve($cache_id);
        }

        foreach ($carriers as $key => $carrier) {
            if ($carrier['name'] == '0') {
                $carriers[$key]['name'] = Carrier::getCarrierNameFromShopName();
            }
        }

        return $carriers;
    }

    public static function checkCarrierZone($id_carrier, $id_zone)
    {
		// #TS - 20160910 -----------------------------------------------------------------------------
        $context 							= Context::getContext();

        $cache_id = 'Carrier::checkCarrierZone_' . (int) $id_carrier . '-' . (int) $id_zone;
        if (!Cache::isStored($cache_id)) {
            $sql = 'SELECT c.`id_carrier`
						FROM `' . _DB_PREFIX_ . 'carrier` c
						LEFT JOIN `' . _DB_PREFIX_ . 'carrier_zone` cz ON (cz.`id_carrier` = c.`id_carrier`)
						LEFT JOIN `' . _DB_PREFIX_ . 'zone` z ON (z.`id_zone` = ' . (int) $id_zone . ')
						WHERE c.`id_carrier` = ' . (int) $id_carrier . '
						AND c.`deleted` = 0
						AND c.`active` = 1
						AND c.`delivery_mode` = '.$context->delivery_mode.'
						AND cz.`id_zone` = ' . (int) $id_zone . '
						AND z.`active` = 1';
            $result = Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue($sql);
            Cache::store($cache_id, $result);
        }

        return Cache::retrieve($cache_id);
    }

    public static function getCarrierForDeliveryMode()
    {
        $context 							= Context::getContext();

        $cache_id = 'Carrier::getCarrierForDeliveryMode' . (int) $context->delivery_mode;
        if (!Cache::isStored($cache_id)) {
            $sql = 'SELECT c.`id_reference`
						FROM `' . _DB_PREFIX_ . 'carrier` c
						WHERE c.`delivery_mode` = '.$context->delivery_mode.' AND c.`active` = 1 AND c.`deleted` = 0';
            $id_reference = Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue($sql);
            Cache::store($cache_id, $id_reference);
        }

        return Cache::retrieve($cache_id);
    }

    /**
     * Get all zones.
     *
     * @return array Zones
     */
    public function getZones()
    {
        return Db::getInstance()->executeS('
			SELECT *
			FROM `' . _DB_PREFIX_ . 'carrier_zone` cz
			LEFT JOIN `' . _DB_PREFIX_ . 'zone` z ON cz.`id_zone` = z.`id_zone`
			WHERE cz.`id_carrier` = ' . (int) $this->id);
    }

    /**
     * Get a specific zones.
     *
     * @return array Zone
     */
    public function getZone($id_zone)
    {
        return Db::getInstance()->executeS('
			SELECT *
			FROM `' . _DB_PREFIX_ . 'carrier_zone`
			WHERE `id_carrier` = ' . (int) $this->id . '
			AND `id_zone` = ' . (int) $id_zone);
    }

    /**
     * Add zone.
     */
    public function addZone($id_zone)
    {
        if (Db::getInstance()->execute('
			INSERT INTO `' . _DB_PREFIX_ . 'carrier_zone` (`id_carrier` , `id_zone`)
			VALUES (' . (int) $this->id . ', ' . (int) $id_zone . ')
		')) {
            // Get all ranges for this carrier
            $ranges_price = RangePrice::getRanges($this->id);
            $ranges_weight = RangeWeight::getRanges($this->id);
            // Create row in ps_delivery table
            if (count($ranges_price) || count($ranges_weight)) {
                $sql = 'INSERT INTO `' . _DB_PREFIX_ . 'delivery` (`id_carrier`, `id_range_price`, `id_range_weight`, `id_zone`, `price`) VALUES ';
                if (count($ranges_price)) {
                    foreach ($ranges_price as $range) {
                        $sql .= '(' . (int) $this->id . ', ' . (int) $range['id_range_price'] . ', 0, ' . (int) $id_zone . ', 0),';
                    }
                }

                if (count($ranges_weight)) {
                    foreach ($ranges_weight as $range) {
                        $sql .= '(' . (int) $this->id . ', 0, ' . (int) $range['id_range_weight'] . ', ' . (int) $id_zone . ', 0),';
                    }
                }
                $sql = rtrim($sql, ',');

                return Db::getInstance()->execute($sql);
            }

            return true;
        }

        return false;
    }

    /**
     * Delete zone.
     */
    public function deleteZone($id_zone)
    {
        if (Db::getInstance()->execute('
			DELETE FROM `' . _DB_PREFIX_ . 'carrier_zone`
			WHERE `id_carrier` = ' . (int) $this->id . '
			AND `id_zone` = ' . (int) $id_zone . ' LIMIT 1
		')) {
            return Db::getInstance()->execute('
				DELETE FROM `' . _DB_PREFIX_ . 'delivery`
				WHERE `id_carrier` = ' . (int) $this->id . '
				AND `id_zone` = ' . (int) $id_zone);
        }

        return false;
    }

    /**
     * Gets a specific group.
     *
     * @since 1.5.0
     *
     * @return array Group
     */
    public function getGroups()
    {
        return Db::getInstance()->executeS('
			SELECT id_group
			FROM ' . _DB_PREFIX_ . 'carrier_group
			WHERE id_carrier=' . (int) $this->id);
    }

    /**
     * Clean delivery prices (weight/price).
     *
     * @param string $rangeTable Table name to clean (weight or price according to shipping method)
     *
     * @return bool Deletion result
     */
    public function deleteDeliveryPrice($range_table)
    {
        $where = '`id_carrier` = ' . (int) $this->id . ' AND (`id_' . bqSQL($range_table) . '` IS NOT NULL OR `id_' . bqSQL($range_table) . '` = 0) ';

        if (Shop::getContext() == Shop::CONTEXT_ALL) {
            $where .= 'AND id_shop IS NULL AND id_shop_group IS NULL';
        } elseif (Shop::getContext() == Shop::CONTEXT_GROUP) {
            $where .= 'AND id_shop IS NULL AND id_shop_group = ' . (int) Shop::getContextShopGroupID();
        } else {
            $where .= 'AND id_shop = ' . (int) Shop::getContextShopID();
        }

        return Db::getInstance()->delete('delivery', $where);
    }

    /**
     * Add new delivery prices.
     *
     * @param array $priceList Prices list in multiple arrays (changed to array since 1.5.0)
     *
     * @return bool Insertion result
     */
    public function addDeliveryPrice($price_list, $delete = false)
    {
        if (!$price_list) {
            return false;
        }

        $keys = array_keys($price_list[0]);
        if (!in_array('id_shop', $keys)) {
            $keys[] = 'id_shop';
        }
        if (!in_array('id_shop_group', $keys)) {
            $keys[] = 'id_shop_group';
        }

        $sql = 'INSERT INTO `' . _DB_PREFIX_ . 'delivery` (' . implode(', ', $keys) . ') VALUES ';
        foreach ($price_list as $values) {
            if (!isset($values['id_shop'])) {
                $values['id_shop'] = (Shop::getContext() == Shop::CONTEXT_SHOP) ? Shop::getContextShopID() : null;
            }
            if (!isset($values['id_shop_group'])) {
                $values['id_shop_group'] = (Shop::getContext() != Shop::CONTEXT_ALL) ? Shop::getContextShopGroupID() : null;
            }

            if ($delete) {
                Db::getInstance()->execute(
                    'DELETE FROM `' . _DB_PREFIX_ . 'delivery`
                    WHERE ' . (null === $values['id_shop'] ? 'ISNULL(`id_shop`) ' : 'id_shop = ' . (int) $values['id_shop']) . '
                    AND ' . (null === $values['id_shop_group'] ? 'ISNULL(`id_shop`) ' : 'id_shop_group=' . (int) $values['id_shop_group']) . '
                    AND id_carrier=' . (int) $values['id_carrier'] .
                    ($values['id_range_price'] !== null ? ' AND id_range_price=' . (int) $values['id_range_price'] : ' AND (ISNULL(`id_range_price`) OR `id_range_price` = 0)') .
                    ($values['id_range_weight'] !== null ? ' AND id_range_weight=' . (int) $values['id_range_weight'] : ' AND (ISNULL(`id_range_weight`) OR `id_range_weight` = 0)') . '
					AND id_zone=' . (int) $values['id_zone']
                );
            }

            $sql .= '(';
            foreach ($values as $v) {
                if (null === $v) {
                    $sql .= 'NULL';
                } elseif (is_int($v) || is_float($v)) {
                    $sql .= $v;
                } else {
                    $sql .= '\'' . Db::getInstance()->escape($v, false, true) . '\'';
                }
                $sql .= ', ';
            }
            $sql = rtrim($sql, ', ') . '), ';
        }
        $sql = rtrim($sql, ', ');

        return Db::getInstance()->execute($sql);
    }

    /**
     * Copy old carrier informations when update carrier.
     *
     * @param int $oldId Old id carrier (copy from that id)
     */
    public function copyCarrierData($old_id)
    {
        if (!Validate::isUnsignedId($old_id)) {
            throw new PrestaShopException('Incorrect identifier for carrier');
        }

        if (!$this->id) {
            return false;
        }

        $old_logo = _PS_SHIP_IMG_DIR_ . '/' . (int) $old_id . '.jpg';
        if (file_exists($old_logo)) {
            copy($old_logo, _PS_SHIP_IMG_DIR_ . '/' . (int) $this->id . '.jpg');
        }

        $old_tmp_logo = _PS_TMP_IMG_DIR_ . '/carrier_mini_' . (int) $old_id . '.jpg';
        if (file_exists($old_tmp_logo)) {
            if (!isset($_FILES['logo'])) {
                copy($old_tmp_logo, _PS_TMP_IMG_DIR_ . '/carrier_mini_' . $this->id . '.jpg');
            }
            unlink($old_tmp_logo);
        }

        // Copy existing ranges price
        foreach (['range_price', 'range_weight'] as $range) {
            $res = Db::getInstance()->executeS('
				SELECT `id_' . $range . '` as id_range, `delimiter1`, `delimiter2`
				FROM `' . _DB_PREFIX_ . $range . '`
				WHERE `id_carrier` = ' . (int) $old_id);
            if (count($res)) {
                foreach ($res as $val) {
                    Db::getInstance()->execute('
						INSERT INTO `' . _DB_PREFIX_ . $range . '` (`id_carrier`, `delimiter1`, `delimiter2`)
						VALUES (' . (int) $this->id . ',' . (float) $val['delimiter1'] . ',' . (float) $val['delimiter2'] . ')');
                    $range_id = (int) Db::getInstance()->Insert_ID();

                    $range_price_id = ($range == 'range_price') ? $range_id : 'NULL';
                    $range_weight_id = ($range == 'range_weight') ? $range_id : 'NULL';

                    Db::getInstance()->execute('
						INSERT INTO `' . _DB_PREFIX_ . 'delivery` (`id_carrier`, `id_shop`, `id_shop_group`, `id_range_price`, `id_range_weight`, `id_zone`, `price`) (
							SELECT ' . (int) $this->id . ', `id_shop`, `id_shop_group`, ' . (int) $range_price_id . ', ' . (int) $range_weight_id . ', `id_zone`, `price`
							FROM `' . _DB_PREFIX_ . 'delivery`
							WHERE `id_carrier` = ' . (int) $old_id . '
							AND `id_' . $range . '` = ' . (int) $val['id_range'] . '
						)
					');
                }
            }
        }

        // Copy existing zones
        $res = Db::getInstance()->executeS('
			SELECT *
			FROM `' . _DB_PREFIX_ . 'carrier_zone`
			WHERE id_carrier = ' . (int) $old_id);
        foreach ($res as $val) {
            Db::getInstance()->execute('
				INSERT INTO `' . _DB_PREFIX_ . 'carrier_zone` (`id_carrier`, `id_zone`)
				VALUES (' . (int) $this->id . ',' . (int) $val['id_zone'] . ')
			');
        }

        //Copy default carrier
        if (Configuration::get('PS_CARRIER_DEFAULT') == $old_id) {
            Configuration::updateValue('PS_CARRIER_DEFAULT', (int) $this->id);
        }

        // Copy reference
        $id_reference = Db::getInstance()->getValue('
			SELECT `id_reference`
			FROM `' . _DB_PREFIX_ . $this->def['table'] . '`
			WHERE id_carrier = ' . (int) $old_id);
        Db::getInstance()->execute('
			UPDATE `' . _DB_PREFIX_ . $this->def['table'] . '`
			SET `id_reference` = ' . (int) $id_reference . '
			WHERE `id_carrier` = ' . (int) $this->id);

        $this->id_reference = (int) $id_reference;

        // Copy tax rules group
        Db::getInstance()->execute('INSERT INTO `' . _DB_PREFIX_ . 'carrier_tax_rules_group_shop` (`id_carrier`, `id_tax_rules_group`, `id_shop`)
												(SELECT ' . (int) $this->id . ', `id_tax_rules_group`, `id_shop`
													FROM `' . _DB_PREFIX_ . 'carrier_tax_rules_group_shop`
													WHERE `id_carrier`=' . (int) $old_id . ')');
    }

    /**
     * Get carrier using the reference id.
     */
    public static function getCarrierByReference($id_reference, $id_lang = null)
    {
        /** @todo class var $table must became static. here I have to use 'carrier' because this method is static */
        $id_carrier = Db::getInstance()->getValue('SELECT `id_carrier` FROM `' . _DB_PREFIX_ . 'carrier`
			WHERE id_reference = ' . (int) $id_reference . ' AND deleted = 0 ORDER BY id_carrier DESC');
        if (!$id_carrier) {
            return false;
        }

        return new Carrier($id_carrier, $id_lang);
    }

    /**
     * Check if Carrier is used (at least one order placed).
     *
     * @return int Order count for this carrier
     */
    public function isUsed()
    {
        $row = Db::getInstance()->getRow('
		SELECT COUNT(`id_carrier`) AS total
		FROM `' . _DB_PREFIX_ . 'orders`
		WHERE `id_carrier` = ' . (int) $this->id);

        return (int) $row['total'];
    }

    /**
     * Get shipping method of the carrier (free, weight or price).
     *
     * @return int Shipping method enumerator
     */
    public function getShippingMethod()
    {
        if ($this->is_free) {
            return Carrier::SHIPPING_METHOD_FREE;
        }

        $method = (int) $this->shipping_method;

        if ($this->shipping_method == Carrier::SHIPPING_METHOD_DEFAULT) {
            // backward compatibility
            if ((int) Configuration::get('PS_SHIPPING_METHOD')) {
                $method = Carrier::SHIPPING_METHOD_WEIGHT;
            } else {
                $method = Carrier::SHIPPING_METHOD_PRICE;
            }
        }

        return $method;
    }

    /**
     * Get range table of carrier.
     *
     * @return bool|string Range table, false if not found
     */
    public function getRangeTable()
    {
        $shipping_method = $this->getShippingMethod();
        if ($shipping_method == Carrier::SHIPPING_METHOD_WEIGHT) {
            return 'range_weight';
        } elseif ($shipping_method == Carrier::SHIPPING_METHOD_PRICE) {
            return 'range_price';
        }

        return false;
    }

    /**
     * Get Range object, price or weight, depending on the shipping method given.
     *
     * @param int|bool $shipping_method Shipping method enumerator
     *                                  Use false in order to let this method find the correct one
     *
     * @return bool|RangePrice|RangeWeight
     */
    public function getRangeObject($shipping_method = false)
    {
        if (!$shipping_method) {
            $shipping_method = $this->getShippingMethod();
        }

        if ($shipping_method == Carrier::SHIPPING_METHOD_WEIGHT) {
            return new RangeWeight();
        } elseif ($shipping_method == Carrier::SHIPPING_METHOD_PRICE) {
            return new RangePrice();
        }

        return false;
    }

    /**
     * Get range suffix.
     *
     * @param Currency|null $currency Currency
     *
     * @return string Currency sign in suffix to use for the range
     */
    public function getRangeSuffix($currency = null)
    {
        if (!$currency) {
            $currency = Context::getContext()->currency;
        }
        $suffix = Configuration::get('PS_WEIGHT_UNIT');
        if ($this->getShippingMethod() == Carrier::SHIPPING_METHOD_PRICE) {
            $suffix = $currency->sign;
        }

        return $suffix;
    }

    /**
     * Get TaxRulesGroup ID for this Carrier.
     *
     * @param Context|null $context Context
     *
     * @return false|string|null TaxrulesGroup ID
     *                           false if not found
     */
    public function getIdTaxRulesGroup(Context $context = null)
    {
        return Carrier::getIdTaxRulesGroupByIdCarrier((int) $this->id, $context);
    }

    /**
     * Get TaxRulesGroup ID for a given Carrier.
     *
     * @param int $id_carrier Carrier ID
     * @param Context|null $context Context
     *
     * @return false|string|null TaxRulesGroup ID
     *                           false if not found
     */
    public static function getIdTaxRulesGroupByIdCarrier($id_carrier, Context $context = null)
    {
        if (!$context) {
            $context = Context::getContext();
        }
        $key = 'carrier_id_tax_rules_group_' . (int) $id_carrier . '_' . (int) $context->shop->id;
        if (!Cache::isStored($key)) {
            $result = Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue('
							SELECT `id_tax_rules_group`
							FROM `' . _DB_PREFIX_ . 'carrier_tax_rules_group_shop`
							WHERE `id_carrier` = ' . (int) $id_carrier . ' AND id_shop=' . (int) Context::getContext()->shop->id);
            Cache::store($key, $result);

            return $result;
        }

        return Cache::retrieve($key);
    }

    /**
     * Set TaxRulesGroup.
     *
     * @param int $id_tax_rules_group Set the TaxRulesGroup with the given ID
     *                                as this Carrier's TaxRulesGroup
     * @param bool $all_shops True if this should be done for all shops
     *
     * @return bool Whether the TaxRulesGroup has been succesfully set
     *              for this Carrier in this Shop or all given Shops
     */
    public function setTaxRulesGroup($id_tax_rules_group, $all_shops = false)
    {
        if (!Validate::isUnsignedId($id_tax_rules_group)) {
            die(Tools::displayError());
        }

        if (!$all_shops) {
            $shops = Shop::getContextListShopID();
        } else {
            $shops = Shop::getShops(true, null, true);
        }

        $this->deleteTaxRulesGroup($shops);

        $values = [];
        foreach ($shops as $id_shop) {
            $values[] = [
                'id_carrier' => (int) $this->id,
                'id_tax_rules_group' => (int) $id_tax_rules_group,
                'id_shop' => (int) $id_shop,
            ];
        }
        Cache::clean('carrier_id_tax_rules_group_' . (int) $this->id . '_' . (int) Context::getContext()->shop->id);

        return Db::getInstance()->insert('carrier_tax_rules_group_shop', $values);
    }

    /**
     * Delete TaxRulesGroup from this Carrier.
     *
     * @param array|null $shops Shops
     *
     * @return bool Whether the TaxRulesGroup has been successfully removed from this Carrier
     */
    public function deleteTaxRulesGroup(array $shops = null)
    {
        if (!$shops) {
            $shops = Shop::getContextListShopID();
        }

        $where = 'id_carrier = ' . (int) $this->id;
        if ($shops) {
            $where .= ' AND id_shop IN(' . implode(', ', array_map('intval', $shops)) . ')';
        }

        return Db::getInstance()->delete('carrier_tax_rules_group_shop', $where);
    }

    /**
     * Returns the Tax rates associated to the Carrier.
     *
     * @since 1.5
     *
     * @param Address $address Address optional
     *
     * @return float Total Tax rate for this Carrier
     */
    public function getTaxesRate(Address $address = null)
    {
        if (!$address || !$address->id_country) {
            $address = Address::initialize();
        }

        $tax_calculator = $this->getTaxCalculator($address);

        return $tax_calculator->getTotalRate();
    }

    /**
     * Returns the taxes calculator associated to the carrier.
     *
     * @since 1.5
     *
     * @param Address $address Address
     *
     * @return TaxCalculator Tax calculator object
     */
    public function getTaxCalculator(Address $address, $id_order = null, $use_average_tax_of_products = false)
    {
        if ($use_average_tax_of_products) {
            return ServiceLocator::get('AverageTaxOfProductsTaxCalculator')->setIdOrder($id_order);
        } else {
            $tax_manager = TaxManagerFactory::getManager($address, $this->getIdTaxRulesGroup());

            return $tax_manager->getTaxCalculator();
        }
    }

    /**
     * This tricky method generates a SQL clause to check if ranged data are overloaded by multishop.
     *
     * @since 1.5.0
     *
     * @param string $range_table Range table
     *
     * @return string SQL quoer to get the delivery range table in this Shop(Group)
     */
    public static function sqlDeliveryRangeShop($range_table, $alias = 'd')
    {
        if (Shop::getContext() == Shop::CONTEXT_ALL) {
            $where = 'AND d2.id_shop IS NULL AND d2.id_shop_group IS NULL';
        } elseif (Shop::getContext() == Shop::CONTEXT_GROUP) {
            $where = 'AND ((d2.id_shop_group IS NULL OR d2.id_shop_group = ' . (int) Shop::getContextShopGroupID() . ') AND d2.id_shop IS NULL)';
        } else {
            $where = 'AND (d2.id_shop = ' . (int) Shop::getContextShopID() . ' OR (d2.id_shop_group = ' . (int) Shop::getContextShopGroupID() . '
					AND d2.id_shop IS NULL) OR (d2.id_shop_group IS NULL AND d2.id_shop IS NULL))';
        }

        $sql = 'AND ' . $alias . '.id_delivery = (
					SELECT d2.id_delivery
					FROM ' . _DB_PREFIX_ . 'delivery d2
					WHERE d2.id_carrier = `' . bqSQL($alias) . '`.id_carrier
						AND d2.id_zone = `' . bqSQL($alias) . '`.id_zone
						AND d2.`id_' . bqSQL($range_table) . '` = `' . bqSQL($alias) . '`.`id_' . bqSQL($range_table) . '`
						' . $where . '
					ORDER BY d2.id_shop DESC, d2.id_shop_group DESC
					LIMIT 1
				)';

        return $sql;
    }

    /**
     * Moves a carrier.
     *
     * @since 1.5.0
     *
     * @param bool $way Up (1) or Down (0)
     * @param int $position Current position of the Carrier
     *
     * @return bool Whether the update was successful
     */
    public function updatePosition($way, $position)
    {
        if (!$res = Db::getInstance()->executeS(
            'SELECT `id_carrier`, `position`
			FROM `' . _DB_PREFIX_ . 'carrier`
			WHERE `deleted` = 0
			ORDER BY `position` ASC'
        )) {
            return false;
        }

        foreach ($res as $carrier) {
            if ((int) $carrier['id_carrier'] == (int) $this->id) {
                $moved_carrier = $carrier;
            }
        }

        if (!isset($moved_carrier) || !isset($position)) {
            return false;
        }

        // < and > statements rather than BETWEEN operator
        // since BETWEEN is treated differently according to databases
        return Db::getInstance()->execute('
			UPDATE `' . _DB_PREFIX_ . 'carrier`
			SET `position`= `position` ' . ($way ? '- 1' : '+ 1') . '
			WHERE `position`
			' . ($way
                ? '> ' . (int) $moved_carrier['position'] . ' AND `position` <= ' . (int) $position
                : '< ' . (int) $moved_carrier['position'] . ' AND `position` >= ' . (int) $position . '
			AND `deleted` = 0'))
        && Db::getInstance()->execute('
			UPDATE `' . _DB_PREFIX_ . 'carrier`
			SET `position` = ' . (int) $position . '
			WHERE `id_carrier` = ' . (int) $moved_carrier['id_carrier']);
    }

    /**
     * Reorder Carrier positions
     * Called after deleting a Carrier.
     *
     * @since 1.5.0
     *
     * @return bool $return
     */
    public static function cleanPositions()
    {
        $return = true;

        $sql = '
		SELECT `id_carrier`
		FROM `' . _DB_PREFIX_ . 'carrier`
		WHERE `deleted` = 0
		ORDER BY `position` ASC';
        $result = Db::getInstance()->executeS($sql);

        $i = 0;
        foreach ($result as $value) {
            $return = Db::getInstance()->execute('
			UPDATE `' . _DB_PREFIX_ . 'carrier`
			SET `position` = ' . (int) $i++ . '
			WHERE `id_carrier` = ' . (int) $value['id_carrier']);
        }

        return $return;
    }

    /**
     * Gets the highest carrier position.
     *
     * @since 1.5.0
     *
     * @return int $position
     */
    public static function getHigherPosition()
    {
        $sql = 'SELECT MAX(`position`)
				FROM `' . _DB_PREFIX_ . 'carrier`
				WHERE `deleted` = 0';
        $position = Db::getInstance()->getValue($sql);

        return (is_numeric($position)) ? $position : -1;
    }

    /**
     * For a given {product, warehouse}, gets the carrier available.
     *
     * @since 1.5.0
     *
     * @param Product $product The id of the product, or an array with at least the package size and weight
     * @param int $id_warehouse Warehouse ID
     * @param int $id_address_delivery Delivery Address ID
     * @param int $id_shop Shop ID
     * @param Cart $cart Cart object
     * @param array &$error contain an error message if an error occurs
     *
     * @return array Available Carriers
     *
     * @throws PrestaShopDatabaseException
     */
    public static function getAvailableCarrierList(Product $product, $id_warehouse, $id_address_delivery = null, $id_shop = null, $cart = null, &$error = [])
    {
        static $ps_country_default = null;

        if ($ps_country_default === null) {
            $ps_country_default = Configuration::get('PS_COUNTRY_DEFAULT');
        }

        if (null === $id_shop) {
            $id_shop = Context::getContext()->shop->id;
        }
        if (null === $cart) {
            $cart = Context::getContext()->cart;
        }

        if (null === $error || !is_array($error)) {
            $error = [];
        }

        $id_address = (int) ((null !== $id_address_delivery && $id_address_delivery != 0) ? $id_address_delivery : $cart->id_address_delivery);
        if ($id_address) {
            $id_zone = Address::getZoneById($id_address);

            // Check the country of the address is activated
            if (!Address::isCountryActiveById($id_address)) {
                return [];
            }
        } else {
            $country = new Country($ps_country_default);
            $id_zone = $country->id_zone;
        }

        // Does the product is linked with carriers?
        $cache_id = 'Carrier::getAvailableCarrierList_' . (int) $product->id . '-' . (int) $id_shop;
        if (!Cache::isStored($cache_id)) {
            $query = new DbQuery();
            $query->select('id_carrier');
            $query->from('product_carrier', 'pc');
            $query->innerJoin(
                'carrier',
                'c',
                'c.id_reference = pc.id_carrier_reference AND c.deleted = 0 AND c.active = 1'
            );
            $query->where('pc.id_product = ' . (int) $product->id);
            $query->where('pc.id_shop = ' . (int) $id_shop);

            $carriers_for_product = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($query);
            Cache::store($cache_id, $carriers_for_product);
        } else {
            $carriers_for_product = Cache::retrieve($cache_id);
        }

        $carrier_list = [];
        if (!empty($carriers_for_product)) {
            //the product is linked with carriers
            foreach ($carriers_for_product as $carrier) { //check if the linked carriers are available in current zone
                if (Carrier::checkCarrierZone($carrier['id_carrier'], $id_zone)) {
                    $carrier_list[$carrier['id_carrier']] = $carrier['id_carrier'];
                }
            }
            if (empty($carrier_list)) {
                return [];
            }//no linked carrier are available for this zone
        }

        // The product is not directly linked with a carrier
        // Get all the carriers linked to a warehouse
        if ($id_warehouse) {
            $warehouse = new Warehouse($id_warehouse);
            $warehouse_carrier_list = $warehouse->getCarriers();
        }

        $available_carrier_list = [];
        $cache_id = 'Carrier::getAvailableCarrierList_getCarriersForOrder_' . (int) $id_zone . '-' . (int) $cart->id;
        if (!Cache::isStored($cache_id)) {
            $customer = new Customer($cart->id_customer);
            $carrier_error = [];
            $carriers = Carrier::getCarriersForOrder($id_zone, $customer->getGroups(), $cart, $carrier_error);
            Cache::store($cache_id, [$carriers, $carrier_error]);
        } else {
            list($carriers, $carrier_error) = Cache::retrieve($cache_id);
        }

        $error = array_merge($error, $carrier_error);

        foreach ($carriers as $carrier) {
            $available_carrier_list[$carrier['id_carrier']] = $carrier['id_carrier'];
        }

        if ($carrier_list) {
            $carrier_list = array_intersect($available_carrier_list, $carrier_list);
        } else {
            $carrier_list = $available_carrier_list;
        }

        if (isset($warehouse_carrier_list)) {
            $carrier_list = array_intersect($carrier_list, $warehouse_carrier_list);
        }

        $cart_quantity = 0;
        $cart_weight = 0;

        foreach ($cart->getProducts(false, false) as $cart_product) {
            if ($cart_product['id_product'] == $product->id) {
                $cart_quantity += $cart_product['cart_quantity'];
            }
            if (isset($cart_product['weight_attribute']) && $cart_product['weight_attribute'] > 0) {
                $cart_weight += ($cart_product['weight_attribute'] * $cart_product['cart_quantity']);
            } else {
                $cart_weight += ($cart_product['weight'] * $cart_product['cart_quantity']);
            }
        }

        if ($product->width > 0 || $product->height > 0 || $product->depth > 0 || $product->weight > 0 || $cart_weight > 0) {
            foreach ($carrier_list as $key => $id_carrier) {
                $carrier = new Carrier($id_carrier);

                // Get the sizes of the carrier and the product and sort them to check if the carrier can take the product.
                $carrier_sizes = [(int) $carrier->max_width, (int) $carrier->max_height, (int) $carrier->max_depth];
                $product_sizes = [(int) $product->width, (int) $product->height, (int) $product->depth];
                rsort($carrier_sizes, SORT_NUMERIC);
                rsort($product_sizes, SORT_NUMERIC);

                if (($carrier_sizes[0] > 0 && $carrier_sizes[0] < $product_sizes[0])
                    || ($carrier_sizes[1] > 0 && $carrier_sizes[1] < $product_sizes[1])
                    || ($carrier_sizes[2] > 0 && $carrier_sizes[2] < $product_sizes[2])) {
                    $error[$carrier->id] = Carrier::SHIPPING_SIZE_EXCEPTION;
                    unset($carrier_list[$key]);
                }

                if ($carrier->max_weight > 0 && ($carrier->max_weight < $product->weight * $cart_quantity || $carrier->max_weight < $cart_weight)) {
                    $error[$carrier->id] = Carrier::SHIPPING_WEIGHT_EXCEPTION;
                    unset($carrier_list[$key]);
                }
            }
        }

        return $carrier_list;
    }

    /**
     * Assign one (ore more) group to all carriers.
     *
     * @since 1.5.0
     *
     * @param int|array $id_group_list Group ID or array of Group IDs
     * @param array $exception List of Carrier IDs to ignore
     *
     * @return bool
     */
    public static function assignGroupToAllCarriers($id_group_list, $exception = [])
    {
        if (!is_array($id_group_list)) {
            $id_group_list = [$id_group_list];
        }

        $id_group_list = array_map('intval', $id_group_list);
        $exception = array_map('intval', $exception);

        Db::getInstance()->execute('
			DELETE FROM `' . _DB_PREFIX_ . 'carrier_group`
			WHERE `id_group` IN (' . implode(',', $id_group_list) . ')');

        $carrier_list = Db::getInstance()->executeS('
			SELECT id_carrier FROM `' . _DB_PREFIX_ . 'carrier`
			WHERE deleted = 0
			' . (is_array($exception) && count($exception) > 0 ? 'AND id_carrier NOT IN (' . implode(',', $exception) . ')' : ''));

        if ($carrier_list) {
            $data = [];
            foreach ($carrier_list as $carrier) {
                foreach ($id_group_list as $id_group) {
                    $data[] = [
                        'id_carrier' => $carrier['id_carrier'],
                        'id_group' => $id_group,
                    ];
                }
            }

            return Db::getInstance()->insert('carrier_group', $data, false, false, Db::INSERT);
        }

        return true;
    }

    /**
     * Set Carrier Groups.
     *
     * @param array $groups Carrier Groups
     * @param bool $delete Delete all previously Carrier Groups which
     *                     were linked to this Carrier
     *
     * @return bool Whether the Carrier Groups have been successfully set
     */
    public function setGroups($groups, $delete = true)
    {
        if ($delete) {
            Db::getInstance()->execute('DELETE FROM ' . _DB_PREFIX_ . 'carrier_group WHERE id_carrier = ' . (int) $this->id);
        }
        if (!is_array($groups) || !count($groups)) {
            return true;
        }
        $sql = 'INSERT INTO ' . _DB_PREFIX_ . 'carrier_group (id_carrier, id_group) VALUES ';
        foreach ($groups as $id_group) {
            $sql .= '(' . (int) $this->id . ', ' . (int) $id_group . '),';
        }

        return Db::getInstance()->execute(rtrim($sql, ','));
    }

    /**
     * Return the carrier name from the shop name (e.g. if the carrier name is '0').
     *
     * The returned carrier name is the shop name without '#' and ';' because this is not the same validation.
     *
     * @return string Carrier name
     */
    public static function getCarrierNameFromShopName()
    {
        return str_replace(
            ['#', ';'],
            '',
            Configuration::get('PS_SHOP_NAME')
        );
    }
}
