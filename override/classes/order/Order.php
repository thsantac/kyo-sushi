<?php
/**
 * Copyright since 2007 PrestaShop SA and Contributors
 * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.md.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/OSL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://devdocs.prestashop.com/ for more information.
 *
 * @author    PrestaShop SA and Contributors <contact@prestashop.com>
 * @copyright Since 2007 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
 */
use PrestaShop\PrestaShop\Adapter\ServiceLocator;

class Order extends OrderCore
{
	public $delivery_when;
	public $to_be_delivered;
	public $sent_to_cashsoft;
	
    public static $definition = [
        'table' => 'orders',
        'primary' => 'id_order',
        'fields' => [
            'id_address_delivery' => ['type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'required' => true],
            'id_address_invoice' => ['type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'required' => true],
            'id_cart' => ['type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'required' => true],
            'id_currency' => ['type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'required' => true],
            'id_shop_group' => ['type' => self::TYPE_INT, 'validate' => 'isUnsignedId'],
            'id_shop' => ['type' => self::TYPE_INT, 'validate' => 'isUnsignedId'],
            'id_lang' => ['type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'required' => true],
            'id_customer' => ['type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'required' => true],
            'id_carrier' => ['type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'required' => true],
            'current_state' => ['type' => self::TYPE_INT, 'validate' => 'isUnsignedId'],
            'secure_key' => ['type' => self::TYPE_STRING, 'validate' => 'isMd5'],
            'payment' => ['type' => self::TYPE_STRING, 'validate' => 'isGenericName', 'required' => true],
            'module' => ['type' => self::TYPE_STRING, 'validate' => 'isModuleName', 'required' => true],
            'recyclable' => ['type' => self::TYPE_BOOL, 'validate' => 'isBool'],
            'gift' => ['type' => self::TYPE_BOOL, 'validate' => 'isBool'],
            'gift_message' => ['type' => self::TYPE_STRING, 'validate' => 'isMessage'],
            'mobile_theme' => ['type' => self::TYPE_BOOL, 'validate' => 'isBool'],
            'total_discounts' => ['type' => self::TYPE_FLOAT, 'validate' => 'isPrice'],
            'total_discounts_tax_incl' => ['type' => self::TYPE_FLOAT, 'validate' => 'isPrice'],
            'total_discounts_tax_excl' => ['type' => self::TYPE_FLOAT, 'validate' => 'isPrice'],
            'total_paid' => ['type' => self::TYPE_FLOAT, 'validate' => 'isPrice', 'required' => true],
            'total_paid_tax_incl' => ['type' => self::TYPE_FLOAT, 'validate' => 'isPrice'],
            'total_paid_tax_excl' => ['type' => self::TYPE_FLOAT, 'validate' => 'isPrice'],
            'total_paid_real' => ['type' => self::TYPE_FLOAT, 'validate' => 'isPrice', 'required' => true],
            'total_products' => ['type' => self::TYPE_FLOAT, 'validate' => 'isPrice', 'required' => true],
            'total_products_wt' => ['type' => self::TYPE_FLOAT, 'validate' => 'isPrice', 'required' => true],
            'total_shipping' => ['type' => self::TYPE_FLOAT, 'validate' => 'isPrice'],
            'total_shipping_tax_incl' => ['type' => self::TYPE_FLOAT, 'validate' => 'isPrice'],
            'total_shipping_tax_excl' => ['type' => self::TYPE_FLOAT, 'validate' => 'isPrice'],
            'carrier_tax_rate' => ['type' => self::TYPE_FLOAT, 'validate' => 'isFloat'],
            'total_wrapping' => ['type' => self::TYPE_FLOAT, 'validate' => 'isPrice'],
            'total_wrapping_tax_incl' => ['type' => self::TYPE_FLOAT, 'validate' => 'isPrice'],
            'total_wrapping_tax_excl' => ['type' => self::TYPE_FLOAT, 'validate' => 'isPrice'],
            'round_mode' => ['type' => self::TYPE_INT, 'validate' => 'isUnsignedId'],
            'round_type' => ['type' => self::TYPE_INT, 'validate' => 'isUnsignedId'],
            'shipping_number' => ['type' => self::TYPE_STRING, 'validate' => 'isTrackingNumber'],
            'conversion_rate' => ['type' => self::TYPE_FLOAT, 'validate' => 'isFloat', 'required' => true],
            'invoice_number' => ['type' => self::TYPE_INT],
            'delivery_number' => ['type' => self::TYPE_INT],
            'invoice_date' => ['type' => self::TYPE_DATE],
            'delivery_date' => ['type' => self::TYPE_DATE],
            'valid' => ['type' => self::TYPE_BOOL],
            'reference' => ['type' => self::TYPE_STRING],
            'date_add' => ['type' => self::TYPE_DATE, 'validate' => 'isDate'],
            'date_upd' => ['type' => self::TYPE_DATE, 'validate' => 'isDate'],
            'delivery_when' => ['type' => self::TYPE_STRING],
            'to_be_delivered' => ['type' => self::TYPE_INT],
            'sent_to_cashsoft' => ['type' => self::TYPE_INT],
        ],
    ];


    public function __construct($id = null, $id_lang = null)
    {
        ObjectModel::__construct($id, $id_lang);

        $is_admin = (is_object(Context::getContext()->controller) && Context::getContext()->controller->controller_type == 'admin');
        if ($this->id_customer && !$is_admin) {
            $customer = new Customer((int) $this->id_customer);
            $this->_taxCalculationMethod = Group::getPriceDisplayMethod((int) $customer->id_default_group);
        } else {
            $this->_taxCalculationMethod = Group::getDefaultPriceDisplayMethod();
        }
	}
	
    /**
     * Get all customer ordered products.
     *
     * @param int $id_customer Customer id
     *
     * @return array Customer orders
     */
    public static function getAllOrderedProducts($id_customer)
    {
        $res = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
        SELECT od.product_id
        FROM `' . _DB_PREFIX_ . 'orders` o
        LEFT JOIN `' . _DB_PREFIX_ . 'order_detail` od ON (od.id_order = o.id_order)
        WHERE o.`id_customer` = ' . (int) $id_customer . '
        GROUP BY od.`product_id`');

        if (!$res) {
            return [];
        }
		
		$product_ids = [];
		foreach($res as $product) {
			$product_ids[] = $product['product_id'];
		}
        return $product_ids;
    }

	// #TS - 20160401 - Interface avec les caisses -------------------------------------------------------------
    public static function logAction($str) {
	
		if(!is_dir($_SERVER['DOCUMENT_ROOT']."/solumag")) {
			$oldmask 						= umask(0);
			mkdir($_SERVER['DOCUMENT_ROOT']."/solumag", 0777);
			umask($oldmask);
			$fp								= fopen($_SERVER['DOCUMENT_ROOT']."/solumag/log.log", "w");
		}
		else
	    	$fp								= fopen($_SERVER['DOCUMENT_ROOT']."/solumag/log.log", "a");
	    fwrite($fp, date("Y-m-d H:i:s")." ==> ".$str."\n");
	    //echo(date("Y-m-d H:i:s")." ==> ".$str."<br>");
	    fclose($fp);
    
    }
    
    public function createProductsCashRegisterFile($f, $customer, $order, $id_address_invoice, $id_address_delivery)
    {
        $context 					= Context::getContext();
		$id_shop 					= $context->shop->id;
		$shop 						= new Shop($id_shop);
		$shopURL					= new ShopUrl($id_shop);
		$id_solumag 				= $shop->id_solumag;

		$site						= 1;

		$lignesCde 					= OrderDetail::getList($order->id);
		
		// Familles ---------------------------------------------------------------------------------------------
		foreach($lignesCde as $detail) {
			$id_product					= $detail['product_id'];
			$categorys 					= Db::getInstance()->executeS('
										SELECT cl.name, cs.position, cs.id_category, c.id_parent, c.level_depth
										FROM `'._DB_PREFIX_.'category_shop` cs
										INNER JOIN `'._DB_PREFIX_.'category_product` cp ON cp.id_category = cs.id_category
										JOIN `'._DB_PREFIX_.'category` c ON c.id_category = cp.id_category
										JOIN `'._DB_PREFIX_.'category_lang` cl ON cl.id_category = cp.id_category
										WHERE cs.id_shop = '.$id_shop.' AND cp.id_product = ' . $id_product . '
										GROUP BY cs.id_category
										ORDER BY cs.id_category'
						            );

			foreach($categorys as $category) {
				if($category['level_depth'] == 2 || substr($category['name'], 0, 4)=="Menu") {
					$categName 				= $category['name'];
					$categParent			= Db::getInstance()->executeS('
												SELECT cl.name, cs.position, cs.id_category, c.id_parent, c.level_depth
												FROM `'._DB_PREFIX_.'category_shop` cs
												JOIN `'._DB_PREFIX_.'category` c ON c.id_category = cs.id_category
												JOIN `'._DB_PREFIX_.'category_lang` cl ON cl.id_category = cs.id_category
												WHERE cs.id_shop = '.$id_shop.' AND cs.id_category = '.$category['id_parent']
											);
					//if(isset($categParent[0]['name']))
					//	$categName 			= $categParent[0]['name']." - ".$categName;
						
					$categName				= str_replace("&", "et", $categName);
					$categName				= str_replace(",", " ", $categName);

					$ligne	 				= "FAM;A;0;".$category['id_category'].";".utf8_decode($categName).";1;".$category['position'].";0;0;0";
					fwrite($f, $ligne."\r\n");
				}
			}
		}

		// Produits ---------------------------------------------------------------------------------------------

		foreach($lignesCde as $detail) {
			$id_product					= $detail['product_id'];	
			$sql 						= '
											SELECT *, ps.price as shop_product_price
											FROM `'._DB_PREFIX_.'product_shop` ps
											JOIN `'._DB_PREFIX_.'product_lang` pl ON pl.id_product = ps.id_product
											JOIN `'._DB_PREFIX_.'image` i ON i.id_product = ps.id_product
											JOIN `'._DB_PREFIX_.'product` p ON p.id_product = ps.id_product
											WHERE ps.id_shop = '.$id_shop.' AND ps.id_product = ' . $id_product . '
											AND ps.active = 1
											GROUP BY ps.id_product
											ORDER BY ps.id_product'
										;
			$products 					= Db::getInstance()->executeS($sql);
			self::logAction("Lignes ART");
			//print_r($products);
			
			foreach($products as $product) {
				$id_product				= (int)$product['id_product'];

				$ligne	 				= "ART";
				$ligne	 			   .= ";";
				$ligne	 			   .= "A";
				$ligne	 			   .= ";";
				// identifiant de l’article interne (999 999 999)
				$ligne	 			   .= ";";
				// identifiant de l’article sur le externe (999 999 999)
				$ligne	 			   .= $product['id_product'];
				$ligne	 			   .= ";";
				// référence de l’article (20 caractères) => ID par defaut
				$ligne	 			   .= $product['id_product'];
				$ligne	 			   .= ";";
				// nom du produit (50 caractères)
				$ligne	 			   .= utf8_decode($product['name']);
				$ligne	 			   .= ";";
				// Proposer une cuisson ? (0/1) => 0
				$ligne	 			   .= "0";
				$ligne	 			   .= ";";
				// Proposer un accompagnement/attribut ? (0/1) => 0
				$ligne	 			   .= "0";
				$ligne	 			   .= ";";
				// ID de la famille accompagnement n°1 logiciel (999 999 999) =>0 
				$ligne	 			   .= "0";
				$ligne	 			   .= ";";
				// ID de la famille accompagnement n°1 externe (999 999 999) =>0 
				$ligne	 			   .= "0";
				$ligne	 			   .= ";";
				// Ne pas proposer l’accompagnement auto (0/1) => 0
				$ligne	 			   .= "0";
				$ligne	 			   .= ";";
				// Article à prix variable ? (0/1) => 0
				$ligne	 			   .= "0";
				$ligne	 			   .= ";";
				// Article visible en caisse ? (0/1) = >1
				$ligne	 			   .= "1";
				$ligne	 			   .= ";";
				// Gérer la fidélité pour ce produit ? (0/1) => 0
				$ligne	 			   .= "0";
				$ligne	 			   .= ";";
				// Gérer le stock ? (0/1) =>0
				$ligne	 			   .= "0";
				$ligne	 			   .= ";";
				// Quantité en stock du produit (±9 999 999,9999999)
				$ligne	 			   .= "0";
				$ligne	 			   .= ";";
				// Quantité mini produit pour alerte (±9 999 999,9999999) 
				$ligne	 			   .= "0";
				$ligne	 			   .= ";";
				// Quantité à réapprovisionner si rupture (±9 999 999,9999999) 
				$ligne	 			   .= "0";
				$ligne	 			   .= ";";
				// Le stock calculé automatiquement à partir des articles de fabrication (0/1) =>0
				$ligne	 			   .= "0";
				$ligne	 			   .= ";";
				// Couleur du texte du bouton en caisse (6 caractères) => noir
				$ligne	 			   .= "noir";
				$ligne	 			   .= ";";
				// Couleur de fond du bouton en caisse (6 caractères) =>si 0 alors jaune clair par défaut (Couleur = palette WEB => blanc = #ffffff / noir = #000000)
				$ligne	 			   .= "0";
				$ligne	 			   .= ";";
				// Sortir un coupon au règlement de la commande ? (0/1) => 0 
				$ligne	 			   .= "0";
				$ligne	 			   .= ";";
				// Description de l’article (mémo).
				$ligne	 			   .= str_replace("&amp;", "&", utf8_decode(html_entity_decode(strip_tags($product['description']))));
				$ligne	 			   .= ";";
				
				// Image dans fichier texte -------------------------------------------------------------------------
				$img_filename			= $_SERVER['DOCUMENT_ROOT']."/solumag/".$shopURL->domain."/".$order->id."/IMG_ART_".$product['id_product']."_".$site.".txt";
				if(is_file($img_filename)) {
					unlink($img_filename);
					self::logAction("!!! Suppression fichier image ".$img_filename);
				}
				
				$fimg					= fopen($img_filename, 'wb');
				self::logAction("Création fichier image ".$img_filename);
	
				$id_image 				= Product::getCover($product['id_product']);
				if ($id_image > 0) {
					$image 				= new Image($id_image['id_image']);
					$img_file 			= $_SERVER['DOCUMENT_ROOT']."/img/p/".$image->getImgPath().".jpg";
					//echo "image=$img_file<br>";
					if(file_exists($img_file)) {
						// image de l’article (mémo binaire ) => l’image est stocké dans un répertoire image avec le nom suivant IMG_ART_IDARTICLE.txt (ex :IMG_ART_112.txt)
						$ligne	 			   .= "1";
						$ligne	 			   .= ";";
						$image_content	= file_get_contents($img_file);
						fwrite($fimg, $image_content);
					}
					else {
						// image de l’article (mémo binaire ) => l’image est stocké dans un répertoire image avec le nom suivant IMG_ART_IDARTICLE.txt (ex :IMG_ART_112.txt)
						$ligne	 			   .= "0";
						$ligne	 			   .= ";";
					}
				} 			
				fclose($fimg);
				
				// Lien vers famille logiciel (999 999 999) 
				$ligne	 			   .= "0";
				$ligne	 			   .= ";";
				// Lien vers famille externe (999 999 999)
				$id_categ				= $product['id_category_default'];
				$categs					= Db::getInstance()->executeS('
											SELECT c.id_category, c.level_depth, cl.name
											FROM `'._DB_PREFIX_.'category_product` cp
											INNER JOIN `'._DB_PREFIX_.'category_shop` cs ON cs.id_category = cp.id_category AND cs.id_shop = '.$id_shop.'
											JOIN `'._DB_PREFIX_.'category` c ON c.id_category = cs.id_category
											JOIN `'._DB_PREFIX_.'category_lang` cl ON cl.id_category = cs.id_category
											WHERE cp.id_product = '.$id_product);
				foreach($categs as $categ) {
					if($categ['level_depth'] == 3 || substr($categ['name'], 0, 4)=="Menu") {
						$id_categ 		= $categ['id_category'];
					}
				}
				$ligne	 			   .= $id_categ;
				$ligne	 			   .= ";";
				// Lien vers Unité logiciel (999 999 999) 
				$ligne	 			   .= "0";
				$ligne	 			   .= ";";
				// Lien vers Unité externe (999 999 999)
				$ligne	 			   .= "0";
				$ligne	 			   .= ";";
				// ID de la famille accompagnement n°2 logiciel (999 999 999) =>0
				$ligne	 			   .= "0";
				$ligne	 			   .= ";";
				// ID de la famille accompagnement n°2 externe (999 999 999) =>0
				$ligne	 			   .= "0";
				$ligne	 			   .= ";";
				// ID de la famille accompagnement n°3 logiciel (999 999 999) =>0
				$ligne	 			   .= "0";
				$ligne	 			   .= ";";
				// ID de la famille accompagnement n°3 externe (999 999 999) =>0
				$ligne	 			   .= "0";
				$ligne	 			   .= ";";
				// ID de la famille accompagnement n°4 logiciel (999 999 999) =>0
				$ligne	 			   .= "0";
				$ligne	 			   .= ";";
				// ID de la famille accompagnement n°4 externe (999 999 999) =>0
				$ligne	 			   .= "0";
				$ligne	 			   .= ";";
				// ID de la famille accompagnement n°5 logiciel (999 999 999) =>0
				$ligne	 			   .= "0";
				$ligne	 			   .= ";";
				// ID de la famille accompagnement n°5 externe (999 999 999) =>0
				$ligne	 			   .= "0";
				$ligne	 			   .= ";";
				// ordre d’affichage de l’article (999 999
				$ligne	 			   .= "0";
				$ligne	 			   .= ";";
				// CA de vente de l’article (999 999 999,99)
				$ligne	 			   .= "0";
				$ligne	 			   .= ";";
				// Nombre d’article vendu (999 999 999)
				$ligne	 			   .= "0";
				$ligne	 			   .= ";";
				// article fabriqué à partir de plusieurs article de base ? (0/1) => 0 
				$ligne	 			   .= "0";
				$ligne	 			   .= ";";
				// article pour la billetterie (0=non/1=oui)=>0
				$ligne	 			   .= "0";
				$ligne	 			   .= ";";
				// nom de l’article sur le billet (50 caractères)
				if($product['reference'])
					$ligne			   .= $product['reference'];
				else
					$ligne			   .= utf8_decode($product['name']);
				$ligne	 			   .= ";";
				// Nombre de billet à imprimer pour cet article (9999)
				$ligne	 			   .= "0";
				$ligne	 			   .= ";";
				// ID du theme billetterie relié à l’article
				$ligne	 			   .= "0";
				$ligne	 			   .= ";";
				// nom subsidiaire pour la fabrication (50 caractères) 
				if($product['reference'])
					$ligne			   .= $product['reference'];
				else
					$ligne			   .= utf8_decode($product['name']);
				$ligne	 			   .= ";";
				// pris en compte dans le happy hour (0/1)
				$ligne	 			   .= "0";
				$ligne	 			   .= ";";
				// c’est un service ou prestation (1/0)
				// Reservé à easy beauté et Easy coiffure
				$ligne	 			   .= "0";
				$ligne	 			   .= ";";
				// durée de la prestation en minutes (9999)
				// Reservé à easy beauté et Easy coiffure
				$ligne	 			   .= "0";
				$ligne	 			   .= ";";
	
				fwrite($f, $ligne."\r\n");
			}
		}
		
		// TVA ---------------------------------------------------------------------------------------------

		$taxes 						= Db::getInstance()->executeS('
										SELECT *
										FROM `'._DB_PREFIX_.'tax` t
										JOIN `'._DB_PREFIX_.'tax_lang` tl ON tl.id_tax = t.id_tax
										ORDER BY t.id_tax'
						            );

		self::logAction("Lignes TVA");

		foreach($taxes as $taxe) {
			$id_tax					= (int)$taxe['id_tax'];

			$ligne	 				= "TVA";
			$ligne	 			   .= ";";
			$ligne	 			   .= "A";
			$ligne	 			   .= ";";
			// identifiant interne (999 999 999)
			$ligne	 			   .= ";";
			// identifiant externe (999 999 999)
			$ligne	 			   .= $taxe['id_tax'];
			$ligne	 			   .= ";";
			// Nom de la tva (50 caractères)
			$ligne	 			   .= utf8_decode($taxe['name']);
			$ligne	 			   .= ";";
			// taux de la tva (ex : 19,6) (±99,99)
			$ligne	 			   .= $taxe['rate'];
			$ligne	 			   .= ";";
			// C’est une multi TVA ? (0/1) => 0 par défaut
			$ligne	 			   .= "0";
			$ligne	 			   .= ";";
			fwrite($f, $ligne."\r\n");
		}
		
		
		// 1=Sur place
		$ligne	 			   		= "TAR;V;1;1;";
		fwrite($f, $ligne."\r\n");
		// 2=A emporter
		$ligne	 			   		= "TAR;V;2;2;";
		fwrite($f, $ligne."\r\n");
		// 3=Livraison
		$ligne	 			   		= "TAR;V;3;3;";
		fwrite($f, $ligne."\r\n");
		
		
		// Prix ---------------------------------------------------------------------------------------------

		self::logAction("Ligne PRI");

		foreach($products as $product) {
			
			if($product['id_tax_rules_group']) {
				$id_product						= (int)$product['id_product'];
				
				$taxe 							= Db::getInstance()->executeS('
													SELECT *
													FROM `'._DB_PREFIX_.'tax_rule` tr
													JOIN `'._DB_PREFIX_.'tax` t ON tr.id_tax = t.id_tax
													WHERE tr.id_tax_rules_group = '.$product['id_tax_rules_group'].' AND tr.id_country=8'
									            );
				//echo "Pour le produit ".$id_product.", prix=".$product['shop_product_price']."<br>";
				for($i=1; $i<=3; $i++) {
					// 1 : Sur place, 2 : A emporter, 3 : Livraison
					$ligne	 				= "PRI";
					$ligne	 			   .= ";";
					$ligne	 			   .= "A";
					$ligne	 			   .= ";";
					// Identifiant de la prix logiciel (999 999 999)
					$ligne	 			   .= ";";
					// Identifiant prix externe (999 999 999)
					$ligne	 			   .= $id_product.$i;
					$ligne	 			   .= ";";
					// Prix HT
					$ligne	 			   .= $product['shop_product_price'];
					$ligne	 			   .= ";";
					// PrixTTC (*2)= Prix TTC de l’article (99 999 999,99)
					$prixTTC				= $product['shop_product_price'] + ($product['shop_product_price'] * $taxe[0]['rate'] / 100);
					if($i==2)
						$prixTTC			= $prixTTC - ($prixTTC * $shop->taux_reduc_emporter / 100);
					// Pierre-Yves (5/09/2016) : En revanche est ce que tu peux appliquer la même remise pour les prix "sur place" mais uniquement pour la boutique 5, Aix les Milles (à Marseille, prix sur place = prix livré; aux Milles, prix sur place = prix emporter soit prix livré * 0.95)
					if($i==1 && $id_shop==5)
						$prixTTC			= $prixTTC - ($prixTTC * $shop->taux_reduc_emporter / 100);
					$ligne	 			   .= $prixTTC;
					$ligne	 			   .= ";";
					// Prix visible en caisse pour cette tarification ? (0/1) =>1
					$ligne	 			   .= "1";
					$ligne	 			   .= ";";
					// Lien vers ARTICLE logiciel (999 999 999)
					$ligne	 			   .= "";
					$ligne	 			   .= ";";
					// Lien vers ARTICLE externe (999 999 999)
					$ligne	 			   .= $id_product;
					$ligne	 			   .= ";";
					// Lien vers TARIFICATION logiciel (999 999 999)
					$ligne	 			   .= "";
					$ligne	 			   .= ";";
					// Lien vers TARIFICATION externe (999 999 999) 
					$ligne	 			   .= $i;
					$ligne	 			   .= ";";
					// Lien vers TVA logiciel (999 999 999)
					$ligne	 			   .= "";
					$ligne	 			   .= ";";
					// Lien vers TVA site internet (999 999 999)
					$ligne	 			   .= $taxe[0]['id_tax'];
					$ligne	 			   .= ";";
					fwrite($f, $ligne."\r\n");
				}
				
				// Prix HT
				$prixHT	 				= $product['shop_product_price'] - ($product['shop_product_price'] * $shop->taux_reduc_emporter / 100);
				// PrixTTC (*2)= Prix TTC de l’article (99 999 999,99)
				$prixTTC 				= $prixHT + ($prixHT * $taxe[0]['rate'] / 100);
			}
		}

		//die();
		
		// Mode règlement ---------------------------------------------------------------------------------------------

		$modules_currency			= Db::getInstance()->executeS('
										SELECT *
										FROM `'._DB_PREFIX_.'module_currency` mc
										JOIN `'._DB_PREFIX_.'module` m ON m.id_module = mc.id_module
										WHERE mc.id_shop = '.$id_shop
						            );

		foreach($modules_currency as $module_currency) {
			if($module_currency['name']!="bankwire" && $module_currency['name']!="cheque")
			{
				$ligne	 					= "MRG";
				$ligne	 				   .= ";";
				$ligne	 				   .= "A";
				$ligne	 				   .= ";";
				// identifiant interne (999 999 999)
				$ligne	 				   .= ";";
				// identifiant externe (999 999 999)
				$ligne	 				   .= $module_currency['id_module'];
				$ligne	 				   .= ";";
				// Nom du mode de règlement (50 caractères)
				if($module_currency['name']=="cardondelivery")
					$ligne				   .= utf8_decode("CB livraison / retrait");
				else
				if($module_currency['name']=="ps_cashondelivery")
					$ligne				   .= utf8_decode("Cash livraison / retrait");
				else
				if($module_currency['name']=="systempay")
					$ligne				   .= "CB (SystemPay)";
				else
				if($module_currency['name']=="payzen")
					$ligne				   .= "CB (PayZen)";
				else
				if($module_currency['name']=="atos")
					$ligne				   .= "CB (Atos)";
				$ligne	 				   .= ";";
				// Abréviation du nom du mode de règlement (4 caractères) 
				$ligne	 				   .= substr(utf8_decode($module_currency['name']), 0, 4);
				$ligne	 				   .= ";";
				// Ouverture du Tiroir caisse ? (0/1) =>0
				if($module_currency['name']=="ps_cashondelivery")
					$ligne				   .= "1";
				else
					$ligne				   .= "0";
				$ligne	 				   .= ";";
				// Doit-on rendre la monnaie ? (0/1) =>0
				$ligne					   .= "1";
				$ligne	 				   .= ";";
				// Activer l'avoir titre resto pour ce règlement ? (0/1)=>0
				$ligne					   .= "0";
				$ligne	 				   .= ";";
				// le mode de règlement à une icône ? (0/1)=>0
				$ligne					   .= "0";
				$ligne	 				   .= ";";
				// Si oui, on devrait trouver un fichier dans le Zip : IMG_MRG_IDREGLEMENT.txt
				// Réglement Fidelité ? (0/1) => 0
				$ligne					   .= "0";
				$ligne	 				   .= ";";
				// Réglement client en compte ? (0/1)=>0
				$ligne					   .= "0";
				$ligne	 				   .= ";";
				// Autorisé un avoir titre resto sur ce mode ? (0/1)=>0 
				$ligne					   .= "0";
				$ligne	 				   .= ";";
				// C'est un règlement Titre resto ? (0/1) => 0 
				$ligne					   .= "0";
				$ligne	 				   .= ";";
				// règlement Avoir sur ticket ? (0/1)=> 0
				$ligne					   .= "0";
				$ligne	 				   .= ";";
				// Mode de règlement Devise ? (0/1)=>0
				$ligne					   .= "0";
				$ligne	 				   .= ";";
				// Mode de règlement carte Pré payée ? (0/1)=>0
				$ligne					   .= "0";
				$ligne	 				   .= ";";
				fwrite($f, $ligne."\r\n");
			}
		}
		
		return $f;
    }

    public function createCashRegisterFile($customer, $order, $id_address_invoice, $id_address_delivery)
    {
	    
		self::logAction("createCashRegisterFile");
	    // Si la date de livraison ou la date à emporter n'est pas aujourd'hui, on ne traite pas la commande -------------------
		$id_cart 					= (int)Db::getInstance()->getValue('
										SELECT `id_cart`
										FROM `'._DB_PREFIX_.'orders`
										WHERE `id_order`='.$order->id
						            );
		$delivery_time 				= Cart::getCartDeliveryHour($id_cart);
	    if(substr($delivery_time, 0, 10)==date("Y-m-d"))
	    	$delivery_date			= date("Ymd");
	    else
	    	$delivery_date 			= str_replace("-", "", substr($delivery_time, 0, 10));
	    	
	    if($delivery_date != date("Ymd") || (int)$order->sent_to_cashsoft != 0) {
			self::logAction("createCashRegisterFile delivery_date pas sur aujourd'hui ou ordre déjà envoyé !");
			self::logAction("Arrêt de la procédure");
	    	return;
	    }
	    
        $context 					= Context::getContext();
		$id_invoice 				= (int)Db::getInstance()->getValue('
										SELECT `id_order_invoice`
										FROM `'._DB_PREFIX_.'order_invoice`
										WHERE `id_order`='.$order->id
						            );

		$id_shop 					= $context->shop->id;
		$shop 						= new Shop($id_shop);
		$shopURL					= new ShopUrl($id_shop);
		$id_solumag 				= $shop->id_solumag;

		$lignesCde 					= OrderDetail::getList($order->id);

		//ob_start();
		
		// Si c'est la première commande alors on envoie tous les produits ------------------------------
		$nb_order	 				= (int)Db::getInstance()->getValue('
										SELECT COUNT(`id_order`) as nb_order
										FROM `'._DB_PREFIX_.'orders`
										WHERE id_shop = '.$id_shop
						            );

		// DANS L’APPLICATION EXTERNE , les données envoyées se présentent de la façon suivante :
		// Dans « DONNEES_date-heure_N°Client_CENTRALE_Site.txt » il y a les données. 
		// Pour chaque image il y a un fichier texte nommé ainsi => « IMG_fichier_IDEXT.txt » Ex : pour une image produit : IMG_ART_10.txt
		// Tous les fichiers texte sont zippés dans un fichier « TRANS_Date-heure_N°Client_CENTRALE_Site.zip ».

		$dirname 					= $_SERVER['DOCUMENT_ROOT']."/solumag";
		if(!is_dir($dirname)) {
			$oldmask 				= umask(0);
			mkdir($_SERVER['DOCUMENT_ROOT']."/solumag", 0777);
			umask($oldmask);
		}
			
		$dirname 					= $_SERVER['DOCUMENT_ROOT']."/solumag/".$shopURL->domain;
		if(!is_dir($dirname)) {
			$oldmask 				= umask(0);
			mkdir($_SERVER['DOCUMENT_ROOT']."/solumag/".$shopURL->domain, 0777);
			umask($oldmask);
		}
			
		$dirname 					= $_SERVER['DOCUMENT_ROOT']."/solumag/".$shopURL->domain."/".$order->id;
		if(!is_dir($dirname)) {
			$oldmask 				= umask(0);
			mkdir($_SERVER['DOCUMENT_ROOT']."/solumag/".$shopURL->domain."/".$order->id, 0777);
			umask($oldmask);
		}
			
		$time						= date("His");
		$site 						= 1;
		$filename 					= $_SERVER['DOCUMENT_ROOT']."/solumag/".$shopURL->domain."/".$order->id."/DONNEES_".date("Ymd")."-".$time."_".$id_solumag."_CENTRALE_$site.txt";
		if(is_file($filename)) {
			$time++;
			$filename 				= $_SERVER['DOCUMENT_ROOT']."/solumag/".$shopURL->domain."/".$order->id."/DONNEES_".date("Ymd")."-".$time."_".$id_solumag."_CENTRALE_$site.txt";
		}
		
		$f 							= fopen($filename, 'w');

		$time 						= date("His");						            
		if($customer->email == "pymercury@gmail.com"
		|| $customer->email == "thierry.santacana@gmail.com"
		|| $customer->email == "thierry@llw.fr") {
			self::logAction("Appel de createProductsCashRegisterFile");
			$this->createProductsCashRegisterFile($f, $customer, $order, $id_address_delivery, $id_address_delivery);
		}

		self::logAction("Création fichier ".$filename);

		// Client -------------------------------------------------------------------------------------------------------

		$address 					= Db::getInstance()->executeS('
										SELECT *
										FROM `'._DB_PREFIX_.'address` a
										WHERE a.id_address = '.$order->id_address_delivery
						            );
		if(strlen($address[0]['phone'])==9)
			$address[0]['phone']		= "0".$address[0]['phone'];
		if(strlen($address[0]['phone_mobile'])==9)
			$address[0]['phone_mobile']	= "0".$address[0]['phone_mobile'];

		$longAdresse				= stripslashes($address[0]['address1']);
		$longAdresse			   .= ' '.$address[0]['postcode'].' '.stripslashes($address[0]['city']);
		$longAdresse				= str_replace("  ", " ", $longAdresse);
		$longAdresse				= str_replace("‘", "'", $longAdresse);
		$longAdresse				= str_replace("’", "'", $longAdresse);
		$longAdresse 				= strtr($longAdresse, 	'ÁÀÂÄÃÅÇÉÈÊËÍÏÎÌÑÓÒÔÖÕÚÙÛÜÝ', 
															'AAAAAACEEEEIIIINOOOOOUUUUY');
		$longAdresse 				= strtr($longAdresse, 	'áàâäãåçéèêëíìîïñóòôöõúùûüýÿ', 
															'aaaaaaceeeeiiiinooooouuuuyy');
		//echo $longAdresse."<br>";
		// 0: Complete address, 1: Number + BIS, TER or QUATER, 2: Number, 3 et 4: BIS, TER or QUATER, 5 à 9: Nothing, 10: Street, 11: Nothing, 12: Postal code, 13: City
		preg_match("/((^[0-9]*).?((BIS)|(TER)|(QUATER))?)?((\W+)|(^))(([a-z,A-Z]+.)*)([0-9]{5})?.(([a-z,A-Z\'']+.)*)$/", $longAdresse, $matches);
		//print_r($matches);
		$numRue						= $matches[1];
		$typeVoie					= "*";
		$rue						= $matches[10];
		$codPTT						= $matches[12];
		$ville						= $matches[13];
		if($address[0]['address2'])
			$specif   				= substr($address[0]['address2'], 0, 100);

		self::logAction("Ligne CLI");

		$ligne	 					= "CLI";
		$ligne	 				   .= ";";
		$ligne	 				   .= "A";
		$ligne	 				   .= ";";
		// identifiant interne (999 999 999)
		$ligne	 				   .= ";";
		// identifiant externe (999 999 999)
		$ligne	 				   .= $customer->id;
		$ligne	 				   .= ";";
		// Nom client ou société (40 caractères) 
		$ligne	 				   .= utf8_decode($customer->lastname);
		$ligne	 				   .= ";";
		// Prénom client (40 caractères)
		$ligne	 				   .= utf8_decode($customer->firstname);
		$ligne	 				   .= ";";
		// C’est une société ? (0/1)
		if($customer->company)
			$ligne				   .= "1";
		else
			$ligne				   .= "0";
		$ligne	 				   .= ";";
		// Numéro carte client (CL+8 chiffres) 
		$ligne	 				   .= ";";
		// Tel 1 (15 chiffres maxi)
		$ligne	 				   .= $address[0]['phone'];
		$ligne	 				   .= ";";
		// Tel 2 (15 chiffres maxi)
		$ligne	 				   .= $address[0]['phone_mobile'];
		$ligne	 				   .= ";";
		// Tel 3 (15 chiffres maxi)
		$ligne	 				   .= ";";
		// Email (250 caractères)
		$ligne	 				   .= $customer->email;
		$ligne	 				   .= ";";
		// Numéro de la rue (4 caractères) 
		$ligne	 				   .= '';
		$ligne	 				   .= ";";
		// Etage (99)
		$ligne	 				   .= ";";
		// N° porte (10 caractères)
		$ligne	 				   .= ";";
		// Adresse ligne 1 (mémo)
		$ligne	 				   .= utf8_decode($address[0]['address1']);
		$ligne	 				   .= ";";
		// Adresse ligne 2,3,4... (mémo)
		$ligne	 				   .= utf8_decode($address[0]['address2']);
		$ligne	 				   .= ";";
		// Code postal (5 caractères)
		$ligne	 				   .= $codPTT;
		$ligne	 				   .= ";";
		// Ville client (50 caractères)
		$ligne	 				   .= $ville;
		$ligne	 				   .= ";";
		// Zone de proximité magasin 1=proche/2=moyen/3=loin (99) 
		$ligne	 				   .= "1";
		$ligne	 				   .= ";";
		// Coordonnées géographique (10 caractères)
		$ligne	 				   .= "";
		$ligne	 				   .= ";";
		// Création du client (AAAAMMJJ)
		$ligne	 				   .= substr($customer->date_add, 0, 4).substr($customer->date_add, 5, 2).substr($customer->date_add, 8, 2);
		$ligne	 				   .= ";";
		// Commentaire personnel sur client (mémo) 
		$ligne	 				   .= "";
		$ligne	 				   .= ";";
		// Commentaire sur ticket livraison (mémo)
		$ligne	 				   .= "";
		$ligne	 				   .= ";";
		// Ne pas appliquer la fidélité pour ce client ? (0/1) 
		$ligne	 				   .= "1";
		$ligne	 				   .= ";";
		// Remise autorisée pour ce client (±99,99) Ex : 0,10 pour 10%
		$ligne	 				   .= "";
		$ligne	 				   .= ";";
		// Numéro de siret de la société (14 caractères)
		$ligne	 				   .= $customer->siret;
		$ligne	 				   .= ";";
		// 0 = le client peut régler <en compte> / 1 = le client ne peut pas
		$ligne	 				   .= "1";
		$ligne	 				   .= ";";
		// CA du client depuis son inscription (±999999999,99) (pas
		$ligne	 				   .= "0";
		$ligne	 				   .= ";";
		// Solde du EN COMPTE (±999999999,99) pas importés
		$ligne	 				   .= "0";
		$ligne	 				   .= ";";
		// Solde de la carte pré-payée (±999999999,99) pas importés 
		$ligne	 				   .= "0";
		$ligne	 				   .= ";";
		// Solde des avoirs tickets (±999999999,99) pas importés 
		$ligne	 				   .= "0";
		$ligne	 				   .= ";";
		// Nombre de point fidélité (±999999999) pas importés 
		$ligne	 				   .= "0";
		$ligne	 				   .= ";";
		// Valeur des points de fidélité (±999999999,99) pas importés
		$ligne	 				   .= "0";
		$ligne	 				   .= ";";
		// Anniversaire du client (aaaammjj)
		$ligne	 				   .= substr($customer->birthday, 0, 4).substr($customer->birthday, 5, 2).substr($customer->birthday, 8, 2);
		$ligne	 				   .= ";";
		// Type du client (1=homme,2=femme,3=enfant)
		$ligne	 				   .= $customer->id_gender;
		$ligne	 				   .= ";";
		fwrite($f, $ligne."\r\n");

		// Commande -------------------------------------------------------------------------------------------------------

		self::logAction("Ligne TIC");

		$ligne	 					= "TIC";
		$ligne	 				   .= ";";
		$ligne	 				   .= "A";
		$ligne	 				   .= ";";
		// identifiant interne (999 999 999)
		$ligne	 				   .= ";";
		// identifiant externe (999 999 999)
		$ligne	 				   .= $order->id;
		$ligne	 				   .= ";";
		// Date création du ticket (AAAAMMJJ)
		$ligne	 				   .= substr($order->date_add, 0, 4).substr($order->date_add, 5, 2).substr($order->date_add, 8, 2);
		$ligne	 				   .= ";";
		// Heure création du ticket (hhmmss)
		$ligne	 				   .= substr($order->date_add, 11, 2).substr($order->date_add, 14, 2).substr($order->date_add, 17, 2);
		$ligne	 				   .= ";";
		// total tva du ticket (999 999 999,999999)
		$ligne	 				   .= $order->total_paid_tax_incl - $order->total_paid_tax_excl;
		$ligne	 				   .= ";";
		// total ttc du ticket (999 999 999,999999)
		$ligne	 				   .= $order->total_paid_tax_incl;
		$ligne	 				   .= ";";
		// Nombre d’articles vendus (999 999 999)
		$ligne	 				   .= count($lignesCde);
		$ligne	 				   .= ";";
		
		// Date + Heure souhaitée pour la commande Date+heure
		$dateLivr 					= str_replace(["-", ":", " "], "", $delivery_time);
		$ligne	 				   .= $dateLivr;
		$ligne	 				   .= ";";

		// Information à imprimer sur ticket (mémo) 
        $cart 						= new Cart($id_cart);

		$particularite				= "Mode règlement : ".$order->module."/RC";
		if($order->to_be_delivered)
			$particularite		   .= " - Commande à livrer ".$order->delivery_when."/RC";
		else
			$particularite		   .= " - Commande à emporter ".$order->delivery_when."/RC";

		$particularite			   .= " - Allergènes : ";
		$elements 					= $cart->getUnwantedEements($id_cart);
		$excluded_feature 			= Configuration::get("LLW_KYO_EXCLUDE_FEATURE");
		$feature_values 			= FeatureValue::getFeatureValuesWithLang($context->language->id, $excluded_feature);
		$unwanted					= [];
        foreach($feature_values as $feature_value) {
			if(in_array($feature_value['id_feature_value'], $elements)) {
				$unwanted[] 		= $feature_value['value'];
			}
        }
		if(count($unwanted)) {
			foreach($unwanted as $unwant) {
				$particularite	   .= $unwant.", ";
			}
			$particularite			= rtrim($particularite, ", ")."/RC";
		}	
		else
			$particularite		   .= "aucun"."/RC";


		$compl_products 			= $cart->getCartComplements($id_cart);
		$particularite			   .= " - Produits complémentaires : ";
		$particularite			   .= " Baguettes = ".$compl_products["nb_baguettes"];
		$particularite			   .= " Sauces salees = ".$compl_products["nb_sauce_salee"];
		$particularite			   .= " Sauces sucrees = ".$compl_products["nb_sauce_sucree"];
		$particularite			   .= " Gingembre = ".$compl_products["nb_gingembre"];
		$particularite			   .= " Wasabis = ".$compl_products["nb_wasabi"];
		$particularite			   .= "/RC";

		$particularite			   .= " - Message : ";
		$message 					= Message::getMessageByCartId($id_cart);
		if($message && $message['message'] && strpos($message['message'], "Transaction ID")===false) {
			$cartMsg 				= str_replace(array("\r", "\n"), "-", $message['message']);
			$cartMsg 				= str_replace("--", " - ", $cartMsg);
			$particularite		   .= $cartMsg."/RC";
		}
		else
			$particularite		   .= "aucun"."/RC";

		if($order->total_shipping_tax_incl) {
			$particularite		   .= "Frais de livraison : ".$order->total_shipping_tax_incl."/RC";
		}
		if(isset($address[0]['company']) && $address[0]['company']) {
			$particularite		   .= "Société : ".$address[0]['company']."/RC";
		}
			
		$msg	 				   	= nl2br(utf8_decode($particularite), false);
		$msg	 				   	= str_replace("<br>", "/RC", $msg);
		$ligne	 				   .= str_replace(";", "/PV", $msg);
		$ligne	 				   .= ";";
		// Info à imprimer sur ticket à payer ? (0/1) 
		$ligne	 				   .= "1";
		$ligne	 				   .= ";";
		// Info à imprimer sur ticket fabrication ? (0/1) 
		$ligne	 				   .= "0";
		$ligne	 				   .= ";";
		// Info à imprimer sur ticket réglé ? (0/1)
		$ligne	 				   .= "0";
		$ligne	 				   .= ";";
		// Le ticket a déjà été réglé (0/1) => 0
		if($order->module=="systempay" || $order->module=="atos" || $order->module=="payzen")
			$ligne				   .= "1";
		else
			$ligne				   .= "0";
		$ligne	 				   .= ";";
		// Lien vers tarification pour le ticket logiciel (999 999 999)
		$ligne	 				   .= "";
		$ligne	 				   .= ";";
		// Lien vers tarification externe (999 999 999) 
		if($order->to_be_delivered==1)
			$ligne				   .= "3";
		else
			$ligne				   .= "2";
		$ligne	 				   .= ";";
		// IDCLIENT (*1)= Lien vers client logiciel (999 999 999) 
		$ligne	 				   .= "";
		$ligne	 				   .= ";";
		// IDCLIENT_EXT (*2)= Lien vers client externe (999 999 999)
		$ligne	 				   .= $customer->id;
		$ligne	 				   .= ";";
		fwrite($f, $ligne."\r\n");

		// Lignes commande -------------------------------------------------------------------------------------
		
		self::logAction("Ligne LTI");

		foreach($lignesCde as $detail) {
			$id_product					= $detail['product_id'];
			$produit 					= new Product($id_product, true, $order->id_lang, $order->id_shop);

			$prixTTC					= $detail['total_price_tax_incl'];
			self::logAction("Ligne LTI - prixTTC pour ID produit ".$id_product." = ".$prixTTC);

			$ligne	 					= "LTI";
			$ligne	 				   .= ";";
			$ligne	 				   .= "A";
			$ligne	 				   .= ";";
			// identifiant interne (999 999 999)
			$ligne	 				   .= ";";
			// identifiant externe (999 999 999)
			$ligne	 				   .= $detail['id_order_detail'];
			$ligne	 				   .= ";";
			// Lien vers article ARTICLE logiciel
			$ligne	 				   .= "";
			$ligne	 				   .= ";";
			// Lien vers article ARTICLE externe
			$ligne	 				   .= $detail['product_id'];
			$ligne	 				   .= ";";
			// Taux_tva d'une ligne (99,99)
			$txTVA 						= ($detail['unit_price_tax_incl'] - $detail['unit_price_tax_excl']) / $detail['unit_price_tax_excl'] * 100;
			$ligne	 				   .= round($txTVA, 2);
			$ligne	 				   .= ";";
			// Prix TTC de l'article/remise (±9 999 999,99)
			$ligne	 				   .= $prixTTC;
			$ligne	 				   .= ";";
			// Niveau (a suivre) dans ligne du ticket (9999) =>1 par défaut 
			$ligne	 				   .= "1";
			$ligne	 				   .= ";";
			// type de la ligne (99) => 1 par défaut:
			$ligne	 				   .= "1";
			$ligne	 				   .= ";";
			// IDArticle de l’article de référence logiciel (999 999 999)
			$ligne	 				   .= "";
			$ligne	 				   .= ";";
			// IDArticle de l’article de référence externe (999 999 999)
			$ligne	 				   .= "";
			$ligne	 				   .= ";";
			// IDLigne de l’article de référence du logiciel (999 999 999)
			$ligne	 				   .= "";
			$ligne	 				   .= ";";
			// IDLigne de l’article de référence du site (999 999 999)
			$ligne	 				   .= "";
			$ligne	 				   .= ";";
			// l'article est-il cumulable ? (0/1) => 1
			$ligne	 				   .= "1";
			$ligne	 				   .= ";";
			// Envoyé en fabrication ? (0/1) =>0
			$ligne	 				   .= "0";
			$ligne	 				   .= ";";
			// l'article a déjà été mis en attente ? (0/1) =>0 
			$ligne	 				   .= "0";
			$ligne	 				   .= ";";
			// Qte de l'unité de mesure (999 999,99) => 1
			$ligne	 				   .= $detail['product_quantity'];
			$ligne	 				   .= ";";
			// Montant déjà réglé pour la ligne (9 999 999,99) 
			if($order->module=="systempay" || $order->module=="atos" || $order->module=="payzen")
				$ligne 				   .= $prixTTC;
			else
				$ligne 				   .= "0";
			$ligne	 				   .= ";";
			// Ratio tva multiple (99999999,999999) => 1,00
			$ligne	 				   .= "1";
			$ligne	 				   .= ";";
			// Libelle libre pour article (30 caractères)
			$ligne	 				   .= "";
			$ligne	 				   .= ";";
			// Article offert (0/1) => 0
			$ligne	 				   .= "0";
			$ligne	 				   .= ";";
			// Lien vers TICKET logiciel (999 999 999) 
			$ligne	 				   .= "";
			$ligne	 				   .= ";";
			// Lien vers TICKET externe (999 999 999) 
			$ligne	 				   .= $order->id;
			$ligne	 				   .= ";";
			// Lien vers cuisson logiciel (999 999 999) 
			$ligne	 				   .= "0";
			$ligne	 				   .= ";";
			// Lien vers cuisson externe (999 999 999)
			$ligne	 				   .= "0";
			$ligne	 				   .= ";";
			// Lien vers Table évènement (999 999 999 999 999 999)
			$ligne	 				   .= "0";
			$ligne	 				   .= ";";
			// Lien vers table billetterie (999 999 999 999 999 999)
			$ligne	 				   .= "0";
			$ligne	 				   .= ";";
			// Lien vers table billetterie externe (999 999 999 999 999 999)
			$ligne	 				   .= "0";
			$ligne	 				   .= ";";
			fwrite($f, $ligne."\r\n");
		}

		if($order->total_discounts_tax_incl > 0) {

			//LTI;A;;;0;0;10.14;-0.76;999;6;;;;;0;0;0;1;0;1;;0;;204;0;0;0;0;0;

			$ligne	 					= "LTI";
			$ligne	 				   .= ";";
			$ligne	 				   .= "A";
			$ligne	 				   .= ";";
			// identifiant interne (999 999 999)
			$ligne	 				   .= ";";
			// identifiant externe (999 999 999)
			$ligne	 				   .= (int)$order->id + 1000000;
			$ligne	 				   .= ";";
			// Lien vers article ARTICLE logiciel
			$ligne	 				   .= "0";
			$ligne	 				   .= ";";
			// Lien vers article ARTICLE externe
			$ligne	 				   .= "0";
			$ligne	 				   .= ";";
			// Taux_tva d'une ligne (99,99)
			$txTVA 						= ($order->total_discounts_tax_incl - $order->total_discounts_tax_excl) / $order->total_discounts_tax_excl * 100;
			$ligne	 				   .= round($txTVA, 0);
			$ligne	 				   .= ";";
			// Prix TTC de l'article/remise (±9 999 999,99)
			$ligne	 				   .= $order->total_discounts_tax_incl * -1;
			$ligne	 				   .= ";";
			// Niveau (a suivre) dans ligne du ticket (9999) =>1 par défaut 
			$ligne	 				   .= "999";
			$ligne	 				   .= ";";
			// type de la ligne (99) => 1 par défaut:
			$ligne	 				   .= "6";
			$ligne	 				   .= ";";
			// IDArticle de l’article de référence logiciel (999 999 999)
			$ligne	 				   .= "";
			$ligne	 				   .= ";";
			// IDArticle de l’article de référence externe (999 999 999)
			$ligne	 				   .= "";
			$ligne	 				   .= ";";
			// IDLigne de l’article de référence du logiciel (999 999 999)
			$ligne	 				   .= "";
			$ligne	 				   .= ";";
			// IDLigne de l’article de référence du site (999 999 999)
			$ligne	 				   .= "";
			$ligne	 				   .= ";";
			// l'article est-il cumulable ? (0/1) => 1
			$ligne	 				   .= "0";
			$ligne	 				   .= ";";
			// Envoyé en fabrication ? (0/1) =>0
			$ligne	 				   .= "0";
			$ligne	 				   .= ";";
			// l'article a déjà été mis en attente ? (0/1) =>0 
			$ligne	 				   .= "0";
			$ligne	 				   .= ";";
			// Qte de l'unité de mesure (999 999,99) => 1
			$ligne	 				   .= "1";
			$ligne	 				   .= ";";
			// Montant déjà réglé pour la ligne (9 999 999,99) 
			$ligne 					   .= "0";
			$ligne	 				   .= ";";
			// Ratio tva multiple (99999999,999999) => 1,00
			$ligne	 				   .= "1";
			$ligne	 				   .= ";";
			// Libelle libre pour article (30 caractères)
			$ligne	 				   .= "";
			$ligne	 				   .= ";";
			// Article offert (0/1) => 0
			$ligne	 				   .= "0";
			$ligne	 				   .= ";";
			// Lien vers TICKET logiciel (999 999 999) 
			$ligne	 				   .= "";
			$ligne	 				   .= ";";
			// Lien vers TICKET externe (999 999 999) 
			$ligne	 				   .= $order->id;
			$ligne	 				   .= ";";
			// Lien vers cuisson logiciel (999 999 999) 
			$ligne	 				   .= "0";
			$ligne	 				   .= ";";
			// Lien vers cuisson externe (999 999 999)
			$ligne	 				   .= "0";
			$ligne	 				   .= ";";
			// Lien vers Table évènement (999 999 999 999 999 999)
			$ligne	 				   .= "0";
			$ligne	 				   .= ";";
			// Lien vers table billetterie (999 999 999 999 999 999)
			$ligne	 				   .= "0";
			$ligne	 				   .= ";";
			// Lien vers table billetterie externe (999 999 999 999 999 999)
			$ligne	 				   .= "0";
			$ligne	 				   .= ";";
			fwrite($f, $ligne."\r\n");
		}

		// Mode règlement ---------------------------------------------------------------------------------------------

		self::logAction("Ligne RGT");

		if($order->module=="systempay" || $order->module=="atos" || $order->module=="payzen") {
			$payments 					= Db::getInstance()->executeS('
											SELECT *
											FROM `'._DB_PREFIX_.'order_payment` op
											WHERE op.order_reference = "'.$order->reference.'"'
							            );
			$module_currency			= Db::getInstance()->executeS('
											SELECT *
											FROM `'._DB_PREFIX_.'module` m
											JOIN `'._DB_PREFIX_.'module_currency` mc ON mc.id_module = m.id_module AND mc.id_shop = '.$id_shop.'
											WHERE m.name = "'.$order->module.'"'
							            );
			// RGT;A;;;;;â‚¬;1;162;0;;
            foreach($payments as $payment) {
    			$ligne	 					= "RGT";
    			$ligne	 				   .= ";";
    			$ligne	 				   .= "A";
    			$ligne	 				   .= ";";
    			// identifiant interne (999 999 999)
    			$ligne	 				   .= ";";
    			// identifiant externe (999 999 999)
    			$ligne	 				   .= $payment['id_order_payment'];
    			$ligne	 				   .= ";";
    			// Montant TTC du règlement
    			$ligne					   .= $payment['amount'];
    			$ligne	 				   .= ";";
    			// Montant TTC en devise
    			$ligne					   .= $payment['amount'];
    			$ligne	 				   .= ";";
    			// Code de la devise
    			// utf8_decode('€')
    			$ligne					   .= "EUR";
    			$ligne	 				   .= ";";
    			// Lien vers Ticket logiciel
    			$ligne					   .= "1";
    			$ligne	 				   .= ";";
    			// Lien vers Ticket externe
    			$ligne					   .= $order->id;
    			$ligne	 				   .= ";";
    			// Lien vers mode règlement logiciel
    			$ligne					   .= "0";
    			$ligne	 				   .= ";";
    			// Lien vers mode règlement externe
    			$ligne					   .= $module_currency[0]['id_module'];
    			$ligne	 				   .= ";";
    
    			fwrite($f, $ligne."\r\n");
            }
		}
		
		fclose($f);
		
		// Construction du zip --------------------------------------------------------------------------
        if (class_exists('ZipArchive', false)) {
			$zip 						= new ZipArchive();
		}
		else {
			die("La classe ZipArchive n'existe pas !!!");
		}
		$zipFilePath					= $_SERVER['DOCUMENT_ROOT']."/solumag/".$shopURL->domain."/TRANS_".date("Ymd")."-".$time."_".$id_solumag."_CENTRALE_$site.zip";
		self::logAction("Construction fichier zip ".$zipFilePath);
		if(file_exists($zipFilePath)) {
			$zip_ok 					= false;
			while (!$zip_ok) {
				$time++;
				$zipFilePath			= $_SERVER['DOCUMENT_ROOT']."/solumag/".$shopURL->domain."/TRANS_".date("Ymd")."-".$time."_".$id_solumag."_CENTRALE_$site.zip";
				self::logAction("Construction fichier zip ".$zipFilePath);
				if(!file_exists($zipFilePath)) {
					$zip_ok				= true;
				}
			}
		}
		//echo "Construction fichier zip ".$zipFilePath."<br>";
		if($zip->open($zipFilePath, ZIPARCHIVE::CREATE) === TRUE) {
			$srcDir 					= $_SERVER['DOCUMENT_ROOT']."/solumag/".$shopURL->domain."/".$order->id."/";
			$files						= scandir($srcDir);
			foreach ($files as $file) {
				$basename 				= basename($file);
				//echo "basename = '$basename'<br>";
				if(substr($basename, 0, 1) != ".") {
					//echo "Ajout du fichier ".$srcDir.$file."<br>";
					self::logAction("Ajout du fichier ".$srcDir.$file);
					if(!$zip->addFile($srcDir.$file, $basename)) 
						die ("ERROR: Could not add file: $srcDir.$file to zip archive $zipFilePath !!!");    
					self::logAction("Fichier ".$srcDir.$file." ajouté !");
				}
			}
			$zip->close();
			foreach ($files as $file) {
				$basename 				= basename($file);
				if(substr($basename, 0, 1) != ".") {
					self::logAction("Suppression du fichier ".$srcDir.$file);
					unlink($srcDir.$file);    
				}
			}
		}
		else {
			self::logAction("*** Could not open archive");
			die();
		}

		// Suppression du sous-répertoire ---------------------------------------------------------------
		$dirPath						= $_SERVER['DOCUMENT_ROOT']."/solumag/".$shopURL->domain."/".$order->id."/";
		self::logAction("Suppression répertoire ".$dirPath);
        rmdir($dirPath); 
		self::logAction("Répertoire ".$dirPath." supprimé !");

		// Mise à jour de la date de livraison/à emporter --------------------------------------------------
		$delivery_time = Db::getInstance()->getValue(
			'
			SELECT `delivery_time` 
			FROM `' . _DB_PREFIX_ . 'cart_delivery`
			WHERE `id_cart` = ' . $id_cart
		);
		$order->delivery_when			= str_replace("Aujourd'hui", "Le ".substr($delivery_time, 8, 2)."/".substr($delivery_time, 5, 2)."/".substr($delivery_time, 0, 4), $order->delivery_when);
        $sql 							= '
			UPDATE `'._DB_PREFIX_.'orders`
			SET `delivery_when` = "'.$order->delivery_when.'", `sent_to_cashsoft` = 1
			WHERE `id_order` = '.(int)$order->id;
        Db::getInstance()->execute($sql);
		
		// Output ---------------------------------------------------------------------------------------
		//$output 						= ob_get_clean();
		//self::logAction("***** Résultat de l'opération : ".$output);
		//die();
    }
    
    public static function getCompleteListOfOrdersToSend($id_shop, $date=null) {
	    
		if($date)
		    $delivery_when 			= "Le ".str_replace("-", "/", $date)."%";
		else
		    $delivery_when 			= "Le ".date("d/m/Y")."%";
		$sql 						= '
										SELECT *
										FROM `'._DB_PREFIX_.'orders`
										WHERE id_shop = '.$id_shop.'
										AND sent_to_cashsoft = 0
										AND delivery_when LIKE "'.$delivery_when.'"'
						            ;
		echo $sql;
		$orders 					= Db::getInstance()->executeS($sql);
		return $orders;
    }
}
?>
