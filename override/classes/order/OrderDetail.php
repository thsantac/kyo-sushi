<?php
/**
 * Copyright since 2007 PrestaShop SA and Contributors
 * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.md.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/OSL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://devdocs.prestashop.com/ for more information.
 *
 * @author    PrestaShop SA and Contributors <contact@prestashop.com>
 * @copyright Since 2007 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
 */
class OrderDetail extends OrderDetailCore
{

    /**
     * Set detailed product price to the order detail.
     *
     * @param object $order
     * @param object $cart
     * @param array $product
     */
    protected function setDetailProductPrice(Order $order, Cart $cart, $product)
    {
        $this->setContext((int) $product['id_shop']);

        if($cart->delivery_mode==1) {
        	$order->to_be_delivered 		= 1;
        	$this->context->delivery_mode 	= 1;
        }
        else {
        	$order->to_be_delivered 		= 0;
        	$this->context->delivery_mode 	= 2;
        }

        Product::getPriceStatic((int) $product['id_product'], true, (int) $product['id_product_attribute'], 6, null, false, true, $product['cart_quantity'], false, (int) $order->id_customer, (int) $order->id_cart, (int) $order->{Configuration::get('PS_TAX_ADDRESS_TYPE')}, $specific_price, true, true, $this->context, true, null, $order->to_be_delivered);
        $this->specificPrice = $specific_price;

        $this->original_product_price = Product::getPriceStatic($product['id_product'], false, (int) $product['id_product_attribute'], 6, null, false, false, 1, false, null, null, null, $null, true, true, $this->context, true, null, $order->to_be_delivered);
        $this->product_price = $this->original_product_price;
        $this->unit_price_tax_incl = (float) $product['price_wt'];
        $this->unit_price_tax_excl = (float) $product['price'];
        $this->total_price_tax_incl = (float) $product['total_wt'];
        $this->total_price_tax_excl = (float) $product['total'];

        $this->purchase_supplier_price = (float) $product['wholesale_price'];
        if ($product['id_supplier'] > 0 && ($supplier_price = ProductSupplier::getProductPrice((int) $product['id_supplier'], $product['id_product'], $product['id_product_attribute'], true)) > 0) {
            $this->purchase_supplier_price = (float) $supplier_price;
        }

        $this->setSpecificPrice($order, $product);

        $this->group_reduction = (float) Group::getReduction((int) $order->id_customer);

        $shop_id = $this->context->shop->id;

        $quantity_discount = SpecificPrice::getQuantityDiscount(
            (int) $product['id_product'],
            $shop_id,
            (int) $cart->id_currency,
            (int) $this->vat_address->id_country,
            (int) $this->customer->id_default_group,
            (int) $product['cart_quantity'],
            false,
            null,
            null,
            $null,
            true,
            true,
            $this->context
        );

        $unit_price = Product::getPriceStatic(
            (int) $product['id_product'],
            true,
            ($product['id_product_attribute'] ? (int) ($product['id_product_attribute']) : null),
            2,
            null,
            false,
            true,
            1,
            false,
            (int) $order->id_customer,
            null,
            (int) $order->{Configuration::get('PS_TAX_ADDRESS_TYPE')},
            $null,
            true,
            true,
            $this->context,
            true, 
            null, 
            $order->to_be_delivered
        );
        $this->product_quantity_discount = 0.00;
        if ($quantity_discount) {
            $this->product_quantity_discount = $unit_price;
            if (Product::getTaxCalculationMethod((int) $order->id_customer) == PS_TAX_EXC) {
                $this->product_quantity_discount = Tools::ps_round($unit_price, Context::getContext()->getComputingPrecision());
            }

            if (isset($this->tax_calculator)) {
                $this->product_quantity_discount -= $this->tax_calculator->addTaxes($quantity_discount['price']);
            }
        }

        $this->discount_quantity_applied = (($this->specificPrice && $this->specificPrice['from_quantity'] > 1) ? 1 : 0);
    }

    /**
     * Create an order detail liable to an id_order.
     *
     * @param object $order
     * @param object $cart
     * @param array $product
     * @param int $id_order_status
     * @param int $id_order_invoice
     * @param bool $use_taxes set to false if you don't want to use taxes
     */
    protected function create(Order $order, Cart $cart, $product, $id_order_state, $id_order_invoice, $use_taxes = true, $id_warehouse = 0)
    {
        if ($use_taxes) {
            $this->tax_calculator = new TaxCalculator();
        }

        $this->id = null;

        $this->product_id = (int) $product['id_product'];
        $this->product_attribute_id = $product['id_product_attribute'] ? (int) $product['id_product_attribute'] : 0;
        $this->id_customization = $product['id_customization'] ? (int) $product['id_customization'] : 0;

		// #TS -------------------------------------------------------------------------------
        $this->product_name = $product['name'] .
            ((isset($product['attributes_small']) && $product['attributes_small'] != null) ?
                ' - ' . $product['attributes_small'] : '');

        $this->product_quantity = (int) $product['cart_quantity'];
        $this->product_ean13 = empty($product['ean13']) ? null : pSQL($product['ean13']);
        $this->product_isbn = empty($product['isbn']) ? null : pSQL($product['isbn']);
        $this->product_upc = empty($product['upc']) ? null : pSQL($product['upc']);
        $this->product_mpn = empty($product['mpn']) ? null : pSQL($product['mpn']);
        $this->product_reference = empty($product['reference']) ? null : pSQL($product['reference']);
        $this->product_supplier_reference = empty($product['supplier_reference']) ? null : pSQL($product['supplier_reference']);
        $this->product_weight = $product['id_product_attribute'] ? (float) $product['weight_attribute'] : (float) $product['weight'];
        $this->id_warehouse = $id_warehouse;

        $product_quantity = (int) Product::getQuantity($this->product_id, $this->product_attribute_id, null, $cart);
        $this->product_quantity_in_stock = ($product_quantity - (int) $product['cart_quantity'] < 0) ?
            $product_quantity : (int) $product['cart_quantity'];

        $this->setVirtualProductInformation($product);
        $this->checkProductStock($product, $id_order_state);

        if ($use_taxes) {
            $this->setProductTax($order, $product);
        }
        $this->setShippingCost($order, $product);
        $this->setDetailProductPrice($order, $cart, $product);

        // Set order invoice id
        $this->id_order_invoice = (int) $id_order_invoice;

        // Set shop id
        $this->id_shop = (int) $product['id_shop'];

        // Add new entry to the table
        $this->save();

        if ($use_taxes) {
            $this->saveTaxCalculator($order);
        }
        unset($this->tax_calculator);
    }

}
