<?php
/**
 * 2007-2019 PrestaShop and Contributors
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/OSL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2019 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
 * International Registered Trademark & Property of PrestaShop SA
 */

/**
 * Class AddressFormatCore.
 */
class AddressFormat extends AddressFormatCore
{
    /** @var array Default required form fields list */
    public static $requireFormFieldsList = array(
        'firstname',
        'lastname',
        'address1',
        'city',
        'Country:name',
    );

    /**
     * Returns address format fields in array by country.
     *
     * @param int $idCountry If null using PS_COUNTRY_DEFAULT
     * @param bool $splitAll
     * @param bool $cleaned
     *
     * @return array String field address format
     */
    public static function getOrderedAddressFields($idCountry = 0, $splitAll = false, $cleaned = false)
    {
        $out = array();
        $fieldSet = explode(AddressFormat::FORMAT_NEW_LINE, AddressFormat::getAddressCountryFormat($idCountry));
        foreach ($fieldSet as $fieldItem) {
	        if($fieldItem == "address1") {
		        $out[]	= "google_address";
		        $out[]	= "location_position";
	        }
	        if($fieldItem == "Country:name") {
				$context = Context::getContext();
		        if(!$context->shop->frais_par_zone) {
			        $out[]	= "Secteur:secteur";
		        }
	        }
            if ($splitAll) {
                if ($cleaned) {
                    $keyList = ($cleaned) ? preg_split(self::_CLEANING_REGEX_, $fieldItem, -1, PREG_SPLIT_NO_EMPTY) :
                        explode(' ', $fieldItem);
                }
                foreach ($keyList as $wordItem) {
                    $out[] = trim($wordItem);
                }
            } else {
                $out[] = ($cleaned) ? implode(' ', preg_split(self::_CLEANING_REGEX_, trim($fieldItem), -1, PREG_SPLIT_NO_EMPTY))
                    : trim($fieldItem);
            }
        }

        return $out;
    }

    /**
     * Returns the formatted fields with associated values.
     *
     * @param Address $address Address object
     * @param AddressFormat $addressFormat The format
     *
     * @return array
     */
    public static function getFormattedAddressFieldsValues($address, $addressFormat, $id_lang = null)
    {
        if (!$id_lang) {
            $id_lang = Context::getContext()->language->id;
        }
        $tab = [];
        $temporyObject = [];

        // Check if $address exist and it's an instanciate object of Address
        if ($address && ($address instanceof Address)) {
            foreach ($addressFormat as $line) {
                if (($keyList = preg_split(self::_CLEANING_REGEX_, $line, -1, PREG_SPLIT_NO_EMPTY)) && is_array($keyList)) {
                    foreach ($keyList as $pattern) {
                        if ($associateName = explode(':', $pattern)) {
                            $totalName = count($associateName);
                            if ($totalName == 1 && isset($address->{$associateName[0]})) {
                                $tab[$associateName[0]] = $address->{$associateName[0]};
                            } else {
                                $tab[$pattern] = '';

                                // Check if the property exist in both classes
                                if (($totalName == 2) && class_exists($associateName[0]) &&
                                    property_exists($associateName[0], $associateName[1]) &&
                                    property_exists($address, 'id_' . strtolower($associateName[0]))) {
                                    $idFieldName = 'id_' . strtolower($associateName[0]);

                                    if (!isset($temporyObject[$associateName[0]])) {
                                        $temporyObject[$associateName[0]] = new $associateName[0]($address->{$idFieldName});
                                    }
                                    if ($temporyObject[$associateName[0]]) {
                                        $tab[$pattern] = (is_array($temporyObject[$associateName[0]]->{$associateName[1]})) ?
                                            ((isset($temporyObject[$associateName[0]]->{$associateName[1]}[$id_lang])) ?
                                            $temporyObject[$associateName[0]]->{$associateName[1]}[$id_lang] : '') :
                                            $temporyObject[$associateName[0]]->{$associateName[1]};
                                    }
                                }

                                if (($totalName == 2) && $associateName[0]=="Secteur") {
	                                $tab[$pattern] = $address->{$associateName[1]};
                                }
                            }
                        }
                    }
                    AddressFormat::_setOriginalDisplayFormat($tab, $line, $keyList);
                }
            }
        }
        AddressFormat::cleanOrderedAddress($addressFormat);
        // Free the instanciate objects
        foreach ($temporyObject as &$object) {
            unset($object);
        }

        return $tab;
    }

    /**
     * Generates the full address text.
     *
     * @param Address $address
     * @param array $patternRules A defined rules array to avoid some pattern
     * @param string $newLine A string containing the newLine format
     * @param string $separator A string containing the separator format
     * @param array $style
     *
     * @return string
     */
    public static function generateAddress(Address $address, $patternRules = array(), $newLine = self::FORMAT_NEW_LINE, $separator = ' ', $style = array())
    {
        $addressFields = AddressFormat::getOrderedAddressFields($address->id_country);
        $addressFormatedValues = AddressFormat::getFormattedAddressFieldsValues($address, $addressFields);

        $addressText = '';
        foreach ($addressFields as $line) {
	        if($line != "google_address" && $line != "location_position") {
	            if (($patternsList = preg_split(self::_CLEANING_REGEX_, $line, -1, PREG_SPLIT_NO_EMPTY))) {
	                $tmpText = '';
	                foreach ($patternsList as $pattern) {
	                    if ((!array_key_exists('avoid', $patternRules)) ||
	                                (is_array($patternRules) && array_key_exists('avoid', $patternRules) && !in_array($pattern, $patternRules['avoid']))) {
	                        $tmpText .= (isset($addressFormatedValues[$pattern]) && !empty($addressFormatedValues[$pattern])) ?
	                                (((isset($style[$pattern])) ?
	                                    (sprintf($style[$pattern], $addressFormatedValues[$pattern])) :
	                                    $addressFormatedValues[$pattern]) . $separator) : '';
	                    }
	                }
	                $tmpText = trim($tmpText);
	                $addressText .= (!empty($tmpText)) ? $tmpText . $newLine : '';
	            }
	        }
        }

        $addressText = preg_replace('/' . preg_quote($newLine, '/') . '$/i', '', $addressText);
        $addressText = rtrim($addressText, $separator);

        return $addressText;
    }

    /**
     * @see ObjectModel::getFieldsRequired()
     */
    public static function getFieldsRequired()
    {
        $address = new CustomerAddress();
		$context = Context::getContext();
        if(!$context->shop->frais_par_zone) {
			array_push(AddressFormat::$requireFormFieldsList, 'Secteur:secteur');
        }

        return array_unique(array_merge($address->getFieldsRequiredDB(), AddressFormat::$requireFormFieldsList));
    }
}
