<?php
use PrestaShop\PrestaShop\Adapter\AddressFactory;
use PrestaShop\PrestaShop\Adapter\Cache\CacheAdapter;
use PrestaShop\PrestaShop\Adapter\Customer\CustomerDataProvider;
use PrestaShop\PrestaShop\Adapter\Database;
use PrestaShop\PrestaShop\Adapter\Group\GroupDataProvider;
use PrestaShop\PrestaShop\Adapter\Product\PriceCalculator;
use PrestaShop\PrestaShop\Adapter\ServiceLocator;
use PrestaShop\PrestaShop\Core\Cart\Calculator;
use PrestaShop\PrestaShop\Core\Cart\CartRow;
use PrestaShop\PrestaShop\Core\Cart\CartRuleData;

class Cart extends CartCore
{
	public $delivery_mode;   
	public $nb_baguettes;
	public $nb_sauce_salee; 
	public $nb_sauce_sucree; 
	public $nb_wasabi; 
	public $nb_gingembre; 

    // TS - 23/03/2023
    public $price_tranche = 15;
	
	protected $taxConfiguration;

    const TO_BE_DELIVERED = 1;
    const TO_GO = 2;

    public function __construct($id = null, $idLang = null)
    {
        $this->taxConfiguration = new TaxConfiguration();

	    self::$definition['fields']['delivery_mode'] = array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt');
	    self::$definition['fields']['nb_baguettes'] = array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt');
	    self::$definition['fields']['nb_sauce_salee'] = array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt');
	    self::$definition['fields']['nb_sauce_sucree'] = array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt');
	    self::$definition['fields']['nb_wasabi'] = array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt');
	    self::$definition['fields']['nb_gingembre'] = array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt');
        parent::__construct($id);
	}
	
    /**
     * Check if product quantities in Cart are available.
     *
     * @param bool $returnProductOnFailure Return the first found product with not enough quantity
     *
     * @return bool|array If all products are in stock: true; if not: either false or an array
     *                    containing the first found product which is not in stock in the
     *                    requested amount
     */
    public function checkQuantities($returnProductOnFailure = false)
    {
        if (Configuration::isCatalogMode() && !defined('_PS_ADMIN_DIR_')) {
            return false;
        }

        return true;
    }

    /**
     * Remove the CartRules from the Cart.
     *
     * @param int $id_cart Cart ID
     *
     * @return bool Whether the Cart rule has been successfully removed
     */
    public function removeCartRules()
    {
        Db::getInstance()->execute('DELETE FROM `' . _DB_PREFIX_ . 'cart_cart_rule` WHERE `id_cart` = ' . (int) $this->id);
    }

	public function getNbBaguettes() 
	{
		return $this->nb_baguettes;		
	}
	
	public function getNbSauceSalee() 
	{
		return $this->nb_sauce_salee;		
	}
	
	public function getNbSauceSucree() 
	{
		return $this->nb_sauce_sucree;		
	}
	
	public function getNbWasabi() 
	{
		return $this->nb_wasabi;		
	}
	
	public function getNbGingembre() 
	{
		return $this->nb_gingembre;		
	}
	
	public function getCartComplements($id_cart=null) 
	{
		if(empty($id_cart))
			$id_cart 				= $this->id;
		if($id_cart) {
			$complements = Db::getInstance()->executeS(
				'
				SELECT `nb_baguettes`, `nb_sauce_sucree`, `nb_sauce_salee`, `nb_wasabi`, `nb_gingembre` 
				FROM `' . _DB_PREFIX_ . 'cart`
				WHERE `id_cart` = ' . $id_cart
			);		
			if(isset($complements[0])) {
				return $complements[0];
			}
		}

		return ['nb_baguettes' => 0, 'nb_sauce_sucree' => 0, 'nb_sauce_salee' => 0, 'nb_wasabi' => 0, 'nb_gingembre' => 0];
	}
	
	public function updateComplements() {
        $sql = 
            'UPDATE `' . _DB_PREFIX_ . 'cart`
            SET  `nb_baguettes` = ' . Tools::getValue("nb_baguettes") . ', `nb_sauce_salee` = ' . Tools::getValue("nb_sauce_salee") . ', `nb_sauce_sucree` = ' . Tools::getValue("nb_sauce_sucree") . ', `nb_gingembre` = ' . Tools::getValue("nb_gingembre") . ', `nb_wasabi` = ' . Tools::getValue("nb_wasabi") . '  
            WHERE `id_cart` = ' . $this->id
        ;
        Db::getInstance()->execute($sql);
	}
	
	public function updateElements() {
        $sql = 
            'DELETE FROM `' . _DB_PREFIX_ . 'cart_unwanted_elements` WHERE `id_cart` = ' . $this->id
        ;
        Db::getInstance()->execute($sql);

		$id_feature_values 			= Tools::getValue("id_feature_values");
        foreach($id_feature_values as $id_feature_value) {
	        $sql = 
	            'INSERT INTO `' . _DB_PREFIX_ . 'cart_unwanted_elements`
	            (`id_cart`, `id_feature_value`) VALUES (' . $this->id . ', ' . $id_feature_value . ')';  
	        ;
	        Db::getInstance()->execute($sql);
        }
	}
	
	public function getUnwantedEements() {
		if($this->id) {
			$unwanted_elements = Db::getInstance()->executeS(
				'
				SELECT `id_feature_value` 
				FROM `' . _DB_PREFIX_ . 'cart_unwanted_elements`
				WHERE `id_cart` = ' . $this->id
			);	
			$elements 				= [];
			foreach($unwanted_elements as $unwanted_element) {
				$elements[] 		= $unwanted_element['id_feature_value'];
			}	
			return $elements;
		}
		return [];
	}
	
    private function includeTaxes()
    {
        return $this->taxConfiguration->includeTaxes();
    }

	public function getCartMaxBaguettes() {
		if($this->id) {
	        $totalCartAmount 	= $this->getOrderTotal($this->includeTaxes(), Cart::ONLY_PRODUCTS);
	        $maxBaguettes		= (int) ceil($totalCartAmount / $this->price_tranche);
	        if($maxBaguettes==0)
	        	$maxBaguettes	= 1;
	        return $maxBaguettes;
		}
		else
			return 0;
	}
	
	public function getCartDelivery() 
	{
		if($this->id)
			return Db::getInstance()->executeS(
				'
				SELECT * 
				FROM `' . _DB_PREFIX_ . 'cart_delivery`
				WHERE `id_cart` = ' . $this->id
			);		
		else
			return false;
	}
	
	public static function getCartDeliveryHour($id_cart) 
	{
		return Db::getInstance()->getValue(
			'
			SELECT `delivery_time` 
			FROM `' . _DB_PREFIX_ . 'cart_delivery`
			WHERE `id_cart` = ' . $id_cart
		);		
	}
	
	public static function getCartDeliveryWhen($id_cart) 
	{
		$delivery_time = Db::getInstance()->getValue(
			'
			SELECT `delivery_time` 
			FROM `' . _DB_PREFIX_ . 'cart_delivery`
			WHERE `id_cart` = ' . $id_cart
		);
		
		if(substr($delivery_time, 0, 10)==date("Y-m-d")) {
			$delivery_when 		= "Aujourd'hui à ".substr($delivery_time, 11, 5);
		}
		else {
			$delivery_when 		= "Le ".substr($delivery_time, 8, 2)."/".substr($delivery_time, 5, 2)."/".substr($delivery_time, 0, 4)." à ".substr($delivery_time, 11, 5);
		}
		return $delivery_when;
	}
	
	public static function getCartDeliveryMode($id_cart) 
	{
		$delivery_mode = Db::getInstance()->getValue(
			'
			SELECT `delivery_mode` 
			FROM `' . _DB_PREFIX_ . 'cart`
			WHERE `id_cart` = ' . $id_cart
		);
		return $delivery_mode;
	}
	
	public static function getCartProductIDs($id_cart) 
	{
		if($id_cart) {
			$res = Db::getInstance()->executeS(
				'
				SELECT `id_product` 
				FROM `' . _DB_PREFIX_ . 'cart_product`
				WHERE `id_cart` = ' . $id_cart
			);		
	        if (!$res) {
	            return [];
	        }
			
			$product_ids = [];
			foreach($res as $product) {
				$product_ids[] = $product['id_product'];
			}
	        return $product_ids;
	    }
	    else
	    	return [];
	}
	
	public function checkDeliveryMode() {
		if($this->delivery_mode==0) {
			$context 			= Context::getContext();
	        $sql = 
	            'UPDATE `' . _DB_PREFIX_ . 'cart`
	            SET  `delivery_mode` = ' . $context->delivery_mode . '  
	            WHERE `id_cart` = ' . $this->id
	        ;
	        Db::getInstance()->execute($sql);
			$this->delivery_mode 	= $context->delivery_mode;
		}
	}
	
	// =========================================================================================================
	// Mise à jour de la date et de l'heure de livraison / retrait
	// =========================================================================================================
    public function updateDeliveryHour($delivery_hour) 
    {
		$sql						= '	SELECT `id_cart_delivery` 
										FROM `' . _DB_PREFIX_ . 'cart_delivery`
										WHERE `id_cart` = ' . $this->id;
		$id_cart_delivery			= (int) Db::getInstance()->getValue($sql);		
		if($id_cart_delivery) {
            $sql = 
                'UPDATE `' . _DB_PREFIX_ . 'cart_delivery`
                SET  `delivery_time` = "' . $delivery_hour . '" 
                WHERE `id_cart_delivery` = ' . $id_cart_delivery
            ;
            Db::getInstance()->execute($sql);
		}	
		else {
            Db::getInstance()->execute(
                'INSERT INTO `' . _DB_PREFIX_ . 'cart_delivery` (`id_cart`, `delivery_time`)
                VALUES (' . $this->id . ', "' . $delivery_hour . '")'
            );
		}			
    }
    
    /**
     * This function returns the total cart amount.
     *
     * @param bool $withTaxes With or without taxes
     * @param int $type Total type enum
     *                  - Cart::ONLY_PRODUCTS
     *                  - Cart::ONLY_DISCOUNTS
     *                  - Cart::BOTH
     *                  - Cart::BOTH_WITHOUT_SHIPPING
     *                  - Cart::ONLY_SHIPPING
     *                  - Cart::ONLY_WRAPPING
     *                  - Cart::ONLY_PRODUCTS_WITHOUT_SHIPPING
     *                  - Cart::ONLY_PHYSICAL_PRODUCTS_WITHOUT_SHIPPING
     * @param array $products
     * @param int $id_carrier
     * @param bool $use_cache @deprecated
     *
     * @return float Order total
     *
     * @throws \Exception
     */
    public function getOrderTotal(
        $withTaxes = true,
        $type = Cart::BOTH,
        $products = null,
        $id_carrier = null,
        $use_cache = false, 
        bool $keepOrderPrices = false
    ) {
        if ((int) $id_carrier <= 0) {
            $id_carrier = null;
        }
		
		$context = Context::getContext();
		if(isset($context->cart->id_carrier))
			$id_carrier = $context->cart->id_carrier;
			
        // deprecated type
        if ($type == Cart::ONLY_PRODUCTS_WITHOUT_SHIPPING) {
            $type = Cart::ONLY_PRODUCTS;
        }

        // check type
        $type = (int) $type;
        $allowedTypes = [
            Cart::ONLY_PRODUCTS,
            Cart::ONLY_DISCOUNTS,
            Cart::BOTH,
            Cart::BOTH_WITHOUT_SHIPPING,
            Cart::ONLY_SHIPPING,
            Cart::ONLY_WRAPPING,
            Cart::ONLY_PHYSICAL_PRODUCTS_WITHOUT_SHIPPING,
        ];
        if (!in_array($type, $allowedTypes)) {
            throw new \Exception('Invalid calculation type: ' . $type);
        }

        // EARLY RETURNS

        // if cart rules are not used
        if ($type == Cart::ONLY_DISCOUNTS && !CartRule::isFeatureActive()) {
            return 0;
        }
        // no shipping cost if is a cart with only virtuals products
        $virtual = $this->isVirtualCart();
        if ($virtual && $type == Cart::ONLY_SHIPPING) {
            return 0;
        }
        if ($virtual && $type == Cart::BOTH) {
            $type = Cart::BOTH_WITHOUT_SHIPPING;
        }

        // filter products
        if (null === $products) {
            $products = $this->getProducts();
        }

        if ($type == Cart::ONLY_PHYSICAL_PRODUCTS_WITHOUT_SHIPPING) {
            foreach ($products as $key => $product) {
                if ($product['is_virtual']) {
                    unset($products[$key]);
                }
            }
            $type = Cart::ONLY_PRODUCTS;
        }

        if (Tax::excludeTaxeOption()) {
            $withTaxes = false;
        }

        // CART CALCULATION
        $cartRules = [];
        if (in_array($type, [Cart::BOTH, Cart::BOTH_WITHOUT_SHIPPING, Cart::ONLY_DISCOUNTS])) {
            $cartRules = $this->getTotalCalculationCartRules($type, $type == Cart::BOTH);
        }

        $computePrecision = Context::getContext()->getComputingPrecision();
        $calculator = $this->newCalculator($products, $cartRules, $id_carrier, $computePrecision);
        switch ($type) {
            case Cart::ONLY_SHIPPING:
                $calculator->calculateRows();
                $calculator->calculateFees();
                $amount = $calculator->getFees()->getInitialShippingFees();

                break;
            case Cart::ONLY_WRAPPING:
                $calculator->calculateRows();
                $calculator->calculateFees();
                $amount = $calculator->getFees()->getInitialWrappingFees();

                break;
            case Cart::BOTH:
                $calculator->processCalculation();
                $amount = $calculator->getTotal();

                break;
            case Cart::BOTH_WITHOUT_SHIPPING:
                $calculator->calculateRows();
                // dont process free shipping to avoid calculation loop (and maximum nested functions !)
                $calculator->calculateCartRulesWithoutFreeShipping();
                $amount = $calculator->getTotal(true);
                break;
            case Cart::ONLY_PRODUCTS:
                $calculator->calculateRows();
                $amount = $calculator->getRowTotal();

                break;
            case Cart::ONLY_DISCOUNTS:
                $calculator->processCalculation();
                $amount = $calculator->getDiscountTotal();

                break;
            default:
                throw new \Exception('unknown cart calculation type : ' . $type);
        }

        // TAXES ?

        $value = $withTaxes ? $amount->getTaxIncluded() : $amount->getTaxExcluded();

        // ROUND AND RETURN

        return Tools::ps_round($value, $computePrecision);
    }

    /**
     * Return shipping total for the cart.
     *
     * @param array|null $delivery_option Array of the delivery option for each address
     * @param bool $use_tax Use taxes
     * @param Country|null $default_country Default Country
     *
     * @return float Shipping total
     */
    public function getTotalShippingCost($delivery_option = null, $use_tax = true, Country $default_country = null)
    {
        if (isset(Context::getContext()->cookie->id_country)) {
            $default_country = new Country(Context::getContext()->cookie->id_country);
        }
        if (null === $delivery_option) {
            $delivery_option = $this->getDeliveryOption($default_country, false, false);
        }

        $_total_shipping = array(
            'with_tax' => 0,
            'without_tax' => 0,
        );

		$no_address 			= true;
		$hors_zone 				= false;

        $delivery_option_list 	= $this->getDeliveryOptionList($default_country);

        if(Context::getContext()->delivery_mode==Context::TO_BE_DELIVERED) {
	        foreach ($delivery_option as $id_address => $key) {
	            if ($id_address==0 || !isset($delivery_option_list[$id_address]) || !isset($delivery_option_list[$id_address][$key])) {
	                continue;
	            }
				
				$userAddress 		= new Address($id_address);
				$distance 			= self::computeDistance($userAddress);
				if($distance==1000 && $id_address == Context::getContext()->cart->id_address_delivery) {
					$hors_zone 		= true;
				}
				
	            $_total_shipping['with_tax'] += $delivery_option_list[$id_address][$key]['total_price_with_tax'];
	            $_total_shipping['without_tax'] += $delivery_option_list[$id_address][$key]['total_price_without_tax'];
				$no_address 		= false;
	        }
		}
		
        $amount 			= ($use_tax) ? $_total_shipping['with_tax'] : $_total_shipping['without_tax'];
		if($no_address) {
			$return 		= ['label' => 'À déterminer', 'value' => $amount];
		}
		else
		if($hors_zone) {
			$return 		= ['label' => 'Hors zone', 'value' => $amount];
		}
		else
			$return 		= ['label' => 'OK', 'value' => $amount];
			
		//print_r($return);
		return $return;
    }

    public static function logAction($str) {
	
	    $fp									= fopen($_SERVER['DOCUMENT_ROOT']."/log/google.log", "a");
	    fwrite($fp, date("Y-m-d H:i:s")." ==> ".$str."\n");
	    //echo(date("Y-m-d H:i:s")." ==> ".$str."<br>");
	    fclose($fp);
    
    }
    
    //======================================================================================================
    // Calcul de la distance entre le point de vente et l'adresse de livraison du client
    //======================================================================================================
    public static function computeDistance($userAddress) {

		$debug 								= false;

        $context 							= Context::getContext();
		$id_shop							= $context->shop->id;
		$id_address  						= 0;
		if($userAddress->id)
			$id_address  					= $userAddress->id;

		$distance 							= Db::getInstance()->getValue('
			SELECT distance FROM `' . _DB_PREFIX_ . 'address_distance_shop`
			WHERE `id_address` = '.$id_address.' AND `id_shop` = '.$id_shop
		);
		//echo "distance=$distance pour id_address=$id_address<br>";
        self::logAction("distance=$distance pour id_address=".$id_address);
		if($distance !== false && number_format($distance, 2) >= '0.00')	return $distance;
		
		$sql = '
			DELETE FROM `' . _DB_PREFIX_ . 'address_distance_shop`
			WHERE `id_address` = '.$id_address.' AND `id_shop` = '.$id_shop;
        if($debug) {
			echo "==============================================================================<br>";
			echo $sql."<br>";
			echo "==============================================================================<br>";
		}
        self::logAction("DELETE (1) address_distance - id_address=".$id_address);
		Db::getInstance()->execute($sql);

		$storeAddress 						= Store::getStoreAddress();
		$default_lang 						= Configuration::get('PS_LANG_DEFAULT');
        $storeCountry 						= Country::getNameById($default_lang, $storeAddress['id_country']);
        $userCountry 						= Country::getNameById($default_lang, $userAddress->id_country);
        $origin_address 					= $storeAddress['address1'].' '.$storeAddress['postcode'].' '.$storeAddress['city'].' '.$storeCountry;
        $dest_address 						= $userAddress->address1.', '.$userAddress->postcode.' '.$userAddress->city.' '.$userCountry;
		//echo $dest_address."<br>";		
        $meters 							= 0;
		$km 								= 1000;
        $url 								= "https://maps.googleapis.com/maps/api/distancematrix/json?origins=" . urlencode($origin_address) . "&destinations=" . urlencode($dest_address) . "&sensor=false&mode=bicycling&alternatives=true&key=".Configuration::get('LLW_KYO_GOOGLE_MAP_API_KEY');
        $jsonArr 							= json_decode(file_get_contents($url));
        self::logAction("Appel de distancematrix - id_address=".$id_address);
        /*echo "id_cart=".$context->cart->id;
        print_r($jsonArr);
        echo $url."<br>";*/
        
        if ($jsonArr->status != "INVALID_REQUEST" && $jsonArr->status != "REQUEST_DENIED" && $jsonArr->status != "OVER_QUERY_LIMIT" && $jsonArr->rows[0]->elements[0]->status != "ZERO_RESULTS" && $jsonArr->rows[0]->elements[0]->status != "NOT_FOUND") {
	        self::logAction("Appel de directions - id_address=".$id_address);
	        $url 							= "https://maps.googleapis.com/maps/api/directions/json?origin=" . urlencode($origin_address) . "&destination=" . urlencode($dest_address) . "&key=".Configuration::get('LLW_KYO_GOOGLE_MAP_API_KEY')."&mode=bicycling";
	        $jsonArr 						= json_decode(file_get_contents($url))->routes;
	
			//sort the routes based on the distance
			usort($jsonArr,create_function('$a,$b','return intval($a->legs[0]->distance->value) - intval($b->legs[0]->distance->value);'));
	
			//print the longest distance
            $meters 						= $jsonArr[0]->legs[0]->distance->value;
            $km 							= ($meters / 1000);
	        /*print_r($jsonArr);
	        echo "<br>".$url."<br>";
			echo "km=$km<br>";*/
			self::logAction("km=$km - id_address=".$id_address);
 
			$fields 						= array();
            $fields['id_shop'] 				= $id_shop;
            $fields['id_address'] 			= $id_address;
            $fields['distance'] 			= (float)$km;
            $fields['date_add'] 			= date("Y-m-d H:i:s");
            $null_values 					= false;
			$sql 							= '
				DELETE FROM `' . _DB_PREFIX_ . 'address_distance_shop`
				WHERE `id_address` = '.$id_address.' AND `id_shop` = '.$id_shop;
			Db::getInstance()->execute($sql);
			self::logAction("DELETE (2) address_distance - id_address=".$id_address);
			if(number_format((float)$km, 2) >= '0.00') {
		        $sql 							= '
		            INSERT INTO `' . _DB_PREFIX_ . 'address_distance_shop`
		            (`id_shop`, `id_address`, `distance`, `date_add`) 
		            VALUES 
		            (' . $id_shop . ', ' . $id_address . ', ' . (float)$km . ', "' . date("Y-m-d H:i:s") . '")';
		        ;
		        Db::getInstance()->execute($sql);
	            if($debug) {
					echo "==============================================================================<br>";
					echo "INSERT address_distance_shop<br>";
					echo "==============================================================================<br>";
	            }
			}
 
 	        return $km;
        }
        if ($jsonArr->status == "OVER_QUERY_LIMIT") {
			$km 							= $jsonArr->status;
	    }
	    if(isset($jsonArr->rows[0]->elements[0]->status) && $jsonArr->rows[0]->elements[0]->status == "ZERO_RESULTS") {
		    $km 							= $jsonArr->rows[0]->elements[0]->status;
	    }
	    if(isset($jsonArr->rows[0]->elements[0]->status) && $jsonArr->rows[0]->elements[0]->status == "NOT_FOUND") {
		    $km 							= 1000;
	    }
	    //echo "km=$km<br>";
        return $km;
    }

	public function getPackageShippingCost(
        $id_carrier = null, 
        $use_tax = true, Country 
        $default_country = null, 
        $product_list = null, 
        $id_zone = null, 
        bool $keepOrderPrices = false)
	{
		if ($this->isVirtualCart()) {
			return 0;
		}
		
		//echo "1-getPackageShippingCost !!!<br>";

        $context 							= Context::getContext();
		$id_shop							= $context->shop->id;
		
        $customer 							= new Customer((int)$this->id_customer);
        $ids_group 							= [$customer->id_default_group];
        
        if(empty($id_carrier))
        	$id_carrier						= Carrier::getCarrierForDeliveryMode();

		if ($id_carrier && $this->id_address_delivery > 0 && $context->delivery_mode==Context::TO_BE_DELIVERED) {

			$default_lang 					= Configuration::get('PS_LANG_DEFAULT');
			$price 							= (float)$this->getOrderTotal(true, Cart::BOTH_WITHOUT_SHIPPING);

			$_carrier 						= new Carrier($id_carrier, $default_lang);
			$_address 						= new Address($this->id_address_delivery);
			$_km 							= self::computeDistance($_address);
			
			$sql 								= '';
			
			if($context->shop->frais_par_zone) {
				if($_km==0)					$_km = 1;
				if($_km >= 0 && $_km < 1000) {
					$sql 						= '
						SELECT * FROM `' . _DB_PREFIX_ . 'bestkit_shippingmatrix_shop` bss
						INNER JOIN `' . _DB_PREFIX_ . 'bestkit_shippingmatrix` bs
						ON bss.`id_bestkit_shippingmatrix` = bs.`id_bestkit_shippingmatrix`
						AND ' . (float)$_km . ' >= bs.`km_from`
						AND ' . (float)$_km . ' < bs.`km_to`
						WHERE bss.`id_shop` =' . $id_shop
					;
			        if ($ids_group) {
			            $sql				   .= ' 
				            AND EXISTS (SELECT 1 FROM '._DB_PREFIX_.'bestkit_shippingmatrix_group bsg
							WHERE bsg.id_bestkit_shippingmatrix = bs.id_bestkit_shippingmatrix
							AND bsg.id_group IN ('.implode(',', array_map('intval', $ids_group)).')) 
						';
			        }
					$sql 					   .= '
						ORDER BY bs.`price_from` ASC
					';
					//echo $sql;
				}
				else
					return 1000;
			}
			else {
				$sql 						= '
					SELECT * FROM `' . _DB_PREFIX_ . 'bestkit_shippingmatrix_shop` bss
					INNER JOIN `' . _DB_PREFIX_ . 'bestkit_shippingmatrix` bs
					ON bss.`id_bestkit_shippingmatrix` = bs.`id_bestkit_shippingmatrix`
					AND bs.`zone_name` = \''.pSQL($_address->secteur).'\'
					WHERE bss.`id_shop` =' . $id_shop
				;
		        if ($ids_group) {
		            $sql				   .= ' 
			            AND EXISTS (SELECT 1 FROM '._DB_PREFIX_.'bestkit_shippingmatrix_group bsg
						WHERE bsg.id_bestkit_shippingmatrix = bs.id_bestkit_shippingmatrix
						AND bsg.id_group IN ('.implode(',', array_map('intval', $ids_group)).')) 
					';
		        }
				$sql 					   .= '
					ORDER BY bs.`price_from` ASC
				';
				
			}

			if($sql) {
				$shipping_rates 			= Db::getInstance()->executeS($sql);

				if (count($shipping_rates)) {
					$module 				= Module::getInstanceByName('bestkit_shippingmatrix');
					if (is_null($product_list)) {
						$products 			= $this->getProducts();
					} else {
						$products 			= $product_list;
					}
	
					foreach ($shipping_rates as $shipping_rate) {
						
						//echo "price=".floatval($price).", mt_min_order=".$shipping_rate['mt_min_order'].", price_from=".$shipping_rate['price_from'].", price_to=".$shipping_rate['price_to']."<br>";

						if($shipping_rate['price_from'] <= $price && $price < $shipping_rate['price_to']) {
							//echo "3-getPackageShippingCost !!!<br>";
							if(strpos($shipping_rate['shipping_price'], "%")===false)
								$shipping_price 		= $shipping_rate['shipping_price'];
							else {
								$rate					= str_replace("%", "", $shipping_rate['shipping_price']);
								$shipping_price			= $price * (int) $rate / 100;
							}
							$fees 						= $module->addFeesToShippingCost($shipping_price, $products, $_carrier, $_address, $this->id_currency);
							if($fees == 9999.00) {	
								return 0;
							}
							else {
								return (float) $fees;
							}
						}
						
						if($shipping_rate['mt_min_order'] && $price < $shipping_rate['mt_min_order']) {
							// Montant minimum non atteint -------------------------------------------------------------------------
							//echo "2-getPackageShippingCost !!!<br>";
							//die();
							return 0;
						}
					}
				}
				
				return 0;
			}
		}
		else
			return false;
			
		return parent::getPackageShippingCost($id_carrier, $use_tax, $default_country, $product_list, $id_zone);
	}

	public function getDeliveryByZone($address_delivery)
	{
        $context 							= Context::getContext();
		$id_shop							= $context->shop->id;
		$mt_min_order						= $context->shop->mt_min_order;
		$default_lang	 					= Configuration::get('PS_LANG_DEFAULT');
		$price 								= (float)$this->getOrderTotal(true, Cart::BOTH_WITHOUT_SHIPPING); // TTC

        $customer 							= new Customer((int)$this->id_customer);
        $ids_group 							= [$customer->id_default_group];

		if (is_object($address_delivery) || $address_delivery > 0 && $context->delivery_mode==Context::TO_BE_DELIVERED) {

			if(is_object($address_delivery))
				$_address					= $address_delivery;
			else
				$_address 					= new Address($address_delivery);

			if($context->shop->frais_par_zone) {
				$_km 						= self::computeDistance($_address);
	
				if($_km >= 0 && $_km < 1000) {
					if($_km==0)			$_km = 1;
					$sql 					= '
						SELECT * FROM `' . _DB_PREFIX_ . 'bestkit_shippingmatrix_shop` bss
						INNER JOIN `' . _DB_PREFIX_ . 'bestkit_shippingmatrix` bs
						ON bss.`id_bestkit_shippingmatrix` = bs.`id_bestkit_shippingmatrix`
						AND ' . (float)$_km . ' >= bs.`km_from`
						AND ' . (float)$_km . ' < bs.`km_to`
						WHERE bss.`id_shop` =' . $id_shop. '
					';
			        if ($ids_group) {
			            $sql				   .= ' 
				            AND EXISTS (SELECT 1 FROM '._DB_PREFIX_.'bestkit_shippingmatrix_group bsg
							WHERE bsg.id_bestkit_shippingmatrix = bs.id_bestkit_shippingmatrix
							AND bsg.id_group IN ('.implode(',', array_map('intval', $ids_group)).')) 
						';
			        }
					$sql 					   .= '
						ORDER BY bs.`price_from` ASC
					';
					$shipping_rates 				= Db::getInstance()->ExecuteS($sql);
					//echo $sql;
					//print_r($shipping_rates);
					if (count($shipping_rates)) {
		
						foreach ($shipping_rates as $key => $shipping_rate) {
		
							if($shipping_rate['shipping_price'] == 9999.00) {
								// Hors zone -------------------------------------------------------------------------------------------
								return "HORS_ZONE";
							}

							if($shipping_rate['mt_min_order'] > 0 && $price < $shipping_rate['mt_min_order'] && $key == 0) {
								// Montant minimum non atteint -------------------------------------------------------------------------
								//echo "1-price=$price, mt_min_order=".$shipping_rate['mt_min_order']."<br>";
								return "MT_MIN-".$shipping_rate['mt_min_order']."-".$_km;
							}

							if($shipping_rate['mt_min_order'] > 0 && $price < $shipping_rate['mt_min_order'] && $price >= $shipping_rate['price_from'] && $price < $shipping_rate['price_to']) {
								// Montant minimum non atteint -------------------------------------------------------------------------
								//echo "1-price=$price, mt_min_order=".$shipping_rate['mt_min_order']."<br>";
								return "MT_MIN-".$shipping_rate['mt_min_order']."-".$_km;
							}
						}
					}
					
					return "OK";
				}
				else {
					if($_km > 0) 				return "HORS_ZONE-".$_km;
					else 						return "NOT_FOUND";
				}
			}
			else {
				if($_address->secteur) {
					$sql 							= '
						SELECT * FROM `' . _DB_PREFIX_ . 'bestkit_shippingmatrix` bs
						INNER JOIN `' . _DB_PREFIX_ . 'bestkit_shippingmatrix_shop` bss
						ON bss.`id_bestkit_shippingmatrix` = bs.`id_bestkit_shippingmatrix` AND bss.`id_shop` =' . $id_shop .'
						WHERE bs.`zone_name` = \''.pSQL($_address->secteur).'\'
					';
			        if ($ids_group) {
			            $sql				   .= ' 
				            AND EXISTS (SELECT 1 FROM '._DB_PREFIX_.'bestkit_shippingmatrix_group bsg
							WHERE bsg.id_bestkit_shippingmatrix = bs.id_bestkit_shippingmatrix
							AND bsg.id_group IN ('.implode(',', array_map('intval', $ids_group)).')) 
						';
			        }
					$sql 					   .= '
						ORDER BY bs.`price_from` ASC
					';
					//echo $sql;
					$shipping_rates 				= Db::getInstance()->ExecuteS($sql);
					if(count($shipping_rates)) {
	
						foreach ($shipping_rates as $key => $shipping_rate) {
							
							if($shipping_rate['mt_min_order'] > 0 && $price < $shipping_rate['mt_min_order'] && $key == 0) {
								// Montant minimum non atteint -------------------------------------------------------------------------
								//echo "1-price=$price, mt_min_order=".$shipping_rate['mt_min_order']."<br>";
								return "MT_MIN-".$shipping_rate['mt_min_order'];
							}

							if($shipping_rate['mt_min_order'] > 0 && $price < $shipping_rate['mt_min_order'] && $price >= $shipping_rate['price_from'] && $price < $shipping_rate['price_to']) {
								// Montant minimum non atteint -------------------------------------------------------------------------
								//echo "1-price=$price, mt_min_order=".$shipping_rate['mt_min_order']."<br>";
								return "MT_MIN-".$shipping_rate['mt_min_order'];
							}
						}
	
						return "OK";
						
					}
					else {
						return "HORS_SECTEUR";
					}
				}
				else {
					return "HORS_SECTEUR";
				}
			}			
		}
		else {
			return "???";
		}
	}

	public static function getDeliveryByZoneReturn($return)
	{
		$delivery_ok								= true;
		$error 										= '';
		$error_code									= '';
		if(substr($return, 0, 6) == "MT_MIN") {
			$elt 									= explode("-", $return);
            $error 									= 'Le montant minimum de commande de '.number_format($elt[1], 2, ',', '').' Euros n\'a pas été atteint, merci d\'ajouter des produits à votre panier !';
            if(isset($elt[2]))
                $error 								= 'Le montant minimum de commande de '.number_format($elt[1], 2, ',', '').' Euros n\'a pas été atteint pour une distance de '.number_format($elt[2], 2, ',', '').'km, merci d\'ajouter des produits à votre panier !';
			$delivery_ok 							= false;
			$error_code								= 'Montant minimum';
		}
		else
		if($return == "NOT_FOUND") {
            $error									= 'Votre adresse ne peut être correctement déterminée par les GPS, merci de bien vouloir la corriger !';
			$delivery_ok 							= false;
			$error_code								= 'Erreur géocodage';
		}
		else
		if(substr($return, 0, 9) == "HORS_ZONE") {
			$km 									= '';
			if(!(strpos($return, "-")===false)) {
				$elt 								= explode("-", $return);
				if($elt[1]!="1000")
					$km 							= " (".$elt[1]." km)";
				else
					$km 							= " (Adresse non géocodée par Google)";
			}
            $error									= 'Nous ne livrons pas à cette distance'.$km.', merci de nous contacter pour définir un point de relais avec nos équipes !';
			$delivery_ok 							= false;
			$error_code								= 'Hors zone';
		}
		else
		if($return == "HORS_SECTEUR") {
            $error									= 'Il semblerait qu\'aucun "secteur" ne soit affecté à votre adresse, merci de bien vouloir la modifier. Si vous ne faites partie d\'aucun des secteurs proposés, vous pouvez modifier votre commande pour venir la retirer en cliquant sur notre logo. Ou contactez nous par téléphone. Merci';
			$delivery_ok 							= false;
			$error_code								= 'Hors secteur';
		}
		else
		if($return == "OVER_QUERY_LIMIT") {
            $error									= 'Trop de requêtes Google pour la journée, merci de nous contacter !';
			$delivery_ok 							= false;
			$error_code								= 'Too much request';
		}
		else
		if($return == "ZERO_RESULTS") {
            $error									= 'Nous ne reconnaissons pas votre adresse de livraison, merci de donner une adresse reconnue par les GPS ou de nous contacter par téléphone !';
			$delivery_ok 							= false;
			$error_code								= 'Adr non reconnue';
		}

        return [
        	'delivery_ok' 	=> $delivery_ok,
        	'error'			=> $error,
        	'error_code'	=> $error_code
        ];
	}
	
	public static function getFirstSecteur()
	{
        $context 							= Context::getContext();
		$id_shop							= $context->shop->id;
		
		if(!$context->shop->frais_par_zone) {
			$sql 							= '
				SELECT * FROM `' . _DB_PREFIX_ . 'bestkit_shippingmatrix_shop` bss
				INNER JOIN `' . _DB_PREFIX_ . 'bestkit_shippingmatrix` bs
				ON bss.`id_bestkit_shippingmatrix` = bs.`id_bestkit_shippingmatrix`
				WHERE bss.`id_shop` =' . $id_shop
			;
			$sql 						   .= '
				ORDER BY bs.id_bestkit_shippingmatrix ASC, bs.`price_from` ASC
			';

			$shipping_rates 				= Db::getInstance()->ExecuteS($sql);
			if (count($shipping_rates)) {
				return $shipping_rates[0]['zone_name'];
			}
		}
	}

    /**
     * Return cart products.
     *
     * @param bool $refresh
     * @param bool $id_product
     * @param int $id_country
     * @param bool $fullInfos
     *
     * @return array Products
     */
    public function getProducts($refresh = false, $id_product = false, $id_country = null, $fullInfos = true, bool $keepOrderPrices = false)
    {
        if (!$this->id) {
            return [];
        }
        // Product cache must be strictly compared to NULL, or else an empty cart will add dozens of queries
        if ($this->_products !== null && !$refresh) {
            // Return product row with specified ID if it exists
            if (is_int($id_product)) {
                foreach ($this->_products as $product) {
                    if ($product['id_product'] == $id_product) {
                        return [$product];
                    }
                }

                return [];
            }

            return $this->_products;
        }

        // Build query
        $sql = new DbQuery();

        // Build SELECT
        $sql->select('cp.`id_product_attribute`, cp.`id_product`, cp.`quantity` AS cart_quantity, cp.id_shop, cp.`id_customization`, pl.`name`, p.`is_virtual`,
                        pl.`description_short`, pl.`available_now`, pl.`available_later`, product_shop.`id_category_default`, p.`id_supplier`,
                        p.`id_manufacturer`, m.`name` AS manufacturer_name, product_shop.`on_sale`, product_shop.`ecotax`, product_shop.`additional_shipping_cost`,
                        product_shop.`available_for_order`, product_shop.`show_price`, product_shop.`price`, product_shop.`active`, product_shop.`unity`, product_shop.`unit_price_ratio`,
                        stock.`quantity` AS quantity_available, p.`width`, p.`height`, p.`depth`, stock.`out_of_stock`, p.`weight`,
                        p.`available_date`, p.`date_add`, p.`date_upd`, IFNULL(stock.quantity, 0) as quantity, pl.`link_rewrite`, cl.`link_rewrite` AS category,
                        CONCAT(LPAD(cp.`id_product`, 10, 0), LPAD(IFNULL(cp.`id_product_attribute`, 0), 10, 0), IFNULL(cp.`id_address_delivery`, 0), IFNULL(cp.`id_customization`, 0)) AS unique_id, cp.id_address_delivery,
                        product_shop.advanced_stock_management, ps.product_supplier_reference supplier_reference');

        // Build FROM
        $sql->from('cart_product', 'cp');

        // Build JOIN
        $sql->leftJoin('product', 'p', 'p.`id_product` = cp.`id_product`');
        $sql->innerJoin('product_shop', 'product_shop', '(product_shop.`id_shop` = cp.`id_shop` AND product_shop.`id_product` = p.`id_product`)');
        $sql->leftJoin(
            'product_lang',
            'pl',
            'p.`id_product` = pl.`id_product`
            AND pl.`id_lang` = ' . (int) $this->id_lang . Shop::addSqlRestrictionOnLang('pl', 'cp.id_shop')
        );

        $sql->leftJoin(
            'category_lang',
            'cl',
            'product_shop.`id_category_default` = cl.`id_category`
            AND cl.`id_lang` = ' . (int) $this->id_lang . Shop::addSqlRestrictionOnLang('cl', 'cp.id_shop')
        );

        $sql->leftJoin('product_supplier', 'ps', 'ps.`id_product` = cp.`id_product` AND ps.`id_product_attribute` = cp.`id_product_attribute` AND ps.`id_supplier` = p.`id_supplier`');
        $sql->leftJoin('manufacturer', 'm', 'm.`id_manufacturer` = p.`id_manufacturer`');

        // @todo test if everything is ok, then refactorise call of this method
        $sql->join(Product::sqlStock('cp', 'cp'));

        // Build WHERE clauses
        $sql->where('cp.`id_cart` = ' . (int) $this->id);
        if ($id_product) {
            $sql->where('cp.`id_product` = ' . (int) $id_product);
        }
        $sql->where('p.`id_product` IS NOT NULL');

        // Build ORDER BY
        $sql->orderBy('cp.`date_add`, cp.`id_product`, cp.`id_product_attribute` ASC');

        if (Customization::isFeatureActive()) {
            $sql->select('cu.`id_customization`, cu.`quantity` AS customization_quantity');
            $sql->leftJoin(
                'customization',
                'cu',
                'p.`id_product` = cu.`id_product` AND cp.`id_product_attribute` = cu.`id_product_attribute` AND cp.`id_customization` = cu.`id_customization` AND cu.`id_cart` = ' . (int) $this->id
            );
            $sql->groupBy('cp.`id_product_attribute`, cp.`id_product`, cp.`id_shop`, cp.`id_customization`');
        } else {
            $sql->select('NULL AS customization_quantity, NULL AS id_customization');
        }

        if (Combination::isFeatureActive()) {
            $sql->select('
                product_attribute_shop.`price` AS price_attribute, product_attribute_shop.`ecotax` AS ecotax_attr,
                IF (IFNULL(pa.`reference`, \'\') = \'\', p.`reference`, pa.`reference`) AS reference,
                (p.`weight`+ pa.`weight`) weight_attribute,
                IF (IFNULL(pa.`ean13`, \'\') = \'\', p.`ean13`, pa.`ean13`) AS ean13,
                IF (IFNULL(pa.`isbn`, \'\') = \'\', p.`isbn`, pa.`isbn`) AS isbn,
                IF (IFNULL(pa.`upc`, \'\') = \'\', p.`upc`, pa.`upc`) AS upc,
                IF (IFNULL(pa.`mpn`, \'\') = \'\', p.`mpn`, pa.`mpn`) AS mpn,
                IFNULL(product_attribute_shop.`minimal_quantity`, product_shop.`minimal_quantity`) as minimal_quantity,
                IF(product_attribute_shop.wholesale_price > 0,  product_attribute_shop.wholesale_price, product_shop.`wholesale_price`) wholesale_price
            ');

            $sql->leftJoin('product_attribute', 'pa', 'pa.`id_product_attribute` = cp.`id_product_attribute`');
            $sql->leftJoin('product_attribute_shop', 'product_attribute_shop', '(product_attribute_shop.`id_shop` = cp.`id_shop` AND product_attribute_shop.`id_product_attribute` = pa.`id_product_attribute`)');
        } else {
            $sql->select(
                'p.`reference` AS reference, p.`ean13`, p.`isbn`,
                p.`upc` AS upc, p.`mpn` AS mpn, product_shop.`minimal_quantity` AS minimal_quantity, product_shop.`wholesale_price` wholesale_price'
            );
        }

        $sql->select('image_shop.`id_image` id_image, il.`legend`');
        $sql->leftJoin('image_shop', 'image_shop', 'image_shop.`id_product` = p.`id_product` AND image_shop.cover=1 AND image_shop.id_shop=' . (int) $this->id_shop);
        $sql->leftJoin('image_lang', 'il', 'il.`id_image` = image_shop.`id_image` AND il.`id_lang` = ' . (int) $this->id_lang);

        $result = Db::getInstance()->executeS($sql);

        // Reset the cache before the following return, or else an empty cart will add dozens of queries
        $products_ids = [];
        $pa_ids = [];
        if($this->delivery_mode==1) {
        	$to_be_delivered 				= 1;
        }
        else {
        	$to_be_delivered 				= 0;
        }
        if ($result) {
            foreach ($result as $key => $row) {
                $products_ids[] = $row['id_product'];
                $pa_ids[] = $row['id_product_attribute'];
                $specific_price = SpecificPrice::getSpecificPrice($row['id_product'], $this->id_shop, $this->id_currency, $id_country, $this->id_shop_group, $row['cart_quantity'], $row['id_product_attribute'], $this->id_customer, $this->id);
				// #TS - 20160302 - Produits livrés : Pas de réduction sur le prix ---------------------------------------------------------
				// Compatible avec les promos ? --------------------------------------------------------------------------------------------
				if($to_be_delivered == 1) {
		            if (isset($specific_price['price']) && $specific_price['to_take_away'] == 1) {
						//die("Suppression de la promo à emporter");
		                $specific_price = [];
		            }
				}
						
                if ($specific_price) {
                    $reduction_type_row = ['reduction_type' => $specific_price['reduction_type']];
                } else {
                    $reduction_type_row = ['reduction_type' => 0];
                }

                $result[$key] = array_merge($row, $reduction_type_row);
            }
        }
        // Thus you can avoid one query per product, because there will be only one query for all the products of the cart
        Product::cacheProductsFeatures($products_ids);
        Cart::cacheSomeAttributesLists($pa_ids, $this->id_lang);

        if (empty($result)) {
            $this->_products = [];

            return [];
        }

        if ($fullInfos) {
            $cart_shop_context = Context::getContext()->cloneContext();

            $givenAwayProductsIds = [];

            if ($this->shouldSplitGiftProductsQuantity && $refresh) {
                $gifts = $this->getCartRules(CartRule::FILTER_ACTION_GIFT, false);
                if (count($gifts) > 0) {
                    foreach ($gifts as $gift) {
                        foreach ($result as $rowIndex => $row) {
                            if (!array_key_exists('is_gift', $result[$rowIndex])) {
                                $result[$rowIndex]['is_gift'] = false;
                            }

                            if (
                                $row['id_product'] == $gift['gift_product'] &&
                                $row['id_product_attribute'] == $gift['gift_product_attribute']
                            ) {
                                $row['is_gift'] = true;
                                $result[$rowIndex] = $row;
                            }
                        }

                        $index = $gift['gift_product'] . '-' . $gift['gift_product_attribute'];
                        if (!array_key_exists($index, $givenAwayProductsIds)) {
                            $givenAwayProductsIds[$index] = 1;
                        } else {
                            ++$givenAwayProductsIds[$index];
                        }
                    }
                }
            }

            $this->_products = [];

            foreach ($result as &$row) {
                if (!array_key_exists('is_gift', $row)) {
                    $row['is_gift'] = false;
                }

                $additionalRow = Product::getProductProperties((int) $this->id_lang, $row);
                $row['reduction'] = $additionalRow['reduction'];
                $row['reduction_without_tax'] = $additionalRow['reduction_without_tax'];
                $row['price_without_reduction'] = $additionalRow['price_without_reduction'];
                $row['specific_prices'] = $additionalRow['specific_prices'];
                unset($additionalRow);

                $givenAwayQuantity = 0;
                $giftIndex = $row['id_product'] . '-' . $row['id_product_attribute'];
                if ($row['is_gift'] && array_key_exists($giftIndex, $givenAwayProductsIds)) {
                    $givenAwayQuantity = $givenAwayProductsIds[$giftIndex];
                }

                if (!$row['is_gift'] || (int) $row['cart_quantity'] === $givenAwayQuantity) {
                    $row = $this->applyProductCalculations($row, $cart_shop_context);
                } else {
                    // Separate products given away from those manually added to cart
                    $this->_products[] = $this->applyProductCalculations($row, $cart_shop_context, $givenAwayQuantity);
                    unset($row['is_gift']);
                    $row = $this->applyProductCalculations(
                        $row,
                        $cart_shop_context,
                        $row['cart_quantity'] - $givenAwayQuantity
                    );
                }

                $this->_products[] = $row;
            }
        } else {
            $this->_products = $result;
        }

        return $this->_products;
    }

    /**
     * @param $row
     * @param $shopContext
     * @param $productQuantity
     *
     * @return mixed
     */
    protected function applyProductCalculations($row, $shopContext, $productQuantity = null, bool $keepOrderPrices = false)
    {
        if (null === $productQuantity) {
            $productQuantity = (int) $row['cart_quantity'];
        }

        if (isset($row['ecotax_attr']) && $row['ecotax_attr'] > 0) {
            $row['ecotax'] = (float) $row['ecotax_attr'];
        }

        $row['stock_quantity'] = (int) $row['quantity'];
        // for compatibility with 1.2 themes
        $row['quantity'] = $productQuantity;

        // get the customization weight impact
        $customization_weight = Customization::getCustomizationWeight($row['id_customization']);

        if (isset($row['id_product_attribute']) && (int) $row['id_product_attribute'] && isset($row['weight_attribute'])) {
            $row['weight_attribute'] += $customization_weight;
            $row['weight'] = (float) $row['weight_attribute'];
        } else {
            $row['weight'] += $customization_weight;
        }

        if (Configuration::get('PS_TAX_ADDRESS_TYPE') == 'id_address_invoice') {
            $address_id = (int) $this->id_address_invoice;
        } else {
            $address_id = (int) $row['id_address_delivery'];
        }
        if (!Address::addressExists($address_id)) {
            $address_id = null;
        }

        if ($shopContext->shop->id != $row['id_shop']) {
            $shopContext->shop = new Shop((int) $row['id_shop']);
        }

        $address = Address::initialize($address_id, true);
        $id_tax_rules_group = Product::getIdTaxRulesGroupByIdProduct((int) $row['id_product'], $shopContext);
        $tax_calculator = TaxManagerFactory::getManager($address, $id_tax_rules_group)->getTaxCalculator();

        $specific_price_output = null;

        if($this->delivery_mode==1) {
        	$to_be_delivered 				= 1;
        }
        else {
        	$to_be_delivered 				= 0;
        }

        $row['price_without_reduction'] = Product::getPriceStatic(
            (int) $row['id_product'],
            true,
            isset($row['id_product_attribute']) ? (int) $row['id_product_attribute'] : null,
            6,
            null,
            false,
            false,
            $productQuantity,
            false,
            (int) $this->id_customer ? (int) $this->id_customer : null,
            (int) $this->id,
            $address_id,
            $specific_price_output,
            true,
            true,
            $shopContext,
            true,
            $row['id_customization'],
            $to_be_delivered
        );

        $row['price_without_reduction_without_tax'] = Product::getPriceStatic(
            (int) $row['id_product'],
            false,
            isset($row['id_product_attribute']) ? (int) $row['id_product_attribute'] : null,
            6,
            null,
            false,
            false,
            $productQuantity,
            false,
            (int) $this->id_customer ? (int) $this->id_customer : null,
            (int) $this->id,
            $address_id,
            $specific_price_output,
            true,
            true,
            $shopContext,
            true,
            $row['id_customization'],
            $to_be_delivered
        );

        $row['price_with_reduction'] = Product::getPriceStatic(
            (int) $row['id_product'],
            true,
            isset($row['id_product_attribute']) ? (int) $row['id_product_attribute'] : null,
            6,
            null,
            false,
            true,
            $productQuantity,
            false,
            (int) $this->id_customer ? (int) $this->id_customer : null,
            (int) $this->id,
            $address_id,
            $specific_price_output,
            true,
            true,
            $shopContext,
            true,
            $row['id_customization'],
            $to_be_delivered
        );

        $row['price'] = $row['price_with_reduction_without_tax'] = Product::getPriceStatic(
            (int) $row['id_product'],
            false,
            isset($row['id_product_attribute']) ? (int) $row['id_product_attribute'] : null,
            6,
            null,
            false,
            true,
            $productQuantity,
            false,
            (int) $this->id_customer ? (int) $this->id_customer : null,
            (int) $this->id,
            $address_id,
            $specific_price_output,
            true,
            true,
            $shopContext,
            true,
            $row['id_customization'],
            $to_be_delivered
        );

        switch (Configuration::get('PS_ROUND_TYPE')) {
            case Order::ROUND_TOTAL:
                $row['total'] = $row['price_with_reduction_without_tax'] * $productQuantity;
                $row['total_wt'] = $row['price_with_reduction'] * $productQuantity;

                break;
            case Order::ROUND_LINE:
                $row['total'] = Tools::ps_round(
                    $row['price_with_reduction_without_tax'] * $productQuantity,
                    Context::getContext()->getComputingPrecision()
                );
                $row['total_wt'] = Tools::ps_round(
                    $row['price_with_reduction'] * $productQuantity,
                    Context::getContext()->getComputingPrecision()
                );

                break;

            case Order::ROUND_ITEM:
            default:
                $row['total'] = Tools::ps_round(
                        $row['price_with_reduction_without_tax'],
                        Context::getContext()->getComputingPrecision()
                    ) * $productQuantity;
                $row['total_wt'] = Tools::ps_round(
                        $row['price_with_reduction'],
                        Context::getContext()->getComputingPrecision()
                    ) * $productQuantity;

                break;
        }

        $row['price_wt'] = $row['price_with_reduction'];
        $row['description_short'] = Tools::nl2br($row['description_short']);

        // check if a image associated with the attribute exists
        if ($row['id_product_attribute']) {
            $row2 = Image::getBestImageAttribute($row['id_shop'], $this->id_lang, $row['id_product'], $row['id_product_attribute']);
            if ($row2) {
                $row = array_merge($row, $row2);
            }
        }

        $row['reduction_applies'] = ($specific_price_output && (float) $specific_price_output['reduction']);
        $row['quantity_discount_applies'] = ($specific_price_output && $productQuantity >= (int) $specific_price_output['from_quantity']);
        $row['id_image'] = Product::defineProductImage($row, $this->id_lang);
        $row['allow_oosp'] = Product::isAvailableWhenOutOfStock($row['out_of_stock']);
        $row['features'] = Product::getFeaturesStatic((int) $row['id_product']);

        if (array_key_exists($row['id_product_attribute'] . '-' . $this->id_lang, self::$_attributesLists)) {
            $row = array_merge($row, self::$_attributesLists[$row['id_product_attribute'] . '-' . $this->id_lang]);
        }

        return Product::getTaxesInformations($row, $shopContext);
    }

}
