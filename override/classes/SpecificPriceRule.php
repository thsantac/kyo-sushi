<?php
/**
 * Copyright since 2007 PrestaShop SA and Contributors
 * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.md.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/OSL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://devdocs.prestashop.com/ for more information.
 *
 * @author    PrestaShop SA and Contributors <contact@prestashop.com>
 * @copyright Since 2007 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
 */
class SpecificPriceRule extends SpecificPriceRuleCore
{
    public $name;
    public $id_shop;
    public $id_currency;
    public $id_country;
    public $id_group;
    public $from_quantity;
    public $price;
    public $reduction;
    public $reduction_tax;
    public $reduction_type;
    public $from;
    public $to;
	// #TS 20160314 ----------------------------------------------------------------------------------------------
    public $to_take_away;

    protected static $rules_application_enable = true;

    /**
     * @see ObjectModel::$definition
     */
    public static $definition = [
        'table' => 'specific_price_rule',
        'primary' => 'id_specific_price_rule',
        'fields' => [
            'name' => ['type' => self::TYPE_STRING, 'validate' => 'isCleanHtml', 'required' => true],
            'id_shop' => ['type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'required' => true],
            'id_country' => ['type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'required' => true],
            'id_currency' => ['type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'required' => true],
            'id_group' => ['type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'required' => true],
            'from_quantity' => ['type' => self::TYPE_INT, 'validate' => 'isUnsignedInt', 'required' => true],
            'price' => ['type' => self::TYPE_FLOAT, 'validate' => 'isNegativePrice', 'required' => true],
            'reduction' => ['type' => self::TYPE_FLOAT, 'validate' => 'isPrice', 'required' => true],
            'reduction_tax' => ['type' => self::TYPE_INT, 'validate' => 'isBool', 'required' => true],
            'reduction_type' => ['type' => self::TYPE_STRING, 'validate' => 'isReductionType', 'required' => true],
            'from' => ['type' => self::TYPE_DATE, 'validate' => 'isDateFormat', 'required' => false],
            'to' => ['type' => self::TYPE_DATE, 'validate' => 'isDateFormat', 'required' => false],
            'to_take_away' =>    array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt', 'required' => false),
        ],
    ];

    public static function applyRuleToProduct($id_rule, $id_product, $id_product_attribute = null)
    {
        $rule = new SpecificPriceRule((int) $id_rule);
        if (!Validate::isLoadedObject($rule) || !Validate::isUnsignedInt($id_product)) {
            return false;
        }

        $specific_price = new SpecificPrice();
        $specific_price->id_specific_price_rule = (int) $rule->id;
        $specific_price->id_product = (int) $id_product;
        $specific_price->id_product_attribute = (int) $id_product_attribute;
        $specific_price->id_customer = 0;
        $specific_price->id_shop = (int) $rule->id_shop;
        $specific_price->id_country = (int) $rule->id_country;
        $specific_price->id_currency = (int) $rule->id_currency;
        $specific_price->id_group = (int) $rule->id_group;
        $specific_price->from_quantity = (int) $rule->from_quantity;
        $specific_price->price = (float) $rule->price;
        $specific_price->reduction_type = $rule->reduction_type;
        $specific_price->reduction_tax = $rule->reduction_tax;
        $specific_price->reduction = ($rule->reduction_type == 'percentage' ? $rule->reduction / 100 : (float) $rule->reduction);
        $specific_price->from = $rule->from;
        $specific_price->to = $rule->to;
        $specific_price->to_take_away = $rule->to_take_away;

        return $specific_price->add();
    }
    
    public static function getShopTakeAwayReductionRate() 
    {
	    $id_shop 				= Context::getContext()->shop->id;
	    $taux 					= Db::getInstance()->getValue(
            'SELECT `reduction`
            FROM `' . _DB_PREFIX_ . 'specific_price_rule`
            WHERE `id_shop` = ' . (int) $id_shop . ' 
            AND `to_take_away` = 1'
        );
		return (int) $taux;
    }
}
