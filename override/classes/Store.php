<?php
/**
 * Copyright since 2007 PrestaShop SA and Contributors
 * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.md.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/OSL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://devdocs.prestashop.com/ for more information.
 *
 * @author    PrestaShop SA and Contributors <contact@prestashop.com>
 * @copyright Since 2007 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
 */

/**
 * Class StoreCore.
 */
class Store extends StoreCore
{
    public $hours_delivery;

    public function __construct($idStore = null, $idLang = null)
    {
        self::$definition['fields']['hours_delivery'] 	= ['type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isJson', 'size' => 65000];
        self::$definition['fields']['latitude'] 		= ['type' => self::TYPE_STRING, 'size' => 18];
        self::$definition['fields']['longitude'] 		= ['type' => self::TYPE_STRING, 'size' => 18];

		StoreCore::__construct($idStore, $idLang);
	}

    /**
     * Get Stores by language.
     *
     * @param $idLang
     *
     * @return array|false|mysqli_result|PDOStatement|resource|null
     */
    public static function getStoreAddress($idShop=null, $idLang=null)
    {
		if($idShop==null)
			$idShop 	= Context::getContext()->shop->id;
		if($idLang==null)
			$idLang 	= Context::getContext()->language->id;

        $id_store = Db::getInstance()->getValue(
            '
            SELECT id_store
            FROM ' . _DB_PREFIX_ . 'store_shop
            WHERE id_shop = '. (int) $idShop
        );

        $storeData = Db::getInstance()->executeS(
            '
            SELECT s.*, sl.*
            FROM ' . _DB_PREFIX_ . 'store s
            LEFT JOIN ' . _DB_PREFIX_ . 'store_lang sl ON (sl.id_store = s.id_store AND sl.id_lang = ' . (int) $idLang . ')
            WHERE s.id_store = '. (int) $id_store
        );

        return $storeData[0];
    }

	public static function hoursFormat($hours) {
		// [["11h30-14h30, 18h00-22h00"],["11h30-14h30, 18h00-22h00"],["11h30-14h30, 18h00-22h00"],["11h30-14h30, 18h00-22h00"],["11h30-14h30, 18h00-22h45"],["11h30-14h30, 18h00-22h45"],[""]]
		$hours					= str_replace('[[', '', $hours);
		$hours					= str_replace(']]', '', $hours);
		$hours					= explode("],[", $hours);
        foreach($hours as &$hour) {
			$hour				= str_replace(', ', '/', $hour);		
        }
		$hours					= str_replace(['"', '[', ']'], '', $hours);
		$hours					= str_replace('h', ':', $hours);

        return $hours;
	}

	/**
	 * Return store open hours
	 */
    public static function getStoreHours()
    {
	    $context 				= Context::getContext();
		$id_shop 				= $context->shop->id;

		// Magasin associé à la boutique ---------------------------------------
        $id_store 				= Db::getInstance()->getValue(
            'SELECT `id_store`
            FROM `' . _DB_PREFIX_ . 'store_shop`
            WHERE `id_shop` = ' . (int) $id_shop
        );

		if($context->delivery_mode==Context::TO_GO) {
	        $hours 					= Db::getInstance()->getValue(
	            'SELECT `hours`
	            FROM `' . _DB_PREFIX_ . 'store_lang`
	            WHERE `id_store` = ' . (int) $id_store
	        );
	        
		}
		else {
	        $hours 					= Db::getInstance()->getValue(
	            'SELECT `hours_delivery`
	            FROM `' . _DB_PREFIX_ . 'store_lang`
	            WHERE `id_store` = ' . (int) $id_store
	        );
		}

        // [["10h00-13h00/18h45-21h30"],["10h00-21h30"],["10h00-21h30"],["10h00-21h30"],["10h00-21h30"],["10h00-21h30"],[""]]
        // Lundi            Mardi...                                                                        Dimanche
		$hours					= self::hoursFormat($hours);
		array_unshift($hours, "");

		return $hours;
    }

	/**
	 * Return store name
	 */
    public static function getStoreName($id_shop)
    {
		// Magasin associé à la boutique ---------------------------------------
        $id_store 				= Db::getInstance()->getValue(
            'SELECT `id_store`
            FROM `' . _DB_PREFIX_ . 'store_shop`
            WHERE `id_shop` = ' . (int) $id_shop
        );

		$sql = '
			SELECT `name`
			FROM `'._DB_PREFIX_.'store_lang`
			WHERE `id_store` = '.$id_store
		;
		return Db::getInstance()->getValue($sql);
    }

	/**
	 * Is store opened ?
	 */
    public static function isOpen($day = null)
    {
		if($day == null)
			$day 				= date("Y-m-d");
			
        $hours                  = self::getStoreHours();		

		$weekDayNumber			= date("N", mktime(0, 0, 0, substr($day, 5, 2), substr($day, 8, 2), substr($day, 0, 4)));
		if(CalendarLLW::isFerie($day) || empty($hours[$weekDayNumber])) {
			return false;
		}
		
		$currentDayHours		= explode("/", $hours [$weekDayNumber]);
		if(isset($currentDayHours[1])) {
			$currentDayHoursMorning 	= explode("-", $currentDayHours[0]);
			$currentDayHoursEvening 	= explode("-", $currentDayHours[1]);
			$currentDayHours[0]			= $currentDayHoursMorning;
			$currentDayHours[1]			= $currentDayHoursEvening;
		}
		else {
			$currentDayHoursMorning 	= explode("-", $currentDayHours[0]);
			$currentDayHoursEvening		= [];
			if(isset($currentDayHoursMorning[1])) {
				if($currentDayHoursMorning[0] > "12:00") {
					$currentDayHoursEvening	= $currentDayHoursMorning;
					$currentDayHoursMorning = [];
				}
				$currentDayHours[0]		= $currentDayHoursMorning;
				$currentDayHours[1]		= $currentDayHoursEvening;
			}
			else {
				$currentDayHoursMorning = [];
				$currentDayHoursEvening	= [];
			}
		}
		
		if(isset($currentDayHoursMorning[0]) && isset($currentDayHoursEvening[0]))
			if((date("H:i") >= $currentDayHoursMorning[0] && date("H:i") <= $currentDayHoursMorning[1])
			|| (date("H:i") >= $currentDayHoursEvening[0] && date("H:i") <= $currentDayHoursEvening[1]))
				return true;

		if(isset($currentDayHoursMorning[0]))
			if(date("H:i") >= $currentDayHoursMorning[0] && date("H:i") <= $currentDayHoursMorning[1])
				return true;

		if(isset($currentDayHoursEvening[0]))
			if(date("H:i") >= $currentDayHoursEvening[0] && date("H:i") <= $currentDayHoursEvening[1])
				return true;

		return false;
    }

	/**
	 * Return store open hours in readable format
	 */
	public static function getDisplayOpenHours() 
	{
        $hours                  = self::getStoreHours();		
		return self::formatOpenHours($hours);
	}
	        
	public static function formatOpenHours($hours) 
	{
        // [["10h00-13h00/18h45-21h30"],["10h00-21h30"],["10h00-21h30"],["10h00-21h30"],["10h00-21h30"],["10h00-21h30"],[""]]
        // Lundi            Mardi...                                                                        Dimanche

		$days 					= [
			0					=> '',
			1					=> 'lundi',
			2					=> 'mardi',
			3					=> 'mercredi',
			4					=> 'jeudi',
			5					=> 'vendredi',
			6					=> 'samedi',
			7					=> 'dimache',
		];
		
		$display_open_hours 		= [];
		$prev_open_hours			= $hours[1];
		$prev_day_number			= '1';
		for($i=1; $i<=7; $i++) {
			if($hours[$i] != $prev_open_hours) {
				$display_open_hours[$prev_day_number]	= $prev_open_hours;
				$prev_day_number	= '';
				$prev_open_hours	= $hours[$i];
			}
			if($prev_day_number)
				$prev_day_number   .= '-';
			$prev_day_number	   .= $i;
		}

	}

	// ============================================================================================
	// Retourne les heures d'ouverture d'une journée donnée
	// ============================================================================================
	public static function getDayOpenHours($day=null) 
	{
		if($day == null)
			$day 				= date("Y-m-d");
			
	    $context 				= Context::getContext();
		$id_shop 				= $context->shop->id;

		// Magasin associé à la boutique ---------------------------------------
        $id_store 				= Db::getInstance()->getValue(
            'SELECT `id_store`
            FROM `' . _DB_PREFIX_ . 'store_shop`
            WHERE `id_shop` = ' . (int) $id_shop
        );

        $calendarHours = CalendarLLW::getOpenHour($day);

		if($context->delivery_mode==Context::TO_GO) {
	        $hours 					= Db::getInstance()->getValue(
	            'SELECT `hours`
	            FROM `' . _DB_PREFIX_ . 'store_lang`
	            WHERE `id_store` = ' . (int) $id_store
	        );
		}
		else {
	        $hours 					= Db::getInstance()->getValue(
	            'SELECT `hours_delivery`
	            FROM `' . _DB_PREFIX_ . 'store_lang`
	            WHERE `id_store` = ' . (int) $id_store
	        );
		}

        // [["10h00-13h00/18h45-21h30"],["10h00-21h30"],["10h00-21h30"],["10h00-21h30"],["10h00-21h30"],["10h00-21h30"],[""]]
        // Lundi            Mardi...                                                                        Dimanche
		$hours					= self::hoursFormat($hours);
		array_unshift($hours, "");

		$thisDay				= date("Y-m-d", mktime(0, 0, 0, substr($day, 5, 2), substr($day, 8, 2), substr($day, 0, 4)));
		$weekDayNumber 			= date("N", mktime(0, 0, 0, substr($thisDay, 5, 2), substr($thisDay, 8, 2), substr($thisDay, 0, 4)));

		if(CalendarLLW::isFerie($thisDay) || empty($hours[$weekDayNumber])) {
			return false;
		}
		
		// 10h00-13h00/18h45-21h30 ou 10h00-22h00 --------------------------------------------------------------------
		if($context->delivery_mode==Context::TO_GO) {
	        if($calendarHours['hours']) {
                $hours[$weekDayNumber] = $calendarHours['hours'];
            }
		}
		else {
	        if($calendarHours['hours_delivery']) {
                $hours[$weekDayNumber] = $calendarHours['hours_delivery'];
            }
		}

		$currentDayHours				= explode("/", $hours [$weekDayNumber]);
		if(isset($currentDayHours[1])) {
			$currentDayHoursMorning 	= explode("-", $currentDayHours[0]);
			$currentDayHoursEvening 	= explode("-", $currentDayHours[1]);
			foreach($currentDayHoursMorning as &$hour) {
				$hour					= str_pad($hour, 5, "0", STR_PAD_LEFT);
			}
			foreach($currentDayHoursEvening as &$hour) {
				$hour					= str_pad($hour, 5, "0", STR_PAD_LEFT);
			}
			$currentDayHours[0]			= $currentDayHoursMorning;
			$currentDayHours[1]			= $currentDayHoursEvening;
		}
		else {
			$dayHours 					= explode("-", $currentDayHours[0]);
			if($dayHours[0] > "12:00") {
				$currentDayHoursEvening	= $dayHours;
				$currentDayHoursMorning = [];
			}
			else {
				$currentDayHoursMorning = [$dayHours[0], '12:00'];
				$currentDayHoursEvening	= ['12:00', $dayHours[1]];
			}
			foreach($currentDayHoursMorning as &$hour) {
				$hour					= str_pad($hour, 5, "0", STR_PAD_LEFT);
			}
			foreach($currentDayHoursEvening as &$hour) {
				$hour					= str_pad($hour, 5, "0", STR_PAD_LEFT);
			}
			$currentDayHours[0]			= $currentDayHoursMorning;
			$currentDayHours[1]			= $currentDayHoursEvening;
		}

        //echo "pour $day, calendarHours=".print_r($currentDayHours, true)."<br>";
		return $currentDayHours;
	}

}
