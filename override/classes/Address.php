<?php
/**
 * 2007-2019 PrestaShop and Contributors
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/OSL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2019 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
 * International Registered Trademark & Property of PrestaShop SA
 */

/**
 * Class AddressCore.
 */
class Address extends AddressCore
{
    public $google_address;
    public $location_position;
    public $secteur;
    public $id_shop;

    /**
     * Build an Address.
     *
     * @param int $id_address Existing Address ID in order to load object (optional)
     */
    public function __construct($id_address = null, $id_lang = null)
    {
        parent::__construct($id_address);

		self::$definition ['fields']['google_address'] = array('type' => self::TYPE_STRING, 'required' => false, 'size' => 255);
		self::$definition ['fields']['location_position'] = array('type' => self::TYPE_STRING, 'required' => false, 'size' => 100);
		self::$definition ['fields']['secteur'] = array('type' => self::TYPE_STRING, 'required' => false, 'size' => 255);
		self::$definition ['fields']['id_shop'] = array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId');
		
		$this->checkPhoneNumber();
    }

    public function update($null_values = false)
    {
        // Empty related caches
        if (isset(self::$_idCountries[$this->id])) {
            unset(self::$_idCountries[$this->id]);
        }
        if (isset(self::$_idZones[$this->id])) {
            unset(self::$_idZones[$this->id]);
        }

        if (Validate::isUnsignedId($this->id_customer)) {
            Customer::resetAddressCache($this->id_customer, $this->id);
        }

        /* Skip the required fields */
        if ($this->isUsed()) {
            self::$fieldsRequiredDatabase['Address'] = [];
        }

        $context 							= Context::getContext();
		$id_shop							= $context->shop->id;
		$id_address  						= $this->id;

        Db::getInstance()->delete('address_distance_shop', '`id_address`='.(int)$this->id.' AND `id_shop`='.$id_shop);

        return ObjectModel::update($null_values);
    }

    /**
     * @see ObjectModel::delete()
     */
    public function delete()
    {
        if (Validate::isUnsignedId($this->id_customer)) {
            Customer::resetAddressCache($this->id_customer, $this->id);
        }

        $context 							= Context::getContext();
		$id_shop							= $context->shop->id;
		$id_address  						= $this->id;

        Db::getInstance()->delete('address_distance_shop', '`id_address`='.(int)$this->id.' AND `id_shop`='.$id_shop);

        if (!$this->isUsed()) {
            $this->deleteCartAddress();
            return ObjectModel::delete();
        } else {
            $this->deleted = true;
            return $this->update();
        }
    }

	public static function updateAddressShop($id_customer, $id_shop)
	{
        $sql = 'UPDATE ' . _DB_PREFIX_ . 'address
                SET id_shop = ' . $id_shop . ' 
                WHERE id_customer = ' . $id_customer;
        Db::getInstance()->execute($sql);
	}

	public function checkPhoneNumber()
	{
		if($this->id && empty($this->phone) && $this->phone_mobile) {
			if(substr($this->phone_mobile, 0, 1) != '0')
				$this->phone_mobile		= '0'.$this->phone_mobile;
	        $sql = 'UPDATE ' . _DB_PREFIX_ . 'address
	                SET phone = "' . $this->phone_mobile . '"  
	                WHERE id_address = ' . $this->id;
	        Db::getInstance()->execute($sql);
		}
	}

}
