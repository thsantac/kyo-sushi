<?php
/**
 * Copyright since 2007 PrestaShop SA and Contributors
 * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.md.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/OSL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://devdocs.prestashop.com/ for more information.
 *
 * @author    PrestaShop SA and Contributors <contact@prestashop.com>
 * @copyright Since 2007 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
 */

/**
 * @property Store $object
 */
class AdminStoresController extends AdminStoresControllerCore
{
    public function renderForm()
    {
        if (!($obj = $this->loadObject(true))) {
            return;
        }

        $image = _PS_STORE_IMG_DIR_ . $obj->id . '.jpg';
        $image_url = ImageManager::thumbnail(
            $image,
            $this->table . '_' . (int) $obj->id . '.' . $this->imageType,
            350,
            $this->imageType,
            true,
            true
        );
        $image_size = file_exists($image) ? filesize($image) / 1000 : false;

        $tmp_addr = new Address();
        $res = $tmp_addr->getFieldsRequiredDatabase();
        $required_fields = [];
        foreach ($res as $row) {
            $required_fields[(int) $row['id_required_field']] = $row['field_name'];
        }

        $this->fields_form = [
            'legend' => [
                'title' => $this->trans('Stores', [], 'Admin.Shopparameters.Feature'),
                'icon' => 'icon-home',
            ],
            'input' => [
                [
                    'type' => 'text',
                    'label' => $this->trans('Name', [], 'Admin.Global'),
                    'name' => 'name',
                    'lang' => true,
                    'required' => false,
                    'hint' => [
                        $this->trans('Store name (e.g. City Center Mall Store).', [], 'Admin.Shopparameters.Feature'),
                        $this->trans('Allowed characters: letters, spaces and %s', [], 'Admin.Shopparameters.Feature'),
                    ],
                ],
                [
                    'type' => 'text',
                    'label' => $this->trans('Address', [], 'Admin.Global'),
                    'name' => 'address1',
                    'lang' => true,
                    'required' => true,
                ],
                [
                    'type' => 'text',
                    'label' => $this->trans('Address (2)', [], 'Admin.Global'),
                    'name' => 'address2',
                    'lang' => true,
                ],
                [
                    'type' => 'text',
                    'label' => $this->trans('Zip/postal code', [], 'Admin.Global'),
                    'name' => 'postcode',
                    'required' => in_array('postcode', $required_fields),
                ],
                [
                    'type' => 'text',
                    'label' => $this->trans('City', [], 'Admin.Global'),
                    'name' => 'city',
                    'required' => true,
                ],
                [
                    'type' => 'select',
                    'label' => $this->trans('Country', [], 'Admin.Global'),
                    'name' => 'id_country',
                    'required' => true,
                    'default_value' => (int) $this->context->country->id,
                    'options' => [
                        'query' => Country::getCountries($this->context->language->id),
                        'id' => 'id_country',
                        'name' => 'name',
                    ],
                ],
                [
                    'type' => 'select',
                    'label' => $this->trans('State', [], 'Admin.Global'),
                    'name' => 'id_state',
                    'required' => true,
                    'options' => [
                        'id' => 'id_state',
                        'name' => 'name',
                        'query' => null,
                    ],
                ],
                [
                    'type' => 'latitude',
                    'label' => $this->trans('Latitude / Longitude', [], 'Admin.Shopparameters.Feature'),
                    'name' => 'latitude',
                    'required' => true,
                    'maxlength' => 12,
                    'hint' => $this->trans('Store coordinates (e.g. 45.265469/-47.226478).', [], 'Admin.Shopparameters.Feature'),
                ],
                [
                    'type' => 'text',
                    'label' => $this->trans('Phone', [], 'Admin.Global'),
                    'name' => 'phone',
                ],
                [
                    'type' => 'text',
                    'label' => $this->trans('Fax', [], 'Admin.Global'),
                    'name' => 'fax',
                ],
                [
                    'type' => 'text',
                    'label' => $this->trans('Email address', [], 'Admin.Global'),
                    'name' => 'email',
                ],
                [
                    'type' => 'textarea',
                    'label' => $this->trans('Note', [], 'Admin.Global'),
                    'name' => 'note',
                    'lang' => true,
                    'cols' => 42,
                    'rows' => 4,
                ],
                [
                    'type' => 'switch',
                    'label' => $this->trans('Active', [], 'Admin.Global'),
                    'name' => 'active',
                    'required' => false,
                    'is_bool' => true,
                    'values' => [
                        [
                            'id' => 'active_on',
                            'value' => 1,
                            'label' => $this->trans('Enabled', [], 'Admin.Global'),
                        ],
                        [
                            'id' => 'active_off',
                            'value' => 0,
                            'label' => $this->trans('Disabled', [], 'Admin.Global'),
                        ],
                    ],
                    'hint' => $this->trans('Whether or not to display this store.', [], 'Admin.Shopparameters.Help'),
                ],
                [
                    'type' => 'file',
                    'label' => $this->trans('Picture', [], 'Admin.Shopparameters.Feature'),
                    'name' => 'image',
                    'display_image' => true,
                    'image' => $image_url ? $image_url : false,
                    'size' => $image_size,
                    'hint' => $this->trans('Storefront picture.', [], 'Admin.Shopparameters.Help'),
                ],
            ],
            'hours' => [
            ],
            'hours_delivery' => [
            ],
            'submit' => [
                'title' => $this->trans('Save', [], 'Admin.Actions'),
            ],
        ];

        if (Shop::isFeatureActive()) {
            $this->fields_form['input'][] = [
                'type' => 'shop',
                'label' => $this->trans('Shop association', [], 'Admin.Global'),
                'name' => 'checkBoxShopAsso',
            ];
        }

        $days = [];
        $days[1] = $this->trans('Monday', [], 'Admin.Shopparameters.Feature');
        $days[2] = $this->trans('Tuesday', [], 'Admin.Shopparameters.Feature');
        $days[3] = $this->trans('Wednesday', [], 'Admin.Shopparameters.Feature');
        $days[4] = $this->trans('Thursday', [], 'Admin.Shopparameters.Feature');
        $days[5] = $this->trans('Friday', [], 'Admin.Shopparameters.Feature');
        $days[6] = $this->trans('Saturday', [], 'Admin.Shopparameters.Feature');
        $days[7] = $this->trans('Sunday', [], 'Admin.Shopparameters.Feature');

        $hours = [];
        $hours_temp = ($this->getFieldValue($obj, 'hours'));
        if (is_array($hours_temp) && !empty($hours_temp)) {
            $langs = Language::getLanguages(false);
            $hours_temp = array_map('json_decode', $hours_temp);
            $hours = array_map(
                [$this, 'adaptHoursFormat'],
                $hours_temp
            );
            $hours = (count($langs) > 1) ? $hours : $hours[reset($langs)['id_lang']];
        }

        $hours_delivery = [];
        $hours_temp = ($this->getFieldValue($obj, 'hours_delivery'));
        if (is_array($hours_temp) && !empty($hours_temp)) {
            $langs = Language::getLanguages(false);
            $hours_temp = array_map('json_decode', $hours_temp);
            $hours_delivery = array_map(
                [$this, 'adaptHoursFormat'],
                $hours_temp
            );
            $hours_delivery = (count($langs) > 1) ? $hours_delivery : $hours_delivery[reset($langs)['id_lang']];
        }

        $this->fields_value = [
            'latitude' => $this->getFieldValue($obj, 'latitude') ? $this->getFieldValue($obj, 'latitude') : '',
            'longitude' => $this->getFieldValue($obj, 'longitude') ? $this->getFieldValue($obj, 'longitude') : '',
            'days' => $days,
            'hours' => $hours,
            'hours_delivery' => $hours_delivery
        ];

        return AdminController::renderForm();
    }

    public function postProcess()
    {
        if (isset($_POST['submitAdd' . $this->table])) {
            $langs = Language::getLanguages(false);
            /* Cleaning fields */
            foreach ($_POST as $kp => $vp) {
                if (!in_array($kp, ['checkBoxShopGroupAsso_store', 'checkBoxShopAsso_store', 'hours', 'hours_delivery'])) {
                    $_POST[$kp] = trim($vp);
                }
                if ('hours' === $kp) {
                    foreach ($vp as $day => $value) {
                        $_POST['hours'][$day] = is_array($value) ? array_map('trim', $_POST['hours'][$day]) : trim($value);
                    }
                }
                if ('hours_delivery' === $kp) {
                    foreach ($vp as $day => $value) {
                        $_POST['hours_delivery'][$day] = is_array($value) ? array_map('trim', $_POST['hours_delivery'][$day]) : trim($value);
                    }
                }
            }

            /* Rewrite latitude and longitude to 8 digits */
            $_POST['latitude'] = number_format((float) $_POST['latitude'], 8);
            $_POST['longitude'] = number_format((float) $_POST['longitude'], 8);

            /* If the selected country does not contain states */
            $id_state = (int) Tools::getValue('id_state');
            $id_country = (int) Tools::getValue('id_country');
            $country = new Country((int) $id_country);

            if ($id_country && $country && !(int) $country->contains_states && $id_state) {
                $this->errors[] = $this->trans('You\'ve selected a state for a country that does not contain states.', [], 'Admin.Advparameters.Notification');
            }

            /* If the selected country contains states, then a state have to be selected */
            if ((int) $country->contains_states && !$id_state) {
                $this->errors[] = $this->trans('An address located in a country containing states must have a state selected.', [], 'Admin.Shopparameters.Notification');
            }

            $latitude = (float) Tools::getValue('latitude');
            $longitude = (float) Tools::getValue('longitude');

            if (empty($latitude) || empty($longitude)) {
                $this->errors[] = $this->trans('Latitude and longitude are required.', [], 'Admin.Shopparameters.Notification');
            }

            $postcode = Tools::getValue('postcode');
            /* Check zip code format */
            if ($country->zip_code_format && !$country->checkZipCode($postcode)) {
                $this->errors[] = $this->trans('Your Zip/postal code is incorrect.', [], 'Admin.Notifications.Error') . '<br />' . $this->trans('It must be entered as follows:', [], 'Admin.Notifications.Error') . ' ' . str_replace('C', $country->iso_code, str_replace('N', '0', str_replace('L', 'A', $country->zip_code_format)));
            } elseif (empty($postcode) && $country->need_zip_code) {
                $this->errors[] = $this->trans('A Zip/postal code is required.', [], 'Admin.Notifications.Error');
            } elseif ($postcode && !Validate::isPostCode($postcode)) {
                $this->errors[] = $this->trans('The Zip/postal code is invalid.', [], 'Admin.Notifications.Error');
            }
            /* Store hours */
            foreach ($langs as $lang) {
                $hours = [];
                for ($i = 1; $i < 8; ++$i) {
                    if (1 < count($langs)) {
                        $hours[] = explode(' | ', $_POST['hours'][$i][$lang['id_lang']]);
                        unset($_POST['hours'][$i][$lang['id_lang']]);
                    } else {
                        $hours[] = explode(' | ', $_POST['hours'][$i]);
                        unset($_POST['hours'][$i]);
                    }
                }
                $encodedHours[$lang['id_lang']] = json_encode($hours);
                $hours_delivery = [];
                for ($i = 1; $i < 8; ++$i) {
                    if (1 < count($langs)) {
                        $hours_delivery[] = explode(' | ', $_POST['hours_delivery'][$i][$lang['id_lang']]);
                        unset($_POST['hours_delivery'][$i][$lang['id_lang']]);
                    } else {
                        $hours_delivery[] = explode(' | ', $_POST['hours_delivery'][$i]);
                        unset($_POST['hours_delivery'][$i]);
                    }
                }
                $encodedHoursDelivery[$lang['id_lang']] = json_encode($hours_delivery);
            }
            $_POST['hours'] = (1 < count($langs)) ? $encodedHours : json_encode($hours);
            $_POST['hours_delivery'] = (1 < count($langs)) ? $encodedHoursDelivery : json_encode($hours_delivery);
        }

        if (!count($this->errors)) {
            AdminController::postProcess();
        } else {
            $this->display = 'add';
        }
    }

    /**
     * Adapt the format of hours.
     *
     * @param array $value
     *
     * @return array
     */
    protected function adaptHoursFormat($value)
    {
	    if(is_array($value)) {
	        $separator = array_fill(0, count($value), ' | ');
	
	        return array_map('implode', $value, $separator);
		}
    }
}
