<?php
/**
 * Copyright since 2007 PrestaShop SA and Contributors
 * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.md.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/OSL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://devdocs.prestashop.com/ for more information.
 *
 * @author    PrestaShop SA and Contributors <contact@prestashop.com>
 * @copyright Since 2007 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
 */
use PrestaShop\PrestaShop\Core\Addon\Theme\ThemeManagerBuilder;

class AdminShopController extends AdminShopControllerCore
{
    public function __construct()
    {
        $this->bootstrap = true;
        $this->table = 'shop';
        $this->className = 'Shop';
        $this->multishop_context = Shop::CONTEXT_ALL;

        AdminController::__construct();

        $this->id_shop_group = (int) Tools::getValue('id_shop_group');

        /* if $_GET['id_shop'] is transmitted, virtual url can be loaded in config.php, so we wether transmit shop_id in herfs */
        if ($this->id_shop = (int) Tools::getValue('shop_id')) {
            $_GET['id_shop'] = $this->id_shop;
        }

        $this->list_skip_actions['delete'] = [(int) Configuration::get('PS_SHOP_DEFAULT')];
        $this->fields_list = [
            'id_shop' => [
                'title' => 'ID',
                'align' => 'center',
                'class' => 'fixed-width-xs',
            ],
            'name' => [
                'title' => 'Nom',
                'filter_key' => 'a!name',
                'width' => 200,
            ],
            'shop_group_name' => [
                'title' => 'Groupe',
                'width' => 150,
                'filter_key' => 'gs!name',
            ],
            'category_name' => [
                'title' => $this->trans('Root category', [], 'Admin.Shopparameters.Feature'),
                'width' => 150,
                'filter_key' => 'cl!name',
            ],
            'taux_reduc_emporter' => array(
                'title' => $this->l('% réduc'),
                'width' => 20,
                'filter_key' => 'a!name'
            ),
            'mt_min_order' => array(
                'title' => $this->l('Mt min à livrer'),
                'width' => 20,
                'filter_key' => 'a!name'
            ),
            'url' => [
                'title' => $this->trans('Main URL for this shop', [], 'Admin.Shopparameters.Feature'),
                'havingFilter' => 'url',
            ],
        ];
    }

    public function renderList()
    {
        $this->addRowAction('edit');
        $this->addRowAction('delete');

        $this->_select = 'gs.name shop_group_name, cl.name category_name, a.taux_reduc_emporter, a.mt_min_order, CONCAT(\'http://\', su.domain, su.physical_uri, su.virtual_uri) AS url';
        $this->_join = '
			LEFT JOIN `' . _DB_PREFIX_ . 'shop_group` gs
				ON (a.id_shop_group = gs.id_shop_group)
			LEFT JOIN `' . _DB_PREFIX_ . 'category_lang` cl
				ON (a.id_category = cl.id_category AND cl.id_lang=' . (int) $this->context->language->id . ')
			LEFT JOIN ' . _DB_PREFIX_ . 'shop_url su
				ON a.id_shop = su.id_shop AND su.main = 1
		';
        $this->_group = 'GROUP BY a.id_shop';

        if ($id_shop_group = (int) Tools::getValue('id_shop_group')) {
            $this->_where = 'AND a.id_shop_group = ' . $id_shop_group;
        }

        return AdminController::renderList();
    }

    public function renderForm()
    {
        /** @var Shop $obj */
        if (!($obj = $this->loadObject(true))) {
            return;
        }

        $this->fields_form = [
            'legend' => [
                'title' => $this->trans('Shop', [], 'Admin.Global'),
                'icon' => 'icon-shopping-cart',
            ],
            'identifier' => 'shop_id',
            'input' => [
                [
                    'type' => 'text',
                    'label' => $this->trans('Shop name', [], 'Admin.Shopparameters.Feature'),
                    'desc' => [
                        $this->trans('This field does not refer to the shop name visible in the front office.', [], 'Admin.Shopparameters.Help'),
                        $this->trans('Follow [1]this link[/1] to edit the shop name used on the front office.', [
                            '[1]' => '<a href="' . $this->context->link->getAdminLink('AdminStores') . '#store_fieldset_general">',
                            '[/1]' => '</a>',
                        ], 'Admin.Shopparameters.Help'), ],
                    'name' => 'name',
                    'required' => true,
                ],
            ],
        ];

        $this->fields_form['input'][] = array(
            'type' => 'text',
            'label' => $this->l('Taux de réduction plats à emporter'),
            'name' => 'taux_reduc_emporter',
            'values' => $obj->taux_reduc_emporter
        );
        $this->fields_form['input'][] = array(
            'type' => 'text',
            'label' => $this->l('Montant mini commandes à livrer'),
            'name' => 'mt_min_order',
            'values' => $obj->mt_min_order
        );
        $this->fields_form['input'][] = array(
            'type' => 'text',
            'label' => $this->l('ID boutique sur Solumag'),
            'name' => 'id_solumag',
            'values' => $obj->id_solumag
        );
        $this->fields_form['input'][] = array(
            'type' => 'switch',
            'label' => $this->l('Frais par zones kilométriques'),
            'name' => 'frais_par_zone',
            'required' => false,
            'class' => 't',
            'is_bool' => true,
            'values' => array(
                array(
                    'id' => 'frais_par_zone_on',
                    'value' => 1,
                    'label' => $this->l('Enabled')
                ),
                array(
                    'id' => 'frais_par_zone_off',
                    'value' => 0,
                    'label' => $this->l('Disabled')
                )
            )
        );

        $display_group_list = true;
        if ($this->display == 'edit') {
            $group = new ShopGroup($obj->id_shop_group);
            if ($group->share_customer || $group->share_order || $group->share_stock) {
                $display_group_list = false;
            }
        }

        if ($display_group_list) {
            $options = [];
            foreach (ShopGroup::getShopGroups() as $group) {
                /** @var ShopGroup $group */
                if ($this->display == 'edit' && ($group->share_customer || $group->share_order || $group->share_stock) && ShopGroup::hasDependency($group->id)) {
                    continue;
                }

                $options[] = [
                    'id_shop_group' => $group->id,
                    'name' => $group->name,
                ];
            }

            if ($this->display == 'add') {
                $group_desc = $this->trans('Warning: You won\'t be able to change the group of this shop if this shop belongs to a group with one of these options activated: Share Customers, Share Quantities or Share Orders.', [], 'Admin.Shopparameters.Notification');
            } else {
                $group_desc = $this->trans('You can only move your shop to a shop group with all "share" options disabled -- or to a shop group with no customers/orders.', [], 'Admin.Shopparameters.Notification');
            }

            $this->fields_form['input'][] = [
                'type' => 'select',
                'label' => $this->trans('Shop group', [], 'Admin.Shopparameters.Feature'),
                'desc' => $group_desc,
                'name' => 'id_shop_group',
                'options' => [
                    'query' => $options,
                    'id' => 'id_shop_group',
                    'name' => 'name',
                ],
            ];
        } else {
            $this->fields_form['input'][] = [
                'type' => 'hidden',
                'name' => 'id_shop_group',
                'default' => $group->name,
            ];
            $this->fields_form['input'][] = [
                'type' => 'textShopGroup',
                'label' => $this->trans('Shop group', [], 'Admin.Shopparameters.Feature'),
                'desc' => $this->trans('You can\'t edit the shop group because the current shop belongs to a group with the "share" option enabled.', [], 'Admin.Shopparameters.Help'),
                'name' => 'id_shop_group',
                'value' => $group->name,
            ];
        }

        $categories = Category::getRootCategories($this->context->language->id);
        $this->fields_form['input'][] = [
            'type' => 'select',
            'label' => $this->trans('Category root', [], 'Admin.Catalog.Feature'),
            'desc' => $this->trans('This is the root category of the store that you\'ve created. To define a new root category for your store, [1]please click here[/1].', [
                '[1]' => '<a href="' . $this->context->link->getAdminLink('AdminCategories') . '&addcategoryroot" target="_blank">',
                '[/1]' => '</a>',
            ], 'Admin.Shopparameters.Help'),
            'name' => 'id_category',
            'options' => [
                'query' => $categories,
                'id' => 'id_category',
                'name' => 'name',
            ],
        ];

        if (Tools::isSubmit('id_shop')) {
            $shop = new Shop((int) Tools::getValue('id_shop'));
            $id_root = $shop->id_category;
        } else {
            $id_root = $categories[0]['id_category'];
        }

        $id_shop = (int) Tools::getValue('id_shop');
        self::$currentIndex = self::$currentIndex . '&id_shop_group=' . (int) (Tools::getValue('id_shop_group') ?
            Tools::getValue('id_shop_group') : (isset($obj->id_shop_group) ? $obj->id_shop_group : Shop::getContextShopGroupID()));
        $shop = new Shop($id_shop);
        $selected_cat = Shop::getCategories($id_shop);

        if (empty($selected_cat)) {
            // get first category root and preselect all these children
            $root_categories = Category::getRootCategories();
            $root_category = new Category($root_categories[0]['id_category']);
            $children = $root_category->getAllChildren($this->context->language->id);
            $selected_cat[] = $root_categories[0]['id_category'];

            foreach ($children as $child) {
                $selected_cat[] = $child->id;
            }
        }

        if (Shop::getContext() == Shop::CONTEXT_SHOP && Tools::isSubmit('id_shop')) {
            $root_category = new Category($shop->id_category);
        } else {
            $root_category = new Category($id_root);
        }

        $this->fields_form['input'][] = [
            'type' => 'categories',
            'name' => 'categoryBox',
            'label' => $this->trans('Associated categories', [], 'Admin.Catalog.Feature'),
            'tree' => [
                'id' => 'categories-tree',
                'selected_categories' => $selected_cat,
                'root_category' => $root_category->id,
                'use_search' => true,
                'use_checkbox' => true,
            ],
            'desc' => $this->trans('By selecting associated categories, you are choosing to share the categories between shops. Once associated between shops, any alteration of this category will impact every shop.', [], 'Admin.Shopparameters.Help'),
        ];
        /*$this->fields_form['input'][] = array(
            'type' => 'switch',
            'label' => $this->trans('Enabled', array(), 'Admin.Global'),
            'name' => 'active',
            'required' => true,
            'is_bool' => true,
            'values' => array(
                array(
                    'id' => 'active_on',
                    'value' => 1
                ),
                array(
                    'id' => 'active_off',
                    'value' => 0
                )
            ),
            'desc' => $this->trans('Enable or disable your store?', array(), 'Admin.Shopparameters.Help')
        );*/

        $themes = (new ThemeManagerBuilder($this->context, Db::getInstance()))
                        ->buildRepository()
                        ->getList();

        $this->fields_form['input'][] = [
            'type' => 'theme',
            'label' => $this->trans('Theme', [], 'Admin.Design.Feature'),
            'name' => 'theme',
            'values' => $themes,
        ];

        $this->fields_form['submit'] = [
            'title' => $this->trans('Save', [], 'Admin.Actions'),
        ];

        if (Shop::getTotalShops() > 1 && $obj->id) {
            $disabled = ['active' => false];
        } else {
            $disabled = false;
        }

        $import_data = [
            'carrier' => $this->trans('Carriers', [], 'Admin.Shipping.Feature'),
            'cms' => $this->trans('Pages', [], 'Admin.Design.Feature'),
            'contact' => $this->trans('Contact information', [], 'Admin.Advparameters.Feature'),
            'country' => $this->trans('Countries', [], 'Admin.Global'),
            'currency' => $this->trans('Currencies', [], 'Admin.Global'),
            'discount' => $this->trans('Discount prices', [], 'Admin.Advparameters.Feature'),
            'employee' => $this->trans('Employees', [], 'Admin.Advparameters.Feature'),
            'image' => $this->trans('Images', [], 'Admin.Global'),
            'lang' => $this->trans('Languages', [], 'Admin.Global'),
            'manufacturer' => $this->trans('Brands', [], 'Admin.Global'),
            'module' => $this->trans('Modules', [], 'Admin.Global'),
            'hook_module' => $this->trans('Module hooks', [], 'Admin.Advparameters.Feature'),
            'meta_lang' => $this->trans('Meta information', [], 'Admin.Advparameters.Feature'),
            'product' => $this->trans('Products', [], 'Admin.Global'),
            'product_attribute' => $this->trans('Product combinations', [], 'Admin.Advparameters.Feature'),
            'stock_available' => $this->trans('Available quantities for sale', [], 'Admin.Advparameters.Feature'),
            'store' => $this->trans('Stores', [], 'Admin.Global'),
            'warehouse' => $this->trans('Warehouses', [], 'Admin.Advparameters.Feature'),
            'webservice_account' => $this->trans('Webservice accounts', [], 'Admin.Advparameters.Feature'),
            'attribute_group' => $this->trans('Attribute groups', [], 'Admin.Advparameters.Feature'),
            'feature' => $this->trans('Features', [], 'Admin.Global'),
            'group' => $this->trans('Customer groups', [], 'Admin.Advparameters.Feature'),
            'tax_rules_group' => $this->trans('Tax rules groups', [], 'Admin.Advparameters.Feature'),
            'supplier' => $this->trans('Suppliers', [], 'Admin.Global'),
            'referrer' => $this->trans('Referrers/affiliates', [], 'Admin.Advparameters.Feature'),
            'zone' => $this->trans('Zones', [], 'Admin.International.Feature'),
            'cart_rule' => $this->trans('Cart rules', [], 'Admin.Advparameters.Feature'),
        ];

        // Hook for duplication of shop data
        $modules_list = Hook::getHookModuleExecList('actionShopDataDuplication');
        if (is_array($modules_list) && count($modules_list) > 0) {
            foreach ($modules_list as $m) {
                $import_data['Module' . ucfirst($m['module'])] = Module::getModuleName($m['module']);
            }
        }

        asort($import_data);

        if (!$this->object->id) {
            $this->fields_import_form = [
                'radio' => [
                    'type' => 'radio',
                    'label' => $this->trans('Import data', [], 'Admin.Advparameters.Feature'),
                    'name' => 'useImportData',
                    'value' => 1,
                ],
                'select' => [
                    'type' => 'select',
                    'name' => 'importFromShop',
                    'label' => $this->trans('Choose the source shop', [], 'Admin.Advparameters.Feature'),
                    'options' => [
                        'query' => Shop::getShops(false),
                        'name' => 'name',
                    ],
                ],
                'allcheckbox' => [
                    'type' => 'checkbox',
                    'label' => $this->trans('Choose data to import', [], 'Admin.Advparameters.Feature'),
                    'values' => $import_data,
                ],
                'desc' => $this->trans('Use this option to associate data (products, modules, etc.) the same way for each selected shop.', [], 'Admin.Advparameters.Help'),
            ];
        }

        if (!$obj->theme_name) {
            $themes = (new ThemeManagerBuilder($this->context, Db::getInstance()))
                            ->buildRepository()
                            ->getList();
            $theme = array_pop($themes);
            $theme_name = $theme->getName();
        } else {
            $theme_name = $obj->theme_name;
        }

        $days = array();
        $days[1] = $this->l('Monday');
        $days[2] = $this->l('Tuesday');
        $days[3] = $this->l('Wednesday');
        $days[4] = $this->l('Thursday');
        $days[5] = $this->l('Friday');
        $days[6] = $this->l('Saturday');
        $days[7] = $this->l('Sunday');

        $this->fields_value = [
            'days' => $days,
            'id_shop_group' => (Tools::getValue('id_shop_group') ? Tools::getValue('id_shop_group') :
                (isset($obj->id_shop_group)) ? $obj->id_shop_group : Shop::getContextShopGroupID()),
            'id_category' => (Tools::getValue('id_category') ? Tools::getValue('id_category') :
                (isset($obj->id_category)) ? $obj->id_category : (int) Configuration::get('PS_HOME_CATEGORY')),
            'theme_name' => $theme_name,
        ];

        $ids_category = [];
        $shops = Shop::getShops(false);
        foreach ($shops as $shop) {
            $ids_category[$shop['id_shop']] = $shop['id_category'];
        }

        $this->tpl_form_vars = [
            'disabled' => $disabled,
            'checked' => (Tools::getValue('addshop') !== false) ? true : false,
            'defaultShop' => (int) Configuration::get('PS_SHOP_DEFAULT'),
            'ids_category' => $ids_category,
        ];
        if (isset($this->fields_import_form)) {
            $this->tpl_form_vars = array_merge($this->tpl_form_vars, ['form_import' => $this->fields_import_form]);
        }

        return AdminController::renderForm();
    }

}
