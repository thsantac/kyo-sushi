<?php
/**
 * 2007-2019 PrestaShop and Contributors
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/OSL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2019 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
 * International Registered Trademark & Property of PrestaShop SA
 */

/**
 * @property Carrier $object
 */
class AdminCarrierWizardController extends AdminCarrierWizardControllerCore
{
    public function renderStepOne($carrier)
    {
        $this->fields_form = array(
            'form' => array(
                'id_form' => 'step_carrier_general',
                'input' => array(
                    array(
                        'type' => 'text',
                        'label' => $this->trans('Carrier name', array(), 'Admin.Shipping.Feature'),
                        'name' => 'name',
                        'required' => true,
                        'hint' => array(
                            $this->trans('Allowed characters: letters, spaces and "%special_chars%".', array('%special_chars%' => '().-'), 'Admin.Shipping.Help'),
                            $this->trans('The carrier\'s name will be displayed during checkout.', array(), 'Admin.Shipping.Help'),
                            $this->trans('For in-store pickup, enter 0 to replace the carrier name with your shop name.', array(), 'Admin.Shipping.Help'),
                        ),
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->trans('Transit time', array(), 'Admin.Shipping.Feature'),
                        'name' => 'delay',
                        'lang' => true,
                        'required' => true,
                        'maxlength' => 512,
                        'hint' => $this->trans('The delivery time will be displayed during checkout.', array(), 'Admin.Shipping.Help'),
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->trans('Speed grade', array(), 'Admin.Shipping.Feature'),
                        'name' => 'grade',
                        'required' => false,
                        'size' => 1,
                        'hint' => $this->trans('Enter "0" for a longest shipping delay, or "9" for the shortest shipping delay.', array(), 'Admin.Shipping.Help'),
                    ),
                    array(
                        'type' => 'logo',
                        'label' => $this->trans('Logo', array(), 'Admin.Global'),
                        'name' => 'logo',
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->trans('Tracking URL', array(), 'Admin.Shipping.Feature'),
                        'name' => 'url',
                        'hint' => $this->trans('Delivery tracking URL: Type \'@\' where the tracking number should appear. It will be automatically replaced by the tracking number.', array(), 'Admin.Shipping.Help'),
                        'desc' => $this->trans('For example: \'http://example.com/track.php?num=@\' with \'@\' where the tracking number should appear.', array(), 'Admin.Shipping.Help'),
                    ),
	                array(
	                    'type' => 'radio',
	                    'label' => 'Mode de livraison',
	                    'name' => 'delivery_mode',
	                    'required' => false,
						'br' => false,
	                    'class' => 't',
	                    'values' => array(
	                        array(
	                            'id' => 'delivery_to_be_delivered',
	                            'value' => Carrier::TO_BE_DELIVERED,
	                            'label' => 'À livrer',
	                        ),
	                        array(
	                             'id' => 'delivery_to_go',
	                            'value' => Carrier::TO_GO,
	                            'label' => 'À emporter',
	                        ),
	                    ),
	                ),
                ),
            ),
        );

        $tpl_vars = array('max_image_size' => (int) Configuration::get('PS_PRODUCT_PICTURE_MAX_SIZE') / 1024 / 1024);
        $fields_value = $this->getStepOneFieldsValues($carrier);

        return $this->renderGenericForm(array('form' => $this->fields_form), $fields_value, $tpl_vars);
    }

    public function getStepOneFieldsValues($carrier)
    {
        return array(
            'id_carrier' => $this->getFieldValue($carrier, 'id_carrier'),
            'name' => $this->getFieldValue($carrier, 'name'),
            'delay' => $this->getFieldValue($carrier, 'delay'),
            'grade' => $this->getFieldValue($carrier, 'grade'),
            'url' => $this->getFieldValue($carrier, 'url'),
            'delivery_mode' => $this->getFieldValue($carrier, 'delivery_mode'),
        );
    }

}
