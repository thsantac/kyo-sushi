<?php
/**
 * Copyright since 2007 PrestaShop SA and Contributors
 * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.md.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/OSL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://devdocs.prestashop.com/ for more information.
 *
 * @author    PrestaShop SA and Contributors <contact@prestashop.com>
 * @copyright Since 2007 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
 */

/**
 * @property Carrier $object
 */
class AdminCarriersController extends AdminCarriersControllerCore
{

    public function renderForm()
    {
        $this->fields_form = [
            'legend' => [
                'title' => $this->trans('Carriers', [], 'Admin.Shipping.Feature'),
                'icon' => 'icon-truck',
            ],
            'input' => [
                [
                    'type' => 'text',
                    'label' => $this->trans('Company', [], 'Admin.Global'),
                    'name' => 'name',
                    'required' => true,
                    'hint' => [
                        $this->trans('Allowed characters: letters, spaces and "%special_chars%".', ['%special_chars%' => '().-'], 'Admin.Shipping.Help'),
                        $this->trans('Carrier name displayed during checkout', [], 'Admin.Shipping.Help'),
                        $this->trans('For in-store pickup, enter 0 to replace the carrier name with your shop name.', [], 'Admin.Shipping.Help'),
                    ],
                ],
                [
                    'type' => 'file',
                    'label' => $this->trans('Logo', [], 'Admin.Global'),
                    'name' => 'logo',
                    'hint' => $this->trans('Upload a logo from your computer.', [], 'Admin.Shipping.Help') . ' (.gif, .jpg, .jpeg ' . $this->trans('or', [], 'Admin.Shipping.Help') . ' .png)',
                ],
                [
                    'type' => 'text',
                    'label' => $this->trans('Transit time', [], 'Admin.Shipping.Feature'),
                    'name' => 'delay',
                    'lang' => true,
                    'required' => true,
                    'maxlength' => 512,
                    'hint' => $this->trans('Estimated delivery time will be displayed during checkout.', [], 'Admin.Shipping.Help'),
                ],
                [
                    'type' => 'text',
                    'label' => $this->trans('Speed grade', [], 'Admin.Shipping.Feature'),
                    'name' => 'grade',
                    'required' => false,
                    'hint' => $this->trans('Enter "0" for a longest shipping delay, or "9" for the shortest shipping delay.', [], 'Admin.Shipping.Help'),
                ],
                [
                    'type' => 'text',
                    'label' => $this->trans('URL', [], 'Admin.Global'),
                    'name' => 'url',
                    'hint' => $this->trans('Delivery tracking URL: Type \'@\' where the tracking number should appear. It will then be automatically replaced by the tracking number.', [], 'Admin.Shipping.Help'),
                ],
                [
                    'type' => 'checkbox',
                    'label' => $this->trans('Zone', [], 'Admin.Global'),
                    'name' => 'zone',
                    'values' => [
                        'query' => Zone::getZones(false),
                        'id' => 'id_zone',
                        'name' => 'name',
                    ],
                    'hint' => $this->trans('The zones in which this carrier will be used.', [], 'Admin.Shipping.Help'),
                ],
                [
                    'type' => 'switch',
                    'label' => $this->l('A emporter ?'),
                    'name' => 'to_take_away',
                    'required' => false,
                    'class' => 't',
                    'is_bool' => true,
                    'values' => array(
                        array(
                            'id' => 'to_take_away_on',
                            'value' => 1,
                            'label' => $this->l('Enabled')
                        ),
                        array(
                            'id' => 'to_take_away_off',
                            'value' => 0,
                            'label' => $this->l('Disabled')
                        )
                    ),
                    'hint' => $this->l('A emporter ou à livrer.')
                ],
                [
                    'type' => 'group',
                    'label' => $this->trans('Group access', [], 'Admin.Shipping.Help'),
                    'name' => 'groupBox',
                    'values' => Group::getGroups(Context::getContext()->language->id),
                    'hint' => $this->trans('Mark the groups that are allowed access to this carrier.', [], 'Admin.Shipping.Help'),
                ],
                [
                    'type' => 'switch',
                    'label' => $this->trans('Status', [], 'Admin.Global'),
                    'name' => 'active',
                    'required' => false,
                    'class' => 't',
                    'is_bool' => true,
                    'values' => [
                        [
                            'id' => 'active_on',
                            'value' => 1,
                            'label' => $this->trans('Enabled', [], 'Admin.Global'),
                        ],
                        [
                            'id' => 'active_off',
                            'value' => 0,
                            'label' => $this->trans('Disabled', [], 'Admin.Global'),
                        ],
                    ],
                    'hint' => $this->trans('Enable the carrier in the front office.', [], 'Admin.Shipping.Help'),
                ],
                [
                    'type' => 'switch',
                    'label' => $this->trans('Apply shipping cost', [], 'Admin.Shipping.Feature'),
                    'name' => 'is_free',
                    'required' => false,
                    'class' => 't',
                    'values' => [
                        [
                            'id' => 'is_free_on',
                            'value' => 0,
                            'label' => '<img src="../img/admin/enabled.gif" alt="' . $this->trans('Yes', [], 'Admin.Global') . '" title="' . $this->trans('Yes', [], 'Admin.Global') . '" />',
                        ],
                        [
                            'id' => 'is_free_off',
                            'value' => 1,
                            'label' => '<img src="../img/admin/disabled.gif" alt="' . $this->trans('No', [], 'Admin.Global') . '" title="' . $this->trans('No', [], 'Admin.Global') . '" />',
                        ],
                    ],
                    'hint' => $this->trans('Apply both regular shipping cost and product-specific shipping costs.', [], 'Admin.Shipping.Help'),
                ],
                [
                    'type' => 'select',
                    'label' => $this->trans('Tax', [], 'Admin.Global'),
                    'name' => 'id_tax_rules_group',
                    'options' => [
                        'query' => TaxRulesGroup::getTaxRulesGroups(true),
                        'id' => 'id_tax_rules_group',
                        'name' => 'name',
                        'default' => [
                            'label' => $this->trans('No Tax', [], 'Admin.Global'),
                            'value' => 0,
                        ],
                    ],
                ],
                [
                    'type' => 'switch',
                    'label' => $this->trans('Shipping and handling', [], 'Admin.Shipping.Feature'),
                    'name' => 'shipping_handling',
                    'required' => false,
                    'class' => 't',
                    'is_bool' => true,
                    'values' => [
                        [
                            'id' => 'shipping_handling_on',
                            'value' => 1,
                            'label' => $this->trans('Enabled', [], 'Admin.Global'),
                        ],
                        [
                            'id' => 'shipping_handling_off',
                            'value' => 0,
                            'label' => $this->trans('Disabled', [], 'Admin.Global'),
                        ],
                    ],
                    'hint' => $this->trans('Include the shipping and handling costs in the carrier price.', [], 'Admin.Shipping.Help'),
                ],
                [
                    'type' => 'radio',
                    'label' => $this->trans('Billing', [], 'Admin.Shipping.Feature'),
                    'name' => 'shipping_method',
                    'required' => false,
                    'class' => 't',
                    'br' => true,
                    'values' => [
                        [
                            'id' => 'billing_default',
                            'value' => Carrier::SHIPPING_METHOD_DEFAULT,
                            'label' => $this->trans('Default behavior', [], 'Admin.Shipping.Feature'),
                        ],
                        [
                            'id' => 'billing_price',
                            'value' => Carrier::SHIPPING_METHOD_PRICE,
                            'label' => $this->trans('According to total price', [], 'Admin.Shipping.Feature'),
                        ],
                        [
                            'id' => 'billing_weight',
                            'value' => Carrier::SHIPPING_METHOD_WEIGHT,
                            'label' => $this->trans('According to total weight', [], 'Admin.Shipping.Feature'),
                        ],
                    ],
                ],
                [
                    'type' => 'select',
                    'label' => $this->trans('Out-of-range behavior', [], 'Admin.Shipping.Feature'),
                    'name' => 'range_behavior',
                    'options' => [
                        'query' => [
                            [
                                'id' => 0,
                                'name' => $this->trans('Apply the cost of the highest defined range', [], 'Admin.Shipping.Help'),
                            ],
                            [
                                'id' => 1,
                                'name' => $this->trans('Disable carrier', [], 'Admin.Shipping.Feature'),
                            ],
                        ],
                        'id' => 'id',
                        'name' => 'name',
                    ],
                    'hint' => $this->trans('Out-of-range behavior occurs when none is defined (e.g. when a customer\'s cart weight is greater than the highest range limit).', [], 'Admin.Shipping.Help'),
                ],
                [
                    'type' => 'text',
                    'label' => $this->trans('Maximum package height', [], 'Admin.Shipping.Feature'),
                    'name' => 'max_height',
                    'required' => false,
                    'hint' => $this->trans('Maximum height managed by this carrier. Set the value to "0," or leave this field blank to ignore.', [], 'Admin.Shipping.Help'),
                ],
                [
                    'type' => 'text',
                    'label' => $this->trans('Maximum package width', [], 'Admin.Shipping.Feature'),
                    'name' => 'max_width',
                    'required' => false,
                    'hint' => $this->trans('Maximum width managed by this carrier. Set the value to "0," or leave this field blank to ignore.', [], 'Admin.Shipping.Help'),
                ],
                [
                    'type' => 'text',
                    'label' => $this->trans('Maximum package depth', [], 'Admin.Shipping.Feature'),
                    'name' => 'max_depth',
                    'required' => false,
                    'hint' => $this->trans('Maximum depth managed by this carrier. Set the value to "0," or leave this field blank to ignore.', [], 'Admin.Shipping.Help'),
                ],
                [
                    'type' => 'text',
                    'label' => $this->trans('Maximum package weight', [], 'Admin.Shipping.Feature'),
                    'name' => 'max_weight',
                    'required' => false,
                    'hint' => $this->trans('Maximum weight managed by this carrier. Set the value to "0," or leave this field blank to ignore.', [], 'Admin.Shipping.Help'),
                ],
                [
                    'type' => 'hidden',
                    'name' => 'is_module',
                ],
                [
                    'type' => 'hidden',
                    'name' => 'external_module_name',
                ],
                [
                    'type' => 'hidden',
                    'name' => 'shipping_external',
                ],
                [
                    'type' => 'hidden',
                    'name' => 'need_range',
                ],
            ],
        ];

        if (Shop::isFeatureActive()) {
            $this->fields_form['input'][] = [
                'type' => 'shop',
                'label' => $this->trans('Shop association', [], 'Admin.Global'),
                'name' => 'checkBoxShopAsso',
            ];
        }

        $this->fields_form['submit'] = [
            'title' => $this->trans('Save', [], 'Admin.Actions'),
        ];

        if (!($obj = $this->loadObject(true))) {
            return;
        }

        $this->getFieldsValues($obj);

        return AdminController::renderForm();
    }

}
