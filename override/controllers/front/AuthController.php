<?php
/**
 * Copyright since 2007 PrestaShop SA and Contributors
 * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.md.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/OSL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://devdocs.prestashop.com/ for more information.
 *
 * @author    PrestaShop SA and Contributors <contact@prestashop.com>
 * @copyright Since 2007 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
 */
use PrestaShop\PrestaShop\Adapter\Presenter\Order\OrderPresenter;

class AuthController extends AuthControllerCore
{
    public function initContent()
    {
        $should_redirect = false;

        if (Tools::isSubmit('submitCreate')  	|| Tools::getValue('postCreateAccount')
         || Tools::isSubmit('create_account') 	|| Tools::getValue('create_account')) {
	        
			if(Tools::getValue('ajaxMode')) {
		        ob_end_clean();
		        header('Content-Type: application/json');
		        header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
			}
			
            $register_form = $this
                ->makeCustomerForm()
                ->setGuestAllowed(false)
                ->fillWith(Tools::getAllValues());

            if (Tools::isSubmit('submitCreate') || Tools::getValue('postCreateAccount')) {
                $hookResult = array_reduce(
                    Hook::exec('actionSubmitAccountBefore', [], null, true),
                    function ($carry, $item) {
                        return $carry && $item;
                    },
                    true
                );
                if ($hookResult && $register_form->submit()) {
                    $should_redirect = true;
                }
            }

			if(Tools::getValue('ajaxMode')) {
	            $this->context->smarty->assign($register_form->getTemplateVariables());
	            $this->context->smarty->assign([
		            'ajaxForm' 					=> 1
	            ]);
		        $jsonReturn = [
		            'registration_form' 		=> $this->render('customer/_partials/customer-form.tpl', ['ui' => $register_form->getProxy()]),
		        ];
		        if($should_redirect) {
			        $jsonReturn ['register_ok'] = true;

			        $url_to_reorder 					= ''; 	
			        $url_to_seeorder					= '';
			        $last_order_id						= ''; 
			        $last_order 						= [];   
					$customer_orders 					= Order::getCustomerOrders($this->context->customer->id);
					$this->context->customer->is_logged = true;
					if(is_array($customer_orders) && count(($customer_orders))) {
						$last_order 			= $customer_orders [0];
			            $order 					= new Order((int) $last_order['id_order']);
			            $url_to_reorder 		= $this->context->link->getPageLink('order', true, null, 'submitReorder=1&id_order=' . (int) $last_order['id_order']);
			            $url_to_seeorder 		= $this->context->link->getPageLink('order', true, null, 'ajax=1&simul=1&action=lastOrder&id_order=' . (int) $last_order['id_order']);
			            $last_order_id 			= $last_order['id_order'];
					}
		            $this->context->smarty->assign([
			            'url_to_reorder' 	=> $url_to_reorder,
			            'url_to_seeorder' 	=> $url_to_seeorder,
			            'lastOrder' 		=> $last_order,
			            'last_order_id'		=> $last_order_id,
			            'customer'			=> (array) $this->context->customer,
		            ]);
					$jsonReturn ['fast_order'] 	= $this->render('catalog/_partials/fast-order.tpl');
		        }
		        else {
			        $jsonReturn ['register_ok'] = false;
		        }
				echo Tools::jsonEncode($jsonReturn);
			}
			else {
	            $this->context->smarty->assign([
	                'register_form' => $register_form->getProxy(),
	                'hook_create_account_top' => Hook::exec('displayCustomerAccountFormTop'),
	            ]);
	            $this->setTemplate('customer/registration');
			}

        } else {
            $login_form = $this->makeLoginForm()->fillWith(
                Tools::getAllValues()
            );

            if (Tools::isSubmit('submitLogin')) {
                if ($login_form->submit()) {
                    $should_redirect = true;
                }
            }

            $this->context->smarty->assign([
                'login_form' => $login_form->getProxy(),
            ]);
            $this->setTemplate('customer/authentication');
        }

        FrontController::initContent();

        if ($should_redirect && !$this->ajax && !Tools::getValue('ajaxMode')) {
            $back = urldecode(Tools::getValue('back'));

            if (Tools::urlBelongsToShop($back)) {
                // Checks to see if "back" is a fully qualified
                // URL that is on OUR domain, with the right protocol
                return $this->redirectWithNotifications($back);
            }

            // Well we're not redirecting to a URL,
            // so...
            if ($this->authRedirection) {
                // We may need to go there if defined
                return $this->redirectWithNotifications($this->authRedirection);
            }

            // go home
            return $this->redirectWithNotifications(__PS_BASE_URI__);
        }
    }
}
