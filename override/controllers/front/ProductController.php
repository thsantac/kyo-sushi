<?php
/*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http:* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http:*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @license    http:*  International Registered Trademark & Property of PrestaShop SA
*/
use PrestaShop\PrestaShop\Adapter\Image\ImageRetriever;
use PrestaShop\PrestaShop\Adapter\Presenter\AbstractLazyArray;
use PrestaShop\PrestaShop\Adapter\Presenter\Product\ProductListingPresenter;
use PrestaShop\PrestaShop\Adapter\Product\PriceFormatter;
use PrestaShop\PrestaShop\Adapter\Product\ProductColorsRetriever;
use PrestaShop\PrestaShop\Core\Addon\Module\ModuleManagerBuilder;
use PrestaShop\PrestaShop\Core\Product\ProductExtraContentFinder;
use PrestaShop\PrestaShop\Core\Product\ProductInterface;

class ProductController extends ProductControllerCore
{
    /**
     * Initialize product controller.
     *
     * @see FrontController::init()
     */
    public function init()
    {
        FrontController::init();

        $id_product = (int) Tools::getValue('id_product');

        $this->setTemplate('catalog/product', [
            'entity' => 'product',
            'id' => $id_product,
        ]);

        if ($id_product) {
            $this->product = new Product($id_product, true, $this->context->language->id, $this->context->shop->id);
        }

        if (!Validate::isLoadedObject($this->product)) {
            Tools::redirect('index.php?controller=404');
        } else {
            $this->canonicalRedirection();
            /*
             * If the product is associated to the shop
             * and is active or not active but preview mode (need token + file_exists)
             * allow showing the product
             * In all the others cases => 404 "Product is no longer available"
             */
            $isAssociatedToProduct = (
                Tools::getValue('adtoken') == Tools::getAdminToken(
                    'AdminProducts'
                    . (int) Tab::getIdFromClassName('AdminProducts')
                    . (int) Tools::getValue('id_employee')
                )
                && $this->product->isAssociatedToShop()
            );
            
            $isPreview = ('1' === Tools::getValue('viewProduct'));
            
            if ((!$this->product->isAssociatedToShop() || !$this->product->active) && !$isPreview) {
                if ($isAssociatedToProduct) {
                    $this->adminNotifications['inactive_product'] = [
                        'type' => 'warning',
                        'message' => $this->trans('This product is not visible to your customers.', [], 'Shop.Notifications.Warning'),
                    ];
                } else {
                    if (!$this->product->id_type_redirected) {
                        if (in_array($this->product->redirect_type, [ProductInterface::REDIRECT_TYPE_CATEGORY_MOVED_PERMANENTLY, ProductInterface::REDIRECT_TYPE_CATEGORY_FOUND])) {
                            $this->product->id_type_redirected = $this->product->id_category_default;
                        } else {
                            $this->product->redirect_type = ProductInterface::REDIRECT_TYPE_NOT_FOUND;
                        }
                    } elseif (in_array($this->product->redirect_type, [ProductInterface::REDIRECT_TYPE_PRODUCT_MOVED_PERMANENTLY, ProductInterface::REDIRECT_TYPE_PRODUCT_FOUND]) && $this->product->id_type_redirected == $this->product->id) {
                        $this->product->redirect_type = ProductInterface::REDIRECT_TYPE_NOT_FOUND;
                    }

                    switch ($this->product->redirect_type) {
                        case ProductInterface::REDIRECT_TYPE_PRODUCT_MOVED_PERMANENTLY:
                            header('HTTP/1.1 301 Moved Permanently');
                            header('Location: ' . $this->context->link->getProductLink($this->product->id_type_redirected));
                            exit;

                        break;
                        case ProductInterface::REDIRECT_TYPE_PRODUCT_FOUND:
                            header('HTTP/1.1 302 Moved Temporarily');
                            header('Cache-Control: no-cache');
                            header('Location: ' . $this->context->link->getProductLink($this->product->id_type_redirected));
                            exit;

                        break;
                        case ProductInterface::REDIRECT_TYPE_CATEGORY_MOVED_PERMANENTLY:
                            header('HTTP/1.1 301 Moved Permanently');
                            header('Location: ' . $this->context->link->getCategoryLink($this->product->id_type_redirected));
                            exit;

                            break;
                        case ProductInterface::REDIRECT_TYPE_CATEGORY_FOUND:
                            header('HTTP/1.1 302 Moved Temporarily');
                            header('Cache-Control: no-cache');
                            header('Location: ' . $this->context->link->getCategoryLink($this->product->id_type_redirected));
                            exit;

                            break;
                        case ProductInterface::REDIRECT_TYPE_NOT_FOUND:
                        default:
                            header('HTTP/1.1 404 Not Found');
                            header('Status: 404 Not Found');
                            $this->errors[] = $this->trans('This product is no longer available.', [], 'Shop.Notifications.Error');
                            $this->setTemplate('errors/404');

                            break;
                    }
                }
            } elseif (!$this->product->checkAccess(isset($this->context->customer->id) && $this->context->customer->id ? (int) $this->context->customer->id : 0)) {
                header('HTTP/1.1 403 Forbidden');
                header('Status: 403 Forbidden');
                $this->errors[] = $this->trans('You do not have access to this product.', [], 'Shop.Notifications.Error');
                $this->setTemplate('errors/forbidden');
            } else {
                if ($isAssociatedToProduct && $isPreview) {
                    $this->adminNotifications['inactive_product'] = [
                        'type' => 'warning',
                        'message' => $this->trans('This product is not visible to your customers.', [], 'Shop.Notifications.Warning'),
                    ];
                }
                // Load category
                $id_category = false;
                if (isset($_SERVER['HTTP_REFERER']) && $_SERVER['HTTP_REFERER'] == Tools::secureReferrer($_SERVER['HTTP_REFERER']) // Assure us the previous page was one of the shop
                    && preg_match('~^.*(?<!\/content)\/([0-9]+)\-(.*[^\.])|(.*)id_(category|product)=([0-9]+)(.*)$~', $_SERVER['HTTP_REFERER'], $regs)) {
                    // If the previous page was a category and is a parent category of the product use this category as parent category
                    $id_object = false;
                    if (isset($regs[1]) && is_numeric($regs[1])) {
                        $id_object = (int) $regs[1];
                    } elseif (isset($regs[5]) && is_numeric($regs[5])) {
                        $id_object = (int) $regs[5];
                    }
                    if ($id_object) {
                        $referers = [$_SERVER['HTTP_REFERER'], urldecode($_SERVER['HTTP_REFERER'])];
                        if (in_array($this->context->link->getCategoryLink($id_object), $referers)) {
                            $id_category = (int) $id_object;
                        } elseif (isset($this->context->cookie->last_visited_category) && (int) $this->context->cookie->last_visited_category && in_array($this->context->link->getProductLink($id_object), $referers)) {
                            $id_category = (int) $this->context->cookie->last_visited_category;
                        }
                    }
                }
                if (!$id_category || !Category::inShopStatic($id_category, $this->context->shop) || !Product::idIsOnCategoryId((int) $this->product->id, ['0' => ['id_category' => $id_category]])) {
                    $id_category = (int) $this->product->id_category_default;
                }
                $this->category = new Category((int) $id_category, (int) $this->context->cookie->id_lang);
                $moduleManagerBuilder = ModuleManagerBuilder::getInstance();
                $moduleManager = $moduleManagerBuilder->build();

                if (isset($this->context->cookie, $this->category->id_category) && !($moduleManager->isInstalled('ps_categorytree') && $moduleManager->isEnabled('ps_categorytree'))) {
                    $this->context->cookie->last_visited_category = (int) $this->category->id_category;
                }
            }
        }
    }

	/*
    * module: jscomposer
    * date: 2016-02-09 03:59:00
    * version: 4.3.13
    */
    /*
    * module: jscomposer
    * date: 2020-12-08 08:48:48
    * version: 4.4.7
    */
    public function display()
	{
            if (Module::isInstalled('jscomposer') && (bool) Module::isEnabled('jscomposer'))
            {
                   $this->product->description = JsComposer::do_shortcode( $this->product->description );
            }
            if (Module::isInstalled('smartshortcode') && (bool) Module::isEnabled('smartshortcode'))
            {
                   $this->product->description = smartshortcode::do_shortcode( $this->product->description );
            }
            return parent::display();
	}

    public function getBreadcrumbLinks()
    {
        $breadcrumb = FrontController::getBreadcrumbLinks();

        $categoryDefault = new Category($this->product->id_category_default, $this->context->language->id);

        foreach ($categoryDefault->getAllParents() as $category) {
            if ($category->id_parent != 0) {
                $breadcrumb['links'][] = $this->getCategoryPath($category);
            }
        }

        $breadcrumb['links'][] = [
            'title' => $this->product->name,
            'url' => $this->context->link->getProductLink($this->product, null, null, null, null, null, (int) $this->getIdProductAttributeByRequest()),
        ];

        return $breadcrumb;
    }

    /**
     * Return id_product_attribute by id_product_attribute request parameter.
     *
     * @return int
     */
    protected function getIdProductAttributeByRequest()
    {
        $requestedIdProductAttribute = (int) Tools::getValue('id_product_attribute');

        return $this->tryToGetAvailableIdProductAttribute($requestedIdProductAttribute);
    }

    /**
     * If the PS_DISP_UNAVAILABLE_ATTR functionality is enabled, this method check
     * if $checkedIdProductAttribute is available.
     * If not try to return the first available attribute, if none are available
     * simply returns the input.
     *
     * @param int $checkedIdProductAttribute
     *
     * @return int
     */
    protected function tryToGetAvailableIdProductAttribute($checkedIdProductAttribute)
    {
        if (!Configuration::get('PS_DISP_UNAVAILABLE_ATTR')) {
            $availableProductAttributes = $this->product->getAttributeCombinations();
            if (!Product::isAvailableWhenOutOfStock($this->product->out_of_stock)) {
                $availableProductAttributes = array_filter(
                    $availableProductAttributes,
                    function ($elem) {
                        return $elem['quantity'] > 0;
                    }
                );
            }
            $availableProductAttribute = array_filter(
                $availableProductAttributes,
                function ($elem) use ($checkedIdProductAttribute) {
                    return $elem['id_product_attribute'] == $checkedIdProductAttribute;
                }
            );

            if (empty($availableProductAttribute) && count($availableProductAttributes)) {
                return (int) array_shift($availableProductAttributes)['id_product_attribute'];
            }
        }

        return $checkedIdProductAttribute;
    }

    public function displayAjaxViewproduct() 
	{
        $productForTemplate = $this->getTemplateVarProduct();
        ob_end_clean();
        header('Content-Type: application/json');

        $this->setQuickViewMode();

        $this->ajaxRender(Tools::jsonEncode([
            'quickview_html' => $this->render(
                'catalog/_partials/quickview',
                $productForTemplate instanceof AbstractLazyArray ?
                $productForTemplate->jsonSerialize() :
                $productForTemplate
            ),
            'product' => $productForTemplate,
        ]));
	}
}
