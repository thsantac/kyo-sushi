<?php
/*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http:* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http:*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @license    http:*  International Registered Trademark & Property of PrestaShop SA
*/
class CmsController extends CmsControllerCore
{
    /**
     * Assign template vars related to page content.
     *
     * @see FrontController::initContent()
     */
    public function initContent()
    {
        if ($this->assignCase == 1) {
            $cmsVar = $this->objectPresenter->present($this->cms);

            $filteredCmsContent = Hook::exec(
                'filterCmsContent',
                ['object' => $cmsVar],
                $id_module = null,
                $array_return = false,
                $check_exceptions = true,
                $use_push = false,
                $id_shop = null,
                $chain = true
            );
            if (!empty($filteredCmsContent['object'])) {
                $cmsVar = $filteredCmsContent['object'];
            }

            $this->context->smarty->assign([
                'cms' => $cmsVar,
            ]);

			// #TS - 20160420 -------------------------------------------------------------------------
			if($this->cms->link_rewrite == "livraison") {
				if($this->context->shop->frais_par_zone) {
					$sql 						= '
						SELECT bss.*, bs.*, gl.name as group_name FROM `' . _DB_PREFIX_ . 'bestkit_shippingmatrix_shop` bss
						LEFT JOIN `' . _DB_PREFIX_ . 'bestkit_shippingmatrix` bs ON bss.`id_bestkit_shippingmatrix` = bs.`id_bestkit_shippingmatrix`
						LEFT JOIN `' . _DB_PREFIX_ . 'bestkit_shippingmatrix_group` bsg ON bsg.id_bestkit_shippingmatrix = bs.id_bestkit_shippingmatrix
						LEFT JOIN `' . _DB_PREFIX_ . 'group_lang` gl ON gl.id_group = bsg.id_group
						WHERE bss.`id_shop` =' . $this->context->shop->id . '
						AND bsg.id_group = 1
						ORDER BY bs.`km_from` ASC, bs.`price_from` ASC
						';
				}
				else {
					$sql 						= '
						SELECT bss.*, bs.*, gl.name as group_name FROM `' . _DB_PREFIX_ . 'bestkit_shippingmatrix_shop` bss
						LEFT JOIN `' . _DB_PREFIX_ . 'bestkit_shippingmatrix` bs ON bss.`id_bestkit_shippingmatrix` = bs.`id_bestkit_shippingmatrix`
						LEFT JOIN `' . _DB_PREFIX_ . 'bestkit_shippingmatrix_group` bsg ON bsg.id_bestkit_shippingmatrix = bs.id_bestkit_shippingmatrix
						LEFT JOIN `' . _DB_PREFIX_ . 'group_lang` gl ON gl.id_group = bsg.id_group
						WHERE bss.`id_shop` =' . $this->context->shop->id . '
						AND bsg.id_group = 1
						ORDER BY bs.`zone_name` ASC, bs.`price_from` ASC
						';
				}
				$shipping_rates 				= Db::getInstance()->ExecuteS($sql);
				//print_r($shipping_rates);
				if (count($shipping_rates)) {
					foreach($shipping_rates as &$shipping_rate) {
						//echo "shipping_price=".$shipping_rate['shipping_price']."<br>";
						if(strpos($shipping_rate['shipping_price'], "%")===false) 
							$shipping_rate['shipping_price']	= number_format($shipping_rate['shipping_price'], 2, ",", ".")." €";
					}
		            $this->context->smarty->assign(array(
		                'shipping_rates' => $shipping_rates,
		                'frais_par_zone' => (int)$this->context->shop->frais_par_zone,
		            ));
		        }
			}

            if ($this->cms->indexation == 0) {
                $this->context->smarty->assign('nobots', true);
            }

            $this->setTemplate(
                'cms/page',
                ['entity' => 'cms', 'id' => $this->cms->id]
            );
        } elseif ($this->assignCase == 2) {
            $cmsCategoryVar = $this->getTemplateVarCategoryCms();

            $filteredCmsCategoryContent = Hook::exec(
                'filterCmsCategoryContent',
                ['object' => $cmsCategoryVar],
                $id_module = null,
                $array_return = false,
                $check_exceptions = true,
                $use_push = false,
                $id_shop = null,
                $chain = true
            );
            if (!empty($filteredCmsCategoryContent['object'])) {
                $cmsCategoryVar = $filteredCmsCategoryContent['object'];
            }

            $this->context->smarty->assign($cmsCategoryVar);
            $this->setTemplate('cms/category');
        }
        parent::initContent();
    }

	/*
    * module: jscomposer
    * date: 2016-02-09 03:58:59
    * version: 4.3.13
    */
    /*
    * module: jscomposer
    * date: 2020-12-08 08:48:47
    * version: 4.4.7
    */
    public function display()
	{
  	    if (Module::isInstalled('jscomposer') && (bool) Module::isEnabled('jscomposer'))
  	    {
            if(isset($this->cms->content)){
  	           $this->cms->content = JsComposer::do_shortcode( $this->cms->content );
  	           if(vc_mode() === 'page_editable'){
                    $this->cms->content = call_user_func(JsComposer::$front_editor_actions['vc_content'],$this->cms->content);
               }
            }
  	    }
  	    if (Module::isInstalled('smartshortcode') && (bool) Module::isEnabled('smartshortcode'))
  	    {
            if(isset($this->cms->content)){
  	           $this->cms->content = smartshortcode::do_shortcode( $this->cms->content );
            }
  	    }
        return parent::display();
	}
}
