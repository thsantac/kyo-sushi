<?php
use PrestaShop\PrestaShop\Adapter\Presenter\Cart\CartPresenter;
use PrestaShop\PrestaShop\Adapter\Product\PriceFormatter;

class CartController extends CartControllerCore
{
    /**
     * Initialize cart controller.
     *
     * @see FrontController::init()
     */
    public function init()
    {
        FrontController::init();

        // Send noindex to avoid ghost carts by bots
        header('X-Robots-Tag: noindex, nofollow', true);

        // Get page main parameters
        $this->id_product = (int) Tools::getValue('id_product', null);
        $this->id_product_attribute = (int) Tools::getValue('id_product_attribute', Tools::getValue('ipa'));
        $this->customization_id = (int) Tools::getValue('id_customization');
        $this->qty = abs(Tools::getValue('qty', 1));
        $this->id_address_delivery = (int) Tools::getValue('id_address_delivery');
        $this->preview = ('1' === Tools::getValue('preview'));

        /* Check if the products in the cart are available */
        if ('show' === Tools::getValue('action')) {

            $isAvailable = $this->areProductsAvailable();
            if (Tools::getIsset('checkout')) {
                return Tools::redirect($this->context->link->getPageLink('order'));
            }
            if (true !== $isAvailable) {
                $this->errors[] = $isAvailable;
            }
        }
    }

    /**
     * @see FrontController::initContent()
     */
    public function initContent()
    {
        if (Configuration::isCatalogMode() && Tools::getValue('action') === 'show') {
            Tools::redirect('index.php');
        }

        /*
         * Check that minimal quantity conditions are respected for each product in the cart
         * (this is to be applied only on page load, not for ajax calls)
         */
        if (!Tools::getValue('ajax')) {
            $this->checkCartProductsMinimalQuantities();
        }
        $presenter 							= new CartPresenter();
        $presented_cart 					= $presenter->present($this->context->cart, $shouldSeparateGifts = true);
        $cartHasNotReachedMinimumAmount 	= false;
		$complements 						= [];
		$elements							= [];
		
        if (count($presented_cart['products']) > 0) {
	        $cartHasReachedMinimumAmount 	= $this->checkMinimumAmount();
	        
	        $cartHasReachedMinimumAmount	= true; // Patch pour éviter les blocages : TS - 02/03/2021
	        
	        if (true !== $cartHasReachedMinimumAmount) {
	            $this->errors[] 				= $cartHasReachedMinimumAmount;
	            $cartHasNotReachedMinimumAmount = true;
	        }
		
	        $complements						= $this->context->cart->getCartComplements();
	        $complements['nb_max_baguettes']	= $this->context->cart->getCartMaxBaguettes();
	
	        $elements							= $this->context->cart->getUnwantedEements();
		}

        $this->context->smarty->assign([
            'cart' => $presented_cart,
            'cartHasNotReachedMinimumAmount' => $cartHasNotReachedMinimumAmount,
            'static_token' => Tools::getToken(false),
            'complements' => $complements,
            'elements' => $elements,
        ]);

        if (count($presented_cart['products']) > 0) {
            $this->setTemplate('checkout/cart');
        } else {
            $this->context->smarty->assign([
                'allProductsLink' => $this->context->link->getCategoryLink(Configuration::get('PS_HOME_CATEGORY')),
            ]);
            $this->setTemplate('checkout/cart-empty');
        }
        FrontController::initContent();
    }

	public function checkMinimumAmount() {

		if($this->context->delivery_mode == Context::TO_BE_DELIVERED) {
	        $taxConfiguration 					= new TaxConfiguration();
			$mt_min_order 						= Shop::getMinimumCommande();
	        $totalCartAmount 					= $this->context->cart->getOrderTotal(true, Cart::ONLY_PRODUCTS);
			if($totalCartAmount >= $mt_min_order)
				return true;
			
	        $priceFormatter 					= new PriceFormatter();
	        $amount 							= $priceFormatter->format($mt_min_order);
	        
	        $error 								= '';
	        if($this->context->cart->id_address_delivery)
		        $error 							= 'Cette commande n\'a pas atteint le montant minimum requis pour une livraison à cette adresse ! Merci de compléter votre panier ou de sélectionner une autre adresse.';
		    else {
		        $error 							= 'Cette commande n\'a pas atteint le montant minimum requis pour une livraison ! Merci de compléter votre panier.';
		    }
		    $error 							   .= '<br><i class=\'fa fa-angle-right\'></i> <a class=\'in_popup\' href=\'/content/1-livraison\'>En savoir plus sur nos zones de livraison et montant minimum de commande</a>';
		    return $error;
		}
		else
			return true;
	}

    public function displayAjax()
    {
        if ($this->errors) {
            $this->ajaxDie(Tools::jsonEncode(array('hasError' => true, 'errors' => $this->errors)));
        }

        if (Tools::getIsset('getHours')) {
			$day 							= urldecode(Tools::getValue('day'));
			$today 							= false;
			if($day == "Aujourd'hui")	{
				$day 						= date("Y-m-d");
			}
			else {
				$day 						= str_replace("Le ", "", $day);
				$day 						= substr($day, 6, 4)."-".substr($day, 3, 2)."-".substr($day, 0, 2);
			}
			
			$heures_possibles				= Shop::getAvailableTimes($day);
            $this->ajaxDie(Tools::jsonEncode($heures_possibles));
        }
    }

    public function displayAjaxUpdateComplements()
    {
	    $this->context->cart->updateComplements();
        $this->ajaxDie(Tools::jsonEncode(['success' => 1]));
    }

    public function displayAjaxUpdateElements()
    {
	    $this->context->cart->updateElements();
        $this->ajaxDie(Tools::jsonEncode(['success' => 1]));
    }

    public function displayAjaxRefresh()
    {
        if (Configuration::isCatalogMode()) {
            return;
        }

        ob_end_clean();
        header('Content-Type: application/json');
        $this->ajaxRender(Tools::jsonEncode([
            'cart_detailed' => $this->render('checkout/_partials/cart-detailed'),
            'cart_detailed_totals' => $this->render('checkout/_partials/cart-detailed-totals'),
            'cart_summary_items_subtotal' => $this->render('checkout/_partials/cart-summary-items-subtotal'),
            'cart_summary_subtotals_container' => $this->render('checkout/_partials/cart-summary-subtotals'),
            'cart_summary_totals' => $this->render('checkout/_partials/cart-summary-totals'),
            'cart_detailed_actions' => $this->render('checkout/_partials/cart-detailed-actions'),
            'cart_bottom_actions' => $this->render('checkout/_partials/cart-bottom-actions'),
            'cart_voucher' => $this->render('checkout/_partials/cart-voucher'),
            'cart_complements' => $this->render('checkout/_partials/cart-complements'),
        ]));
    }

    public function displayAjaxUpdate()
    {
        if (Configuration::isCatalogMode()) {
            return;
        }

        $productsInCart = $this->context->cart->getProducts();
        $updatedProducts = array_filter($productsInCart, [$this, 'productInCartMatchesCriteria']);
        $updatedProduct = reset($updatedProducts);
        $productQuantity = $updatedProduct['quantity'];

        if (!$this->errors) {
            $cartPresenter = new CartPresenter();
            $presentedCart = $cartPresenter->present($this->context->cart);

            // filter product output
            $presentedCart['products'] = $this->get('prestashop.core.filter.front_end_object.product_collection')
                ->filter($presentedCart['products']);

            $this->ajaxRender(Tools::jsonEncode([
                'success' => true,
                'id_product' => $this->id_product,
                'id_product_attribute' => $this->id_product_attribute,
                'id_customization' => $this->customization_id,
                'quantity' => $productQuantity,
                'cart' => $presentedCart,
                'errors' => empty($this->updateOperationError) ? '' : reset($this->updateOperationError),
            ]));

            return;
        } else {
            $this->ajaxRender(Tools::jsonEncode([
                'hasError' => true,
                'errors' => $this->errors,
                'quantity' => $productQuantity,
            ]));

            return;
        }
    }

    /**
     * Check product quantity availability to acknowledge whether
     * an availability error should be raised.
     *
     * If shop has been configured to oversell, answer is no.
     * If there is no items available (no stock), answer is yes.
     * If there is items available, but the Cart already contains more than the quantity,
     * answer is yes.
     *
     * @param Product $product
     * @param int $qtyToCheck
     *
     * @return bool
     */
    protected function shouldAvailabilityErrorBeRaised($product, $qtyToCheck)
    {
        return false;
    }

    /**
     * Check that minimal quantity conditions are respected for each product in the cart
     */
    protected function checkCartProductsMinimalQuantities()
    {
        $productList = $this->context->cart->getProducts();

        foreach ($productList as $product) {
            if ($product['minimal_quantity'] > $product['cart_quantity']) {
                // display minimal quantity warning error message
                $this->errors[] = $this->trans(
                    'The minimum purchase order quantity for the product %product% is %quantity%.',
                    [
                        '%product%' => $product['name'],
                        '%quantity%' => $product['minimal_quantity'],
                    ],
                    'Shop.Notifications.Error'
                );
            }
        }
    }
}
