<?php
/**
 * Copyright since 2007 PrestaShop SA and Contributors
 * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.md.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/OSL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://devdocs.prestashop.com/ for more information.
 *
 * @author    PrestaShop SA and Contributors <contact@prestashop.com>
 * @copyright Since 2007 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
 */
use PrestaShop\PrestaShop\Adapter\Product\PriceFormatter;
use PrestaShop\PrestaShop\Core\Foundation\Templating\RenderableProxy;
use PrestaShop\PrestaShop\Adapter\Presenter\Cart\CartPresenter;

class OrderController extends OrderControllerCore
{
    public function initContent()
    {
		if(Tools::getValue('action')=='lastOrder') {
	        FrontController::initContent();
			return;
		}

        if (Configuration::isCatalogMode()) {
            Tools::redirect('index.php');
        }

        $this->restorePersistedData($this->checkoutProcess);
        $this->checkoutProcess->handleRequest(
            Tools::getAllValues()
        );

        $presentedCart = $this->cart_presenter->present($this->context->cart);

        if (count($presentedCart['products']) <= 0 || $presentedCart['minimalPurchaseRequired']) {
            // if there is no product in current cart, redirect to cart page
            $cartLink = $this->context->link->getPageLink('cart');
            Tools::redirect($cartLink);
        }

        $product = $this->context->cart->checkQuantities(true);
        if (is_array($product)) {
            // if there is an issue with product quantities, redirect to cart page
            $cartLink = $this->context->link->getPageLink('cart', null, null, ['action' => 'show']);
            Tools::redirect($cartLink);
        }

        $this->checkoutProcess
            ->setNextStepReachable()
            ->markCurrentStep()
            ->invalidateAllStepsAfterCurrent();

        $this->saveDataToPersist($this->checkoutProcess);

        if (!$this->checkoutProcess->hasErrors()) {
            if ($_SERVER['REQUEST_METHOD'] !== 'GET' && !$this->ajax) {
                return $this->redirectWithNotifications(
                    $this->checkoutProcess->getCheckoutSession()->getCheckoutURL()
                );
            }
        }

        $complements						= $this->context->cart->getCartComplements();
        $elements							= $this->context->cart->getUnwantedEements();

        $this->context->smarty->assign([
            'checkout_process' => new RenderableProxy($this->checkoutProcess),
            'cart' => $presentedCart,
            'complements' => $complements,
            'elements' => $elements,
        ]);

        $this->context->smarty->assign([
            'display_transaction_updated_info' => Tools::getIsset('updatedTransaction'),
        ]);
        
        $ask_for_delivery_mode	= 0;
        if($this->context->delivery_mode != 1 && $this->context->delivery_mode != 2) {
	        $ask_for_delivery_mode	= 1;
        }
        $this->context->cart->checkDeliveryMode();

        $this->context->smarty->assign([
            'ask_for_delivery_mode' => $ask_for_delivery_mode,
        ]);

        FrontController::initContent();
        $this->setTemplate('checkout/checkout');
    }

    public function postProcess()
    {
        FrontController::postProcess();

        if ((Tools::isSubmit('submitReorder') || Tools::getValue('submitReorder'))
            && $this->context->customer->isLogged()
            && $id_order = (int) Tools::getValue('id_order')
        ) {
            $oldCart = new Cart(Order::getCartIdStatic($id_order, $this->context->customer->id));
            $duplication = $oldCart->duplicate();
            if (!$duplication || !Validate::isLoadedObject($duplication['cart'])) {
                $this->errors[] = $this->trans('Sorry. We cannot renew your order.', [], 'Shop.Notifications.Error');
            } elseif (!$duplication['success']) {
                $this->errors[] = $this->trans(
                    'Some items are no longer available, and we are unable to renew your order.',
                    [],
                    'Shop.Notifications.Error'
                );
            } else {
                $products 				= $duplication['cart']->getProducts();
                foreach ($products as $product) {
                    $currentProduct 	= new Product($product['id_product']);
                    if(!$currentProduct->active) {
        	            $duplication['cart']->deleteProduct($product['id_product'], $product['id_product_attribute'], $product['id_customization'], $duplication['cart']->id_address_delivery);
                    }
                }
                $this->context->cookie->id_cart = $duplication['cart']->id;
                $context = $this->context;
                $context->cart = $duplication['cart'];
                CartRule::autoAddToCart($context);
                $this->context->cookie->write();
                Tools::redirect('index.php?controller=order');
            }
        }

		if(Tools::getValue('action')=='lastOrder') {
			return;
		}

        $this->bootstrap();
    }

    public function displayAjaxLastOrder()
    {
	    $id_order				= Tools::getValue('id_order');
        $lastCart 				= new Cart(Order::getCartIdStatic($id_order, $this->context->customer->id));
        $lastCart->removeCartRules();
        
        $products 				= $lastCart->getProducts();

        foreach ($products as $product) {
            $currentProduct 	= new Product($product['id_product']);
            if(!$currentProduct->active) {
	            $lastCart->deleteProduct($product['id_product'], $product['id_product_attribute'], $product['id_customization'], $lastCart->id_address_delivery);
            }
        }

        $presenter 				= new CartPresenter();
        $presented_cart 		= $presenter->present($lastCart, $shouldSeparateGifts = true);

        ob_end_clean();
        header('Content-Type: application/json');
        $this->ajaxRender(Tools::jsonEncode([
            'preview' => $this->render('checkout/_partials/cart-summary', [
                'cart' => $presented_cart,
                'static_token' => Tools::getToken(false),
            ]),
        ]));
    }

}
