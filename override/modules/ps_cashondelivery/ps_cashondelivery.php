<?php
/**
 * Copyright since 2007 PrestaShop SA and Contributors
 * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License version 3.0
 * that is bundled with this package in the file LICENSE.md.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * @author    PrestaShop SA and Contributors <contact@prestashop.com>
 * @copyright Since 2007 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License version 3.0
 */
if (!defined('_PS_VERSION_')) {
    exit;
}

if (file_exists(__DIR__ . '/vendor/autoload.php')) {
    require_once __DIR__ . '/vendor/autoload.php';
}

use PrestaShop\PrestaShop\Core\Payment\PaymentOption;

class Ps_CashondeliveryOverride extends Ps_Cashondelivery
{
    const HOOKS = [
        'displayOrderConfirmation',
        'paymentOptions',
    ];

    const CONFIG_OS_CASH_ON_DELIVERY = 'PS_OS_COD_VALIDATION';

    public function __construct()
    {
        $this->name = 'ps_cashondelivery';
        $this->tab = 'payments_gateways';
        $this->author = 'PrestaShop';
        $this->version = '2.0.1';
        $this->need_instance = 1;
        $this->ps_versions_compliancy = ['min' => '1.7.6.0', 'max' => _PS_VERSION_];
        $this->controllers = ['validation'];
        $this->currencies = false;

        PaymentModule::__construct();

        $this->displayName = $this->trans('Paiement espèces, Tickets Resto, Chèques Déjeuner et Chèques Vacances à la livraison', [], 'Modules.Cashondelivery.Admin');
        $this->description = $this->trans('Accept cash payments on delivery to make it easy for customers to purchase on your store.', [], 'Modules.Cashondelivery.Admin');
    }

    /**
     * @param array{cookie: Cookie, cart: Cart, altern: int} $params
     *
     * @return array|PaymentOption[] Should always returns an array to avoid issue
     */
    public function hookPaymentOptions(array $params)
    {
        if (empty($params['cart'])) {
            return [];
        }

        /** @var Cart $cart */
        $cart = $params['cart'];

        if ($cart->isVirtualCart()) {
            return [];
        }

        $cashOnDeliveryOption = new PaymentOption();
        $cashOnDeliveryOption->setModuleName($this->name);
        $cashOnDeliveryOption->setCallToActionText($this->trans('Paiement espèces, Tickets Resto, Chèques Déjeuner et Chèques Vacances à la livraison', [], 'Modules.Cashondelivery.Shop'));
        $cashOnDeliveryOption->setAction($this->context->link->getModuleLink($this->name, 'validation', [], true));
        $cashOnDeliveryOption->setAdditionalInformation($this->fetch('module:ps_cashondelivery/views/templates/hook/paymentOptions-additionalInformation.tpl'));

        return [$cashOnDeliveryOption];
    }

    /**
     * @param array{cookie: Cookie, cart: Cart, altern: int, order: Order, objOrder: Order} $params
     *
     * @return string
     */
    public function hookDisplayOrderConfirmation(array $params)
    {
        /** @var Order $order */
        $order = (isset($params['objOrder'])) ? $params['objOrder'] : $params['order'];

        if (!Validate::isLoadedObject($order) || $order->module !== $this->name) {
            return '';
        }

        $this->context->smarty->assign([
            'shop_name' => $this->context->shop->name,
            'total' => $this->context->getCurrentLocale()->formatPrice($params['order']->getOrdersTotalPaid(), (new Currency($params['order']->id_currency))->iso_code),
            'reference' => $order->reference,
            'contact_url' => $this->context->link->getPageLink('contact', true),
        ]);

        return $this->fetch('module:ps_cashondelivery/views/templates/hook/displayOrderConfirmation.tpl');
    }

}
