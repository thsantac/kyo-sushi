<?php
/*
 * 2007-2016 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author PrestaShop SA <contact@prestashop.com>
 *  @copyright  2007-2016 PrestaShop SA
 *  @version  Release: $Revision: 7060 $
 *  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 *  International Registered Trademark & Property of PrestaShop SA
 */

class HTMLBlocksOverride extends HTMLBlocks
{
    public $htmlBlockPresenter;
    public $htmlBlockRepository;

    public function __construct()
    {
        $this->name = 'htmlblocks';
        $this->author = 'Marek Rost, PrestaShop';
        $this->version = '0.0.1';
        $this->need_instance = 0;

        $this->bootstrap = true;
        parent::__construct();

        $this->displayName = $this->trans('HTML Blocks', array(), 'Modules.HTMLBlocks');
        $this->description = $this->trans('Adds blocks with custom HTML.', array(), 'Modules.HTMLBlocks');
        $this->secure_key = Tools::encrypt($this->name);

        $this->ps_versions_compliancy = array('min' => '1.7', 'max' => _PS_VERSION_);

        $this->htmlBlockPresenter = new HTMLBlockPresenter($this->context->language);
        $this->htmlBlockRepository = new HTMLBlockRepository(Db::getInstance(), $this->context->shop);
    }

    public function renderWidget($hookName, array $configuration)
    {
        $key = 'htmlblocks|' . $hookName;

        $this->templateFile = 'module:htmlblocks/views/templates/hook/htmlblock.tpl';
        if($hookName=="displayHomePub")
	        $this->templateFile = 'module:htmlblocks/views/templates/hook/home_pub.tpl';
        if (!$this->isCached($this->templateFile, $this->getCacheId($key))) {
            $this->smarty->assign($this->getWidgetVariables($hookName, $configuration));
        }

        return $this->fetch($this->templateFile, $this->getCacheId($key));
    }

    public function getWidgetVariables($hookName, array $configuration)
    {
        $id_hook = Hook::getIdByName($hookName);

        $htmlBlocks = $this->htmlBlockRepository->getByIdHook($id_hook, $this->context->shop->id);

        $blocks = array();
        $i = 0;
        foreach ($htmlBlocks as $block) {
            $blocks[$i]['content'] = $this->htmlBlockPresenter->present($block);
            $blocks[$i]['name'] = "hook-".$block->id_hook."-".$block->position;
            $i++;
        }

        return array(
            'htmlBlocks' => $blocks,
            'hookName' => $hookName,
        );
    }
}
