<?php
/**
 * 2007-2020 PrestaShop.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2020 PrestaShop SA
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 * International Registered Trademark & Property of PrestaShop SA
 */

/**
 * @since   1.5.0
 */

if (!defined('_PS_VERSION_')) {
    exit;
}

class Ps_ImageSliderOverride extends Ps_ImageSlider
{
    public function hookdisplayHeader($params)
    {
	    $slider_or_video = Configuration::get('LLW_KYO_SLIDER_OR_VIDEO', 1);
	    if($slider_or_video==1) {
	        $this->context->controller->registerStylesheet('modules-homeslider', 'modules/'.$this->name.'/css/homeslider.css', ['media' => 'all', 'priority' => 150]);
	        $this->context->controller->registerJavascript('modules-responsiveslides', 'modules/'.$this->name.'/js/responsiveslides.min.js', ['position' => 'bottom', 'priority' => 150]);
	        $this->context->controller->registerJavascript('modules-homeslider', 'modules/'.$this->name.'/js/homeslider.js', ['position' => 'bottom', 'priority' => 150]);
		}
    }

    public function renderWidget($hookName = null, array $configuration = [])
    {
        $this->smarty->assign($this->getWidgetVariables($hookName, $configuration));

	    $slider_or_video = Configuration::get('LLW_KYO_SLIDER_OR_VIDEO', 1);
	    if($slider_or_video==1) {
	        $this->templateFile = 'module:ps_imageslider/views/templates/hook/slider.tpl';
		}
		else {
	        $this->templateFile = 'module:ps_imageslider/views/templates/hook/video.tpl';
		}
        return $this->fetch($this->templateFile);
    }

    public function getWidgetVariables($hookName = null, array $configuration = [])
    {
	    $slider_or_video = Configuration::get('LLW_KYO_SLIDER_OR_VIDEO', 1);
	    if($slider_or_video==1) {
	        $slides = $this->getSlides(true);
	        if (is_array($slides)) {
	            foreach ($slides as &$slide) {
	                $slide['sizes'] = @getimagesize((__DIR__ . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . $slide['image']));
	                if (isset($slide['sizes'][3]) && $slide['sizes'][3]) {
	                    $slide['size'] = $slide['sizes'][3];
	                }
	            }
	        }
	
	        $config = $this->getConfigFieldsValues();
	
	        return [
	            'homeslider' => [
	                'speed' => $config['HOMESLIDER_SPEED'],
	                'pause' => $config['HOMESLIDER_PAUSE_ON_HOVER'] ? 'hover' : '',
	                'wrap' => $config['HOMESLIDER_WRAP'] ? 'true' : 'false',
	                'slides' => $slides,
	            ],
	        ];
		}
		else {
			return [
				'homeVideoSource' => Configuration::get('LLW_KYO_VIDEO_SOURCE')
			];
		}
    }

}
