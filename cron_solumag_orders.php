<?php
/*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

include('config/config.inc.php');
error_reporting(E_ALL);
ini_set('display_errors', 1);
echo "--> Démarrage...<br>";
// 0755a32a881609c2489e8210155d6b2e
if (isset($_GET['secure_key'])) {
	echo "secure_key=".md5(_COOKIE_KEY_.Configuration::get('PS_SHOP_NAME'))."<br>";
    $secureKey = md5(_COOKIE_KEY_.Configuration::get('PS_SHOP_NAME'));
    if (!empty($secureKey) && $secureKey === $_GET['secure_key']) {
		echo "--> Secure key OK !<br>";
	    
        $shop_ids = Shop::getCompleteListOfShopsID();
        foreach ($shop_ids as $shop_id) {
	        if($shop_id == $_GET['id_shop']) {
	            Shop::setContext(Shop::CONTEXT_SHOP, (int)$shop_id);
				$context 			= Context::getContext();
				if(isset($_GET['date']))
		        	$orders 		= Order::getCompleteListOfOrdersToSend($shop_id, $_GET['date']);
				else
		        	$orders 		= Order::getCompleteListOfOrdersToSend($shop_id);
		        echo "<br>--> shop_id $shop_id, nombre d'ordres à traiter = ".count($orders)."<br>";
		        foreach ($orders as $commande) {
			        $order 					= new Order($commande['id_order']);
		            $context->id_cart		= $order->id_cart;
		            $context->cart			= new Cart($order->id_cart);
			        $customer 				= new Customer($order->id_customer);
	                $id_address_invoice		= new Address((int)$order->id_address_invoice);
	                $id_address_delivery	= new Address((int)$order->id_address_delivery);
					$order->createCashRegisterFile($customer, $order, $id_address_invoice, $id_address_delivery);
					// Tempo pour éviter des fichiers avec le même nom ----------------------------------------------
					sleep(1);
				}
			}
        }
        
    }
}
