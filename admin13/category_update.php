<?php
/**
 * Copyright since 2007 PrestaShop SA and Contributors
 * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.md.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/OSL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://devdocs.prestashop.com/ for more information.
 *
 * @author    PrestaShop SA and Contributors <contact@prestashop.com>
 * @copyright Since 2007 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
 */

if (!defined('_PS_ADMIN_DIR_')) {
    define('_PS_ADMIN_DIR_', __DIR__);
}
include _PS_ADMIN_DIR_.'/../config/config.inc.php';

if (isset($_GET['checkDefaultCategory'])) {
	$context = Context::getContext();
    echo "Mise à jour des catégories des produits...<br>";
    $sql = 'SELECT * 
			FROM `' . _DB_PREFIX_ . 'product` p
			' . Shop::addSqlAssociation('product', 'p') . ' 
			WHERE product_shop.`id_shop` = ' . (int) $context->shop->id ;
	
    $result = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);
	foreach($result as $product) {
	    $sql = 'SELECT * 
				FROM `' . _DB_PREFIX_ . 'category` c
				WHERE c.`id_category` = ' . (int) $product['id_category_default'] ;
		$result_categ = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);
		if($result_categ[0]['level_depth'] > '2') {
			$sql = 'UPDATE 
					`' . _DB_PREFIX_ . 'product`
					SET id_category_default = ' . $result_categ[0]['id_parent'] . ' 
					WHERE `id_product` = ' . (int) $product['id_product'];
			Db::getInstance(_PS_USE_SQL_SLAVE_)->execute($sql);

			$sql = 'UPDATE 
					`' . _DB_PREFIX_ . 'product_shop`
					SET id_category_default = ' . $result_categ[0]['id_parent'] . ' 
					WHERE `id_product` = ' . (int) $product['id_product'] . ' AND `id_shop` = ' . (int) $context->shop->id ;
			Db::getInstance(_PS_USE_SQL_SLAVE_)->execute($sql);
			echo "Produit ".$product['id_product']." mis à jour <br>";
		}
	}
	die();
}
