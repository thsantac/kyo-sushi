<?php
/**
 * 2007-2014 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 *         DISCLAIMER   *
 * *************************************** */
/* Do not edit or add to this file if you wish to upgrade Prestashop to newer
* versions in the future.
* ****************************************************
*
*  @author     LA-LIGNE-WEB.COM (contact@la-ligne-web.com)
*  @copyright  http://www.la-ligne-web.com
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

class CalendarLLW extends ObjectModel
{			            
    public $id;
    public $id_calendar;
    public $day_name;
    public $day;
    public $day_from;
    public $day_to;
    public $hours;
    public $hours_delivery;

    public static $definition = array(
        'table' 				=> 'calendar_llw',
        'primary' 				=> 'id_calendar',
        'multilang' 			=> false,
        'multishop' 			=> true,
        'fields' 				=> array(
            'day_name' 			=> array('type' => self::TYPE_STRING),
            'day' 				=> array('type' => self::TYPE_STRING),
            'hours' 			=> array('type' => self::TYPE_STRING),
            'hours_delivery' 	=> array('type' => self::TYPE_STRING),
            'day_from'			=> array('type' => self::TYPE_STRING),
            'day_to'			=> array('type' => self::TYPE_STRING),
        ),
    );

    public function __construct($id_calendar = null, $id_lang = null, $id_shop = null)
    {
        parent::__construct($id_calendar, $id_lang, $id_shop);
    }

	public static function isFerie($day) {
		$context = Context::getContext();
		$id_shop = $context->shop->id;

		$sql = '
        	SELECT c.`id_calendar` 
        	FROM '._DB_PREFIX_.'calendar_llw c 
        	JOIN '._DB_PREFIX_.'calendar_llw_shop cs ON cs.`id_calendar` = c.`id_calendar` AND cs.`id_shop` = '.(int)$id_shop.' 
        	WHERE (c.`day` = \''.pSQL($day).'\' OR (c.`day_from` <= \''.pSQL($day).'\' AND c.`day_to` >= \''.pSQL($day).'\'))
            AND c.hours = "" AND c.hours_delivery = ""
        	';
        $id_calendar = Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue($sql);
        return (bool)$id_calendar;
	}

	public static function getIdByDay($day) {
		$context = Context::getContext();
		$id_shop = $context->shop->id;

        $query = new DbQuery();
        $query->select('c.id_calendar');
        $query->from('calendar_llw', 'c');
        $query->leftJoin('calendar_llw_shop', 'cls', 'cls.id_calendar = c.id_calendar AND cls.id_shop='.(int)$id_shop);
        $query->where('c.`day` = \''.pSQL($day).'\'');

        return Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue($query);
	}

	public static function getOpenHour($day) {

		$context = Context::getContext();
		$id_shop = $context->shop->id;

		$sql = '
        	SELECT c.`hours` 
        	FROM '._DB_PREFIX_.'calendar_llw c 
        	JOIN '._DB_PREFIX_.'calendar_llw_shop cs ON cs.`id_calendar` = c.`id_calendar` AND cs.`id_shop` = '.(int)$id_shop.' 
        	WHERE c.`day` = \''.pSQL($day).'\' 
        	';
        $hours = Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue($sql);

		$sql = '
        	SELECT c.`hours_delivery` 
        	FROM '._DB_PREFIX_.'calendar_llw c 
        	JOIN '._DB_PREFIX_.'calendar_llw_shop cs ON cs.`id_calendar` = c.`id_calendar` AND cs.`id_shop` = '.(int)$id_shop.' 
        	WHERE c.`day` = \''.pSQL($day).'\' 
        	';
        $hours_delivery = Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue($sql);
            
        return ['hours' => $hours, 'hours_delivery' => $hours_delivery];
	}

	//===================================================================================================
	// Récupération des heures d'ouverture paramétrées dans le magasin
	//===================================================================================================
	public static function getOpenDays() 
	{
		$hours					= Store::getStoreHours();

		$thisDay				= date("Y-m-d");
		$i 						= 1;
        $days 					= [];
        $days[1] 				= ""; // Lundi
        $days[2] 				= "";
        $days[3] 				= "";
        $days[4] 				= "";
        $days[5] 				= "";
        $days[6] 				= "";
        $days[7] 				= ""; // Dimanche
		for($i=1; $i<=7; $i++) {
			$isFerie			= CalendarLLW::isFerie($thisDay);
			$wn 				= date("N", mktime(0, 0, 0, substr($thisDay, 5, 2), substr($thisDay, 8, 2), substr($thisDay, 0, 4)));
			if($isFerie || empty($hours[$wn])) {
				$days[$wn]		= '';
			}
			else {
				$days[$wn]		= $hours[$wn];
			}
			$thisDay 			= date("Y-m-d", mktime(0, 0, 0, date("m"), date("d")+$i, date("Y")));
		}

		return $days;	
	}
	
	public static function getOpenDayHours() 
	{
		$i 						= 1;
		$i 						= 1;
		foreach($hours as $hour) {
			if(!(strpos($hour, '/')===false)) {
				$moments			= explode("/", $hour);
				$j 					= 0;
				foreach($moments as $moment) {
					$heures			= explode("-", $moment);
					$k				= 0;
					foreach($heures as $heure) {
						$heure 		= str_replace("h", ":", $heure);
						$heure 		= trim(str_replace("H", ":", $heure));
						$days[$i][$j][$k] = $heure;
						$k++;
					}
					$j++;
				}
			}
			else {
				$j 					= 0;
				$heures				= explode("-", $hour);
				$k					= 0;
				foreach($heures as $heure) {
					$heure 			= str_replace("h", ":", $heure);
					$heure 			= trim(str_replace("H", ":", $heure));
					if($k==0) {
						$days[$i][$j][0] = $heure;
						$days[$i][$j][1] = "12:00";
					}
					else {
						$days[$i][$j][0] = "12:00";
						$days[$i][$j][1] = $heure;
					}
					$k++;
				}
				$j++;
			}
			$i++;
		}	
		return $days;		
	}
}

