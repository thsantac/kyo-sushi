{**
 * Copyright since 2007 PrestaShop SA and Contributors
 * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.md.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/OSL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://devdocs.prestashop.com/ for more information.
 *
 * @author    PrestaShop SA and Contributors <contact@prestashop.com>
 * @copyright Since 2007 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
 *}
<tr>
	<th bgcolor="#FDFDFD" style="font-family: Open sans, Arial, sans-serif; font-size: 12px; background-color: #FDFDFD; color: #353943; font-weight: 600; padding: 10px 5px; border: 1px solid #DFDFDF;" colspan="5">Aliments indésirables</th>
</tr>
{if count($list)}
<tr>
	<td style="border:1px solid #D6D4D4;" colspan="5">
		<table class="table">
			<tr>
				<td width="5">&nbsp;</td>
				<td>
					<div class="alert alert-info">Vous n'avez déclaré aucun aliment indésirable.</div>
				</td>
				<td width="5">&nbsp;</td>
			</tr>
		</table>
	</td>
</tr>
{else}
{foreach from=$list key="filter_key" item="filter"}
	{if $filter.selected}
<tr>
	<td style="border:1px solid #D6D4D4;" colspan="5">
		<table class="table">
			<tr>
				<td width="5">&nbsp;</td>
				<td>
					<font size="2" face="Open-sans, sans-serif" color="#555454">
						{$filter.label}
					</font>
				</td>
				<td width="5">&nbsp;</td>
			</tr>
		</table>
	</td>
</tr>
	{/if}
{/foreach}
{/if}

