{**
 * Copyright since 2007 PrestaShop SA and Contributors
 * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.md.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://devdocs.prestashop.com/ for more information.
 *
 * @author    PrestaShop SA and Contributors <contact@prestashop.com>
 * @copyright Since 2007 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 *}
						<div class="col-xs-3 col-md-3">
							<div id="_desktop_user_info">
							  <div class="user-info">
							    
							      <a
							        class="account withToolTip"
							        href="{$my_account_url}"
							        title="{l s='View my customer account' d='Shop.Theme.Customeraccount'}"
							        rel="nofollow" 
								    id="customerLoggedLink" 
									style="display: {if $logged}inline-block{else}none{/if}"
							      >
							        <img src="{$urls.img_url}picto_header_account.png" class="animate__animated animate__pulse" alt="Mon compte" />
								    <span class="user_logged_flag"></span>
							      </a>

							      <a
							        href="{$my_account_url}"
							        title="{l s='Log in to your customer account' d='Shop.Theme.Customeraccount'}"
							        rel="nofollow"
							        title="{l s='Sign in' d='Shop.Theme.Actions'}"
							        class="withToolTip" 
								    id="customerLoginLink" 
									style="display: {if $logged}none{else}inline-block{/if}"
							      >
							        <img src="{$urls.img_url}picto_header_account.png" class="animate__animated animate__pulse" alt="Mon compte" />
							      </a>

							  </div>
							</div>
						</div>