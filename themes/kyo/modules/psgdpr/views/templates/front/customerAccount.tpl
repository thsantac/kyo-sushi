{**
 * 2007-2020 PrestaShop and Contributors
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2020 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}

 <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6 col-xs-12">
 <a class="border-animation" id="psgdpr-link" href="{$front_controller}">
     <div class=" border-animation__inner">
         <span class="link-item">
             <img src="{$urls.img_url}account-rgpd.png" />
             <span class="link-item-text">RGPD, {l s='GDPR - Personal data' mod='psgdpr'}</span>
         </span>
     </div>
 </a>
</div>
