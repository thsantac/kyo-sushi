$(document).ready(function() {
	var elemSideBar = $('<div id="js-cart-sidebar" class="cart-preview"></div>').insertAfter('main').wrapAll('<div class="sb-menu-right"></div>'),
		elemOverlay = $('<div class="sb-overlay"></div>').insertAfter('main'),
		elemCloseBtn = $('<div class="close"></div>');
	
	elemCloseBtn.prependTo('.sb-menu-right');
	
	$('#js-cart-sidebar').html($('.js-cart-source').html());
	
	$('#_desktop_cart').on('click', function(e) {
		$('html').addClass('sb-open');
		$('.sb-overlay ').fadeIn(500);
		$('#scrolltopbtn').hide();
		return false;
	});
	
	elemCloseBtn.on('click', function(e) {
		$('html').removeClass('sb-open');
		$('.sb-overlay ').fadeOut(500);
		$('#scrolltopbtn').show();
		return false;
	})
	
	elemOverlay.on('click', function(e) {
		$('html').removeClass('sb-open');
		$('.sb-overlay ').fadeOut(500);
		$('#scrolltopbtn').show();
		return false;
	})
	
	console.log("sidebarcart.js");

	prestashop.on('updateCart', function (event) {
		console.log("sidebarcart.js - on updateCart");
		var refreshURL = $('.blockcart').data('refresh-url');
		var requestData = {};
		var addedProductID = '';
		if (event && event.reason) {
			requestData = {
				id_product_attribute: event.reason.idProductAttribute,
				id_product: event.reason.idProduct,
				action: event.reason.linkAction
			};
			addedProductID = event.reason.idProduct; 
		}
		
		$.post(refreshURL, requestData).then(function (resp) {
			$('.blockcart .header').replaceWith($(resp.preview).find('.blockcart .header'));
			$('.blockcart .cart-dropdown-wrapper').replaceWith($(resp.preview).find('.blockcart .cart-dropdown-wrapper'));
			$('.blockcart').removeClass('inactive').addClass('active');

			if ($('.sb-menu-right').length) {
				$('#js-cart-sidebar .cart-dropdown-wrapper').replaceWith($(resp.preview).find('.blockcart .cart-dropdown-wrapper'));
				if (prestashop.page.page_name != 'cart' && prestashop.page.page_name != 'checkout') {
					if(requestData.action != "refresh") {
						$('html').addClass('sb-open');
						$('.sb-overlay ').fadeIn(500);
						$('#scrolltopbtn').hide();
					}
				}

				if($(".thumbnail-container").length) {
					$(".thumbnail-container").removeClass("in-cart");
					if($(".cart-items li").length) {
						$(".cart-items li").each(function() {
							var id_product = $(this).find("a.remove-from-cart").data("id-product");
							$("#thumbnail-container-"+id_product).addClass("in-cart");
						})
					}
				}
				
				if($("body#product").length) {
					if($(".cart-items li").length) {
						$(".cart-items li").each(function() {
							var id_product = $(this).find("a.remove-from-cart").data("id-product");
							if($("#product-cover-"+id_product).length) {
								$("#product-cover-"+id_product).addClass("in-cart");
							}
						})
					}
				}
				
				qtyButtons();				

				if(loadingDiv) {
					loadingDiv.hide();
				}
				
			} else {
				if (resp.modal) {
					console.dir(requestData);
					if(requestData.action != "refresh") {
						showModal(resp.modal);
					}
				}
			}
		}).fail(function (resp) {
			prestashop.emit('handleError', {eventType: 'updateShoppingCart', resp: resp});
		});
	});
});