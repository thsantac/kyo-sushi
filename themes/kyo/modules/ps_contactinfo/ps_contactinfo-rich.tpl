{**
 * Copyright since 2007 PrestaShop SA and Contributors
 * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.md.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://devdocs.prestashop.com/ for more information.
 *
 * @author    PrestaShop SA and Contributors <contact@prestashop.com>
 * @copyright Since 2007 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 *}

<div class="contact-rich">
	<h4>{l s='Store information' d='Shop.Theme.Global'}</h4>
    <div class="block row">
		<div class="col-xs-2 col-md-2">
			<div class="icon"><img src="{$urls.img_url}picto_contact_location.png" /></div>
		</div>
		<div class="col-xs-10 col-md-10">
			<h5>{$contact_infos.company}</h5>
			<div class="data">
				{$contact_infos.address.address1}<br>
				{$contact_infos.address.address2}<br>
				{$contact_infos.address.postcode} {$contact_infos.address.city}
			</div>
		</div>
    </div>
  {if $contact_infos.phone}
    <div class="block row">
		<div class="col-xs-2 col-md-2">
			<div class="icon"><img src="{$urls.img_url}picto_contact_phone.png" /></div>
		</div>
		<div class="col-xs-10 col-md-10">
			<h5>Appelez-nous</h5>
			<div class="data">
				{l s='Call us:' d='Shop.Theme.Global'}<br/>
				<a href="tel:{$contact_infos.phone}">{$contact_infos.phone}</a>
			</div>
		</div>
    </div>
  {/if}
  {if $contact_infos.email && $display_email}
    <div class="block row">
		<div class="col-xs-2 col-md-2">
			<div class="icon"><img src="{$urls.img_url}picto_contact_mail.png" /></div>
		</div>
		<div class="col-xs-10 col-md-10">
			<h5>Écrivez-nous</h5>
			<div class="data email">
				{l s='Email us:' d='Shop.Theme.Global'}<br/>
				<a href="mailto:{$contact_infos.email}">{$contact_infos.email}</a>
			</div>
		</div>
    </div>
  {/if}
    <div class="block row">
		<div class="col-xs-2 col-md-2">
			<div class="icon"><img src="{$urls.img_url}picto_contact_hours.png" /></div>
		</div>
		<div class="col-xs-10 col-md-10">
			<h5>Horaires</h5>
			<div class="data">
				{hook h='displayContactHours'}
			</div>
		</div>
    </div>
</div>
