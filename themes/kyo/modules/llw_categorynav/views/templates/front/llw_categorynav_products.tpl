{*
* 2007-2016 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2016 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

	<div class="row" id="category-item-{$id_category}-products">
		<div class="category-item-products col-xs-12">
			<div class="row">
				<div class="col-xs-10">
					<h4>{$category_name} : <span>{$nb_products} produits</span></h4>
				</div>
				<div class="col-xs-2 text-xs-right">
					<button class="close" type="button">X</button>
				</div>
			</div>
			<div class="row">
			    {foreach from=$products item="product" key="position"}
			        {include file="catalog/_partials/miniatures/product.tpl" product=$product position=$position productByLine=6}
			    {/foreach}
			</div>
			<div class="row">
				<div class="col-xs-12 text-xs-center">
					<button class="closeBtn btn btn-primary" type="button">Fermer</button>
				</div>
			</div>
		</div>
	</div>
