/*
 * Custom code goes here.
 * A template should always ship with an empty custom.js
 */

jQuery(window).scroll(function() { 
	if($("body#category").length) {
		checkLeftCategoryMenuPosition();
		scrollspyActivate();
	}
});

var homePubBlocWidth 		= 0;
var headerTopHeight 		= 0;
var loadingDiv 				= '';
var initWrapperMarginTop	= 0;

$(document).ready(function () {

    headerTopHeight = $('#header-top').height();

	// Quantité de produit dans panier latéral (sidebarcart)
	qtyButtons();

	if(window.matchMedia("(min-width: 1025px)").matches) {
		$(document).tooltip({
			items: ".withToolTip",
			position: { my: "top bottom", at: "center top-10" },
			classes: {
				"ui-tooltip": "custom-tooltip",
				"ui-tooltip-content": "custom-tooltip"
	  		},
	  		hide: { duration: 500 }
		});
	}
	
	$("a.noTitle").attr("title", "");
	
	if($("input[name=address1]").length) {
		if($("input[name=google_address]").val()) {
			var streetNumb, streetName;
			var addressElt			= $("input[name=google_address]").val().split(" ");
			if($.isNumeric(addressElt[0].substring(0, 1))) {
				streetNumb			= addressElt.shift();
				streetNumb			= streetNumb.replace(",", "");
				streetName			= addressElt.join(' ');
			}
			else {
				streetNumb			= '';
				streetName			= addressElt.join(' ');
			}
		}
		addressGeolocationInitialize();
	}

    $("a.in_popup").on("click", function(t) {
        t.preventDefault();
        var e = $(t.target).attr("href");
        e && (e += "?content_only=1", $.get(e, function(t) {
            $("#modal-cms").find(".js-modal-content").html($(t).find(".page-cms").contents());
            $(".modal.fade.in").css("opacity", 0);
            $("#modal-cms").modal("show");
        }).fail(function(t) {
	        alert("Impossible d'afficher le contenu de la page CMS !");
        })); 
    })
    
    activeProductQuickView();
    
    $("#modal-cms .close").on("click", function() {
        $("#modal-cms").modal("hide");
        $(".modal.fade.in").css("opacity", 1);
    })

    
	$("input[type=date], input[name=birthday]").bind('keyup','keydown', function(e){
	
	  //To accomdate for backspacing, we detect which key was pressed - if backspace, do nothing:
	    if(e.which !== 8) { 
	        var numChars = $(this).val().length;
	        if(numChars === 2 || numChars === 5){
	            var thisVal = $(this).val();
	            thisVal += '/';
	            $(this).val(thisVal);
	        }
	  }
	});
    
	$(".reorder-link").click(function () {
		var link = $(this).attr("data-link-action");
		console.log("url="+link);
		$('#deliveryModeReorder-modal #livraison').attr("href", link+"&set_reorder_delivery_mode=1&delivery_mode=1&redirect="+encodeURIComponent(link));
		$('#deliveryModeReorder-modal #emporter').attr("href", link+"&set_reorder_delivery_mode=1&delivery_mode=2&redirect="+encodeURIComponent(link));
		$('#deliveryModeReorder-modal').modal({
		    backdrop: 'static',
		    keyboard: false
		})
	})

	// Mise à jour du panier avec les attributs cochés par défaut -----------------------------------------------
	if(typeof _global_auto_refresh_attributes !== "undefined" && _global_auto_refresh_attributes) {
        prestashop.emit('updateProduct', {eventType: 'updatedProductCombination', resp: {}, reason: { productUrl: prestashop.urls.pages.product || "" } });
	}
	
	if($("#select_delivery_date").length) {
		if($("#select_delivery_date").val()=='') {
			$("button[name=confirmDeliveryOption]").attr("disabled", true);
		}
		if($("#deliveryHours").hasClass("hidden")) {
			$("button[name=confirmDeliveryOption]").attr("disabled", true);
		}
	}
	
	$('.form-control-select').select2({
		language: 'fr',
	});

	if($('#select_delivery_date').length) {
		if($('#select_delivery_date').val) {
			$('#select_delivery_date').trigger("change");
		}
	}
	
	// Bannière d'information ----------------------------------------------------------------------------------
	if($(".header-banner").length) {
		$("body").addClass("withBanner");
	}
	$("#header .header-banner .closebanner").on("click", function() {
		console.log("Close banner");
		$("#header .header-banner").addClass("closed");
		setTimeout(function() {
			$("#header .header-banner").addClass("hidden");
		}, 1000);
		setCookie('bannerClosed', '1', 10);

		manageTopFixedPositions();
	})
	
	// Carte du point de vente ---------------------------------------------------------------------------------
	if($("#map").length) {
		initializeMap();
	}
	
  	if($("body#checkout").length==0 && $("#dropdownShopSelector").length) {
	  	fillShopMap();
  	}

  	// Largeur de la pub de la page d'accueil ------------------------------------------------------------------
    if($("#hook-13-2").length) {
		homePubBlocWidth = $("#hook-13-2").width();
	}
	
	if($(".thumbnail-container").length) {
		$(".select2-results__options").hover (
			function() {
				console.log("hover !!!");
				if(!$("body").find(".thumbnail-container").find(".select2-container--open").closest(".thumbnail-container").hasClass("over")) {
					$("body").find(".thumbnail-container").find(".select2-container--open").closest(".thumbnail-container").addClass("over");
				}
			}, 
			function() {
				if($("body").find(".thumbnail-container").find(".select2-container--open").closest(".thumbnail-container").hasClass("over")) {
					$("body").find(".thumbnail-container").find(".select2-container--open").closest(".thumbnail-container").removeClass("over");
				}
			}
		)
	}

	$(".clickToRegister").on("click", function() {
		displayRegisterForm();
	})
	
	$("#categListDeliveryChoiceModifier").on("click", function() {
		$('#modalChoixLivraison').modal('toggle')
	})
	
	// ===========================================================================================
	// GESTION DES EXCLUSIONS D'ALIMENTS
	// ===========================================================================================
	$("#search_filter_toggler").on("click", function() {
		$("#search_filters_wrapper").find(".close").on("click", function() {
			$("#search_filters_wrapper").removeClass("open");
		})
		if($("#search_filters_wrapper").hasClass("open"))
			$("#search_filters_wrapper").removeClass("open");
		else
			$("#search_filters_wrapper").addClass("open");
	})

    $("#search_filter_controls .ok").on("click", function(t) {

		$("#_desktop_search_filters_clear_all").hide();
		$("#product_list_active_filters").hide();
		var products_excluded = excludeProducts();

		$("#search_filters_wrapper").find(".close").click();
		if(products_excluded) {
			$("#_desktop_search_filters_clear_all").show();
			$("#product_list_active_filters").show();
		}
    })
    
	// ===========================================================================================
	// Force du mot de passe saisi
	// ===========================================================================================
	if($('input[type=password]').length) {
		managePassword();
	}

	// ===========================================================================================
	// Gestion du clic sur les liens de la navigation dans les catégories -------------------------
	// ===========================================================================================
	$("#spy a").on("click", function(e) {
		goToCategoryProducts($(this), e);
	})
	
	// ===========================================================================================
	// Sélection des attributs et gestion des quantités sur la page catégorie --------------------
	// ===========================================================================================
	if($(".products").length) {
		if($(".products").find('select.form-control-select').length==0) {
			$(".products").find('select.form-control').addClass('form-control-select');
			$(".products").find('.form-control-select').select2({
				language: 'fr',
			});
		}
		
		if($("products").find(".bootstrap-touchspin").length==0) {
		    $(".an_productattributes-qty").TouchSpin({
		        buttondown_class: "btn btn-touchspin",
		        buttonup_class: "btn btn-touchspin",
		        min: 1,
		        max: 20
		    })
		}
	}

	// ===========================================================================================
	// Produits complémentaires
	// ===========================================================================================
    if($("input.nb_complement").length) {
		complementaryQuantities();
    }

	// ===========================================================================================
	// Aliments indésirables
	// ===========================================================================================
    if($("input.feature_value_to_exclude").length) {
        $("body").on("change", ".feature_value_to_exclude", function (t) {
	        console.log("change on feature_value_to_exclude");

			var checkedData 			= [];
			var i 						= 0;
			$("input.feature_value_to_exclude").each(function() {
				if($(this).prop("checked")) {
					checkedData[i]		= $(this).data("feature");
					i++;
				}
			})

			$.ajax({
				type: 'POST',
				headers: { "cache-control": "no-cache" },
				url: '/index.php?controller=cart&action=updateElements',
				async: false,
				cache: false,
				dataType : "json",
				data: { id_feature_values: checkedData},
				success: function(jsonData, textStatus, jqXHR)
				{
				},
				error: function(XMLHttpRequest, textStatus, errorThrown)
				{
					var error = "Impossible de mettre à jour les aliments indésirables.<br/>textStatus: '" + textStatus + "'<br/>errorThrown: '" + errorThrown + "'<br/>responseText:<br/>" + XMLHttpRequest.responseText;
					if (!!$.prototype.fancybox)
						$.fancybox.open([
						{
							type: 'inline',
							autoScale: true,
							minHeight: 30,
							content: '<p class="fancybox-error">' + error + '</p>'
						}],
						{
							padding: 0
						});
					else
						alert(error);
				}
			});
        });
    }
    
    // Sushi animation on order confirmation page --------------------------------------------
    if($("#sushi-container").length) {
	    setTimeout(function() {
		    $("#sushi").addClass("delivering")
	    }
	    , 1500);
    }

	// Lien vers produits complémentaires ----------------------------------------------------
	// Boutons "Commander" sur page panier ---------------------------------------------------
	if($("body#cart").length) {
		gotoComplementsClick();
		checkComplementaryProducts();
	}
	
	if(errorNotification) {
	    bootbox.alert({
			title: '<i class="fa fa-warning"></i>',
		    message: errorNotification,
			locale: 'fr'
		});
	    $("a.in_popup").on("click", function(t) {
	        t.preventDefault();
	        var e = $(t.target).attr("href");
	        e && (e += "?content_only=1", $.get(e, function(t) {
	            $("#modal-cms").find(".js-modal-content").html($(t).find(".page-cms").contents());
	            $(".modal.fade.in").css("opacity", 0);
	            $("#modal-cms").modal("show");
	        }).fail(function(t) {
		        alert("Impossible d'afficher le contenu de la page CMS !");
	        })); 
	    })
	}
	

	// Hauteur du carousel de la home --------------------------------------------------
    var headerBanner		= 0;
    if($(".header-banner").length && !$(".header-banner").hasClass('closed')) {
	    headerBanner  		= $(".header-banner").outerHeight();
    }
    var headerTopHeight 	= $(".header-top").outerHeight();
    if($("body#index").length) {
	    if($(".carousel-inner").length) {
			$(".carousel-inner").css("height", "calc(100vh - "+(headerTopHeight+headerBanner)+"px)");
		}
	    if($("#home-video").length) {
			$("#home-video").css("margin-top", (headerTopHeight+headerBanner)+"px");
		}
    }

	if(window.matchMedia("(max-width: 767px)").matches) {
		$("#_desktop-header-top").remove();
		$("#category-nav.desktop").remove();
		setTimeout(function() { headerClick(); }, 1500);
	}
	else {
		$("#_mobile-header-top").remove();
		$("#category-nav.mobile").remove();
		headerClick();
	}
	
	// Dropdown catégories sur mobile --------------------------------------------------------
	$("#dropdownCategMenu").on("click", function() {
		if($(this).parent().hasClass("open")) {
			$(this).parent().find(".caret").css("transform", "rotate(-135deg)");
			$(this).parent().find(".caret").css("top", "20px");
		}
		else {
			$(this).parent().find(".caret").css("transform", "rotate(45deg)");
			$(this).parent().find(".caret").css("top", "25px");
		}	
	})
	
	$("#google_address").on("focus", function() {
		$(this).attr("autocomplete", "google-off");
	});
	
	// Ajout automatique du produit dans le panier après le choix du mode de livraison --------------------------
	if(typeof _global_auto_add_to_cart !== "undefined" && _global_auto_add_to_cart) {
		$('[data-button-action="add-to-cart"]').attr("disabled", false);
		var positionToScroll 			= 0;
		if(_global_auto_add_page=="produit") {
			$('[data-button-action="add-to-cart"]').click();
		}
		else {
			$("input[value='"+_global_auto_add_id_product+"']").closest("form").find('[name=qty]').val(_global_auto_add_quantity_wanted);
			$("input[value='"+_global_auto_add_id_product+"']").closest("form").find('[data-button-action="add-to-cart"]').click();
		}
		// On nettoie l'URL affichée dans le navigateur ----------------------------------
		var originalURL 	= window.location.href ;
		var alteredURL 		= removeParam("add-to-cart", originalURL);
		alteredURL 			= removeParam("quantity_wanted", alteredURL);
		alteredURL 			= removeParam("id_product", alteredURL);
		alteredURL 			= removeParam("page", alteredURL);
		alteredURL 			= alteredURL.replace('?', '');
		window.history.replaceState({}, document.title, alteredURL);

		positionToScroll = $("input[value='"+_global_auto_add_id_product+"']").offset().top;
		$([document.documentElement, document.body]).animate({
	    	scrollTop: positionToScroll
		}, 2000);		
	}

})

function removeParam(key, sourceURL) {
    var rtn = sourceURL.split("?")[0],
        param,
        params_arr = [],
        queryString = (sourceURL.indexOf("?") !== -1) ? sourceURL.split("?")[1] : "";
    if (queryString !== "") {
        params_arr = queryString.split("&");
        for (var i = params_arr.length - 1; i >= 0; i -= 1) {
            param = params_arr[i].split("=")[0];
            if (param === key) {
                params_arr.splice(i, 1);
            }
        }
        rtn = rtn + "?" + params_arr.join("&");
    }
    return rtn;
}

function hidePromoAlert() {
	$("body#checkout .alert.js-error").hide();
}

// ===========================================================================================
// Affichage modale après ajout au panier
// ===========================================================================================
function addToCart(e, quantity_wanted, id_product) {
    var t = $(e.target).closest("form"),
        n = t.serialize() + "&add=1&action=update",
        i = t.attr("action"),
        a = function(e) {
            e.parents(".product-add-to-cart").first().find(".product-minimal-quantity").addClass("error"), e.parent().find("label").addClass("error")
        },
        s = t.find("input[min]");
    if (! function(e) {
            var t = !0;
            return e.each(function(e, n) {
                var o = $(n),
                    i = parseInt(o.attr("min"), 10);
                i && o.val() < i && (a(o), t = !1)
            }), t
        }(s)) return void a(s);
    $.post(i, n, null, "json").then(function(e) {
        prestashop.emit("updateCart", {
            reason: {
                idProduct: e.id_product,
                idProductAttribute: e.id_product_attribute,
                idCustomization: e.id_customization,
                linkAction: "add-to-cart",
                cart: e.cart
            },
            resp: e
        })
    }).fail(function(e) {
        prestashop.emit("handleError", {
            eventType: "addProductToCart",
            resp: e
        })
    })
}

// ===========================================================================================
// Pop-up Quick View des produits
// ===========================================================================================
function activeProductQuickView() {
    $("a.link-to-product").on("click", function(t) {
        t.preventDefault();
        var n = {
            action: "viewProduct",
            id_product: $(this).data("id-product"),
            id_product_attribute: $(this).data("id-product-attribute")
        };
        $.post(prestashop.urls.pages.product, n, null, "json").then(function(t) {
            $("body").append(t.quickview_html);
            var n = $("#quickview-modal-" + t.product.id + "-" + t.product.id_product_attribute);
            n.modal("show"); 
			n.find('.form-control-select').select2({
				language: 'fr',
			});
			var q = n.find("#quantity_wanted");
			q.TouchSpin({
				buttondown_class: "btn btn-touchspin js-touchspin",
				buttonup_class: "btn btn-touchspin js-touchspin",
				min: parseInt(q.attr("min"), 10),
				max: 1e6
			})
			n.on("change keyup", "#quantity_wanted", function(t) {
				return false;
            })
			n.on("hidden.bs.modal", function() {
                n.remove()
            })
        }).fail(function(t) {
	        alert("Impossible d'afficher le contenu du produit !");
        }); 
    })
}

// ===========================================================================================
// Gestion des mots de passe
// ===========================================================================================
function managePassword() {

	$('input[type=password]').password({
		enterPass: 'Saisissez votre mot de passe',
		shortPass: 'Le mot de passe est trop court',
		containsField: 'Le mot de passe contient votre nom',
		steps: {
			// Easily change the steps' expected score here
			13: 'Mot de passe très faible',
			33: 'Faible : essayez de combiner les lettres et les chiffres',
			67: 'Moyen : essayez d\'ajouter des caractères spéciaux',
			94: 'Mot de passe fort !',
		},
		showPercent: false,
		showText: true, // shows the text tips
		animate: true, // whether or not to animate the progress bar on input blur/focus
		animateSpeed: 'slow', // the above animation speed
		field: false, // select the match field (selector or jQuery instance) for better password checks
		fieldPartialMatch: true, // whether to check for partials in field
		closestSelector: 'div.password-container',
		minimumLength: 5, // minimum password length (below this threshold, the score is 0)
		useColorBarImage: true, // use the (old) colorbar image
		customColorBarRGB: {
			red: [0, 240],
			green: [0, 240],
			blue: 10,
		} // set custom rgb color ranges for colorbar.
	});
}

// ===========================================================================================
// Gestion des quantités de produits complémentaires
// ===========================================================================================
function complementaryQuantities() {

    $("input.nb_complement").TouchSpin({
	    min : 0,
	    max : $("#nb_max_baguettes").val(),
        buttondown_class: "btn btn-touchspin",
        buttonup_class: "btn btn-touchspin",
    });
    $("body").on("change keyup", ".nb_complement", function (t) {
        console.log("change keyup on nb_complement");
		var thisLoading = $(this).closest("div.input").find(".loading");
			thisLoading.show();
        var initTarget 		= t.currentTarget;
        var triggerChange	= true;
        if($(initTarget).hasClass("sauce")) {
	        console.log("hasClass sauce");
            var nbSauces 		= 0;
            var maxSauces		= $("#nb_max_baguettes").val();
			$(".nb_complement.sauce").each(function () {
				console.log($(this).val());
				nbSauces	   += parseInt($(this).val());
			})
			console.log("val init="+$(initTarget).val()+", nbSauces="+nbSauces+", maxSauces="+maxSauces);
			if(nbSauces > maxSauces) {
				$(initTarget).val($(initTarget).val() - 1);
				triggerChange	= false;
			}
        }
		if(triggerChange) {
			$.ajax({
				type: 'POST',
				headers: { "cache-control": "no-cache" },
				url: '/index.php?controller=cart&action=updateComplements',
				async: false,
				cache: false,
				dataType : "json",
				data: {
					'nb_baguettes': $("input[name=nb_baguettes]").val(),
					'nb_sauce_salee': $("input[name=nb_sauce_salee]").val(),
					'nb_sauce_sucree': $("input[name=nb_sauce_sucree]").val(),
					'nb_gingembre': $("input[name=nb_gingembre]").val(),
					'nb_wasabi': $("input[name=nb_wasabi]").val(),
				},
				success: function(jsonData, textStatus, jqXHR)
				{
					thisLoading.hide();
				},
				error: function(XMLHttpRequest, textStatus, errorThrown)
				{
					var error = "Impossible de mettre à jour les produits complémentaires.<br/>textStatus: '" + textStatus + "'<br/>errorThrown: '" + errorThrown + "'<br/>responseText:<br/>" + XMLHttpRequest.responseText;
					if (!!$.prototype.fancybox)
						$.fancybox.open([
						{
							type: 'inline',
							autoScale: true,
							minHeight: 30,
							content: '<p class="fancybox-error">' + error + '</p>'
						}],
						{
							padding: 0
						});
					else
						alert(error);
				}
			});
		}
		else
			thisLoading.hide();
    });
}

// ===========================================================================================
// Déplacement vers le top de la liste des produits
// ===========================================================================================
function goToCategoryProducts(that, e) {
	e.preventDefault();
	e.stopPropagation();

	// Sur mobile, on ferme le dropdown après clic sur une catégorie -------------------------
	if(window.matchMedia("(max-width: 767px)").matches) {
		$("#dropdownCategMenu").click();
	}

	$("#spy li").removeClass("active");
	var link 						= that.data("target");
	$(this).parent().addClass("active");

    var headerTopHeight 	= $(".header-top").outerHeight();
    var headerTop 			= headerTopHeight;

    var headerBanner		= 0;
    if($(".header-banner").length && !$(".header-banner").hasClass('closed')) {
	    headerBanner  		= $(".header-banner").outerHeight();
	    headerTop	 	   += headerBanner;
    }
    var deliveryMode		= 0;
    if($("#product-list-delivery-mode").length) {
	    deliveryMode	  	= $("#product-list-delivery-mode").outerHeight();
    }

	//var headerHeight				= $('#header-top').height() + $('#product-list-delivery-mode').height() + 78 + 45;  
	//var newPos 						= parseInt($("#"+link).offset().top) - headerTop;
    var listHeaderHeight 			= $("#product-list-header").outerHeight();
	if(window.matchMedia("(max-width: 767px)").matches) {
		var categNavHeight 			= 0;
		if($('#category-nav.mobile').hasClass('sticky')) {
			categNavHeight 			= $("#category-nav.mobile").outerHeight();
		}
		var newPos 					= parseInt($("#"+link).offset().top) - headerTop - categNavHeight;
	}
	else
	if(window.matchMedia("(max-width: 1023px)").matches) {
		var newPos 					= parseInt($("#"+link).offset().top) - headerTop - listHeaderHeight - 10;
	}
	else
		var newPos 					= parseInt($("#"+link).offset().top) - headerTop - 65;

	console.log("offset top de #"+link+"="+$("#"+link).offset().top+", newPos="+newPos);
	$([document.documentElement, document.body]).animate({
    	scrollTop: newPos
	}, 2000);		
}

// ===========================================================================================
// Gestion des clics dans le header
// ===========================================================================================
function headerClick() {
	$('#headerCurrentLocator').on('click', function () {
		$('#modal-map').modal('toggle')
  	})
	$('.displayMap').on('click', function () {
		$('#modal-map').modal('toggle')
  	})
	$('#_desktop_cart').on('click', function(e) {
		$('html').addClass('sb-open');
		$('.sb-overlay ').fadeIn(500);
		$('#scrolltopbtn').hide();
		return false;
	});
	$('#dropdownShopSelector .caret').on('click', function () {
	    var headerTopHeight 	= $(".header-top").outerHeight();
		var shopList = $("body").find("[aria-labelledby=dropdownShopSelector]");
		if($("#shopSelectorList").hasClass("down")) {
			$("#shopSelectorList").removeClass("down");
			$("#shopSelectorList").css("top", "-300%");
			$("#header").removeClass("shopListDown");
			$('#dropdownShopSelector .caret i').removeClass("fa-angle-up");
			$('#dropdownShopSelector .caret i').addClass("fa-angle-down");
		}
		else {
			$("#shopSelectorList").addClass("down");
			$("#shopSelectorList").css("top", (headerTopHeight-10)+"px");
			$("#header").addClass("shopListDown");
			$('#dropdownShopSelector .caret i').removeClass("fa-angle-down");
			$('#dropdownShopSelector .caret i').addClass("fa-angle-up");
		}
  	})
}

// ===========================================================================================
// Vérification produits complémentaires
// ===========================================================================================
function gotoComplementsClick() {
	$("#gotoComplements").on("click", function() {
	    var headerTopHeight 			= $(".header-top").outerHeight();
		var newPos 						= parseInt($("#complements").offset().top) - headerTopHeight;
		$([document.documentElement, document.body]).animate({
        	scrollTop: newPos
		}, 1000);		
	})
}
	
// ===========================================================================================
// Vérification produits complémentaires
// ===========================================================================================
function checkComplementaryProducts() {
	$(".btn-to-check").on("click", function() {
		var targetURL 				= $(this).data("url");
		var nbComplements 			= 0;
		var nbValToCheck 			= $(".nb_complement").length;
		var i 						= 0;
		$(".nb_complement").each(function() {
			nbComplements		   += parseInt($(this).val());
			i++;
			if(i==5) {
				if(nbComplements==0) {
					$([document.documentElement, document.body]).animate({
				    	scrollTop: 0
					}, 2000);		
					var locale = {
					    OK: 'I Suppose',
					    CONFIRM: 'Commander sans',
					    CANCEL: 'Retourner au panier'
					};
					bootbox.addLocale('custom', locale);
					bootbox.confirm({
						title: "Attention",
					    message: "Vous n'avez pas ajouté de produits complémentaires (baguettes, sauces et condiments). <br>Êtes-vous sûr de vouloir poursuivre votre commande ?",
					    locale: 'custom',
					    callback: function (result) {
					        if(result) {
						        location.href = targetURL;
					        }
					        else
						        $("#gotoComplements").click();
					    }
					});
				}
				else
			        location.href = targetURL;
			}
		})
	})
}

// ===========================================================================================
// Quantité de produit dans panier latéral (sidebarcart)
// ===========================================================================================
function qtyButtons() {
	$('.product-qty-container').each(function() {
		var spinner = jQuery(this),
			input = spinner.find('input[type="text"]'),
			btnUp = spinner.find('.quantity-up'),
			btnDown = spinner.find('.quantity-down'),
			min = input.attr('min'),
			max = input.attr('max');
		btnUp.on("click", function() {
			$("#js-cart-sidebar").find(".loading").show();
			var oldValue = parseFloat(input.val());
			if (oldValue >= max) {
				var newVal = oldValue;
			} else {
				var newVal = oldValue + 1;
			}
			spinner.find("input").val(newVal);
			spinner.find("input").trigger("focusout");
		});
		btnDown.on("click", function() {
			$("#js-cart-sidebar").find(".loading").show();
			var oldValue = parseFloat(input.val());
			if (oldValue <= min) {
				var newVal = oldValue;
			} else {
				var newVal = oldValue - 1;
			}
			spinner.find("input").val(newVal);
			spinner.find("input").trigger("focusout");
		});
	});
}

// ===========================================================================================
// EXCLUSION D'ALIMENTS
// ===========================================================================================
function excludeProducts() {

	var features_to_exclude 		= new Array();
	var i 							= 0;
	var products_excluded			= false;
	var current_feature 			= '';
	var current_label 				= '';
	$("#product_list_active_filters").find("ul").empty();
	$("#search_filters").find(".feature_value_to_exclude").each(function() {
		if($(this).prop("checked")) {
			features_to_exclude[i]	= $(this).attr("data-feature");
			i++;
			current_feature 		= $(this).attr("data-feature");
			current_label 			= $(this).closest("label").find(".filter-label").text();
			$("#product_list_active_filters").find("ul").append(
				$("<li/>").addClass("filter-block").text(current_label).append($("<a/>").addClass("js-exclude-link").attr("href", "javascript:").attr("data-feature", current_feature).append($("<i/>").addClass("material-icons close").html("&#xE5CD;")))
			)
		}
	})

	$(".js-exclude-link").on("click", function(e) {
		refreshActiveFilters($(this));
	})

	if(features_to_exclude.length) {
		$("#js-active-search-filters").show();
	}

	$(".product-miniature").each(function() {
		//console.log("Traitement de l'article "+$(this).attr("data-id-product"));
		var product_display 			= "block";
		if($(this).attr("data-features")) {
			var product_features		= $(this).attr("data-features").split("-");
			for(var i=0; i<product_features.length; i++) {
				for(var j=0; j<features_to_exclude.length; j++) {
					if(product_features[i]==features_to_exclude[j]) {
						product_display 	= "none";
						products_excluded	= true;
					}
				}
			}
		}
		$(this).css("display", product_display);
	})

	// Compteurs de produits ---------------------------------------------------------------
	$("#spy").find("a").each(function() {
		var link 					= $(this).data("target");
		var nbProducts 				= 0;
		$("#"+link).find(".product-miniature").each(function() {
			if($(this).css("display")=="block") {
				nbProducts++;
			}
			$("#nb_products_"+link).text(nbProducts);
		})	
	})

	return products_excluded;
}		

function refreshActiveFilters(clickedFilter) {
	var current_feature			= clickedFilter.attr("data-feature");
	$("#search_filters").find("input[data-feature="+current_feature+"]").prop("checked", false);
	excludeProducts();
	if($("#js-active-search-filters").find("ul").find("li").length==0) {
		$("#js-active-search-filters").hide();
	}
}

// ===========================================================================================
// POSITION DE LA NAVIGATION DANS LES CATEGORIES
// ===========================================================================================
function checkLeftCategoryMenuPosition() {

    var headerTopHeight 	= $(".header-top").outerHeight();
    var topPosition 		= headerTopHeight;
    var bannerHeight 		= 0;
    if($(".header-banner").length && !$(".header-banner").hasClass('closed')) {
	    bannerHeight 		= $(".header-banner").outerHeight();
	    topPosition		   += bannerHeight;
    }
	var deliveryModeHeight			= 0;
	if($("#product-list-delivery-mode").length) {
		deliveryModeHeight		= $("#product-list-delivery-mode").outerHeight();
		topPosition			   += deliveryModeHeight;
	}
		
    var scrollTop 			= $(window).scrollTop();
    var contentHeight 		= $("#content-wrapper").height();
    var categoryNavHeight 	= $("#category-nav.desktop").outerHeight();
    var listHeaderHeight 	= $("#product-list-header").outerHeight();
    var listUpHeight 		= $("#product_list .up").outerHeight();
	
	if(window.matchMedia("(min-width: 768px)").matches) {
		//console.log("scrollTop="+scrollTop+", listHeaderHeight="+listHeaderHeight+", contentHeight="+contentHeight+", categoryNavHeight="+categoryNavHeight);
		if(scrollTop >= listHeaderHeight){
			if((scrollTop+categoryNavHeight-listUpHeight) > contentHeight) {
				$('#category-nav.desktop').addClass('abs');
			    $('#category-nav.desktop').css('top', (contentHeight-categoryNavHeight-listUpHeight)+"px");
			}
			else {
			    $('#category-nav.desktop').addClass('sticky');
				$('#category-nav.desktop').removeClass('abs');
			    $('#category-nav.desktop').css('top', topPosition+"px");
			}
	    }
	    else {
	        $('#category-nav.desktop').removeClass('sticky');
	        $('#category-nav.desktop').removeClass('abs');
	        $('#category-nav.desktop').css('top', "auto");
	    }
	}
	else {
		var fromPosition 		= deliveryModeHeight + listHeaderHeight;
		var categNavHeight 		= $("#category-nav.mobile").outerHeight();
		console.log("scrollTop="+scrollTop+", fromPosition="+fromPosition);
		if(scrollTop >= fromPosition){
		    $('#category-nav.mobile').addClass('sticky');
		    $('#category-nav.mobile').css('top', (headerTopHeight-1)+"px");
		    $('#wrapper').css('margin-top', categNavHeight+"px");
	    }
	    else {
	        $('#category-nav.mobile').removeClass('sticky');
	        $('#category-nav.mobile').css('top', "auto");
		    $('#wrapper').css('margin-top', "0px");
	    }
	}
}

// ===========================================================================================
// APRES CHARGEMENT DE LA FENETRE
// ===========================================================================================
$(window).on("load", function() {
    $("#mainTransition").hide();
    $("main").addClass("shown");
	AOS.init({
		duration: 300
	});

	// Colonne de gauche de la page contact --------------------------------------------------
	if($("body#contact").length) {
		if (window.matchMedia("(min-width: 768px)").matches) {
			var leftColHeight				= $("#left-column").outerHeight();
			var rightColHeight				= $("#content-wrapper").outerHeight();
			if(parseInt(leftColHeight) > parseInt(rightColHeight))
				$("#content-wrapper").css("height", leftColHeight+"px");
			else
				$("#left-column .contact-rich").css("height", rightColHeight+"px");
		}
	}

	initWrapperMarginTop 		= parseInt($("#wrapper").css("margin-top").replace("px", ""));

	manageTopFixedPositions();

	//$("#homeVideo").get(0).play();
})

// ===========================================================================================
// Gestion de la top position des div fixed 
// ===========================================================================================
function manageTopFixedPositions() {		

	if($("body#checkout").length)	return false;

	var bannerHeight				= 0;
    if($(".header-banner").length && !$(".header-banner").hasClass('closed')) {
		bannerHeight 				= $("#header .header-banner").outerHeight();
	}
	var deliveryModeHeight			= 0;
	if($("#product-list-delivery-mode").length) {
		deliveryModeHeight			= $("#product-list-delivery-mode").outerHeight();
	}
	var breadcrumbHeight			= 0;
	if($(".breadcrumb").length) {
		breadcrumbHeight			= $(".breadcrumb").outerHeight();
	}
	// Gestion des top position des div fixed ------------------------------------------------------------------
	$(".header-top").css("top", bannerHeight+"px");
	var topHeight 					= $("#header .header-top").outerHeight();
	if($("body#index").length) {
		$("#carousel").css("margin-top", (bannerHeight+topHeight-1)+"px");
	}
	else {
		if($("body#category").length) {
			if(window.matchMedia("(min-width: 768px)").matches) {
				console.log("manageTopFixedPositions-1");
				$("#product-list-delivery-mode").css("top", (bannerHeight+topHeight)+"px");
				$("#product-list-header").css("margin-top", (bannerHeight+topHeight+deliveryModeHeight)+"px");
			}
			else {
				console.log("manageTopFixedPositions-2");
				$("#product-list-delivery-mode").css("margin-top", (bannerHeight+topHeight)+"px");
				$("#product-list-header").css("margin-top", "0px");
			}
			checkLeftCategoryMenuPosition();
			scrollspyActivate();
		}
		else {
			$(".breadcrumb").addClass("sticky");
			$("#wrapper").css("margin-top", (initWrapperMarginTop+bannerHeight+topHeight)+"px");
			var containerPaddingTop 	= parseInt($("#wrapper > .container").css("padding-top").replace("px", ""));
			$("#wrapper > .container").css("padding-top", (containerPaddingTop+breadcrumbHeight)+"px");
			if($("body#address").length && $(".pac-container").length) {
				$(".pac-container").css("margin-top", "-"+(bannerHeight+topHeight)+"px");
			}
		}
	}
}

// ===========================================================================================
// DATE DE LIVRAISON
// ===========================================================================================
$('#select_delivery_date').on('change', function(e){

	var select = $('#select_delivery_hour');
	select.empty();
	
	if($("#select_delivery_date").val()) {

		$("#deliveryDateHoursWaiting").show();
				
		var selectedHour = '';
		if($("#select_delivery_date").data('selected-hour')) {
			selectedHour = $("#select_delivery_date").data('selected-hour');
		}
				
		e.preventDefault();
		e.stopPropagation();
		$.ajax({
			type: 'POST',
			headers: { "cache-control": "no-cache" },
			url: '/index.php?rand=' + new Date().getTime(),
			async: false,
			cache: false,
			dataType : "json",
			data: 'controller=cart&getHours=1&ajax=true&day='+encodeURIComponent($("#select_delivery_date").val()),
			success: function(jsonData,textStatus,jqXHR)
			{
				if(jsonData.select) {
					select.append(jsonData.select);
	
					$("#deliveryHours").removeClass("hidden");
					$("#deliveryDateHoursWarning").hide();
					$("#deliveryDebug").hide();
	
					$("#select_delivery_hour").on("change", function() {
						if($("#select_delivery_hour").val()) {
							$("button[name=confirmDeliveryOption]").attr("disabled", false);
						}
						else {
							$("button[name=confirmDeliveryOption]").attr("disabled", true);
						}
					})
					$("#deliveryDateHoursWaiting").hide();

					if(jsonData.debug_delivery) {
						$("#deliveryDebug textarea").val(jsonData.debug_message);
						$("#deliveryDebug").hide();
					}

					if($("#select_delivery_date").val() == "Aujourd'hui")	{
						var today 					= new Date();
						var y 						= today.getFullYear();
						var m 						= today.getMonth() + 1;
						var d 						= today.getDay();
						var selectedDay 			= y+"-"+checkTime(m)+"-"+checkTime(d);
					}
					else {
						var selectedDay				= $("#select_delivery_date").val().replace("Le ", "");
						selectedDay					= selectedDay.substr(6, 4)+"-"+selectedDay.substr(3, 2)+"-"+selectedDay.substr(0, 2);
					}
					if(selectedHour) {
						$("#select_delivery_hour").val(selectedDay+" "+selectedHour+":00");
						$("button[name=confirmDeliveryOption]").show();
						$("button[name=confirmDeliveryOption]").attr("disabled", false);
					}
				}
				else {
					$("#deliveryDebug").hide();
					$("#deliveryHours").addClass("hidden");
					$("#deliveryDateHoursWarning").show();
					$("#deliveryDateHoursWaiting").hide();
				}
				
			},
			error: function(XMLHttpRequest, textStatus, errorThrown)
			{
				var error = "Impossible de déterminer les tranches horaires.<br/>textStatus: '" + textStatus + "'<br/>errorThrown: '" + errorThrown + "'<br/>responseText:<br/>" + XMLHttpRequest.responseText;
				$("#deliveryDateHoursWaiting").hide();
				if (!!$.prototype.fancybox)
					$.fancybox.open([
					{
						type: 'inline',
						autoScale: true,
						minHeight: 30,
						content: '<p class="fancybox-error">' + error + '</p>'
					}],
					{
						padding: 0
					});
				else
					alert(error);
			}
		});

	}
});

function checkTime(i) {
	if (i < 10) {
		i = "0" + i;
	}
	return i;
}
function displayCurrentHour() {
	var today = new Date();
	var h = today.getHours();
	var m = today.getMinutes();
	var s = today.getSeconds();
	// add a zero in front of numbers<10
	m = checkTime(m);
	s = checkTime(s);
	$('#currentHour').html(h + ":" + m);
	t = setTimeout(function() {
		displayCurrentHour()
	}, 30000);
}


// ===========================================================================================
// VOIR LA DERNIERE COMMANDE
// ===========================================================================================
function seeLastOrder(id_order) {
	$.ajax({
		type: 'GET',
		headers: { "cache-control": "no-cache" },
		url: '/index.php?rand=' + new Date().getTime(),
		async: false,
		cache: false,
		dataType : "json",
		data: 'controller=cart&getHours=1&ajax=true&day='+encodeURIComponent($("#select_delivery_date").val()),
		success: function(jsonData,textStatus,jqXHR)
		{
			if(jsonData) {
				select.append(jsonData.select);

				$("#deliveryHours").removeClass("hidden");
				$("#deliveryDateHoursWarning").hide();

				$("#select_delivery_hour").on("change", function() {
					if($("#select_delivery_hour").val()) {
						$("button[name=confirmDeliveryOption]").attr("disabled", false);
					}
					else {
						$("button[name=confirmDeliveryOption]").attr("disabled", true);
					}
				})
				$("#deliveryDateHoursWaiting").hide();
			}
			else {
				$("#deliveryHours").addClass("hidden");
				$("#deliveryDateHoursWarning").show();
				$("#deliveryDateHoursWaiting").hide();
			}
			
		},
		error: function(XMLHttpRequest, textStatus, errorThrown)
		{
			var error = "Impossible de déterminer les tranches horaires.<br/>textStatus: '" + textStatus + "'<br/>errorThrown: '" + errorThrown + "'<br/>responseText:<br/>" + XMLHttpRequest.responseText;
			$("#deliveryDateHoursWaiting").hide();
			if (!!$.prototype.fancybox)
				$.fancybox.open([
				{
					type: 'inline',
					autoScale: true,
					minHeight: 30,
					content: '<p class="fancybox-error">' + error + '</p>'
				}],
				{
					padding: 0
				});
			else
				alert(error);
		}
	});
	
}

// =======================================================================================
// Gestion du sticky header
// =======================================================================================
function addStickyHeader() {
	console.log("addStickyHeader");
	if($("body#checkout").length)
		return;

    var headerTopHeight 	= $(".header-top").outerHeight();
    var headerTop 			= headerTopHeight;

    var headerBanner		= 0;
    if($(".header-banner").length && !$(".header-banner").hasClass('closed')) {
	    headerBanner  		= $(".header-banner").outerHeight();
	    headerTop	 	   += headerBanner;
    }
    var headerBread			= 0;
    if($("body#index").length==0) {
	    if($(".breadcrumb").length) {
		    headerBread  	= $(".breadcrumb").outerHeight();
		    headerTop 	   += headerBread;
	    }
	}
    var deliveryMode		= 0;
    if($("#product-list-delivery-mode").length) {
	    deliveryMode	  	= $("#product-list-delivery-mode").outerHeight();
    }
	
    $("#product-list-delivery-mode").css('top',headerTopHeight+"px");
    console.log("scrollTop="+$(window).scrollTop()+', headerTop='+headerTop);
    if($(window).scrollTop()>=headerTop){
        if(!$('.header-top').hasClass('sticky')) {
	        $('.header-top').addClass('sticky');
	        if($("body#index").length==0) {
		        $("#product-list-delivery-mode").addClass('sticky');
		        if(headerBread) {
			        $(".breadcrumb").addClass('sticky');
			        $(".breadcrumb").css('top', headerTopHeight+"px");
		        }
				if($("body#product").length==0) {
					console.log("addStickyHeader-1");
				    $('#content-wrapper').css('padding-top', ((headerTop*2)-(headerBanner)-headerBanner)+"px");
				}
				else {
					console.log("addStickyHeader-2");
				    $('#content-wrapper').css('padding-top', ((headerTop*2)-(headerBanner)-45)+"px");
				}
			    $('#left-column').css('padding-top', ((headerTop*2)-(headerBanner)-30)+"px");
			}
			else {
				console.log("addStickyHeader-3");
			    $('#carousel').css('margin-top', (headerTopHeight*2)+"px");
			}
        }
    }
    else {
        if($('.header-top').hasClass('sticky')) {
	        $('.header-top').removeClass('sticky');
	        if($("body#index").length==0) {
		        $("#product-list-delivery-mode").removeClass('sticky');
		        $(".breadcrumb").removeClass('sticky');
		        $(".breadcrumb").css('top', "auto");
			    $('#content-wrapper').css('padding-top', "0px");
			    $('#left-column').css('padding-top', "0px");
			}
			else {
			    $('#carousel').css('margin-top', "0px");
			}
		}
    }
}

// =======================================================================================
// Navigation de la page catégorie
// =======================================================================================
function scrollspyActivate() {
	var anchorToActivate				= '';
	$("#spy").find("li").removeClass("active");
	$("#spy").find(".category-line").each(function() {
		var link 						= $(this).find("a").data("target");
		var fromTop 					= parseInt($("#"+link).offset().top);
			fromTop 				   += $('#header-top').height();  
		var winBottom 					= $(window).scrollTop() + $(window).height();
        if(fromTop < winBottom) {
			anchorToActivate			= link;  
        }
	})
	if(anchorToActivate) {
		$("#spy").find("a[data-target='"+anchorToActivate+"']").parent().addClass("active");
	}
}

// =======================================================================================
// Taille de la pub de la home
// =======================================================================================
function resizeHomePub() {
    if($("#hook-13-2").length) {
	    var winW = $(window).width();
	    var margin = (winW - homePubBlocWidth) / 2;
	    $("#hook-13-2").css("margin-left", "-"+margin+"px");
	    $("#hook-13-2").css("margin-right", "-"+margin+"px");
    }
}

// =======================================================================================
// Google Map du magasin en cours
// =======================================================================================
function fillShopMap() {

	var myLatlng = new google.maps.LatLng(shopLat, shopLong);
	var myOptions = {
		zoom: 13,
		center: myLatlng,
		mapTypeId: google.maps.MapTypeId.ROADMAP,
		disableDefaultUI: true
	}
	
	var map = new google.maps.Map(document.getElementById("shop_map"), myOptions);
	
	var contentString = '<div id="infoWindow">\n\
                            <div class="title">'+shopName+'</div>\n\
                            <div class="address">'+shopAddress+'</div>\n\
                            <div class="link"><a href="http://maps.google.com/maps?&z=12&q='+shopLat+'+'+shopLong+'&ll='+shopLat+'+'+shopLong+'" target="_blank" rel="noreferrer">Afficher dans Google Map</a></div>\n\
						</div>';
	
	var infowindow = new google.maps.InfoWindow({
	    content: contentString
	});
	
	var marker = new google.maps.Marker({
	    position: myLatlng,
	    map: map,
	    title: shopName
	});
	
	infowindow.open(map,marker);
	//map.panBy(0, -70);
}

// =======================================================================================
// Création d'un cookie
// =======================================================================================
function setCookie(cname, cvalue, exdays) {
	var d = new Date();
	d.setTime(d.getTime() + (exdays*24*60*60*1000));
	var expires = "expires="+ d.toUTCString();
	document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}   

// =======================================================================================
// Affichage du pop-up de saisie de l'adresse de livraison (pas utilisé)
// =======================================================================================
function showDeliveryAddressForm() {

	$('#modal-delivery-address').modal('toggle')

	if($("input[name=google_address]").val()) {
		var streetNumb, streetName;
		var addressElt			= $("input[name=google_address]").val().split(" ");
		if($.isNumeric(addressElt[0].substring(0, 1))) {
			streetNumb			= addressElt.shift();
			streetNumb			= streetNumb.replace(",", "");
			streetName			= addressElt.join(' ');
		}
		else {
			streetNumb			= '';
			streetName			= addressElt.join(' ');
		}
	}
	addressGeolocationInitialize(true);
}

var placeSearch, autocomplete, googlePlace;
var componentForm = {
	street_number				: 'short_name',
	route						: 'long_name',
	locality					: 'long_name',
	postal_code					: 'short_name',
	country						: 'long_name'
};

var inputCorrespondances = {
	street_number				: 'address1',
	route						: 'address1',
	locality					: 'city',
	postal_code					: 'postcode',
	country						: 'id_country'
}

function addressGeolocationInitialize(testAddress) {
	console.log('initialize');
	$("#google_address").attr("autocomplete", "google-off");
	// Create the autocomplete object, restricting the search
	// to geographical location types.
	var bounds;
	var options = {
		componentRestrictions: {country: "fr"},
		types: ['geocode']
	};
	autocomplete = new google.maps.places.Autocomplete((document.getElementById('google_address')), options);
	// When the user selects an address from the dropdown,
	// populate the address fields in the form.
	google.maps.event.addListener(autocomplete, 'place_changed', function() {
		codeAddress(testAddress);
	});
}

function codeAddress(testAddress) {
	console.log('codeAddress');

	googlePlace 						= autocomplete.getPlace();
	if(!testAddress) {
		$("input[name=address1]").val('');
		$("input[name=city]").val('');
		$("input[name=postcode]").val('');
		$('select[name=id_country]').find('option').attr('selected', false);
	
		// Get each component of the address from the place details
		// and fill the corresponding field on the form.
		for (var i = 0; i < googlePlace.address_components.length; i++) {
			var addressType 				= googlePlace.address_components[i].types[0];
			if (inputCorrespondances[addressType]) {
				var val 					= googlePlace.address_components[i][componentForm[addressType]];
				if($('input[name='+inputCorrespondances[addressType]+']').length)
					if($('input[name='+inputCorrespondances[addressType]+']').val())
						$('input[name='+inputCorrespondances[addressType]+']').val($('input[name='+inputCorrespondances[addressType]+']').val()+' '+val);
					else
						$('input[name='+inputCorrespondances[addressType]+']').val(val);
				else
					$('select[name='+inputCorrespondances[addressType]+']').find('option[text="'+val+'"]').attr('selected', true);
			}
		}
	}

	var geocoder;
	geocoder 				= new google.maps.Geocoder();
	var address 			= $('#google_address').val();
	geocoder.geocode( { 'address': address}, function(results, status) {
		if (status == google.maps.GeocoderStatus.OK) {
			$('#location_position').val(results[0].geometry.location);
			$("#deliveryAddressCheck").prop("disabled", false);
		} else {
			alert(_('Problème de géocodage (Geocode) pour la raison suivante : ') + status);
		}
	});
	return true;
}

function geolocate() {
	if (navigator.geolocation) {
		navigator.geolocation.getCurrentPosition(function(position) {
			var geolocation = {lat: position.coords.latitude, lng: position.coords.longitude};
			var bounds = new google.maps.LatLngBounds(geolocation, geolocation);
			var circle = new google.maps.Circle({center: geolocation, radius: position.coords.accuracy});
			autocomplete.setBounds(circle.getBounds());
		});
	}
}

// ==========================================================================================
// Affichage du formulaire latéral d'enregistrement de compte
// ==========================================================================================
function displayRegisterForm() {

	var elemOverlay = '';
	if($(".sb-overlay").length==0) {
		elemOverlay = $('<div class="sb-overlay"></div>').insertAfter('main');
	}

	$('html').addClass('form-open');
	$('.sb-overlay ').fadeIn(500);
	$('#scrolltopbtn').hide();
	
	$("#slideRegisterForm #registerLoading").show();
    $.ajax({
		url : $("#registerURL").val()+"&ajaxMode=1",
		type : 'GET',
		dataType : 'json',
		success : function(data, statut) {
			displayAjaxRegisterForm(data.registration_form);
		},
		error : function(resultat, statut, erreur) {
		
		}

    });

	$('#slideRegisterForm .close').on('click', function() {
		$('.sb-overlay').fadeOut('500');
		$('html').removeClass('form-open');
		$('#scrolltopbtn').show();
	});
	
}

// ==========================================================================================
// Enregistrement de compte
// ==========================================================================================
function displayAjaxRegisterForm(registration_form) {

	$('#slideRegisterForm #registerFormContainer').html(registration_form);
	$('#slideRegisterForm .displayCustomerFormAfter').show();
	$("#slideRegisterForm #registerOK").hide();
    $("#slideRegisterForm #registerFormContainer").find("[data-link-action='save-customer']").prop('type', 'button');

	$("#slideRegisterForm #registerFormContainer").find("[data-link-action='save-customer']").on('click', function(e) {
		console.log("[data-link-action='save-customer']");
		e.preventDefault();
		$("#slideRegisterForm #registerLoading").show();
        var thisForm = $(e.target).closest("form"),
            thisData = thisForm.serialize();
	    $.ajax({
			url : $("#registerURL").val()+"&ajaxMode=1&postCreateAccount=1",
			type : 'POST',
			dataType : 'json',
			data : thisData,
			success : function(data, statut) { // success est toujours en place, bien sûr !
				if(data.register_ok) {
					$("#slideRegisterForm #registerFormContainer").html('');
					$("#slideRegisterForm #registerOK").show();
					$('#slideRegisterForm .displayCustomerFormAfter').hide();
					$("#customerLoggedLink").show();
					$("#customerLoginLink").hide();
					$("#slideRegisterForm #closeRegisterForm").on("click", function() {
						$('#slideRegisterForm .close').trigger("click");					
					})
					$("#fast-order").replaceWith(data.fast_order);
				}
				else {
					displayAjaxRegisterForm(data.registration_form);
				}
				$("#slideRegisterForm #registerLoading").hide();
			},
			error : function(resultat, statut, erreur) {
			
			}
	
	    });
	})

    $('button[data-action="show-password"]').on("click", function () {
	    console.log("click sur show-password");
        var t = $(this).closest(".input-group").children("input.js-visible-password");
        "password" === t.attr("type")
            ? (t.attr("type", "text"), $(this).text($(this).data("textHide")))
            : (t.attr("type", "password"), $(this).text($(this).data("textShow")));
    });

    $("a.in_popup").on("click", function(t) {
        t.preventDefault();
        var e = $(t.target).attr("href");
        e && (e += "?content_only=1", $.get(e, function(t) {
            $("#modal-cms").find(".js-modal-content").html($(t).find(".page-cms").contents())
        }).fail(function(t) {
        })), $("#modal-cms").modal("show")
    })

	$("#slideRegisterForm").tooltip({
		position: { my: "top bottom", at: "center top-10" },
		classes: {
			"ui-tooltip": "custom-tooltip",
			"ui-tooltip-content": "custom-tooltip"
  		},
  		hide: { duration: 500 }
	});
	$("#slideRegisterForm #registerLoading").hide();

	managePassword();

}

