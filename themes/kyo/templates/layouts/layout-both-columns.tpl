{**
 * Copyright since 2007 PrestaShop SA and Contributors
 * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.md.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://devdocs.prestashop.com/ for more information.
 *
 * @author    PrestaShop SA and Contributors <contact@prestashop.com>
 * @copyright Since 2007 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 *}
<!doctype html>
<html lang="{$language.iso_code}">

	<head>
		{block name='head'}
		{include file='_partials/head.tpl'}
		{/block}
	</head>

	<body id="{$page.page_name}" class="{$page.body_classes|classnames}">
		{if $shop.id == 5}
			{literal}
				<!-- Google Tag Manager (noscript) -->
					<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-KJZ57DN"
					height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
				<!-- End Google Tag Manager (noscript) -->
			{/literal}
		{elseif $shop.id == 4}
			{literal}
				<!-- Google Tag Manager (noscript) -->
					<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5VBX48M"
					height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
				<!-- End Google Tag Manager (noscript) -->
			{/literal}
		{elseif $shop.id == 1}
			{literal}
				<!-- Google Tag Manager (noscript) -->
					<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MPDSBFX"
					height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
				<!-- End Google Tag Manager (noscript) -->
			{/literal}

		{elseif $shop.id == 2}
			{literal}
				<!-- Google Tag Manager (noscript) -->
					<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-P62V3NJ"
					height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
				<!-- End Google Tag Manager (noscript) -->
			{/literal}
		{/if}

		{block name='hook_after_body_opening_tag'}
			{hook h='displayAfterBodyOpeningTag'}
		{/block}

		<main>
			{block name='product_activation'}
				{include file='catalog/_partials/product-activation.tpl'}
			{/block}
		
			<header id="header">
				{block name='header'}
					{include file='_partials/header.tpl'}
				{/block}
			</header>
		
			{block name='product_list_delivery_mode'}{/block}

			{block name='product_list_header'}
			{if isset($listing.label)}
			<h2 id="js-product-list-header" class="h2">{$listing.label}</h2>
			{/if}
			{/block}

			{block name='product_list_top_navig'}{/block}

			<section id="wrapper">
			
				{block name='notifications'}
					{include file='_partials/notifications.tpl'}
				{/block}
			
				{hook h="displayWrapperTop"}
			
				{block name='breadcrumb'}
					{include file='_partials/breadcrumb.tpl'}
				{/block}

				<div class="container">

					{block name='product_list_title'}
					{if isset($listing.label)}
					<h2 id="js-product-list-header" class="h2">{$listing.label}</h2>
					{/if}
					{/block}
		
					{block name="left_column"}
					<div id="left-column" class="col-xs-12 col-sm-4 col-md-3 col-lg-2">
					{if $page.page_name == 'product'}
						{hook h='displayLeftColumnProduct'}
					{else}
						{hook h="displayLeftColumn"}
					{/if}
					</div>
					{/block}
			
					{block name="content_wrapper"}
					<div id="content-wrapper" class="left-column right-column col-sm-4 col-md-6 col-lg-7">
						{hook h="displayContentWrapperTop"}
						{block name="content"}
							<p>Hello world! This is HTML5 Boilerplate.</p>
						{/block}
						{hook h="displayContentWrapperBottom"}
					</div>
					{/block}
			
					{block name="right_column"}
					<div id="right-column" class="col-xs-12 col-sm-4 col-md-3 col-lg-4">
					{if $page.page_name == 'product'}
						{hook h='displayRightColumnProduct'}
					{else}
						{hook h="displayRightColumn"}
					{/if}
					</div>
					{/block}

					{block name='hook_reassurance'}{/block}

				</div>
				
				{hook h="displayWrapperBottom"}
			
			</section>
		
			{block name='hook_home_pub'}{/block}
			{block name='hook_home_categ_nav'}{/block}
			{block name='hook_home_seo'}{/block}
			
			<footer id="footer">
			{block name="footer"}
				{include file="_partials/footer.tpl"}
			{/block}
			</footer>
		
		</main>

	    <div id="mainTransition">
	        <div class="logo">
		        <h2>Kyo Sushi</h2>
	            <h3>Cuisine japonaise authentique</h3>
		        <div>
		            <img src="{$urls.img_url}logo_kyo_rouge.png" class="fa-spin">
		        </div>
	        </div>
	    </div>

		{block name='javascript_bottom'}
			{include file="_partials/javascript.tpl" javascript=$javascript.bottom}
		{/block}

		{block name='hook_before_body_closing_tag'}
			{hook h='displayBeforeBodyClosingTag'}
		{/block}
  	
  	</body>

</html>
