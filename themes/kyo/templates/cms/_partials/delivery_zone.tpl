{**
 * Copyright since 2007 PrestaShop SA and Contributors
 * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.md.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://devdocs.prestashop.com/ for more information.
 *
 * @author    PrestaShop SA and Contributors <contact@prestashop.com>
 * @copyright Since 2007 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 *}
				{if $frais_par_zone}
				<table class="table table-bordered">
					<thead>
						<tr>
							<th class="first_item">Km</th>
							<th class="item">Montant commande</th>
							<th class="item">Montant minimum</th>
							<th class="last_item">Frais de livraison</th>
						</tr>
					</thead>
					<tbody>
					{assign var="prev_secteur" value=''}
					{foreach from=$shipping_rates item=shipping_rate name="shippingRates"}
						{if $smarty.foreach.shippingRates.last}
							{assign var="this_secteur" value='Au delà de '|cat:$shipping_rate.km_from|cat:' km'}
						{else}
							{assign var="this_secteur" value='De '|cat:$shipping_rate.km_from|cat:' à '|cat:$shipping_rate.km_to|cat:' km'}
						{/if}
							<tr class="{if $smarty.foreach.shippingRates.first}first_item{elseif $smarty.foreach.shippingRates.last}last_item{/if} {if $smarty.foreach.shippingRates.index % 2}alternate_item{else}item{/if}">
								{if $prev_secteur != $this_secteur}
								<td>{$this_secteur}</td>
								{else}
								<td></td>
								{/if}
								{if $smarty.foreach.shippingRates.last}
								<td colspan="3" style="text-align: center"><strong>Hors zone de livraison</strong></td>
								{else}
									{if $shipping_rate.price_to==9999}
								<td>Au delà de {$shipping_rate.price_from|number_format:2:",":"."} €</td>
									{else}
								<td>De {$shipping_rate.price_from|number_format:2:",":"."} € à {$shipping_rate.price_to|number_format:2:",":"."} €</td>
									{/if}
								
								<td>{$shipping_rate.mt_min_order|number_format:2:",":"."} €</td>
								{if $shipping_rate.shipping_price==0}
								<td>Gratuit</td>
								{else}
								<td>{$shipping_rate.shipping_price}</td>
								{/if}
								{/if}
							</tr>
							{assign var="prev_secteur" value=$this_secteur}
					{/foreach}
					</tbody>
				</table>
				{else}
				<table class="table table-bordered">
					<thead>
						<tr>
							<th class="first_item">Secteur</th>
							<th class="item">Montant commande</th>
							<th class="item">Montant minimum</th>
							<th class="last_item">Frais de livraison</th>
						</tr>
					</thead>
					<tbody>
					{assign var="prev_secteur" value=''}
					{foreach from=$shipping_rates item=shipping_rate name="shippingRates"}
						<tr class="{if $smarty.foreach.shippingRates.first}first_item{elseif $smarty.foreach.shippingRates.last}last_item{/if} {if $smarty.foreach.shippingRates.index % 2}alternate_item{else}item{/if}">
							{if $shipping_rate.zone_name != $prev_secteur}
							<td>{$shipping_rate.zone_name}</td>
							{else}
							<td></td>
							{/if}
							{assign var="prev_secteur" value=$shipping_rate.zone_name}
							{if $shipping_rate.price_to==9999}
							<td>Au delà de {$shipping_rate.price_from|number_format:2:",":"."} €</td>
							{else}
							<td>De {$shipping_rate.price_from|number_format:2:",":"."} € à {$shipping_rate.price_to|number_format:2:",":"."} €</td>
							{/if}
							<td>{$shipping_rate.mt_min_order|number_format:2:",":"."} €</td>
							{if $shipping_rate.shipping_price==0}
							<td>Gratuit</td>
							{else}
							<td>{if $shipping_rate.shipping_price|strpos:"%"}{$shipping_rate.shipping_price|number_format:0:",":"."} %{else}{$shipping_rate.shipping_price|number_format:2:",":"."} €{/if}</td>
							{/if}
						</tr>
					{/foreach}
					</tbody>
				</table>
				{/if}
