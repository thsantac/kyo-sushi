{**
 * Copyright since 2007 PrestaShop SA and Contributors
 * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.md.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://devdocs.prestashop.com/ for more information.
 *
 * @author    PrestaShop SA and Contributors <contact@prestashop.com>
 * @copyright Since 2007 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 *}
{block name='header_banner'}
	{hook h='displayBanner'}
{/block}

{block name='header_nav'}
{/block}

{block name='header_top'}
	<div class="header-top" id="header-top">
		<div class="header-top-container">
			<div class="container">
				<div class="hidden-sm-down" id="_desktop-header-top">
					<div class="row">
						<div class="col-md-2" id="_desktop_logo">
					        <a href="{$urls.base_url}">
					          <img class="logo img-responsive" src="{$shop.logo}" alt="{$shop.name}">
					        </a>
						</div>
						<div class="col-md-4 col-sm-12" id="_desktop_shop_selector">
							{hook h='displayNav1' canChangeShop=1}
						</div>
						<div class="col-md-2 col-sm-12" id="_desktop_contact">
							{hook h='displayTopContact'}
						</div>
						<div class="col-md-4 col-sm-12" id="_desktop_nav">
							<div class="nav_container">
								<div class="row">
									{hook h='displayNav2'}
									<div class="col-xs-3 col-md-3">
										<div id="menu-icon">
											<a href="/2-la-carte" class="withToolTip" title="Voir la carte"><img src="{$urls.img_url}picto_header_menu.png" class="animate__animated animate__pulse" alt="La carte" /></a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="hidden-md-up mobile" id="_mobile-header-top">
					<div class="row with-gradient">
						<div class="col-xs-4" id="_mobile_logo">
					        <a href="{$urls.base_url}">
					          <img class="logo img-responsive" src="{$shop.logo}" alt="{$shop.name}">
					        </a>
						</div>
						<div class="col-xs-8" id="_mobile_nav">
							<div class="nav_container">
								<div class="row">
									{hook h='displayNav2'}
									<div class="col-xs-3">
										<div id="menu-icon">
											<a href="/2-la-carte" class="widthToolTip" title="Voir la carte"><img src="{$urls.img_url}picto_header_menu.png" class="animate__animated animate__pulse" alt="La carte" /></a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12" id="_mobile_shop_selector">
							{hook h='displayNav1' canChangeShop=1}
						</div>
					</div>
					<div id="mobile_top_menu_wrapper" class="row hidden-md-up" style="display:none;">
						<div class="js-top-menu mobile" id="_mobile_top_menu"></div>
					</div>
				</div>
			</div>
		</div>
		{hook h='displayShopList'}
	</div>
    {if $page.page_name == 'index'}
		{hook h='displayHomeSlider'}
	{/if}
	{hook h='displayNavFullWidth'}
{/block}
