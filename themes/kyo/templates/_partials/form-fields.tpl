{**
 * Copyright since 2007 PrestaShop SA and Contributors
 * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.md.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://devdocs.prestashop.com/ for more information.
 *
 * @author    PrestaShop SA and Contributors <contact@prestashop.com>
 * @copyright Since 2007 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 *}
{if $field.type == 'hidden'}

  {block name='form_field_item_hidden'}
    <input type="hidden" name="{$field.name}" value="{$field.value}">
  {/block}

{else}

	{if $field.name == "address1"}
	
	  <div class="form-group row no-margin">
	    <label class="col-md-4 form-control-label required">
			Adresse complète
	    </label>
	    <div class="col-md-8">
		{block name='form_field_item_other'}
			<input
				class="form-control"
				name="google_address"
				id="google_address"
				type="text"
				value="" 
				placeholder="Recherchez votre adresse" 
				autocomplete="chrome-off"
			>
			<input
				id="location_position"
				name="location_position"
				type="hidden"
				value="{$field.value}"
			>
		{/block}
	    </div>
	  </div>
	  <div class="form-group row" style="margin-bottom: 5px">
	    <label class="col-md-4"></label>
	    <div class="col-md-8">
			<p class="hint" style="margin-bottom: 5px"><i class="fa fa-warning"></i> Renseigner ce champ pour remplir automatiquement les lignes de votre adresse.</p>
	    </div>
	  </div>
	  <div class="form-group row">
	    <label class="col-md-4"></label>
	    <div class="col-md-8">
			<div class="alert alert-info"><span class="icon"><i class="fa fa-info-circle"></i></span> <span class="text">Si Google ne trouve pas votre adresse, merci de nous appeler au <a href="tel:{$shop_phone}">{$shop_phone}</a>.</span></div>
	    </div>
	  </div>

	{/if}

	{if $field.name != "google_address" && $field.name != "location_position"}
	  <div class="form-group row{if !empty($field.errors)} has-error{/if}{if $field.type === 'checkbox'} checkbox{/if}">
	    <label class="col-md-4 form-control-label{if $field.required} required{else} optional{/if}">
	      {if $field.type !== 'checkbox'}
	        {$field.label}{if $field.required} *{/if}
	      {/if}
	    </label>
	    <div class="col-md-8{if ($field.type === 'radio-buttons')} form-control-valign{/if}{if $field.type === 'checkbox'} col-checkbox{/if}{if $field.type === 'password'} password-container{/if}">
	
	      {if $field.type === 'select'}
	
	        {block name='form_field_item_select'}
	          <select class="form-control form-control-select" name="{$field.name}" {if $field.required}required{/if}>
	            <option value disabled selected>{l s='-- please choose --' d='Shop.Forms.Labels'}</option>
	            {foreach from=$field.availableValues item="label" key="value"}
	              <option value="{$value}" {if $value eq $field.value} selected {/if}>{$label}</option>
	            {/foreach}
	          </select>
	        {/block}
	
	      {elseif $field.type === 'countrySelect'}
	
	        {block name='form_field_item_country'}
	          <select
	          class="form-control form-control-select js-country"
	          name="{$field.name}"
	          {if $field.required}required{/if}
		      readonly
	          >
	            <option value disabled selected>{l s='-- please choose --' d='Shop.Forms.Labels'}</option>
	            {foreach from=$field.availableValues item="label" key="value"}
	              <option value="{$value}" {if $value eq $field.value} selected {/if}>{$label}</option>
	            {/foreach}
	          </select>
	        {/block}
	
	      {elseif $field.type === 'secteurSelect'}
	
	        {block name='form_field_item_secteur'}
	          <select
	          class="form-control form-control-select"
	          name="{$field.name}"
	          required
	          >
	            <option value disabled selected>{l s='-- please choose --' d='Shop.Forms.Labels'}</option>
	            {foreach from=$field.availableValues item="label" key="value"}
	              <option value="{$value}" {if $value eq $field.value} selected {/if}>{$label}</option>
	            {/foreach}
	          </select>
	        {/block}
	
	      {elseif $field.type === 'radio-buttons'}
	
	        {block name='form_field_item_radio'}
	          {foreach from=$field.availableValues item="label" key="value"}
	            <label class="radio-inline">
	              <span class="custom-radio">
	                <input
	                  name="{$field.name}"
	                  type="radio"
	                  value="{$value}"
	                  {if $field.required}required{/if}
	                  {if $value eq $field.value} checked {/if}
	                >
	                <span></span>
	              </span>
	              {$label}
	            </label>
	          {/foreach}
	        {/block}
	
	      {elseif $field.type === 'checkbox'}
	
	        {block name='form_field_item_checkbox'}
	          <span class="custom-checkbox">
	            <label>
	              <input name="{$field.name}" type="checkbox" value="1" {if $field.value}checked="checked"{/if} {if $field.required}required{/if}>
	              <span><i class="material-icons rtl-no-flip checkbox-checked">&#xE5CA;</i></span>
	              {$field.label nofilter}
	            </label>
	          </span>
	        {/block}
	
	      {elseif $field.type === 'date'}
	
	        {block name='form_field_item_date'}
	          <input 
	          	name="{$field.name}" 
	          	class="form-control" 
	          	type="date" 
	          	value="{$field.value}" 
	          	placeholder="{if isset($field.availableValues.placeholder)}{$field.availableValues.placeholder}{/if}"
				autocomplete="chrome-off"
	          >
	          {if isset($field.availableValues.comment)}
	            <span class="form-control-comment">
	              {$field.availableValues.comment}
	            </span>
	          {/if}
	        {/block}
	
	      {elseif $field.type === 'birthday'}
	
	        {block name='form_field_item_birthday'}
	          <div class="js-parent-focus">
	            {html_select_date
	            field_order=DMY
	            time={$field.value}
	            field_array={$field.name}
	            prefix=false
	            reverse_years=true
	            field_separator='<br>'
	            day_extra='class="form-control form-control-select"'
	            month_extra='class="form-control form-control-select"'
	            year_extra='class="form-control form-control-select"'
	            day_empty={l s='-- day --' d='Shop.Forms.Labels'}
	            month_empty={l s='-- month --' d='Shop.Forms.Labels'}
	            year_empty={l s='-- year --' d='Shop.Forms.Labels'}
	            start_year={'Y'|date}-100 end_year={'Y'|date}
	            }
	          </div>
	        {/block}
	
	      {elseif $field.type === 'password'}
	
	        {block name='form_field_item_password'}
	          <div class="input-group js-parent-focus">
	            <input
	              class="form-control js-child-focus js-visible-password withToolTip"
	              name="{$field.name}"
	              title="Saisir au moins 5 caractères"
	              type="password"
	              value=""
	              pattern=".{literal}{{/literal}5,{literal}}{/literal}"
	              {if $field.required}required{/if}
				  autocomplete="chrome-off"
	            >
	            <span class="input-group-btn">
	              <button
	                class="btn"
	                type="button"
	                data-action="show-password"
	                data-text-show="{l s='Show' d='Shop.Theme.Actions'}"
	                data-text-hide="{l s='Hide' d='Shop.Theme.Actions'}"
	              >
	                {l s='Show' d='Shop.Theme.Actions'}
	              </button>
	            </span>
	          </div>
	        {/block}
	
	      {else}
	
	        {block name='form_field_item_other'}
	          <input
	            class="form-control"
	            name="{$field.name}"
	            type="{$field.type}"
	            value="{$field.value}"
	            {if isset($field.availableValues.placeholder)}placeholder="{$field.availableValues.placeholder}"{/if}
	            {if $field.maxLength}maxlength="{$field.maxLength}"{/if}
	            {if $field.required}required{/if}
	            {if $field.name=="address1" || $field.name=="postcode" || $field.name=="city"}readonly{/if}
				autocomplete="chrome-off"
	          >
	          {if isset($field.availableValues.comment)}
	            <span class="form-control-comment">
	              {$field.availableValues.comment}
	            </span>
	          {/if}
		        {if $field.name=="address1" || $field.name=="postcode" || $field.name=="city" || $field.type=="countrySelect"} 
		        	<p class="auto">Remplissage automatique</p>
		        {/if}
	        {/block}
	
	      {/if}
	
	      {block name='form_field_errors'}
	        {include file='_partials/form-errors.tpl' errors=$field.errors}
	      {/block}
	
	    </div>
	
	  </div>
	{/if}

{/if}
