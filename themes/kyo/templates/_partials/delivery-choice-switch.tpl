
	<div class="delivery_choice row">
    	<div class="row delivery_choice">
        	<div class="col-xs-12">
	        	<a href="/index.php?set_checkout_delivery_mode=1&delivery_mode=2" class="home_choice{if $delivery_mode==2} current{/if}">
		        	<img src="{$urls.img_url}picto_emporter.png" />
		        	<div>
			        	<span>Je veux <strong>prendre à emporter</strong></span>
			        	{if $taux_reduc_emporter}
			        	<span class="small">Je bénéficie d'une réduction de {$taux_reduc_emporter}%</span>
			        	{/if}
		        	</div>
			    </a>
        	</div>		        	
    	</div>
    	<div class="row delivery_choice">
        	<div class="col-xs-12">
	        	<!-- <a href="javascript:" onclick="showDeliveryAddressForm()" data-url="/index.php?set_home_delivery_mode=1&delivery_mode=1" class="home_choice{if $delivery_mode==1} current{/if}"> -->
	        	<a href="/index.php?set_checkout_delivery_mode=1&delivery_mode=1" class="home_choice{if $delivery_mode==1} current{/if}">
		        	<img src="{$urls.img_url}picto_livrer.png" />
		        	<div>
			        	<span>Je veux <strong>me faire livrer</strong></span>
			        	<span class="small">Livraison gratuite sous condition</span>
		        	</div>
			    </a>
        	</div>
    	</div>
		<div class="clearfix"></div>
	</div>
