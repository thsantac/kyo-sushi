{**
 * Copyright since 2007 PrestaShop SA and Contributors
 * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.md.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://devdocs.prestashop.com/ for more information.
 *
 * @author    PrestaShop SA and Contributors <contact@prestashop.com>
 * @copyright Since 2007 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 *}
<div class="container">
  	<div class="row">
    {block name='hook_footer_before'}
	    <div id="footer_before">
        {JsComposer::do_shortcode($footer_before) nofilter}
	    </div>
    {/block}
  	</div>
</div>
<div class="footer-container aos-item aos-init aos-animate" data-aos="fade-up">
	<div class="container">
		<div class="row">
		{block name='hook_footer_top'}
			{hook h='displayFooterTop'}
		{/block}
		</div>
		<div class="row margin-top">
		{block name='hook_footer'}
			<div class="col-md-8 col-xs-12">
				{hook h='displayFooter'}
				<div class="footer-block-app">
				    <div class="row">
				        <div class="col-lg-12 text-xs-center">
							<label>Retrouvez-nous facilement en téléchargeant notre application :</label>
				        </div>
				    </div>
				    <div class="row">
					    <div class="col-lg-12">
					        <div class="col-lg-6 col-xs-6" style="text-align: right;">
					            <a class="app" href="https://play.google.com/store/apps/details?id=com.kyosushi.mobile" target="_blank" rel="noreferrer" title="Notre application Android"><img src="{$urls.img_url}google_play.png" class="img-responsive" alt="Google Store" ></a>
					        </div>
					        <div class="col-lg-6 col-xs-6" style="text-align: left;">
					            <a class="app" href="https://itunes.apple.com/us/app/kyo-sushi/id1417116987?l=fr&amp;ls=1&amp;mt=8" target="_blank" rel="noreferrer" title="Notre application iOS"><img src="{$urls.img_url}app_store.png" class="img-responsive" alt="Apple Store" ></a>
					        </div>
				        </div>
				    </div>
				</div>
			</div>
			<div class="col-md-4 col-xs-12">
				{hook h='displayFooterInfos'}
			</div>
		{/block}
		</div>
		<div class="row">
		{block name='hook_footer_after'}
			{hook h='displayFooterAfter'}
		{/block}
		</div>
		<div class="row">
			<div class="col-md-12">
				{block name='copyright_link'}
				    <p class="copyright">
				      {block name='copyright_link'}
						© {'Y'|date} KYO SUSHI&nbsp;&nbsp;|&nbsp;&nbsp;Site développé par <a href="https://www.la-ligne-web.com" target="_blank" rel="noreferrer">La Ligne Web</a> - Agence web Aix-en-Provence
				      {/block}
				    </p>
				{/block}
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="modal-cms">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content no-title">
			<button type="button" class="close" data-dismiss="modal" aria-label="{l s='Close' d='Shop.Theme.Global'}"></button>
			<div class="js-modal-content"><div style="text-align: center"><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i></div></div>
		</div>
	</div>
</div>

<div class="modal fade" id="modal-map">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content no-title">
			<button type="button" class="close" data-dismiss="modal" aria-label="{l s='Close' d='Shop.Theme.Global'}"></button>
			<div id="shop_map"></div>
		</div>
	</div>
</div>

<div class="modal fade" id="modal-delivery-address">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="{l s='Close' d='Shop.Theme.Global'}"></button>
				<h4 class="modal-title">Modal title</h4>
      		</div>
	  		<div class="modal-body">
	  			<div class="form-group row">
				    <label class="col-md-4 form-control-label">
						Adresse de livraison souhaitée
				    </label>
				    <div class="col-md-6">
						<input class="form-control pac-target-input" name="modal_google_address" id="modal_google_address" type="text" value="{if isset($customer.last_delivery_address)}{$customer.last_delivery_address}{/if}" placeholder="Recherchez votre adresse" autocomplete="chrome-off">
						<input id="modal_location_position" name="modal_location_position" type="hidden" value="">
				    </div>
				</div>
				<div class="form-group row">
				    <label class="col-md-4"></label>
				    <div class="col-md-6">
						<div class="alert alert-info"><i class="fa fa-info-circle"></i> Si Google ne trouve pas votre adresse, merci de nous appeler au <a href="tel:{$shop_phone}">{$shop_phone}</a>.</div>
				    </div>
				</div>
			</div>
	  		<div class="modal-footer">
	  			<button type="button" class="btn btn-default" data-dismiss="modal">{l s='Close' d='Shop.Theme.Global'}</button>
	  			<button type="button" class="btn btn-primary" id="deliveryAddressCheck" disabled="disabled">Tester cette adresse</button>
	  			<button type="button" class="btn btn-primary" id="deliveryAddressSave" disabled="disabled" style="display: none">Valider cette adresse</button>
      		</div>
		</div>
	</div>
</div>

<div id="slideRegisterForm" class="sb-form-right">
	<div class="close"></div>
	<div class="form-preview">
		<div class="form-title">
          <p class="h4 text-center">Créer votre compte Kyo Sushi</p>
        </div>
		<input id="registerURL" type="hidden" value="{$urls.pages.register}" />
		<section class="register-form" id="registerForm">
			<div id="registerFormContainer"></div>
		    <div id="registerLoading">
		        <div class="logo">
		            <img src="{$urls.img_url}logo_kyo_rouge.png" class="fa-spin" alt="Loading" >
		            <p>Merci de patienter</p>
		        </div>
		    </div>
		    <div id="registerOK" style="display: none">
		        <div class="alert alert-info">La création de votre compte a été correctement prise en compte. Vous êtes connecté à votre Espace Client.</div>
		        <div class="form-footer">
			        <button class="btn btn-secondary" id="closeRegisterForm">Fermer</button>
		        </div>
		    </div>
    	</section>
		<div class="displayCustomerFormAfter" style="display: none">
			<p>* Informations obligatoires</p>
			{hook h='displayCustomerFormAfter'}
		</div>
	</div>
</div>

<div class="modal fade" id="modalChoixLivraison">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="{l s='Close' d='Shop.Theme.Global'}"></button>
				<h2 class="modal-title">Choix du mode de livraison</h2>
      		</div>
	  		<div class="modal-body">
				<div class="row">
					<div class="col-md-12">
						<h4>Vous êtes actuellement sur la boutique en ligne du point de vente :<br /><br /><span class="shop_name">{$store_name}</span></h4>
					</div>
				</div>
				{if $taux_reduc_emporter > 0}
				<div class="row">
					<div class="col-md-12">
						<div class="well well-sm">
							<p>Les plats à emporter de ce point de vente<br />bénéficient d'une remise de <span class="taux_reduc">{$taux_reduc_emporter}%</span></p>
						</div>
					</div>
				</div>
				{/if}
				<div class="row">
					<div class="col-md-12 text-center">
						<i class="fa fa-angle-right"></i> 
						<a class="in_popup" href="/content/1-livraison">En savoir plus sur nos zones de livraison et montant minimum de commande</a>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<h3>Veuillez choisir votre mode de livraison</h3>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12 col-sm-6 col-md-6">
						<a class="btn btn-primary{if $delivery_mode==1} current{/if} modalChoixLivraisonLink" title="" rel="nofollow" href="/index.php?set_categ_delivery_mode=1&delivery_mode=1" id="modalChoixLivraison_livrer_link" data_delivery_mode="1">
							<span><img src="{$urls.img_url}picto_livrer.png" />LIVRAISON</span>
						</a>
					</div>
					<div class="col-xs-12 col-sm-6 col-md-6">
						<a class="btn btn-primary{if $delivery_mode==2} current{/if} modalChoixLivraisonLink" title="" rel="nofollow" href="/index.php?set_categ_delivery_mode=1&delivery_mode=2" id="modalChoixLivraison_emporter_link" data_delivery_mode="2">
							<span><img src="{$urls.img_url}picto_emporter.png" />À EMPORTER</span>
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

{if isset($notifications.error) && $notifications.error}
    <script type="text/javascript">
	    var errorNotification = '<ul class="text">';
        {foreach $notifications.error as $notif}
        	errorNotification += "<li>{$notif nofilter}</li>";
        {/foreach}
        errorNotification += '</ul>';
	</script>
{else}
    <script type="text/javascript">
	    var errorNotification = '';
	</script>
{/if}

<script type="text/javascript">
    var delivery_mode = '{$delivery_mode}';
</script>

{if $auto_add_to_cart}
<script type="text/javascript">
	var _global_auto_add_to_cart = true;
	var _global_auto_add_quantity_wanted 	= '{$auto_add_quantity_wanted}';
	var _global_auto_add_id_product 		= '{$auto_add_id_product}';
	var _global_auto_add_page 				= '{$auto_add_page}';
</script>
{/if}
