
    <div class="homeSliderWelcome {$delivery_mode}" id="{$time}">
	    <div class="container">
		    <h2>Bienvenue</h2>
			{if $booking_link}
	    	<div class="row delivery_choice">
	        	<div class="col-lg-4 col-md-3 hidden-sm-down"></div>
	        	<div class="col-lg-4 col-md-6 col-xs-12">
		        	<a href="{$booking_link}" class="home_choice" target="_blank" rel="noreferrer">
			        	<img src="{$urls.img_url}picto_reserver.png" alt="Réserver" />
			        	<div>
				        	<span>Je veux <strong>réserver une table</strong></span>
				        	<span class="small">Pour aujourd'hui ou pour plus tard</span>
			        	</div>
				    </a>
	        	</div>		        	
	        	<div class="col-lg-4 col-md-3 hidden-sm-down"></div>
	    	</div>
			{/if}
	    	<div class="row delivery_choice">
	        	<div class="col-lg-4 col-md-3 col-xs-1"></div>
	        	<div class="col-lg-4 col-md-6 col-xs-12">
		        	<a href="/index.php?set_home_delivery_mode=1&delivery_mode=2" class="home_choice{if $delivery_mode==2} current{/if}">
			        	<img src="{$urls.img_url}picto_emporter.png" alt="Emporter" />
			        	<div>
				        	<span>Je veux <strong>prendre à emporter</strong></span>
				        	{if $taux_reduc_emporter}
				        	<span class="small">Je bénéficie d'une réduction de {$taux_reduc_emporter}%</span>
				        	{/if}
			        	</div>
				    </a>
	        	</div>		        	
	        	<div class="col-lg-4 col-md-3 hidden-sm-down"></div>
	    	</div>
	    	<div class="row delivery_choice">
	        	<div class="col-lg-4 col-md-3 hidden-sm-down"></div>
	        	<div class="col-lg-4 col-md-6 col-xs-12">
		        	<!-- <a href="javascript:" onclick="showDeliveryAddressForm()" data-url="/index.php?set_home_delivery_mode=1&delivery_mode=1" class="home_choice{if $delivery_mode==1} current{/if}"> -->
		        	<a href="/index.php?set_home_delivery_mode=1&delivery_mode=1" class="home_choice{if $delivery_mode==1} current{/if}">
			        	<img src="{$urls.img_url}picto_livrer.png" alt="Livrer" />
			        	<div>
				        	<span>Je veux <strong>me faire livrer</strong></span>
				        	<span class="small">Livraison gratuite sous conditions</span>
			        	</div>
				    </a>
	        	</div>
	        	<div class="col-lg-4 col-md-3 hidden-sm-down"></div>
	    	</div>
	  	</div>
    </div>