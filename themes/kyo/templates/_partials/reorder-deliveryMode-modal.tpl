{**
 * 2007-2019 PrestaShop and Contributors
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2019 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}

<div class="modal fade" id="modalChoixLivraison">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="{l s='Close' d='Shop.Theme.Global'}"></button>
				<h2 class="modal-title">Choix du mode de livraison</h2>
      		</div>
	  		<div class="modal-body">
				<div class="row">
					<div class="col-md-12">
						<h4>Vous êtes actuellement sur la boutique en ligne du point de vente :<br /><br /><span class="shop_name">{$store_name}</span></h4>
					</div>
				</div>
				{if $taux_reduc_emporter > 0}
				<div class="row">
					<div class="col-md-12">
						<div class="well well-sm">
							<p>Les plats à emporter de ce point de vente<br />bénéficient d'une remise de <span class="taux_reduc">{$taux_reduc_emporter}%</span></p>
						</div>
					</div>
				</div>
				{/if}
				<div class="row">
					<div class="col-md-12 text-center">
						<i class="fa fa-angle-right"></i> 
						<a class="in_popup" href="/content/1-livraison">En savoir plus sur nos zones de livraison et montant minimum de commande</a>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<h3>Veuillez choisir votre mode de livraison</h3>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12 col-sm-6 col-md-6">
				    	<a href="/index.php?set_checkout_delivery_mode=1&delivery_mode=1" id="livraison" class="btn btn-primary"><span><img src="{$urls.img_url}picto_livrer.png" />Je veux me faire livrer</span></a>
					</div>
					<div class="col-xs-12 col-sm-6 col-md-6">
				    	<a href="/index.php?set_checkout_delivery_mode=1&delivery_mode=2" id="emporter" class="btn btn-primary"><span><img src="{$urls.img_url}picto_emporter.png" />Je vais venir la chercher</span></a>
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
