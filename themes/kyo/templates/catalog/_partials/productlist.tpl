{**
 * Copyright since 2007 PrestaShop SA and Contributors
 * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.md.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://devdocs.prestashop.com/ for more information.
 *
 * @author    PrestaShop SA and Contributors <contact@prestashop.com>
 * @copyright Since 2007 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 *}
<div class="products" itemscope itemtype="http://schema.org/ItemList">
	{assign var="categoryName" value=""}
    {foreach from=$products item="product" key="position"}

    	{if $product.category_default != $categoryName}
	    	{if $categoryName}
	</div>
	<div class="clearfix"></div>
	<div class="row"><div class="col-xs-12"><hr size="1" /></div></div>
	    	{/if}
	    	{assign var="categoryName" value=$product.category_default}
	<div class="row" id="{$product.category}">
		<div class="col-md-12 products_category_name">
			<h2 id="{$product.category}_title">{$categoryName} : <span class="nb_products"><span id="nb_products_{$product.category}">{$nb_products_by_category[$product.id_category_default]}</span> produits</span></h2>
		</div>
	</div>
	<div class="row">
		{/if}

        {include file="catalog/_partials/miniatures/product.tpl" product=$product position=$position}
    {/foreach}
	</div>
	<div class="clearfix"></div>
	<div class="row"><div class="col-xs-12"><hr size="1" /></div></div>
</div>