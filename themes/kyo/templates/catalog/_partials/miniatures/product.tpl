{**
 * Copyright since 2007 PrestaShop SA and Contributors
 * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.md.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://devdocs.prestashop.com/ for more information.
 *
 * @author    PrestaShop SA and Contributors <contact@prestashop.com>
 * @copyright Since 2007 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 *}
{block name='product_miniature_item'}
  {if isset($position)}<meta itemprop="position" content="{$position}" />{/if}
	<article class="
		{if isset($productByLine)} 
			{if $productByLine==6}
				col-xl-2 col-lg-3 col-md-3 col-sm-4 col-xs-6 product-miniature js-product-miniature sixByLine
			{else}
			{if $productByLine==4}
				col-lg-3 col-md-4 col-sm-6 col-xs-6 product-miniature js-product-miniature fourByLine
			{else}
			{if $productByLine==1}
				product-miniature js-product-miniature oneByLine 
			{else}
				col-xl-4 col-lg-4 col-md-4 col-sm-6 col-xs-6 product-miniature js-product-miniature threeByLine
			{/if}
			{/if}
			{/if}
		{else}
			col-xl-2 col-lg-3 col-md-3 col-sm-4 col-xs-6 product-miniature js-product-miniature sixByLine
		{/if}
		" data-id-product="{$product.id_product}" data-id-product-attribute="{$product.id_product_attribute}" itemprop="item" itemscope itemtype="http://schema.org/Product" data-features="{$product.data_features}"		
	>
		<div itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
		    <div class="thumbnail-container{if $product.in_cart} in-cart withToolTip{/if}" id="thumbnail-container-{$product.id_product}"{if $product.in_cart} title="Ce produit est déjà dans votre panier"{/if}>
				{block name='product_thumbnail'}
		        {if $product.cover}
		          <a href="javascript:" data-product-url="{$product.url}" data-id-product="{$product.id_product}" data-id-product-attribute="{$product.id_product_attribute}" class="link-to-product thumbnail product-thumbnail">
		            <img
		              src="{$product.cover.bySize.home_default.url}"
		              alt="{if !empty($product.cover.legend)}{$product.cover.legend}{else}{$product.name}{/if}"
		              data-full-size-image-url="{$product.cover.large.url}"
		              loading="lazy" 
		              />
					  {if isset($tpl_dir)}
					  {include file="$tpl_dir/templates/catalog/_partials/product-flags.tpl"}
					  {else}
					  {include file='catalog/_partials/product-flags.tpl'}
					  {/if}
					  <div class="in-cart-marker"></div>
		          </a>
		        {else}
		          <a href="javascript:" data-product-url="{$product.url}" class="link-to-product thumbnail product-thumbnail">
					<img 
						src="{$urls.no_picture_image.bySize.home_default.url}" 
						loading="lazy" 
					/>
					  {if isset($tpl_dir)}
					  {include file="$tpl_dir/templates/catalog/_partials/product-flags.tpl"}
					  {else}
					  {include file='catalog/_partials/product-flags.tpl'}
					  {/if}
					<div class="in-cart-marker"></div>
		          </a>
		        {/if}
				{/block}
		
				<div class="product-description">
					{block name='product_name'}
					{if $page.page_name == 'index'}
					<h3 class="h3 product-title" itemprop="name"><a href="{$product.url}" itemprop="url" content="{$product.url}">{$product.name|truncate:50:'...'}</a></h3>
					{else}
					<h2 class="h3 product-title" itemprop="name"><a href="{$product.url}" itemprop="url" content="{$product.url}">{$product.name|truncate:50:'...'}</a></h2>
					{/if}
					{/block}
				</div>
		
		        {block name='product_price_and_shipping'}
				{if $product.show_price}
					<div class="product-price-and-shipping">
	
						{if $product.has_discount}
	
							{hook h='displayProductPriceBlock' product=$product type="old_price"}
							
							<span class="regular-price" aria-label="{l s='Regular price' d='Shop.Theme.Catalog'}">{$product.regular_price}</span>
							{if $product.discount_type === 'percentage'}
							<span class="discount-percentage discount-product">{$product.discount_percentage}</span>
							{elseif $product.discount_type === 'amount'}
							<span class="discount-amount discount-product">{$product.discount_amount_to_display}</span>
							{/if}
	
						{/if}
						
						{hook h='displayProductPriceBlock' product=$product type="before_price"}
						
						<span class="price" aria-label="{l s='Price' d='Shop.Theme.Catalog'}">{$product.price}</span>
						
						<div itemprop="offers" itemscope itemtype="http://schema.org/Offer" class="invisible">
							<meta itemprop="priceCurrency" content="{$currency.iso_code}" />
							<meta itemprop="price" content="{$product.price_amount}" />
						</div>
						
						{hook h='displayProductPriceBlock' product=$product type='unit_price'}
	
						<div class="add-to-cart">
							{hook h='displayProductPriceBlock' product=$product type='weight'}
						</div>
				
					</div>
				{/if}
		        {/block}
	
		        {block name='product_reviews'}
		          {hook h='displayProductListReviews' product=$product}
		        {/block}
		
				<div class="highlighted-informations{if !$product.main_variants} no-variants{/if} hidden-sm-down">
				{block name='product_variants'}
				  {if $product.main_variants}
					  {if isset($tpl_dir)}
					  {include file="$tpl_dir/templates/catalog/_partials/variant-links.tpl" variants=$product.main_variants}
					  {else}
					  {include file='catalog/_partials/variant-links.tpl' variants=$product.main_variants}
					  {/if}
				  {/if}
				{/block}
				</div>
	
		    </div>
		</div>
	</article>
{/block}
