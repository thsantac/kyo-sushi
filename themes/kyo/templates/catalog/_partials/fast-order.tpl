{*
* 2007-2016 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2016 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

		{assign var=categIndex value=$categIndex+1}
		{if $lastOrder}
			<div id="fast-order" class="col-lg-6 col-md-12 col-sm-12 col-xs-12 aos-item aos-init aos-animate movable-parent" data-aos="fade-up">
				<div class="category-item-order movable-bloc" id="category-item-{$categIndex}" data-categindex="{$categIndex}">
					<img src="{$urls.img_url}picto_reorder.png" alt="Commander" />
					<div class="text">
					    <h3>Je souhaite utiliser ma dernière commande</h3>
					    <button class="btn btn-primary" type="button" data-reorder_url="{$url_to_reorder}" data-seeorder_url="{$url_to_seeorder}" id="seeLastOrderButton">Voir ma dernière commande</button>
					</div>
				</div>
			</div>
		{else}
			{if $customer.is_logged}
			<div id="fast-order" class="col-lg-6 col-md-12 col-sm-12 col-xs-12 aos-item aos-init aos-animate movable-parent" data-aos="fade-up">
				<div class="category-item-order movable-bloc" id="category-item-{$categIndex}" data-categindex="{$categIndex}">
					<img src="{$urls.img_url}picto_reorder.png" alt="Commander" />
					<div class="text">
					    <h3>Passez votre première commande !</h3>
						<h4>Et bénéficiez, la prochaine fois, de notre fonction<br><span>« Commande rapide »</span> !</h4>
					</div>
				</div>
			</div>
			{else}
			<div id="fast-order" class="not-registered col-lg-6 col-md-12 col-sm-12 col-xs-12 aos-item aos-init aos-animate movable-parent" data-aos="fade-up">
				<div class="category-item-order movable-bloc" id="category-item-{$categIndex}" data-categindex="{$categIndex}">
					<img src="{$urls.img_url}picto_reorder.png" alt="Commander" />
					<div class="text">
					    <h3>Enregistrez-vous sur Kyo Sushi !</h3>
						<h4>Et bénéficiez, la prochaine fois, de notre fonction<br><span>« Commande rapide »</span> !</h4>
					    <button class="btn btn-primary clickToRegister" type="button">Je m'enregistre</button>
					</div>
				</div>
			</div>
			{/if}
		{/if}
		</div>
<script type="text/javascript">
	var url_to_reorder = "{$url_to_reorder}";
</script>
