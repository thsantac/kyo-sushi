{**
 * Copyright since 2007 PrestaShop SA and Contributors
 * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.md.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://devdocs.prestashop.com/ for more information.
 *
 * @author    PrestaShop SA and Contributors <contact@prestashop.com>
 * @copyright Since 2007 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 *}
{extends file=$layout}

{block name='product_list_title'}
<h2 id="js-product-list-header" class="h2">{$listing.label}</h2>
{/block}

{block name='product_list_delivery_mode'}{/block}
{block name='product_list_top_navig'}
	{hook h="displayProductListTop"}
{/block}

{block name='content'}
  <section id="main">

    <section id="products">
      {if $listing.products|count}

        <div id="product_list_top">
			<!--
			{block name='product_list_top'}
				{include file='catalog/_partials/products-top.tpl' listing=$listing}
			{/block}
			-->
        </div>

		<div id="search_filters_wrapper">
			<a class="float-xs-right close" href="javascript:"></a>
			<h2>Filtrez notre carte</h2>
			<h3>et retirez facilement tout ce que vous ne voulez pas trouver</h3>
		
			{hook h="displayExclusionFilters"}
		
			<div id="search_filter_controls">
				<span id="_mobile_search_filters_clear_all"></span>
				<button class="btn btn-secondary btn-red ok">
					<i class="material-icons rtl-no-flip">&#xE876;</i>
					Appliquer et fermer
				</button>
			</div>
		</div>

        {block name='product_list_active_filters'}
          <div id="product_list_active_filters" style="display: none">
            {include file='catalog/_partials/active_filters.tpl' listing=$listing}
          </div>
        {/block}

        <div id="product_list">
          {block name='product_list'}
            {include file='catalog/_partials/products.tpl' listing=$listing}
          {/block}
        </div>

        <div id="js-product-list-bottom">
          {block name='product_list_bottom'}
            {include file='catalog/_partials/products-bottom.tpl' listing=$listing}
          {/block}
        </div>

      {else}
        <div id="js-product-list-top"></div>

        <div id="js-product-list">
          {include file='errors/not-found.tpl'}
        </div>

        <div id="js-product-list-bottom"></div>
      {/if}
    </section>

    {hook h="displayFooterCategory"}

  </section>
{/block}
