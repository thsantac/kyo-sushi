{**
 * Copyright since 2007 PrestaShop SA and Contributors
 * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.md.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://devdocs.prestashop.com/ for more information.
 *
 * @author    PrestaShop SA and Contributors <contact@prestashop.com>
 * @copyright Since 2007 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 *}
{extends file='catalog/listing/product-list.tpl'}

{block name='product_list_title'}
{/block}

{block name='product_list_header'}
	<div id="product-list-header" class="product_list_header">
    {include file='catalog/_partials/category-header.tpl' listing=$listing category=$category}
	</div>
{/block}

{block name='product_list_delivery_mode'}
	<div id="product-list-delivery-mode">
		<div class="container">
			{if $delivery_mode==1}
	    	<div class="row">
	        	<div class="col-lg-4 col-md-6 col-xs-12">
			    	<div class="delivery_choice choosen">
			        	<!-- <a href="javascript:" onclick="showDeliveryAddressForm()" data-url="/index.php?set_home_delivery_mode=1&delivery_mode=1" class="home_choice{if $delivery_mode==1} current{/if}"> -->
			        	<img src="{$urls.img_url}picto_livrer.png" />
			        	<div>
				        	<span>Je veux <strong>me faire livrer</strong></span>
				        	<span class="small">Livraison possible à partir de <strong>{$mt_min_order}€</strong> de commande</span>
			        	</div>
		        	</div>		        	
	        	</div>		        	
	        	<div class="col-lg-4 col-md-6 col-xs-12">
			    	<div class="delivery_infos">
			        	<div>
				        	<span><strong>Livraison : </strong>
							{if $nextDeliveryDateAndHour.today}
								{$nextDeliveryDateAndHour.hour}
							{else}
								<span class="day-hour">Le {$nextDeliveryDateAndHour.day} à {$nextDeliveryDateAndHour.hour}</span>
							{/if}
				        	</span>
				        	<span class="small">Horaire approximatif modifiable avant le paiement</span>
			        	</div>
		        	</div>		        	
	        	</div>
	        	<div class="col-lg-2 col-md-6 col-xs-6">
			    	<div class="delivery_link">
				    	<a class="btn btn-primary btn-black withSubtitle" href="javascript:" id="categListDeliveryChoiceModifier">Modifier<span>le mode de livraison</span></a>
		        	</div>		        	
	        	</div>
	        	<div class="col-lg-2 col-md-6 col-xs-6">
			    	<div class="delivery_link">
				    	<a class="btn btn-primary btn-black in_popup" href="/content/1-livraison">En savoir plus</a>
		        	</div>		        	
	        	</div>
	    	</div>
			{else}
			{if $delivery_mode==2}
	    	<div class="row">
	        	<div class="col-lg-4 col-md-6 col-xs-12">
			    	<div class="delivery_choice choosen">
			        	<img src="{$urls.img_url}picto_emporter.png" />
			        	<div>
				        	<span>Je veux <strong>prendre à emporter</strong></span>
				        	{if $taux_reduc_emporter}
				        	<span class="small">Je bénéficie d'une réduction de {$taux_reduc_emporter}%</span>
				        	{/if}
			        	</div>
		        	</div>		        	
	        	</div>		        	
	        	<div class="col-lg-4 col-md-6 col-xs-12">
			    	<div class="delivery_infos">
			        	<div>
				        	<span><strong>Retrait au plus tôt : </strong>
							{if $nextDeliveryDateAndHour.today}
								{$nextDeliveryDateAndHour.hour}
							{else}
								<span class="day-hour">Le {$nextDeliveryDateAndHour.day} à {$nextDeliveryDateAndHour.hour}</span>
							{/if}
				        	</span>
				        	<span class="small">Horaire approximatif modifiable avant le paiement</span>
			        	</div>
		        	</div>		        	
	        	</div>
	        	<div class="col-lg-2 col-md-6 col-xs-6">
			    	<div class="delivery_link">
				    	<a class="btn btn-primary btn-black withSubtitle" href="javascript:" id="categListDeliveryChoiceModifier">Modifier<span>le mode de livraison</span></a>
		        	</div>		        	
	        	</div>
	        	<div class="col-lg-2 col-md-6 col-xs-6">
			    	<div class="delivery_link">
				    	<a class="btn btn-primary btn-black in_popup" href="/content/7-vente-a-emporter">En savoir plus</a>
		        	</div>		        	
	        	</div>
	    	</div>
			{else}
	    	<div class="row">
	        	<div class="col-lg-6 col-md-6 col-xs-12">
			    	<div class="delivery_choice">
			        	<!-- <a href="javascript:" onclick="showDeliveryAddressForm()" data-url="/index.php?set_home_delivery_mode=1&delivery_mode=1" class="home_choice{if $delivery_mode==1} current{/if}"> -->
			        	<a href="/index.php?set_categ_delivery_mode=1&delivery_mode=1" class="home_choice">
				        	<img src="{$urls.img_url}picto_livrer.png" />
				        	<div>
					        	<span>Je veux <strong>me faire livrer</strong></span>
					        	<span class="small">Livraison gratuite sous conditions</span>
				        	</div>
					    </a>
		        	</div>		        	
	        	</div>
	        	<div class="col-lg-6 col-md-6 col-xs-12">
			    	<div class="delivery_choice">
			        	<a href="/index.php?set_categ_delivery_mode=1&delivery_mode=2" class="home_choice">
				        	<img src="{$urls.img_url}picto_emporter.png" />
				        	<div>
					        	<span>Je veux <strong>prendre à emporter</strong></span>
					        	{if $taux_reduc_emporter}
					        	<span class="small">Je bénéficie d'une réduction de {$taux_reduc_emporter}%</span>
					        	{/if}
				        	</div>
					    </a>
		        	</div>		        	
	        	</div>		        	
	    	</div>
			{/if}
			{/if}
		</div>
	</div>
{/block}
