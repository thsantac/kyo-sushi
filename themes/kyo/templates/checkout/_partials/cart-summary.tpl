{**
 * Copyright since 2007 PrestaShop SA and Contributors
 * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.md.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://devdocs.prestashop.com/ for more information.
 *
 * @author    PrestaShop SA and Contributors <contact@prestashop.com>
 * @copyright Since 2007 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 *}
<section id="js-checkout-summary" class="card js-cart cart-summary" data-refresh-url="{$urls.pages.cart}?ajax=1&action=refresh">
	
	<div class="cart-summary-container">

		<h2>Récapitulatif</h2>
		
		{block name='hook_checkout_summary_top'}
		  {hook h='displayCheckoutSummaryTop'}
		{/block}
		
		{block name='cart_summary_products'}
		<div class="cart-summary-products">
		
			<p class="summary"><span><img src="{$urls.img_url}picto_panier_small_gold.png" />{$cart.summary_string}</span><a href="/panier?action=show"><i class="fa fa-eye"></i> Voir mon panier</a></p>
			
			<p class="show-details">
				<a href="#" data-toggle="collapse" data-target="#cart-summary-product-list" id="showCartSummaryProductList">
					{l s='show details' d='Shop.Theme.Actions'}
					<i class="material-icons">expand_more</i>
				</a>
			</p>
			
			{block name='cart_summary_product_list'}
			<div class="collapse" id="cart-summary-product-list">
				<ul class="media-list">
				  {foreach from=$cart.products item=product}
				    <li class="media">{include file='checkout/_partials/cart-summary-product-line.tpl' product=$product}</li>
				  {/foreach}
				</ul>
			</div>
			{/block}
		</div>
		{/block}
			
		<div class="delivery_choice">
	    	<div class="row delivery_choice">
	        	<div class="col-xs-12">
		        	<a href="/index.php?set_checkout_delivery_mode=1&delivery_mode=2" class="home_choice{if $delivery_mode==2} current{/if}">
			        	<img src="{$urls.img_url}picto_emporter.png" />
			        	<div>
				        	<span>Je veux <strong>prendre à emporter</strong></span>
				        	{if $taux_reduc_emporter}
				        	<span class="small">Je bénéficie d'une réduction de {$taux_reduc_emporter}%</span>
				        	{/if}
			        	</div>
				    </a>
	        	</div>		        	
	    	</div>
	    	<div class="row delivery_choice">
	        	<div class="col-xs-12">
		        	<!-- <a href="javascript:" onclick="showDeliveryAddressForm()" data-url="/index.php?set_home_delivery_mode=1&delivery_mode=1" class="home_choice{if $delivery_mode==1} current{/if}"> -->
		        	<a href="/index.php?set_checkout_delivery_mode=1&delivery_mode=1" class="home_choice{if $delivery_mode==1} current{/if}">
			        	<img src="{$urls.img_url}picto_livrer.png" />
			        	<div>
				        	<span>Je veux <strong>me faire livrer</strong></span>
				        	<span class="small">Livraison gratuite sous condition</span>
			        	</div>
				    </a>
	        	</div>
	    	</div>
			<div class="clearfix"></div>
		</div>
	  	
		{block name='cart_summary_voucher'}
		{include file='checkout/_partials/cart-voucher.tpl'}
		{/block}
	
		{block name='cart_summary_subtotals'}
		{include file='checkout/_partials/cart-summary-subtotals.tpl' cart=$cart}
		{/block}

	</div>

	{block name='cart_summary_totals'}
	{include file='checkout/_partials/cart-summary-totals.tpl' cart=$cart}
	{/block}
	
</section>
