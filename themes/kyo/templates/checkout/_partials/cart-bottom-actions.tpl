{**
 * Copyright since 2007 PrestaShop SA and Contributors
 * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.md.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://devdocs.prestashop.com/ for more information.
 *
 * @author    PrestaShop SA and Contributors <contact@prestashop.com>
 * @copyright Since 2007 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 *}
	<div class="cart-bottom-actions">
	    <div class="row">
			<div class="{if empty($cart.products)}col-md-12{else}col-md-6{/if} col-xs-12">
	        {block name='continue_shopping'}
	          <a class="btn btn-primary btn-black continue-buying" href="{$urls.pages.index}">
	            <i class="material-icons">chevron_left</i>{l s='Continue shopping' d='Shop.Theme.Actions'}
	          </a>
	        {/block}
			</div>
			{if !empty($cart.products)}
			<div class="col-md-6 col-xs-12 text-xs-right">
			    {if $cart.minimalPurchaseRequired}
			      <div class="alert alert-warning" role="alert">
			        {$cart.minimalPurchaseRequired}
			      </div>
			      <div class="text-xs-right">
			        <button type="button" class="btn btn-primary disabled checkout" disabled>{l s='Proceed to checkout' d='Shop.Theme.Actions'}</button>
			      </div>
			    {elseif empty($cart.products)}
			      <div class="text-xs-right">
			        <button type="button" class="btn btn-primary disabled checkout" disabled>{l s='Proceed to checkout' d='Shop.Theme.Actions'}</button>
			      </div>
			    {elseif $cartHasNotReachedMinimumAmount}
			      <div class="text-xs-right">
			        <button type="button" class="btn btn-primary disabled checkout" disabled>{l s='Proceed to checkout' d='Shop.Theme.Actions'}</button>
			      </div>
			    {else}
			      <div class="text-xs-right">
			        <a href="javascript:" data-url="{$urls.pages.order}" class="btn btn-primary btn-to-check checkout">{l s='Proceed to checkout' d='Shop.Theme.Actions'}</a>
			        {hook h='displayExpressCheckout'}
			      </div>
			    {/if}
			</div>
			{/if}
		</div>
    </div>
