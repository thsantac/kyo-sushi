{**
 * Copyright since 2007 PrestaShop SA and Contributors
 * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.md.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://devdocs.prestashop.com/ for more information.
 *
 * @author    PrestaShop SA and Contributors <contact@prestashop.com>
 * @copyright Since 2007 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 *}
{extends file='checkout/_partials/steps/checkout-step.tpl'}

{block name='step_content'}
  <div id="hook-display-before-carrier">
    {$hookDisplayBeforeCarrier nofilter}
  </div>

  <div class="delivery-options-list">
    {if $delivery_options|count}
	      <form
	        class="clearfix"
	        id="js-delivery"
	        data-url-update="{url entity='order' params=['ajax' => 1, 'action' => 'selectDeliveryOption']}"
	        method="post"
	      >
		  <h3>Transporteur</h3>
	        <div class="form-fields">
	          {block name='delivery_options'}
	            <div class="delivery-options">
					{foreach from=$delivery_options item=carrier key=carrier_id}
					<div class="row">
				    	<div class="col-xs-12">
							<div class="delivery-option">
						    	<div class="col-sm-1">
									<span class="custom-radio float-xs-left">
										<input type="radio" name="delivery_option[{$id_address}]" id="delivery_option_{$carrier.id}" value="{$carrier_id}"{if $delivery_option == $carrier_id} checked{/if}>
										<span></span>
									</span>
								</div>
							    <label for="delivery_option_{$carrier.id}" class="col-xs-9 col-sm-11 delivery-option-2">
									<div class="row">
										<div class="col-sm-5 col-xs-12">
										  <div class="row carrier{if $carrier.logo} carrier-hasLogo{/if}">
										    {if $carrier.logo}
										    <div class="col-xs-12 col-md-4 carrier-logo">
										        <img src="{$carrier.logo}" alt="{$carrier.name}" />
										    </div>
										    {/if}
										    <div class="col-xs-12 carriere-name-container{if $carrier.logo} col-md-8{/if}">
										      <span class="h6 carrier-name">{$carrier.name}</span>
										    </div>
										  </div>
										</div>
										<div class="col-sm-4 col-xs-12">
										  <span class="carrier-delay">{$carrier.delay}</span>
										</div>
										<div class="col-sm-3 col-xs-12">
										  <span class="carrier-price">{$carrier.price}</span>
										</div>
									</div>
							    </label>
							</div>
						</div>
					</div>
					<div class="row carrier-extra-content"{if $delivery_option != $carrier_id} style="display:none;"{/if}>
					{$carrier.extraContent nofilter}
					</div>
					<div class="clearfix"></div>
					{/foreach}
	            </div>
	          {/block}
	          <div class="order-options">
	            <div id="delivery">
	              <label for="delivery_message">{l s='If you would like to add a comment about your order, please write it in the field below.' d='Shop.Theme.Checkout'}</label>
	              <textarea rows="2" cols="120" id="delivery_message" name="delivery_message" class="form-control">{$delivery_message}</textarea>
	            </div>
	
	            {if $recyclablePackAllowed}
	              <span class="custom-checkbox">
	                <input type="checkbox" id="input_recyclable" name="recyclable" value="1" {if $recyclable} checked {/if}>
	                <span><i class="material-icons rtl-no-flip checkbox-checked">&#xE5CA;</i></span>
	                <label for="input_recyclable">{l s='I would like to receive my order in recycled packaging.' d='Shop.Theme.Checkout'}</label>
	              </span>
	            {/if}
	
	            {if $gift.allowed}
	              <span class="custom-checkbox">
	                <input class="js-gift-checkbox" id="input_gift" name="gift" type="checkbox" value="1" {if $gift.isGift}checked="checked"{/if}>
	                <span><i class="material-icons rtl-no-flip checkbox-checked">&#xE5CA;</i></span>
	                <label for="input_gift">{$gift.label}</label >
	              </span>
	
	              <div id="gift" class="collapse{if $gift.isGift} in{/if}">
	                <label for="gift_message">{l s='If you\'d like, you can add a note to the gift:' d='Shop.Theme.Checkout'}</label>
	                <textarea rows="2" cols="120" id="gift_message" name="gift_message">{$gift.message}</textarea>
	              </div>
	            {/if}

            </div>

            <div id="delivery_hour" class="summary-selected-delivery_hour">
	            <h3>Date et heure de livraison / retrait</h3>

	            {if $delivery_dates_selected}
	            <div class="alert alert-info"><span class="icon"><i class="fa fa-info-circle"></i></span><span class="text">
					{if $delivery_mode==1}
						{if $cart_delivery_hour}
							Le jour et l'heure de livraison que vous avez choisis pour votre commande est : 
						{else}
							Le jour de livraison que vous avez choisi pour votre commande est : 
						{/if}
					{else}
						{if $cart_delivery_hour}
							Le jour et l'heure de retrait que vous avez choisis pour votre commande est : 
						{else}
							Le jour de retrait que vous avez choisi pour votre commande est : 
						{/if}
					{/if}
					{$delivery_dates_selected}
					{if $cart_delivery_hour}
					 à {$cart_delivery_hour}
					{/if}
					</span>
	            </div>
				{/if}

				<div class="row selected-delivery_hour" id="deliveryDateHours" style="position: relative">

				  	<div class="loading" id="deliveryDateHoursWaiting" style="display: none; position: absolute; top: 0px; left: 0; width: 100%; text-align: center; padding: 3px; background-color: rgba(255,255,255, 0.5); z-index: 10;">
					  	<i class="fa fa-circle-o-notch fa-spin fa-3x fa-fw" style="color: #ef1a00"></i>
				  	</div>

					<div class="col-xl-1 col-lg-1 col-md-1 col-sm-2 col-xs-2 icon">
						<i class="fa fa-clock-o"></i>
					</div>
					<div class="col-xl-11 col-lg-11 col-md-11 col-sm-10 col-xs-10">
						<div class="row">
							{if $delivery_mode==1}
			                <label for="select_delivery_date" class="col-xs-12 col-sm-8 col-md-8 col-lg-8 control-label">Quand souhaitez-vous être livré ? <br>Temps de préparation : <strong>{$delai_mini}</strong> (il est {$now})</label>
							{else}
			                <label for="delivery_when_date" class="col-xs-12 col-sm-8 col-md-8 col-lg-8 control-label">Quand souhaitez-vous retirer votre commande ? <br>Temps de préparation : <strong>{$delai_mini}</strong> (il est {$now})</label>
							{/if}
							<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
								<select id="select_delivery_date" name="delivery_date" class="form-control form-control-select">
									<option value="">Dates possibles</option>
									{foreach from=$delivery_dates key=n item=delivery_date}
										<option value="{$delivery_date}" {if $delivery_date == $delivery_dates_selected}selected="selected"{/if}>{$delivery_date|escape:'html':'UTF-8'}</option>
									{/foreach}
								</select>
							</div>
						</div>
						<div class="row" id="deliveryDateHoursWarning" style="display: none">
							<div class="col-xs-12">
								<div class="alert alert-warning">Aucune tranche horaire n'est disponible pour cette journée !</div>
							</div>
						</div>
						<div class="row hidden" id="deliveryHours">
			                <label for="delivery_when_hour" class="col-xs-12 col-sm-8 col-md-8 col-lg-8 control-label hour text-right clear-label">Et à quelle heure ?</label>
							<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 hour">
								<select id="select_delivery_hour" name="delivery_hour" class="form-control form-control-select">
								</select>
							</div>
						</div>
					</div>
				</div>

				<div id="deliveryDebug" style="display: none">
					<textarea class="form-control" rows="10" style="height:240px"></textarea>
				</div>

          </div>
        </div>
        <button type="submit" class="continue btn btn-primary float-xs-right" name="confirmDeliveryOption" value="1"{if $delivery_form_has_no_continue_button} style="display: none"{/if}>
          {l s='Continue' d='Shop.Theme.Actions'}
        </button>
      </form>
    {else}
      <p class="alert alert-danger">{l s='Unfortunately, there are no carriers available for your delivery address.' d='Shop.Theme.Checkout'}</p>
    {/if}
  </div>

  <div id="hook-display-after-carrier">
    {$hookDisplayAfterCarrier nofilter}
  </div>

  <div id="extra_carrier"></div>
{/block}
