{**
 * Copyright since 2007 PrestaShop SA and Contributors
 * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.md.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://devdocs.prestashop.com/ for more information.
 *
 * @author    PrestaShop SA and Contributors <contact@prestashop.com>
 * @copyright Since 2007 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 *}

    <div id="complements-and-elements">
	    {if !empty($cart.products)}
	    <div id="complements" class="summary-complements">
		    <div class="row">
				<div class="col-md-12">
					<h2>Produits complémentaires</h2>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 padding-10">
					<div class="text">
						Choisissez le nombre de produits complémentaires que vous souhaitez ajouter à votre commande. Les quantités dépendent du montant total TTC de votre commande !
					</div>
				</div>
			</div>
			<div class="row">
				<div class="complement">
					<input type="hidden" id="nb_max_baguettes" value="{$complements.nb_max_baguettes}">
					<input type="hidden" id="nb_sauces" value="{$complements.nb_sauce_salee + $complements.nb_sauce_sucree}">
					<img src="{$urls.img_url}picto_baguettes.png" />
					<label>Baguettes</label>
					<div class="input">
						<input
							type="text"
							name="nb_baguettes"
							value="{$complements.nb_baguettes}"
							class="input-group nb_complement"
							min="0"
							max="{$complements.nb_max_baguettes}"
						>
					    <div class="loading">
							<div class="logo">
				            	<img src="{$urls.img_url}logo_kyo_rouge_small.png" class="fa-spin">
							</div>
					    </div>
					</div>
					<span>Gratuites jusqu'à <strong>{$complements.nb_max_baguettes}</strong></span>
				</div>
				<div class="complement">
					<img src="{$urls.img_url}picto_sauce_salee.png" />
					<label>Sauce salée</label>
					<div class="input">
						<input
							type="text"
							name="nb_sauce_salee"
							value="{$complements.nb_sauce_salee}"
							class="input-group nb_complement sauce"
							min="0"
							max="{$complements.nb_max_baguettes}"
						>
					    <div class="loading">
							<div class="logo">
				            	<img src="{$urls.img_url}logo_kyo_rouge_small.png" class="fa-spin">
							</div>
					    </div>
					</div>
					<span>Gratuite jusqu'à <strong>{$complements.nb_max_baguettes}</strong> <span class="sup">*</span></span>
				</div>
				<div class="complement">
					<img src="{$urls.img_url}picto_sauce_sucree.png" />
					<label>Sauce sucrée</label>
					<div class="input">
						<input
							type="text"
							name="nb_sauce_sucree"
							value="{$complements.nb_sauce_sucree}"
							class="input-group nb_complement sauce"
							min="0"
							max="{$complements.nb_max_baguettes}"
						>
					    <div class="loading">
							<div class="logo">
				            	<img src="{$urls.img_url}logo_kyo_rouge_small.png" class="fa-spin">
							</div>
					    </div>
					</div>
					<span>Gratuite jusqu'à <strong>{$complements.nb_max_baguettes}</strong> <span class="sup">*</span></span>
				</div>
				<div class="complement">
					<img src="{$urls.img_url}picto_gingembre.png" />
					<label>Gingembre</label>
					<div class="input">
						<input
							type="text"
							name="nb_gingembre"
							value="{$complements.nb_gingembre}"
							class="input-group nb_complement"
							min="0"
							max="{$complements.nb_max_baguettes}"
						>
					    <div class="loading">
							<div class="logo">
				            	<img src="{$urls.img_url}logo_kyo_rouge_small.png" class="fa-spin">
							</div>
					    </div>
					</div>
					<span>Gratuits jusqu'à <strong>{$complements.nb_max_baguettes}</strong></span>
				</div>
				<div class="complement">
					<img src="{$urls.img_url}picto_wasabi.png" />
					<label>Wasabi</label>
					<div class="input">
						<input
							type="text"
							name="nb_wasabi"
							value="{$complements.nb_wasabi}"
							class="input-group nb_complement"
							min="0"
							max="{$complements.nb_max_baguettes}"
						>
					    <div class="loading">
							<div class="logo">
				            	<img src="{$urls.img_url}logo_kyo_rouge_small.png" class="fa-spin">
							</div>
					    </div>
					</div>
					<span>Gratuits jusqu'à <strong>{$complements.nb_max_baguettes}</strong></span>
				</div>
			</div>
		    <div class="row">
				<div class="col-md-12">
					<p class="sauces">(<span class="sup">*</span>) Vous pouvez choisir jusqu'à <strong>{$complements.nb_max_baguettes}</strong> sauce{if $complements.nb_max_baguettes > 1}s{/if} au total.</p>
				</div>
			</div>
	    </div>
	
	    <div id="elements">
		    <div class="row">
				<div class="col-md-12">
					<h2>Aliments indésirables</h2>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 padding-10">
					<div class="text">
						Si vous êtes allergique à certains aliments, merci de nous indiquer ci-dessous ceux que vous souhaitez écarter de votre commande.
					</div>
				</div>
			</div>
			<div class="row">
				{hook h="displayExclusionFilters" elements=$elements from='cart'}
			</div>
	    </div>
    </div>
	{/if}
	
