{**
 * Copyright since 2007 PrestaShop SA and Contributors
 * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.md.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://devdocs.prestashop.com/ for more information.
 *
 * @author    PrestaShop SA and Contributors <contact@prestashop.com>
 * @copyright Since 2007 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 *}
{block name='footer'}
<div class="container">
	<div class="row">
	{block name='hook_footer_after'}
		{hook h='displayFooterAfter'}
	{/block}
	</div>
	{block name='copyright_link'}
	<div class="copyright">
	© {'Y'|date} Kyo Sushi, site développé par <a href="https://www.la-ligne-web.com" target="_blank">La Ligne Web</a> - Agence web Aix-en-Provence
	</div>
	{/block}
</div>
{/block}

<div class="modal fade" id="modal-cms">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<button type="button" class="close" data-dismiss="modal" aria-label="{l s='Close' d='Shop.Theme.Global'}">
				<span aria-hidden="true"><i class="fa fa-times"></i></span>
			</button>
			<div class="js-modal-content"><div style="text-align: center"><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i></div></div>
		</div>
	</div>
</div>

{if $notifications.error}
    <script type="text/javascript">
	    var errorNotification = '<ul class="text">';
        {foreach $notifications.error as $notif}
        	errorNotification += "<li>{$notif nofilter}</li>";
        {/foreach}
        errorNotification += '</ul>';
	</script>
{else}
    <script type="text/javascript">
	    var errorNotification = '';
	</script>
{/if}
