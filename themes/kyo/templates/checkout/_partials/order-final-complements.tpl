{**
 * Copyright since 2007 PrestaShop SA and Contributors
 * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.md.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://devdocs.prestashop.com/ for more information.
 *
 * @author    PrestaShop SA and Contributors <contact@prestashop.com>
 * @copyright Since 2007 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 *}
	<div class="row">
		<div class="col-md-12">
			<h4 class="h4">Produits complémentaires</h4>
			<div class="row">
				<div class="complement">
					<img src="{$urls.img_url}picto_baguettes.png" />
					<label>Baguettes</label>
					<div class="input">{$complements.nb_baguettes}</div>
				</div>
				<div class="complement">
					<img src="{$urls.img_url}picto_sauce_salee.png" />
					<label>Sauce salée</label>
					<div class="input">{$complements.nb_sauce_salee}</div>
				</div>
				<div class="complement">
					<img src="{$urls.img_url}picto_sauce_sucree.png" />
					<label>Sauce sucrée</label>
					<div class="input">{$complements.nb_sauce_sucree}</div>
				</div>
				<div class="complement">
					<img src="{$urls.img_url}picto_gingembre.png" />
					<label>Gingembre</label>
					<div class="input">{$complements.nb_gingembre}</div>
				</div>
				<div class="complement">
					<img src="{$urls.img_url}picto_wasabi.png" />
					<label>Wasabi</label>
					<div class="input">{$complements.nb_wasabi}</div>
				</div>
			</div>
		</div>
	</div>

	<div class="row" id="elements">
		<div class="col-md-12">
			<h4 class="h4">Aliments indésirables</h4>
			<div class="row">
				<div class="col-md-12">
					{if count($elements)}
					{hook h="displayExclusionFilters" elements=$elements from='order'}
					{else}
					<div class="alert alert-info">Vous n'avez déclaré aucun aliment indésirable.</div>
					{/if}
				</div>
			</div>
		</div>
	</div>
