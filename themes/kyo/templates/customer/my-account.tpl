{**
 * Copyright since 2007 PrestaShop SA and Contributors
 * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.md.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://devdocs.prestashop.com/ for more information.
 *
 * @author    PrestaShop SA and Contributors <contact@prestashop.com>
 * @copyright Since 2007 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 *}
{extends file='customer/page.tpl'}

{block name='page_title'}
  {l s='Your account' d='Shop.Theme.Customeraccount'}
{/block}

{block name='page_content'}
	<div class="intro">
		<h2>Bienvenue sur votre page d'accueil.<br>Vous pouvez gérer ici vos informations personnelles ainsi que vos commandes.</h2>
	</div>
  	<div class="row">
    	<div class="links">

			<div class="col-xl-3 col-lg-4 col-md-6 col-sm-6 col-xs-12">
				<a class="border-animation" id="identity-link" href="{$urls.pages.identity}">
					<div class=" border-animation__inner">
						<span class="link-item">
							<img src="{$urls.img_url}account-identity.png" />
							<span class="link-item-text">Mes informations personnelles</span>
						</span>
					</div>
				</a>
			</div>
			
			{if $customer.addresses|count}
			<div class="col-xl-3 col-lg-4 col-md-6 col-sm-6 col-xs-12">
				<a class="border-animation" id="addresses-link" href="{$urls.pages.addresses}">
					<div class=" border-animation__inner">
						<span class="link-item">
							<img src="{$urls.img_url}account-addresses.png" />
							<span class="link-item-text">Mes adresses</span>
						</span>
					</div>
				</a>
			</div>
			{else}
			<div class="col-xl-3 col-lg-4 col-md-6 col-sm-6 col-xs-12">
				<a class="border-animation" id="address-link" href="{$urls.pages.address}">
					<div class=" border-animation__inner">
						<span class="link-item">
							<img src="{$urls.img_url}account-addresses.png" />
							<span class="link-item-text">Ma première adresse</span>
						</span>
					</div>
				</a>
			</div>
			{/if}

			{if !$configuration.is_catalog}
			<div class="col-xl-3 col-lg-4 col-md-6 col-sm-6 col-xs-12">
				<a class="border-animation" id="history-link" href="{$urls.pages.history}">
					<div class=" border-animation__inner">
						<span class="link-item">
							<img src="{$urls.img_url}account-history.png" />
							<span class="link-item-text">{l s='Order history and details' d='Shop.Theme.Customeraccount'}</span>
						</span>
					</div>
				</a>
			</div>
			{/if}

			{if !$configuration.is_catalog}
			<div class="col-xl-3 col-lg-4 col-md-6 col-sm-6 col-xs-12">
				<a class="border-animation" id="order-slips-link" href="{$urls.pages.order_slip}">
					<div class=" border-animation__inner">
						<span class="link-item">
							<img src="{$urls.img_url}account-order_slip.png" />
							<span class="link-item-text">Mes avoirs</span>
						</span>
					</div>
				</a>
			</div>
			{/if}

			{if $configuration.voucher_enabled && !$configuration.is_catalog}
			<div class="col-xl-3 col-lg-4 col-md-6 col-sm-6 col-xs-12">
				<a class="border-animation" id="discounts-link" href="{$urls.pages.discount}">
					<div class=" border-animation__inner">
						<span class="link-item">
							<img src="{$urls.img_url}account-discount.png" />
							<span class="link-item-text">Mes bons de réduction</span>
						</span>
					</div>
				</a>
			</div>
			{/if}

			{block name='display_customer_account'}
				{hook h='displayCustomerAccount'}
			{/block}

    	</div>
  	</div>
{/block}


{block name='page_footer'}
  {block name='my_account_links'}
    <div class="text-sm-center">
      <a href="{$logout_url}" class="btn btn-secondary btn-red">
        Me déconnecter
      </a>
    </div>
  {/block}
{/block}

{block name='hook_reassurance'}
	{hook h='displayReassurance'}
{/block}
