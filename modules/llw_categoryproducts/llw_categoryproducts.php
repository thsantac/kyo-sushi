<?php
/**
 * PrestaShop module created by VEKIA, a guy from official PrestaShop community ;-)
 *
 * @author    VEKIA https://www.prestashop.com/forums/user/132608-vekia/
 * @copyright 2010-2020 VEKIA
 * @license   This program is not free software and you can't resell and redistribute it
 *
 * CONTACT WITH DEVELOPER http://mypresta.eu
 * support@mypresta.eu
 */

if (!defined('_PS_VERSION_')) {
    exit;
}

use PrestaShop\PrestaShop\Core\Module\WidgetInterface;
use PrestaShop\PrestaShop\Adapter\Image\ImageRetriever;

class Llw_categoryProducts extends Module implements WidgetInterface
{
    /**
     * @var ImageRetriever
     */
    private $imageRetriever;

    public function __construct()
    {
        $this->name = 'llw_categoryproducts';
        $this->tab = 'administration';
        $this->version = '1.0.0';
        $this->author = 'La Ligne Web';
        $this->need_instance = 0;

        $this->bootstrap = true;
        parent::__construct();

        $this->displayName = $this->l('Affectation des produits aux catégories');
        $this->description = $this->l('Ce module permet d\'assigner les produits aux catégories');
        $this->ps_versions_compliancy = [
            'min' => '1.7.1.0',
            'max' => _PS_VERSION_,
        ];
    }

    public function install()
    {
        if (!parent::install() || !$this->installConfiguration()) {
            return false;
        }
        return true;
    }

    public function installConfiguration()
    {
        return true;
    }


    public function getContent()
    {
        if (Tools::getValue('AddProductsCategoryBoxBulk', 'false') != 'false') {
            $products = array();
            foreach (explode(',', Tools::getValue('AddProductsCategoryBoxBulk')) AS $key) {
                $category = new Category($key, $this->context->language->id, $this->context->shop->id);
                $counter_category_products = $category->getProducts($this->context->language->id, 0, 0, 0, 0, true);
                $category_products = $category->getProducts($this->context->language->id, 0, $counter_category_products);
                foreach ($category_products AS $product) {
                    $products[$product['id_product']]['pname'] = $product['name'];
                    $products[$product['id_product']]['id_product'] = $product['id_product'];
                }
                unset($category, $category_products, $counter_category_products);
            }
            die(json_encode($products));
        }
        $this->postProcess();
        return $this->displayForm();
    }

    public function postProcess()
    {
        if (Tools::isSubmit('submitProductsCategory')) {
            $this->MassUpdateProducts();
            if ($this->errors == false) {
                Tools::redirectAdmin(AdminController::$currentIndex . '&configure=' . $this->name . '&conf=4&token=' . Tools::getAdminTokenLite('AdminModules'));
            }
        }
    }

    private function MassUpdateProducts()
    {
        $products = $this->getAllProducts();

		foreach($products as $product) {
            $field_id 	= 'id_category_default_'.$product['id_product'];

			$sql 		= 'UPDATE `' . _DB_PREFIX_ . 'product`
							SET id_category_default = '. (int) Tools::getValue($field_id) . '
							WHERE `id_product` = ' . (int) $product['id_product'];
			Db::getInstance(_PS_USE_SQL_SLAVE_)->execute($sql);

			$sql 		= 'UPDATE `' . _DB_PREFIX_ . 'product_shop`
							SET id_category_default = '. (int) Tools::getValue($field_id) . '
							WHERE `id_shop` = ' . (int) $this->context->shop->id . ' AND `id_product` = ' . (int) $product['id_product'];
			Db::getInstance(_PS_USE_SQL_SLAVE_)->execute($sql);

			$sql 		= 'DELETE FROM `' . _DB_PREFIX_ . 'category_product`
							WHERE `id_product` = ' . (int) $product['id_product'] . ' 
							AND `id_category` <> 2 
							AND `id_category` <> ' . (int) Tools::getValue($field_id);
			Db::getInstance(_PS_USE_SQL_SLAVE_)->execute($sql);

	        $sql 		= 'SELECT cp.id_product
							FROM `' . _DB_PREFIX_ . 'category_product` cp
							WHERE cp.`id_product` = ' .  (int) $product['id_product'] . ' AND cp.id_category = '. (int) Tools::getValue($field_id);
	        $id_product = Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue($sql);
	        if(!$id_product) {
		        $sql 	= 'INSERT INTO `' . _DB_PREFIX_ . 'category_product` 
		                    (`id_category`, `id_product`, `position`)
		                    VALUES (' . (int) Tools::getValue($field_id) . ',' . (int) $product['id_product'] . ',0)';
		        Db::getInstance(_PS_USE_SQL_SLAVE_)->execute($sql);
	        }
		}

    }

    public function displayForm()
    {
        $default_lang = (int)Configuration::get('PS_LANG_DEFAULT');
        $token = Tools::getAdminTokenLite('AdminModules');
        $module_link = $this->context->link->getAdminLink('AdminModules', false);
        $url = $module_link . '&configure=llw_categoryproducts&token=' . $token . '&tab_module=administration&module_name=llw_categoryproducts';

        $products = $this->getAllProducts();
        $categories = $this->getAllCategories();
        $categories_list = [];
        foreach ($categories as $category) {
            $categories_list[] = array('id_category_default' => $category['id_category'], 'name' => $category['name']);
        }

        $helper = new HelperForm();
        $helper->module = $this;
        $helper->name_controller = $this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->currentIndex = AdminController::$currentIndex . '&configure=' . $this->name;

        $helper->default_form_language = $default_lang;
        $helper->allow_employee_form_lang = $default_lang;
        $helper->title = $this->displayName;
        $helper->submit_action = $this->name;

        $fields_form = array(
            'form' => array(
                'legend' => array(
                    'title' => 'Produits',
                    'icon' => 'icon-cogs'
                ),
                'description' => '',
                'input' => array(
                ),
                'submit' => array(
                    'title' 		=> $this->trans('Save', array(), 'Admin.Actions'),
                    'class' 		=> 'btn btn-default pull-right',
                    'name' 			=> 'submitProductsCategory',
                ),
            ),
        );

		foreach($products as $product) {
            $field_id = 'product_name_'.$product['id_product'];
            $input = array(
                'type' => 'text',
                'label' => 'Produit',
                'name' => $field_id
			);
            $helper->fields_value[$field_id] = $product['name'];
            $fields_form['form']['input'][] = $input;

            $field_id = 'product_img_'.$product['id_product'];
            $input = array(
                'type' => 'img',
                'label' => 'Image',
                'name' => $field_id
			);
			if(isset($product['img']['bySize']['small_default']['url']))
	            $helper->fields_value[$field_id] = $product['img']['bySize']['small_default']['url'];
			else
	            $helper->fields_value[$field_id] = "";
            $fields_form['form']['input'][] = $input;

            $field_id = 'id_category_default_'.$product['id_product'];
            $input = array(
                'type' => 'select',
                'label' => 'Catégorie par défaut',
                'name' => $field_id,
                'default_value' => (int) $product['id_category_default'],
                'options' => array(
                    'query' => $categories_list,
                    'id' => 'id_category_default',
                    'name' => 'name',
                )
			);
            $helper->fields_value[$field_id] = $product['id_category_default'];
            $fields_form['form']['input'][] = $input;
		}

        $helper->tpl_vars = array(
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id
        );

        return $helper->generateForm([$fields_form]);
    }

    public function getAllProducts()
    {
        $sql = 'SELECT p.id_product, pl.name, product_shop.id_category_default
				FROM `' . _DB_PREFIX_ . 'product` p
				' . Shop::addSqlAssociation('product', 'p') . ' 
				LEFT JOIN `' . _DB_PREFIX_ . 'product_lang` pl ON p.id_product = pl.id_product
				WHERE product_shop.`id_shop` = ' . (int) $this->context->shop->id . '
				GROUP BY p.id_product
				ORDER BY p.id_category_default ASC';
        $result = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);

        $imageRetriever = new ImageRetriever($this->context->link);
        $products = [];
        foreach ($result AS $product) {

	        $products[$product['id_product']]['id_product'] 			= $product['id_product'];
	        $products[$product['id_product']]['name'] 					= $product['name'];
	        $products[$product['id_product']]['id_category_default'] 	= $product['id_category_default'];
	        $products[$product['id_product']]['img'] 					= '';

	        // Get all product images, including potential cover
	        $productImages = $imageRetriever->getAllProductImages(
	            $product,
	            $this->context->language
	        );
	
	        // Get default image for selected combination (used for product page, cart details, ...)
	        foreach ($productImages as $image) {
	            // If one of the image is a cover it is used as such
	            if (isset($image['cover']) && null !== $image['cover']) {
	                $products[$product['id_product']]['img'] = $image;
	                break;
	            }
	        }
        }
        return $products;
    }

    public function getAllCategories()
    {
        $sql = 'SELECT c.id_category, cl.name
				FROM `' . _DB_PREFIX_ . 'category` c
				LEFT JOIN `' . _DB_PREFIX_ . 'category_shop` cs ON c.id_category = cs.id_category AND cs.`id_shop` = ' . (int) $this->context->shop->id . '
				LEFT JOIN `' . _DB_PREFIX_ . 'category_lang` cl ON c.id_category = cl.id_category
				WHERE cs.`id_shop` = ' . (int) $this->context->shop->id . '
				GROUP BY c.id_category
				ORDER BY cs.position ASC';
		
        $result = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);
        return $result;
    }

    public function renderWidget($hookName = null, array $params = [])
    {

    }

    public function getWidgetVariables($hookName = null, array $params = [])
    {

    }
}

?>
