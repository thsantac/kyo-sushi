{**
 * Copyright since 2007 PrestaShop SA and Contributors
 * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.md.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://devdocs.prestashop.com/ for more information.
 *
 * @author    PrestaShop SA and Contributors <contact@prestashop.com>
 * @copyright Since 2007 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 *}

{extends file="helpers/form/form.tpl"}

{block name="label"}

{/block}

{block name="field"}
	{if $input.type == 'text'}
		<table width="80%">
			<tr>
				<td width="50%" style="text-align: right; padding-right: 10px;">
					{$fields_value[$input.name]|escape:'html':'UTF-8'} : 
				</td>
	{else}
	{if $input.type == 'img'}
				<td width="10%">
					<img src="{$fields_value[$input.name]}" width="60" />
				</td>
	{else}
	{if $input.type == 'select'}
				<td width="40%">
					{$smarty.block.parent}
				</td>
			</tr>
		</table>
    {/if}
    {/if}
    {/if}
{/block}

