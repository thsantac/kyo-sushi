<?php
/**
* 2007-2019 PrestaShop
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
* @author    PrestaShop SA <contact@prestashop.com>
* @copyright 2007-2017 PrestaShop SA
* @license   http://addons.prestashop.com/en/content/12-terms-and-conditions-of-use
* International Registered Trademark & Property of PrestaShop SA
*/

class AtosReturnModuleFrontController extends ModuleFrontController
{
    public function initContent()
    {
        $this->ajax = true;
        $datas = Tools::getValue('DATA');

        if (empty($datas)) {
	        die("ATOS - return = empty data !!!");
            Tools::redirect('index.php');
        } else {
            $atos = $this->module;
            $is_win = (Tools::strtoupper(Tools::substr(PHP_OS, 0, 3)) === 'WIN');
            $exec = $atos->bin_dir.'response'.(((int)$is_win === 1) ? '.exe' : '');
            $result = exec($exec.' pathfile='._PS_MODULE_DIR_.'/atos/pathfile message='.preg_replace('#[^a-z0-9]#Ui', '', $datas));
            $result_array = explode('!', $result);
            if (!empty($result_array)) {
                $cart = new Cart((int)$result_array[22]);
                $customer = new Customer((int)$cart->id_customer);

                $return_page = 'index.php?controller=order-confirmation&id_cart='.$cart->id.'&id_module='.$atos->id.'&key='.$customer->secure_key;
                // If orders are not supposed to be created in error OR if the customer cancel
                if (Configuration::get('ATOS_ERROR_BEHAVIOR') || in_array((int)$result_array[11], [12, 17])) {
                    $return_page = $this->context->link->getPageLink('order');
                }

                Tools::redirect($return_page);
            } else {
		        die("ATOS - return = empty result_array !!!");
                Tools::redirect('index.php');
            }
        }
    }
}
