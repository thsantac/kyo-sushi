<?php
/**
 * 2007-2019 PrestaShop
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 * @file        orderconfirmation.php
 * @author      PrestaShop SA <contact@prestashop.com>
 * @copyright   2007-2014 PrestaShop SA
 * @license     http://addons.prestashop.com/en/content/12-terms-and-conditions-of-use
 *
 * International Registered Trademark & Property of PrestaShop SA
*/
class AtosOrderConfirmationModuleFrontController extends ModuleFrontController
{
    /**
     * @throws PrestaShopDatabaseException
     */
    public function initContent()
    {
        $this->ajax = true;
        Tools::redirect($this->getOrderConfirmationUrl());
    }

    public function getOrderConfirmationUrl()
    {
        $id_cart = Tools::getValue('id_cart');
        $id_customer = Tools::getValue('id_customer');
        $secure_key = Tools::getValue('secure_key');

        $orderConfirmationUrl = Tools::getShopDomainSsl(true).'/index.php?controller=order-confirmation&key='.$secure_key.'&id_cart='.(int)$id_cart.'&id_module='.(int)$this->module->id.'&id_order='.(int)Order::getIdByCartId($id_cart);

        return $orderConfirmationUrl;
    }
}
