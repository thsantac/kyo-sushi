<h1 class="text-center">{l s='Your order is being processed, please wait...' mod='atos'}</h1>
{literal}
<script type="text/javascript">
setTimeout(function() {
  window.location.href = "{/literal}{$orderConfirmationUrl}{literal}";
}, 3000);
</script>
{/literal}
