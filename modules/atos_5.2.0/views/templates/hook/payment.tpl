{*
* 2007-2014 PrestaShop
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
* @author    PrestaShop SA <contact@prestashop.com>
* @copyright 2007-2014 PrestaShop SA
* @license   http://addons.prestashop.com/en/content/12-terms-and-conditions-of-use
* International Registered Trademark & Property of PrestaShop SA
*}
<section class="js-payment-binary js-payment-atos disabled">
  <p class="alert alert-warning accept-cgv">{l s="You must accept the terms of service to be able to process your order" mod='atos'}</p>
  {if $atos}
    {$atos nofilter}
  {else}
    {l s='Your order total must be greater than ' mod='atos'} {$minimumAmount} {l s=' in order to pay by credit card.' mod='atos'}
  {/if}
</section>
