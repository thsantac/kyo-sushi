{*
* 2007-2014 PrestaShop
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
* @author    PrestaShop SA <contact@prestashop.com>
* @copyright 2007-2014 PrestaShop SA
* @license   http://addons.prestashop.com/en/content/12-terms-and-conditions-of-use
* International Registered Trademark & Property of PrestaShop SA
*}

{* selected done *}
<h3><i class="icon-book"></i> {l s='Configuration' mod='atos'} <small>{$module_display|escape:'htmlall':'UTF-8'}</small></h3>
<div id="wizard" class="swMain">
	{$html}

	<div class="clearfix"></div>

	<div class="panel-group" id="accordion">
{* Setup your Merchant ID *}
		<div class="panel">
			<div class="panel-heading">
				{if $ps_version == 0}<h3>{/if}
					1 - {l s='Setup your Merchant ID' mod='atos'}
				{if $ps_version == 0}</h3>{/if}
			</div>
			<form id="form_step_one" name="form_step_one" action="{$form_uri|escape:'htmlall':'UTF-8'}" method="post">
			<div id="collapse1">
				<div class="table-responsive clearfix">
					<div class="form-group clear">
						<label for="form-field-1" class="col-sm-4 control-label">
							{l s='Merchant ID' mod='atos'}
						</label>
						<div class="col-sm-6">
							<input type="text" class="form-control" value="{if isset($id_merchant) & !empty($id_merchant)}{$id_merchant|escape:'htmlall':'UTF-8'}{/if}" id="ATOS_MERCHANT_ID" name="ATOS_MERCHANT_ID" placeholder="{l s='Merchant ID' mod='atos'}" maxlength="15"/>
							<div class="clear">&nbsp;</div>
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
			<div class="panel-footer">
				<div class="btn-group pull-right">
					<button name="submitAtosID" type="submit" class="btn btn-default" value="1"><i class="process-icon-save"></i> {l s='Save' mod='atos'}</button>
				</div>
			</div>
			</form>
		</div>
    {* Upload your Certificat *}
    <div class="panel">
        <div class="panel-heading">
            {if $ps_version == 0}<h3>{/if}
                2 - {l s='Upload your Certificat' mod='atos'}
                {if $ps_version == 0}</h3>{/if}
        </div>
        <form id="form_step_three" name="form_step_tree" action="{$form_uri|escape:'htmlall':'UTF-8'}" method="post" enctype="multipart/form-data">
            <div id="collapse3">
                <div class="table-responsive clearfix">
                    <div class="form-group clear">
                        <label for="form-field-1" class="col-sm-4 control-label">
                            {l s='Certificat' mod='atos'}
                        </label>
                        <div class="col-sm-6">
                            <input type="file" class="form-control" id="ATOS_CERTIF" name="ATOS_CERTIF" placeholder="{l s='Certificate' mod='atos'}" />
                            <div class="clear">&nbsp;</div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <div class="panel-footer">
                <div class="btn-group pull-right">
                    <button name="submitAtosCertif" type="submit" class="btn btn-default" value="1"><i class="process-icon-save"></i> {l s='Save' mod='atos'}</button>
                </div>
            </div>
        </form>
    </div>
{* Setup your Bank *}
		<div class="panel">
			<div class="panel-heading">
				{if $ps_version == 0}<h3>{/if}
					3 - {l s='Setup your Bank' mod='atos'}
				{if $ps_version == 0}</h3>{/if}
			</div>
			<form id="form_step_two" name="form_step_two" action="{$form_uri|escape:'htmlall':'UTF-8'}" method="post">
			<div id="collapse2">
				<div class="table-responsive clearfix">
				<div class="row">
				{foreach from=$bank_list key=k item=v}
					<div class="radio_select col-sm-6 col-md-4 col-lg-3">
						<div class="well">
							<h4>{$v|escape:'htmlall':'UTF-8'} ({$k|escape:'htmlall':'UTF-8'})</h4>
							<span id="atos_bank{$k|escape:'htmlall':'UTF-8'}" class="switch prestashop-switch input-group col-sm-12 col-md-8 col-lg-8">
								<input type="radio" name="atos_bank{$k|escape:'htmlall':'UTF-8'}" id="atos_bank{$k|escape:'htmlall':'UTF-8'}_yes" {if $bank == $k}checked="checked"{/if} value="{$k|escape:'htmlall':'UTF-8'}" />
								<label for="atos_bank{$k|escape:'htmlall':'UTF-8'}_yes" class="radioCheck">
									<i class="color_success"></i> {l s=' Active' mod='atos'}
								</label>
								<input type="radio" class="switch_off" name="atos_bank{$k|escape:'htmlall':'UTF-8'}" id="atos_bank{$k|escape:'htmlall':'UTF-8'}_no" {if empty($bank) || $bank != $k}checked="checked"{/if} value="" />
								<label for="atos_bank{$k|escape:'htmlall':'UTF-8'}_no" class="radioCheck">
									<i class="color_success"></i> {l s=' Inactive ' mod='atos'}
								</label>
								<a class="slide-button btn"></a>
							</span>
						</div>
					</div>
				{/foreach}
				</div>
				</div>
			</div>
			<div class="panel-footer">
				<div class="btn-group pull-right">
					<button name="submitAtosBank" type="submit" class="btn btn-default" value="1"><i class="process-icon-save"></i> {l s='Save' mod='atos'}</button>
				</div>
			</div>
			</form>
		</div>
{* Setup options *}
		<div class="panel">
			<div class="panel-heading">
				{if $ps_version == 0}<h3>{/if}
					4 - {l s='Setup options' mod='atos'}
				{if $ps_version == 0}</h3>{/if}
			</div>
			<form id="form_step_four" name="form_step_four" action="{$form_uri|escape:'htmlall':'UTF-8'}" method="post">
			<div id="collapse4">
				<div class="table-responsive clearfix">
					<div class="row">
						<div class="col-sm-6 col-md-4 col-lg-3">
							<div class="well">
								<i class="icon-retweet"></i>
								<h4>{l s='Redirection after payment' mod='atos'}</h4>
								<div class="radio">
									<label>
										<input type="radio" name="ATOS_REDIRECT" id="ATOS_REDIRECT1" value="1" {if $redirect|intval == 1}checked="checked"{/if} />
										{l s='Your shop' mod='atos'}
									</label>
								</div>
								<div class="radio">
									<label>
										<input type="radio" name="ATOS_REDIRECT" id="ATOS_REDIRECT2" value="0" {if $redirect|intval == 0}checked="checked"{/if} />
										{l s='Atos confirmation page' mod='atos'}
									</label>
								</div>
							</div>
						</div>

						<div class="col-sm-6 col-md-4 col-lg-3">
							<div class="well">
								<i class="icon-cogs"></i>
								<h4>{l s='Payment error behavior' mod='atos'}</h4>
								<div class="radio">
									<label>
										<input type="radio" name="ATOS_ERROR_BEHAVIOR" id="ATOS_ERROR_BEHAVIOR1" value="0" {if $behavior|intval == 0}checked="checked"{/if} />
										{l s='Save order as a payment error' mod='atos'}
									</label>
								</div>
								<div class="radio">
									<label>
										<input type="radio" name="ATOS_ERROR_BEHAVIOR" id="ATOS_ERROR_BEHAVIOR2" value="1" {if $behavior|intval == 1}checked="checked"{/if} />
										{l s='Send me an e-mail' mod='atos'}
									</label>
								</div>
								<div class="radio">
									<label>
										<input type="radio" name="ATOS_ERROR_BEHAVIOR" id="ATOS_ERROR_BEHAVIOR3" value="2" {if $behavior|intval == 2}checked="checked"{/if} />
										{l s='Do nothing' mod='atos'}
									</label>
								</div>
							</div>
						</div>

						<div class="col-sm-6 col-md-4 col-lg-3">
							<div class="well">
								<i class="icon-archive"></i>
								<h4>{l s='Notification e-mail' mod='atos'}</h4>
								<div class="form-group clear">
									<label for="form-field-1" class="col-sm-4 control-label">
										{l s='Your mail' mod='atos'}
									</label>
									<div class="col-sm-6">
										<input type="text" class="form-control" value="{if isset($notification) & !empty($notification)}{$notification|escape:'htmlall':'UTF-8'}{/if}" id="ATOS_NOTIFICATION_EMAIL" name="ATOS_NOTIFICATION_EMAIL" placeholder="{l s='Your mail' mod='atos'}" />
									</div>
								</div>
							</div>
						</div>

						<div class="col-sm-6 col-md-3 col-lg-3">
							<div class="well">
								<i class="icon-lock"></i>
								<h4>{l s='Disable 3D Secure' mod='atos'}</h4>
								<div class="form-group clear">
									<p>{l s='Would you like to disable 3D Secure?' mod='atos'}</p>
									<span id="atos_3d" class="switch prestashop-switch input-group col-sm-12 col-md-8 col-lg-8">
										<input type="radio" name="ATOS_DISABLE_3D" id="atos_3d_yes" {if $treeDSecure|intval === 1}checked="checked"{/if} value="1" />
										<label for="atos_3d_yes" class="radioCheck">
											<i class="color_success"></i> {l s=' Yes' mod='atos'}
										</label>
										<input type="radio" class="switch_off" name="ATOS_DISABLE_3D" id="atos_3d_no"{if empty($treeDSecure) || $treeDSecure|intval === 0}checked="checked"{/if} value="0" />
										<label for="atos_3d_no" class="radioCheck">
											<i class="color_success"></i> {l s=' No ' mod='atos'}
										</label>
										<a class="slide-button btn"></a>
									</span>
									<br />
									<div name="min_3d_secure_table_data" id="min_3d_secure_table_data"{if empty($treeDSecure) || $treeDSecure|intval === 0} style="display:none"{/if} >
										{l s='while cart total is less than' mod='atos'}
										<div class="input-group">
											<span class="input-group-addon">{$currency_sign|escape:'htmlall':'UTF-8'}</span>
											<input type="text" value="{$treeDSecureMinimum}" id="ATOS_DISABLE_3D_MINIMUM" name="ATOS_DISABLE_3D_MINIMUM" class="form-control" maxlength="3" style="width:50px;">
										</div>
										<small>{l s='Leave empty to disable 3D Secure for all orders.' mod='atos'}</small>
									</div>
								</div>
							</div>
						</div>

						<div class="clearfix"></div>

						{if $bank == 'mercanet' || $bank == 'scelliusnet' || $bank == 'sogenactif'}
							<div class="col-sm-6 col-md-4 col-lg-3">
								<div class="well">
									<i class="icon-lock"></i>
									<h4>{l s='Enable Paylib payment card' mod='atos'}</h4>
									<p>{l s='Check with your bank if you can enable this option' mod='atos'}</p>
									<span id="atos_paylib" class="switch prestashop-switch input-group col-sm-12 col-md-8 col-lg-8">
										<input type="radio" name="ATOS_ALLOW_PAYLIB" id="atos_paylib_yes" {if $paylib|intval === 1}checked="checked"{/if} value="1" />
										<label for="atos_paylib_yes" class="radioCheck">
											<i class="color_success"></i> {l s=' Active' mod='atos'}
										</label>
										<input type="radio" class="switch_off" name="ATOS_ALLOW_PAYLIB" id="atos_paylib_no" {if empty($paylib) || $paylib|intval === 0}checked="checked"{/if} value="0" />
										<label for="atos_paylib_no" class="radioCheck">
											<i class="color_success"></i> {l s=' Inactive ' mod='atos'}
										</label>
										<a class="slide-button btn"></a>
									</span>
								</div>
							</div>
						{/if}

						<div class="col-sm-6 col-md-4 col-lg-3">
							<div class="well">
								<i class="icon-lock"></i>
								<h4>{l s='Enable AMEX payment card' mod='atos'}</h4>
								<p>{l s='Check with your bank if you can enable this option' mod='atos'}</p>
								<span id="atos_amex" class="switch prestashop-switch input-group col-sm-12 col-md-8 col-lg-8">
									<input type="radio" name="ATOS_ALLOW_AMEX" id="atos_amex_yes" {if $amex|intval === 1}checked="checked"{/if} value="1" />
									<label for="atos_amex_yes" class="radioCheck">
										<i class="color_success"></i> {l s=' Active' mod='atos'}
									</label>
									<input type="radio" class="switch_off" name="ATOS_ALLOW_AMEX" id="atos_amex_no"{if empty($amex) || $amex|intval === 0}checked="checked"{/if} value="0" />
									<label for="atos_amex_no" class="radioCheck">
										<i class="color_success"></i> {l s=' Inactive ' mod='atos'}
									</label>
									<a class="slide-button btn"></a>
								</span>
							</div>
						</div>

						<div class="col-sm-6 col-md-4 col-lg-3">
							<div class="well">
								<i class="icon-lock"></i>
								<h4>{l s='Select capture mode to apply' mod='atos'}</h4>
								<span id="atos_capture_mode" class="switch prestashop-switch input-group col-sm-12 col-md-8 col-lg-8">
									<input type="radio" name="ATOS_CAPTURE_MODE" id="atos_capture_mode_yes" {if $capture_mode|intval === 1}checked="checked"{/if} value="1" />
									<label for="atos_capture_mode_yes" class="radioCheck">
										<i class="color_success"></i> {l s=' CAPTURE' mod='atos'}
									</label>
									<input type="radio" class="switch_off" name="ATOS_CAPTURE_MODE" id="atos_capture_mode_no"{if empty($capture_mode) || $capture_mode|intval === 0}checked="checked"{/if} value="0" />
									<label for="atos_capture_mode_no" class="radioCheck">
										<i class="color_success"></i> {l s=' VALIDATION ' mod='atos'}
									</label>
									<a class="slide-button btn"></a>
								</span>

								<h4 >{l s='Select capture delay' mod='atos'} </h4>
								<select name="ATOS_CAPTURE_DAY" class="form-control">
								{for $days=0 to 99}
									<option value="{$days|intval}" {if $capture_day|intval === $days|intval}selected="selected"{/if}>{$days|intval}</option>
								{/for}
								</select>
							</div>
						</div>

						{*
						<div class="col-sm-6 col-md-4 col-lg-3">
							<div class="well">
								<h4>{l s='Enable Custome template' mod='atos'} *</h4>
								<span id="atos_custom_tpl" class="switch prestashop-switch input-group col-sm-12 col-md-8 col-lg-8">
									<input type="radio" name="ATOS_ALLOW_CUSTOM" id="atos_custom_tpl_yes" {if $custom == 1}checked="checked"{/if} value="1" />
									<label for="atos_custom_tpl_yes" class="radioCheck">
										<i class="color_success"></i> {l s=' Active' mod='atos'}
									</label>
									<input type="radio" class="switch_off" name="ATOS_ALLOW_CUSTOM" id="atos_custom_tpl_no"{if empty($custom) || $custom|intval === 0}checked="checked"{/if} value="0" />
									<label for="atos_custom_tpl_no" class="radioCheck">
										<i class="color_success"></i> {l s=' Inactive ' mod='atos'}
									</label>
									<a class="slide-button btn"></a>
								</span>
							</div>
						</div>
						*}
					</div>
					{*
					<p>* {l s='To enable Custome template you must first send an email containing your merchant_id and the domain name of your bank and attach your "custom_tpl" file with your images' mod='atos'}.
						<br />
						{l s='Some banks will send you a confirmation email to you specify the date on which the template is installed, other banks will contact you only in case of failure of the installation template.' mod='atos'}
					</p>
					<p><b>{l s="Once the template is installed on the bank's server you can enable Custome template" mod='atos'}</b></p>
					*}
				</div>
			</div>
			<div class="panel-footer">
				<div class="btn-group pull-right">
					<button name="submitAtosOptions" type="submit" class="btn btn-default" value="1"><i class="process-icon-save"></i> {l s='Save' mod='atos'}</button>
				</div>
			</div>
			</form>
		</div>
	</div>
</div>