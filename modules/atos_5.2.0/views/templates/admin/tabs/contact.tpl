{*
* 2007-2014 PrestaShop
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
* @author    PrestaShop SA <contact@prestashop.com>
* @copyright 2007-2014 PrestaShop SA
* @license   http://addons.prestashop.com/en/content/12-terms-and-conditions-of-use
* International Registered Trademark & Property of PrestaShop SA
*}

<div class="clearfix"></div>
<div class="tab-pane panel" id="contacts">
	<h3>
		Prestashop Addons
	</h3>
	<div class="form-group">
		<b>{l s='Thank you for choosing a module developed by the Addons Team of PrestaShop.' mod='atos'}</b><br /><br />

		{l s='Would you like our developers to guide you in order to configure the module and make it operate?' mod='atos'} <a target="_blank" href="http://addons.prestashop.com/fr/divers/186-installation-de-module.html">{l s='Click here' mod='atos'}.</a><br /><br />

		{l s='If you encounter a problem using the module, our team is at your service via the ' mod='atos'} <a target="_blank" href="http://addons.prestashop.com/contact-form.php">{l s='contact form' mod='atos'}.</a><br /><br />

		{l s='To save you time, before you contact us:' mod='atos'}<br />
		- {l s='make sure you have read the documentation well. ' mod='atos'}<br />
		- {l s='in the event you would be contacting us via the form, do not hesitate to give us your first message, maximum of details on the problem and its origins (screenshots, reproduce actions to find the bug, etc. ..) ' mod='atos'}<br /><br />
		{l s='This module has been developped by PrestaShop and can only be sold through' mod='atos'} <a target="_blank" href="http://addons.prestashop.com">addons.prestashop.com</a>.<br /><br />

		The PrestaShop Addons Team<br />
	</div>
</div>