/**
 * 2007-2019 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author    PrestaShop SA <contact@prestashop.com>
 *  @copyright 2007-2019 PrestaShop SA
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 *  International Registered Trademark & Property of PrestaShop SA
 *
 * Don't forget to prefix your containers with your own identifier
 * to avoid any conflicts with others containers.
 */

// Vanilla document ready
document.addEventListener('DOMContentLoaded', function() {
    if (site_key) {
        // Create structure variables
        var submit_button = findCM([
            '#product #id_new_comment_form button[name="submitMessage"]'
        ]);
        var captcha_form = findCM([
            '#product #id_new_comment_form .new_comment_form_content #commentCustomerName'
        ]);
        var error_header = findCM([
            '#product #id_new_comment_form .page-subheading'
        ]);
        var captcha_invisible_form = findCM([
            '#product #id_new_comment_form'
        ]);

        // Check if is whitelisted and display relevant content
        if (!whitelisted) {
            // Add our captcha depending to reCaptcha version
            if (re_version == 1) {
                // reCaptcha Version 2
                $(captcha_form).after('<div style="margin-top: 10px;" class="g-recaptcha" data-sitekey="' + site_key + '" data-theme="' + re_theme + '" data-size="' + re_size + '"></div>');
                
                var clicked_first = true;

                // Disable ajax until everything is ok
                $(submit_button).attr('id', 'submitNewMessage_disabled');

                $(submit_button).on('click', function() {
                    if (clicked_first == true) {
                        if ($(submit_button).prop('type') == 'submit') {
                            $(submit_button).prop('type', 'button');
                        }
                    }

                    if (clicked_first == true) {
                        if (grecaptcha.getResponse()) {
                            $('.alert').remove();
                            $(submit_button).prop('type', 'submit');
                            $(submit_button).attr('id', 'submitNewMessage');
                            clicked_first = false;
                            $(submit_button).click();
                        } else {
                            $('.alert').remove();
                            $("<div class='alert alert-danger'><p>" + there_is1 + "</p><ol><li>" + wrong_captcha + "</ol></div>").hide().insertAfter(error_header).fadeIn(600);
                        }
                    }
                });
            } else if (re_version == 2) {
                // reCaptcha Invisible
                regCOMM(submit_button, error_header);

                $(submit_button).click(function(event) {
                    if (!grecaptcha.getResponse()) {
                        event.preventDefault();
                        grecaptcha.execute();
                    }
                });

                $(captcha_invisible_form).append('<div id="recaptcha" class="g-recaptcha" data-sitekey="' + site_key + '" data-callback="onreCSubmitCOMM" data-size="invisible"></div>');

                // Disable ajax until everything is ok
                $(submit_button).attr('id', 'submitNewMessage_disabled');
            }
        } else {
            if (whitelist_m) {
                $(captcha_form).append('<input type="hidden" name="recaptcha_whitelisted" value="1" />');
                $(captcha_form).after('<p class="recaptcha_whitelist">' + whitelist_m + '</p>');
            } else {
                $(captcha_form).append('<input type="hidden" name="recaptcha_whitelisted" value="1" />');
            }
        }
    }
});

// Utility functions
function onreCSubmitCOMM(token) {
    if (token) {
        $($('#re_comments_captcha_invisible_form').attr('data-parameter')).attr('id', 'submitNewMessage');
        $($('#re_comments_captcha_invisible_form').attr('data-parameter')).submit();
    }
}

function findCM(str) {
    var temp = false;

    $.each(str, function(key, value) {
        if ($(value).length) {
            temp = value;
            return false;
        }
    });

    if (temp) {
        return temp;
    } else {
        return false;
    }
}

function regCOMM(captcha_invisible_form, error_header) {
    $('body').append("<div id='re_comments_error_header' data-parameter='" + error_header + "'></div>");
    $('body').append("<div id='re_comments_captcha_invisible_form' data-parameter='" + captcha_invisible_form + "'></div>");
}