/**
 * 2007-2019 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author    PrestaShop SA <contact@prestashop.com>
 *  @copyright 2007-2019 PrestaShop SA
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 *  International Registered Trademark & Property of PrestaShop SA
 *
 * Don't forget to prefix your containers with your own identifier
 * to avoid any conflicts with others containers.
 */

// Vanilla document ready
document.addEventListener('DOMContentLoaded', function() {
    if (site_key) {
        var newsletter_form = findCM([
            '.block_newsletter form',
            '#newsletter_block_left form',
            '#footercustom_newsletter_social form',
            '.st_news_letter_form',
            '#newsletter form',
            '.footer-newsletter form',
            '.newsletter-form form'
        ]);
        
        if (typeof(display_error) !== 'undefined') {
            if (p_version) {
                $(newsletter_form).append("<div style='margin-top: 10px;' class='alert alert-danger error'><p>" + there_is1 + "</p><ol><li>" + error_message + "</ol></div>");
            } else {
                $('.columns-container > #columns').prepend("<div style='margin-top: 10px;' class='alert alert-danger error'><p>" + there_is1 + "</p><ol><li>" + error_message + "</ol></div>");
            }
        }

        var clicked_first = false;
        var clicked_first_warning = false;
        var news_grecaptcha = false;
        var news_validated = true;

        // Add our captcha depending to reCaptcha version
        if (re_version == 1) {
            // reCaptcha Version 2
            $(newsletter_form).submit(function(event) {
                if (clicked_first == false) {
                    // Remove all others reCaptchas
                    $('.g-recaptcha').remove();

                    $(newsletter_form).append('<div style="margin-top: 10px;" id="news-recaptcha-re" class="g-recaptcha" data-sitekey="' + site_key + '" data-theme="' + re_theme + '" data-size="' + re_size + '"></div>');
                    news_grecaptcha = grecaptcha.render('news-recaptcha-re');

                    clicked_first = true;
                }

                if (grecaptcha.getResponse(news_grecaptcha) && news_validated) {
                    news_validated = false;
                    $(newsletter_form).submit();
                } else {
                    if (! grecaptcha.getResponse(news_grecaptcha)) {
                        if (clicked_first_warning == false) {
                            $('.alert, .error').remove();
                            $("<div style='margin-top: 10px;' class='alert alert-warning error'><p>" + check_bellow + "</p><ol><li>" + validate_first + "</ol></div>").hide().insertBefore(newsletter_form + ' .g-recaptcha').fadeIn(600);
                            
                            clicked_first_warning = true;
                        } else {
                            $('.alert, .error').remove();
                            $("<div style='margin-top: 10px;' class='alert alert-danger error'><p>" + there_is1 + "</p><ol><li>" + wrong_captcha + "</ol></div>").hide().insertBefore(newsletter_form + ' .g-recaptcha').fadeIn(600);
                        }   
                    }
                    
                    // Prevent default submition
                    event.preventDefault();
                }
            });
        } else if (re_version == 2) {
            // reCaptcha Invisible
            var clickedd_first = false;

            regN(newsletter_form);

            $(newsletter_form).submit(function(event) {
                if (clickedd_first == false) {
                    $(newsletter_form).append('<input type="hidden" id="news-letter-validated" value="" />');
                }

                // Prevent default submition
                if (!$('#news-letter-validated').val()) {
                    event.preventDefault();
                }

                if (clickedd_first == false) {
                    // Remove all others reCaptchas
                    $('.g-recaptcha').remove();

                    $(newsletter_form).append('<div id="news-recaptcha-re" class="g-recaptcha" data-sitekey="' + site_key + '" data-callback="onreCSubmitN" data-size="invisible"></div>');
                    $(newsletter_form).append('<input type="hidden" name="submitNewsletter" />');

                    news_grecaptcha = grecaptcha.render('news-recaptcha-re');

                    clickedd_first = true;
                }

                if (!grecaptcha.getResponse(news_grecaptcha)) {
                    grecaptcha.execute(news_grecaptcha);
                }
            });
        }
    }
});

// Utility functions
function onreCSubmitN(token) {
    if (token) {
        $('#news-letter-validated').val('true');
        $($('#re_newsletter_captcha_invisible_form').attr('data-parameter')).submit();
    }
}

function findCM(str) {
    var temp = false;

    $.each(str, function(key, value) {
        if ($(value).length) {
            temp = value;
            return false;
        }
    });

    if (temp) {
        return temp;
    } else {
        return false;
    }
}

function regN(newsletter_form) {
    $('body').append("<div id='re_newsletter_captcha_invisible_form' data-parameter='" + newsletter_form + "'></div>");
}