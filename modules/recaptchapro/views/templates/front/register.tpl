{**
 * 2007-2019 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author    PrestaShop SA <contact@prestashop.com>
 *  @copyright 2007-2019 PrestaShop SA
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 *  International Registered Trademark & Property of PrestaShop SA
 *}

{if $site_key}
	{if $re_version == 1}
		<div style="margin-top: 10px; margin-bottom: 15px;" id="g-recaptcha-re" data-callback="getRecaptcha" class="g-recaptcha" data-sitekey="{$site_key|escape:"htmlall":"UTF-8"}" data-theme="{$re_theme|escape:"htmlall":"UTF-8"}" data-size="{$re_size|escape:"htmlall":"UTF-8"}"></div>
		<input type="hidden" value="" name="g-recaptcha-response" id="order-recaptcha-response" />
	{else}
		<div id="g-recaptcha-re" class="g-recaptcha" data-sitekey="{$site_key|escape:"htmlall":"UTF-8"}" data-callback="onreCSubmitREG" data-size="invisible"></div>
		<input type="hidden" value="" name="invisible-g-recaptcha-response" id="order-recaptcha-response" />
		<input type="hidden" value="" name="submitAccount" />
	{/if}
	{if isset($p_version)}
		<script>
			p_version = '1.7';
		</script>
	{/if}
	<script>
		if (document.readyState === 'complete') {
			// Load static reCaptcha
			var new_recaptcha = grecaptcha.render('g-recaptcha-re');
			
			check_recaptcha_REG(new_recaptcha);
		} else {
			// Vanilla document ready
			document.addEventListener('DOMContentLoaded', function() {
				// Load static reCaptcha
				check_recaptcha_REG(false);
			});
		}
		
		// Utility functions
		function check_recaptcha_REG(new_recaptcha) {
			// Create structure variables
			var submit_button = findCM([
				'#authentication #customer-form .form-footer .btn-primary',
				'#authentication #account-creation_form #submitAccount',
				'#slideRegisterForm #registerForm .form-footer .btn-primary'
			]);
			var error_header = findCM([
				'#authentication .register-form',
				'#slideRegisterForm #registerForm form',
				'#center_column > .page-heading',
				'#center_column .page-subheading',
				'#center_column #noSlide > h1',
				'#center_column .page_heading',
				'#center_column #account-creation_form',
				'#center_column #customer-form'
			]);
			var captcha_invisible_form = findCM([
				'#authentication #customer-form',
				'#slideRegisterForm #registerForm',
				'#authentication #account-creation_form'
			]);
			
			// Add our captcha depending to reCaptcha version
			if (re_version == 1) {
				// reCaptcha Version 2
				var clicked_first = true;
				
				$(submit_button).on('click', function() {
					if (clicked_first == true) {
                        if ($(submit_button).prop('type') == 'submit') {
                            $(submit_button).prop('type', 'button');
                        }
                    }
					
					if (clicked_first == true) {
						if ((new_recaptcha ? grecaptcha.getResponse(new_recaptcha) : grecaptcha.getResponse())) {
							$(submit_button).prop('type', 'submit');
							clicked_first = false;
							$(submit_button).click();
						} else {
							$('.alert, .error').remove();
							if (p_version == '1.7') {
								$('<div class="alert alert-danger error"><p>{$there_is1|escape:"htmlall":"UTF-8"}</p><ol><li>{$wrong_captcha|escape:"htmlall":"UTF-8"}</ol></div>').hide().insertBefore(error_header).fadeIn(600);
							} else {
								$('<div class="alert alert-danger error"><p>{$there_is1|escape:"htmlall":"UTF-8"}</p><ol><li>{$wrong_captcha|escape:"htmlall":"UTF-8"}</ol></div>').hide().insertAfter(error_header).fadeIn(600);
							}
						}
					}
				});
			} else if (re_version == 2) {
				// reCaptcha Invisible
                regREG(captcha_invisible_form, error_header);
				
				$(captcha_invisible_form).submit(function(event) {
                    if (! (new_recaptcha ? grecaptcha.getResponse(new_recaptcha) : grecaptcha.getResponse())) {
                        event.preventDefault();
                        (new_recaptcha ? grecaptcha.execute(new_recaptcha) : grecaptcha.execute());
                    }
                });
			}
		}
		
		function onreCSubmitREG(token) {
			if (token) {
				$('#order-recaptcha-response').remove();
				$($('#re_register_captcha_invisible_form').attr('data-parameter')).submit();
			}
		}
		
		function findCM(str) {
            var temp = false;
            
            $.each(str, function(key, value) {
                if ($(value).length) {
                    temp = value;
                    return false;
                }
            });
            
            if (temp) {
                return temp;   
            } else {
                return false;
            }
        }
		
		function regREG(captcha_invisible_form, error_header) {
			$('body').append("<div id='re_register_error_header' data-parameter='" + error_header + "'></div>");
			$('body').append("<div id='re_register_captcha_invisible_form' data-parameter='" + captcha_invisible_form + "'></div>");
		}
		
		function getRecaptcha() {
			$('#order-recaptcha-response').val(grecaptcha.getResponse());
		}
	</script>
{/if}