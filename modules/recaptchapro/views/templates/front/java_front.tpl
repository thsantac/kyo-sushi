{**
 * 2007-2019 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author    PrestaShop SA <contact@prestashop.com>
 *  @copyright 2007-2019 PrestaShop SA
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 *  International Registered Trademark & Property of PrestaShop SA
 *}

{literal}
    <script type="text/javascript">
        there_is1 = '{/literal}{$there_is1|escape:'htmlall':'UTF-8'}{literal}';
		wrong_captcha = '{/literal}{$wrong_captcha|escape:'htmlall':'UTF-8'}{literal}';
		site_key = '{/literal}{$site_key|escape:'htmlall':'UTF-8'}{literal}';
		whitelisted = '{/literal}{$whitelisted|escape:'htmlall':'UTF-8'}{literal}';
		whitelist_m = '{/literal}{$whitelist_m|escape:'htmlall':'UTF-8'}{literal}';
		re_version = {/literal}{$re_version|escape:'htmlall':'UTF-8'}{literal};
		p_version = '{/literal}{$p_version|escape:'htmlall':'UTF-8'}{literal}';
		re_theme = '{/literal}{$re_theme|escape:'htmlall':'UTF-8'}{literal}';
		re_size = '{/literal}{$re_size|escape:'htmlall':'UTF-8'}{literal}';
    </script>
{/literal}