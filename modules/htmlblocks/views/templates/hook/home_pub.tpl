{foreach $htmlBlocks as $htmlBlock}
	<div id="{$htmlBlock.name}" class="{$hookName} aos-item aos-init aos-animate" data-aos="fade-up">
		<div class="container">
		{$htmlBlock.content.content nofilter}
		</div>
	</div>
{/foreach}
