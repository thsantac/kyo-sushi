{*
* 2007-2016 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2016 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

<div id="category-navigation">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 title">
				<h2>Faites vous plaisir !</h2>
				<h3>et découvrez toutes nos recettes</h3>
			</div>
		</div>
		<div class="row categ-list">
		{foreach from=$categories key=categIndex item=category}
			<div class="col-lg-3 col-md-4 col-sm-4 col-xs-6 aos-item aos-init aos-animate movable-parent" data-aos="fade-up">
				<div class="category-item movable-bloc" id="category-item-{$category.id_category}" style="background-image: url(/img/c/{$category.id_category}_thumb.jpg)" data-categindex="{$categIndex}">
					<div>
					    <h3>{$category.name}</h3>
					</div>
				    <div id="category-loading-{$category.id_category}" class="categoryProductsLoading">
				        <div class="logo">
				            <img src="{$urls.img_url}logo_kyo_rouge.png" class="fa-spin">
				        </div>
				    </div>
				</div>
			</div>
		{/foreach}

        {include file="catalog/_partials/fast-order.tpl" categindex={$categIndex}}

		</div>
	</div>
</div>
<div id="category-navigation-bottom">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 link-to-all">
				<a href="{$allProductsLink}">Voir la carte</a>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	var categoryNavController = "{$link->getModuleLink('llw_categorynav', 'ajax', array(), null, null, null, true)}";
</script>
