
var currentOpenCateg		= 0;
var lastCategOfLineID		= 0;
if (window.matchMedia("(max-width: 767px)").matches) {
	var nbCategByLine 		= 2;
}
else
if (window.matchMedia("(max-width: 1023px)").matches) {
	var nbCategByLine 		= 3;
}
else {
	var nbCategByLine 		= 4;
}

$(document).ready(function() { 
	$(".category-item").on("click", function(e) {
		console.log("--------------------------------------------------------");
		console.log("Click sur "+$(this).attr('id'));

		if(currentOpenCateg) {
			console.log("Fermeture de "+currentOpenCateg);
			$("#"+currentOpenCateg).removeClass('current');
			var thisProducts = $("#"+currentOpenCateg+"-products-cloned_wrapper");
			thisProducts.fadeOut(500);
			thisProducts.remove();
			replaceMovedCategs($(this).attr('id'));
		}
		else {
			console.log("Ouverture de "+$(this).attr('id'));
			showProducts($(this).attr('id'));
		}		
	})
})

function showProducts(clickedCateg) {

	currentOpenCateg			= clickedCateg;
	console.log("showProducts de "+currentOpenCateg);
	$("#"+currentOpenCateg).addClass('current');
	var thisCategIndex 			= $("#"+currentOpenCateg).data('categindex');
	console.log("thisCategIndex="+thisCategIndex);
	var lineIndex 				= Math.floor(parseInt(thisCategIndex) / nbCategByLine) + 1;
	console.log("lineIndex="+lineIndex);
	var firstCategIndexOfLine	= (nbCategByLine * lineIndex) - nbCategByLine;
	var lastCategIndexOfLine	= (nbCategByLine * lineIndex) - 1;
	console.log("lastCategIndexOfLine="+lastCategIndexOfLine);
	
	var lastCategOfLineNotFound	= true;
	while(lastCategOfLineNotFound) {
		if($("#category-navigation").find('*[data-categindex="'+lastCategIndexOfLine+'"]').length) {
			lastCategOfLineNotFound		= false;	
		}
		else {
			lastCategIndexOfLine--;
		}
	}
	
	lastCategOfLine 			= $("#category-navigation").find('*[data-categindex="'+lastCategIndexOfLine+'"]');
	var lastCategOfLineID		= lastCategOfLine.attr('id');
	var lastCategOfLineParent	= $("#"+lastCategOfLineID).closest(".container");
	console.log("ID de lastCategOfLine="+lastCategOfLineID);

	$("#category-navigation").append(
		$("<div/>")
			.addClass("container")
			.append(
				$("<div/>")
					.addClass("row")
					.attr("id", "movedOtherCategs")
			)
		);

	$("#category-navigation").find(".movable-bloc").each(function() {
		var thisCategIndex 		= $(this).data("categindex");
		if(thisCategIndex > lastCategIndexOfLine) {
			$(this).parent().appendTo("#movedOtherCategs");
		}	
	})

	if($("#"+currentOpenCateg+"-products").length==0) {
		var elements			= currentOpenCateg.split("-");
		var id_category 		= elements[2];
		$("#category-loading-"+id_category).show();
		// La liste des produist n'a pas été insérée dans le DOM ------------------------------------------
		$.ajax({
	        type: 'POST',
	        url: categoryNavController,
	        async: true,
	        cache: false,
			dataType : 'json',
			data: {
				'ajax': 1,
				'action': 'displayProducts',
				'id_category': id_category
			},
			success : function(data, statut) {
				$("#category-navigation-bottom").append(data.products_list);
				showProductsList(currentOpenCateg, lastCategOfLineParent, lastCategOfLineID);
				$("#category-loading-"+id_category).hide();
			},
			error : function(resultat, statut, erreur) {
				alert("showProducts error : "+erreur);
			}
	    });
	}
	else	
		showProductsList(currentOpenCateg, lastCategOfLineParent, lastCategOfLineID);
}

function showProductsList(currentOpenCateg, lastCategOfLineParent, lastCategOfLineID) {

	var clonedListID 			= currentOpenCateg+"-products-cloned";
	var clonedListWrapperID 	= clonedListID+"_wrapper";
	
	$("#"+currentOpenCateg+"-products").clone().attr("id", clonedListID).insertAfter(lastCategOfLineParent);
	$("#"+clonedListID).wrapAll('<div id="'+clonedListWrapperID+'" class="home_product_list_wrapper"><div class="container"></div></div>');

	$("#"+clonedListID).show();
	$("#"+clonedListID).find(".category-item-products").show();
	$([document.documentElement, document.body]).animate({
    	scrollTop: $("#"+lastCategOfLineID).offset().top - $(".header-top").height() - 70
	}, 1000);
	$("#"+clonedListWrapperID).fadeIn(1500);

	if($("#"+clonedListID).find('select.form-control-select').length==0) {
		$("#"+clonedListID).find('select.form-control').addClass('form-control-select');
		$("#"+clonedListID).find('.form-control-select').select2({
			language: 'fr',
		});
	}
	
	if($("#"+clonedListID).find(".bootstrap-touchspin").length==0) {
	    console.log("TouchSpin sur an_productattributes-qty");
	    $(".an_productattributes-qty").TouchSpin({
	        buttondown_class: "btn btn-touchspin",
	        buttonup_class: "btn btn-touchspin",
	        min: 1,
	        max: 20
	    })
	}
	
    $(".an_productattributes-qty").on("touchspin.on.startupspin", function() {
	    console.log("touchspin.on.startupspin");
		var qty = parseInt($(this).val());
		//$(this).val(qty++);
    })
    $(".an_productattributes-qty").on("touchspin.on.startdownspin", function() {
	    console.log("touchspin.on.startdownspin");
		var qty = parseInt($(this).val());
		//if(qty > 1)
		//	$(this).val(qty--);
    })

	$(".home_product_list_wrapper .close, .home_product_list_wrapper .closeBtn").on("click", function(e) {
		$("#"+currentOpenCateg).removeClass('current');
		var thisProducts = $("#"+currentOpenCateg+"-products-cloned_wrapper");
		thisProducts.fadeOut(500);
		thisProducts.remove();

	    var headerTopHeight = $('.header-top').height();
	    if($(".header-banner").length && !$(".header-banner").hasClass('closed')) {
		    headerTopHeight += $(".header-banner").outerHeight();
	    }

		$([document.documentElement, document.body]).animate({
	    	scrollTop: $("#"+lastCategOfLineID).offset().top - headerTopHeight
		}, 1000);
		replaceMovedCategs(false);
	})

    activeProductQuickView();
}

function replaceMovedCategs(currentOpenCategory) {
	console.log("replaceMovedCategs de "+currentOpenCategory);
	var nbElements			= $("#movedOtherCategs").find(".movable-parent").length;
	var iElement			= 0;
	currentOpenCateg		= currentOpenCategory;
	console.log("replaceMovedCategs currentOpenCateg="+currentOpenCateg);
	if($("#movedOtherCategs").find(".movable-parent").length) {
		$("#movedOtherCategs").find(".movable-parent").each(function() {
			$("#category-navigation").first(".container").find(".categ-list").append($(this));	
			iElement++;
			console.log("replaceMovedCategs nbElements="+nbElements+", iElement="+iElement);
			if(iElement==nbElements) {
				$("#movedOtherCategs").parent().remove();
				if(currentOpenCategory) {
					showProducts(currentOpenCategory);
				}
			}
		})
	}
	else {
		if(currentOpenCategory) {
			showProducts(currentOpenCategory);
		}
	}
}

// ============================================================================================
// Dernière commande passée
// ============================================================================================
$("#seeLastOrderButton").on("click", function(e) {
	var url_seeorder 	= $(e.target).data("seeorder_url");
	var url_reorder 	= $(e.target).data("reorder_url");
    $.ajax({
		url : url_seeorder,
		type : 'GET',
		dataType : 'json',
		success : function(data, statut) { // success est toujours en place, bien sûr !
			$('#js-cart-sidebar').css("height", "100%");
			$('#js-cart-sidebar .cart-dropdown-wrapper').css("height", "100%");

			$('#js-cart-sidebar .cart-dropdown-wrapper').html(data.preview);
			$('#js-cart-sidebar .cart-dropdown-wrapper .cart-summary').css("height", "100%");

			$('#js-cart-sidebar h2').text("Ma dernière commande");
			$("#showCartSummaryProductList").click();
			$("#showCartSummaryProductList").hide();
			$('#js-cart-sidebar .delivery_choice').hide();
			$('#js-cart-sidebar .block-promo').hide();
			$('#js-cart-sidebar #reOrderButtonContainer a').attr("href", url_reorder);
			$('#js-cart-sidebar #reOrderButtonContainer').show();

			$('html').addClass('sb-open');
			$('.sb-overlay ').fadeIn(500);
		},
		error : function(resultat, statut, erreur) {
		
		}

    });
})

