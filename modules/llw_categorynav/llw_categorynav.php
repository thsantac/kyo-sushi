<?php
/*
* 2007-2016 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2016 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

if (!defined('_PS_VERSION_')) {
    exit;
}

use PrestaShop\PrestaShop\Core\Module\WidgetInterface;
use PrestaShop\PrestaShop\Adapter\Presenter\Order\OrderPresenter;

class Llw_categorynav extends Module implements WidgetInterface
{
	public $presenter;
    public $assembler;
    public $presentationSettings;
	
    public function __construct()
    {
        $this->name = 'llw_categorynav';
        $this->tab = 'front_office_features';
        $this->version = '1.0.0';
        $this->author = 'La Ligne Web';

        $this->bootstrap = true;
        parent::__construct();

        $this->displayName = $this->getTranslator()->trans('Navigation dans les catégories', array(), 'Modules.Categorytree.Admin');
        $this->description = $this->getTranslator()->trans('Adds a block featuring product categories.', array(), 'Modules.Categorytree.Admin');
        $this->ps_versions_compliancy = array('min' => '1.7.1.0', 'max' => _PS_VERSION_);
    }

    public function install()
    {
        return parent::install()
            && $this->registerHook('displayHome')
            && $this->registerHook('displayHeader')
        ;
    }

    public function uninstall()
    {
        if (!parent::uninstall()) {
            return false;
        }
        return true;
    }

    public function getContent()
    {
        $this->html = '';

        $this->postProcess();

        $this->html .= $this->renderForm();

        return $this->html;
    }

    protected function postProcess()
    {
        $errors = array();

        if (Tools::isSubmit('buidCategPage')) {
        	if (Tools::getValue('in_cache')) {
	        	$fileName 						= 'root_productlist.tpl';
	        	$filePath						= _PS_THEME_DIR_.'templates/catalog/_partials/'.$fileName;
	        	if(is_file($filePath)) {
		        	unlink($filePath);
	        	}

				$context 						= Context::getContext();
				$root_category					= Category::getRootCategory();
				$category 						= new Category($root_category->id, $context->language->id, $context->shop->id);
		        $assembler 						= new ProductAssembler($context);
		        $presenterFactory 				= new ProductPresenterFactory($context);
		        $presentationSettings 			= $presenterFactory->getPresentationSettings();
		        $presenter 						= new PrestaShop\PrestaShop\Core\Product\ProductListingPresenter(
		            new PrestaShop\PrestaShop\Adapter\Image\ImageRetriever(
		                $context->link
		            ),
		            $context->link,
		            new PrestaShop\PrestaShop\Adapter\Product\PriceFormatter(),
		            new PrestaShop\PrestaShop\Adapter\Product\ProductColorsRetriever(),
		            $context->getTranslator()
		        );
		
				$raw_products 					= Product::getProducts($context->language->id, 0, 1000, 'position', 'ASC', $root_category->id, true, $context);
		        $products_for_template 			= array();
		        foreach ($raw_products as &$item) {
			        $item['data_features']		= '';
		            $products_for_template[] 	= $presenter->present(
		                $presentationSettings,
		                $assembler->assembleProduct($item),
		                $context->language
		            );
		        }
				
		        $product_features 					= [];
				$excluded_feature 					= Configuration::get("LLW_KYO_EXCLUDE_FEATURE");
				$prev_category_default 				= $products_for_template[0]['id_category_default'];
				$nb_products 						= 0;
				$nb_products_by_category 			= [];
		        foreach($products_for_template as &$product) {
			        $data_features					= '';
					foreach($product['features'] as $product_feature) {
						if($product_feature['id_feature'] == $excluded_feature) {
							$data_features 		   .= $product_feature['id_feature_value']."-";
						}
					}
					$data_features 					= rtrim($data_features, "-");
					$product['data_features'] 		= $data_features;
					if($product['id_category_default'] != $prev_category_default) {
						$nb_products_by_category [$prev_category_default]	= $nb_products;
						$prev_category_default		= $product['id_category_default'];
						$nb_products 				= 0;
					}
					$nb_products++;
		        }
				$nb_products_by_category [$prev_category_default]	= $nb_products;

				$nb_products					= count($raw_products);

		        $this->context->smarty->assign([
	                'tpl_dir'             		=> _PS_THEME_DIR_,
		            'products' 					=> $products_for_template,
		            'nb_products_by_category' 	=> $nb_products_by_category,
		        ]);
				
				$content 						= $this->fetch('module:llw_categorynav/views/templates/front/productlist.tpl');
				$fp 							= fopen($filePath, 'w');
				fwrite($fp, $content);
				fclose($fp);
				$this->html .= $this->displayInformation('Le fichier cache de la page "La carte" a été généré !');
			}
        } 

        if (count($errors) > 0) {
            $this->html .= $this->displayError(implode('<br />', $errors));
        }
    }

    public function renderForm()
    {
        $fields_form_1 = array(
            'form' => array(
                'legend' => array(
                    'title' 		=> $this->trans('Mise en cache de la page catégorie', array(), 'Modules.Mailalerts.Admin'),
                    'icon' 			=> 'icon-cog',
                ),
                'input' => array(
                    array(
                        'type' => 'switch',
                        'is_bool' => false, //retro compat 1.5
                        'label' => $this->trans('Lancer la mise en cache ?', array(), 'Modules.Mailalerts.Admin'),
                        'name' => 'in_cache',
                        'values' => array(
                            array(
                                'id' => 'in_cache_on',
                                'value' => true,
                                'label' => 'Oui',
                            ),
                            array(
                                'id' => 'in_cache_off',
                                'value' => false,
                                'label' => 'Non',
                            ),
                        ),
                    ),
                ),
                'submit' => array(
                    'title' 		=> 'Lancer',
                    'class' 		=> 'btn btn-default pull-right',
                    'name' 			=> 'buidCategPage',
                ),
            ),
        );
        
        $helper = new HelperForm();
        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $lang = new Language((int) Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language = $lang->id;
        $helper->module = $this;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submitBuildCategPage';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false)
            .'&configure='.$this->name
            .'&tab_module='.$this->tab
            .'&module_name='.$this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars = array(
            'fields_value' => ['in_cache' => false],
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id,
        );

        return $helper->generateForm(array($fields_form_1));
    }

    private function getCategories()
    {
        $range = '';

        $resultIds = array();
        $resultParents = array();
        $result = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
			SELECT c.id_parent, c.id_category, cl.name, cl.description, cl.link_rewrite
			FROM `'._DB_PREFIX_.'category` c
			INNER JOIN `'._DB_PREFIX_.'category_lang` cl ON (c.`id_category` = cl.`id_category` AND cl.`id_lang` = '.(int)$this->context->language->id.Shop::addSqlRestrictionOnLang('cl').')
			INNER JOIN `'._DB_PREFIX_.'category_shop` cs ON (cs.`id_category` = c.`id_category` AND cs.`id_shop` = '.(int)$this->context->shop->id.')
			WHERE (c.`active` = 1 OR c.`id_category` = '.(int)Configuration::get('PS_HOME_CATEGORY').')
			AND c.`id_category` != '.(int)Configuration::get('PS_ROOT_CATEGORY').'
			AND c.`level_depth` = 2
			AND c.id_category IN (
				SELECT id_category
				FROM `'._DB_PREFIX_.'category_group`
				WHERE `id_group` IN ('.pSQL(implode(', ', Customer::getGroupsStatic((int)$this->context->customer->id))).')
			)
			ORDER BY cs.`position` ASC');

        return $result;
    }

	public function displayProducts($id_category) {
		
		$context 						= Context::getContext();
		$category 						= new Category($id_category, $context->language->id, $context->shop->id);
        $assembler 						= new ProductAssembler($context);
        $presenterFactory 				= new ProductPresenterFactory($context);
        $presentationSettings 			= $presenterFactory->getPresentationSettings();
        $presenter 						= new PrestaShop\PrestaShop\Core\Product\ProductListingPresenter(
            new PrestaShop\PrestaShop\Adapter\Image\ImageRetriever(
                $context->link
            ),
            $context->link,
            new PrestaShop\PrestaShop\Adapter\Product\PriceFormatter(),
            new PrestaShop\PrestaShop\Adapter\Product\ProductColorsRetriever(),
            $context->getTranslator()
        );

		$raw_products 					= Product::getProducts($context->language->id, 0, 1000, 'position', 'ASC', $id_category, true, $context);
        $products_for_template 			= array();
        foreach ($raw_products as &$item) {
	        $item['data_features']		= '';
            $products_for_template[] 	= $presenter->present(
                $presentationSettings,
                $assembler->assembleProduct($item),
                $context->language
            );
        }
		$nb_products					= count($raw_products);

        $this->smarty->assign([
	        'nb_products'				=> $nb_products,
	        'id_category'				=> $id_category,
	        'category_name'				=> $category->name,
	        'products'					=> $products_for_template,
        ]);

        return $this->fetch('module:llw_categorynav/views/templates/front/llw_categorynav_products.tpl');
	}

    public function hookDisplayHeader($param)
    {
        $this->context->controller->addJS($this->_path . 'views/js/llw_categorynav.js');
    }

    public function renderWidget($hookName = null, array $configuration = [])
    {
		if($hookName=="displayHeader") {
	        $this->context->controller->addJS($this->_path . 'views/js/llw_categorynav.js');
		}
		else {
	        $this->smarty->assign($this->getWidgetVariables($hookName, $configuration));
			return $this->fetch('module:llw_categorynav/views/templates/hook/llw_categorynav.tpl');
		}
    }

    public function getWidgetVariables($hookName = null, array $configuration = [])
    {
        $presented_order 				= '';
        $url_to_reorder 				= ''; 	
        $last_order_id					= '';  
        $url_to_seeorder				= '';  
        if($this->context->customer->isLogged(true)) {
            $order_presenter 			= new OrderPresenter();
			$customer_orders 			= Order::getCustomerOrders($this->context->customer->id);
			if(is_array($customer_orders) && count(($customer_orders))) {
				$last_order 			= $customer_orders [0];
	            $order 					= new Order((int) $last_order['id_order']);
	            $presented_order 		= $order_presenter->present($order);
	            $url_to_reorder 		= $this->context->link->getPageLink('order', true, null, 'submitReorder=1&id_order=' . (int) $last_order['id_order']);
	            $url_to_seeorder 		= $this->context->link->getPageLink('order', true, null, 'ajax=1&simul=1&action=lastOrder&id_order=' . (int) $last_order['id_order']);
	            $last_order_id 			= $last_order['id_order'];
			}
	    }
        return [
            'categories' 		=> $this->getCategories(),
            'url_to_reorder' 	=> $url_to_reorder,
            'url_to_seeorder' 	=> $url_to_seeorder,
            'lastOrder' 		=> $presented_order,
            'last_order_id'		=> $last_order_id,
            'allProductsLink' 	=> $this->context->link->getCategoryLink(Configuration::get('PS_HOME_CATEGORY')),
       ];
    }
}
