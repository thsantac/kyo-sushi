/**
 * Copyright © Lyra Network.
 * This file is part of Systempay plugin for PrestaShop. See COPYING.md for license details.
 *
 * @author    Lyra Network (https://www.lyra.com/)
 * @copyright Lyra Network
 * @license   https://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */

/**
 * Misc JavaScript functions.
 */

function systempayAddMultiOption(first) {
    if (first) {
        $('#systempay_multi_options_btn').hide();
        $('#systempay_multi_options_table').show();
    }

    var timestamp = new Date().getTime();

    var rowTpl = $('#systempay_multi_row_option').html();
    rowTpl = rowTpl.replace(/SYSTEMPAY_MULTI_KEY/g, '' + timestamp);

    $(rowTpl).insertBefore('#systempay_multi_option_add');
}

function systempayDeleteMultiOption(key) {
    $('#systempay_multi_option_' + key).remove();

    if ($('#systempay_multi_options_table tbody tr').length === 1) {
        $('#systempay_multi_options_btn').show();
        $('#systempay_multi_options_table').hide();
        $('#systempay_multi_options_table').append("<input type=\"hidden\" id=\"SYSTEMPAY_MULTI_OPTIONS\" name=\"SYSTEMPAY_MULTI_OPTIONS\" value=\"\">");
    }
}

function systempayAddOneyOption(first, suffix = '') {
    if (first) {
        $('#systempay_oney' + suffix + '_options_btn').hide();
        $('#systempay_oney' + suffix + '_options_table').show();
    }

    var timestamp = new Date().getTime();
    var key = suffix != '' ? /SYSTEMPAY_ONEY34_KEY/g : /SYSTEMPAY_ONEY_KEY/g;
    var rowTpl = $('#systempay_oney' + suffix + '_row_option').html();
    rowTpl = rowTpl.replace(key, '' + timestamp);

    $(rowTpl).insertBefore('#systempay_oney' + suffix + '_option_add');
}

function systempayDeleteOneyOption(key, suffix = '') {
    $('#systempay_oney' + suffix + '_option_' + key).remove();

    if ($('#systempay_oney' + suffix + '_options_table tbody tr').length === 1) {
        $('#systempay_oney' + suffix + '_options_btn').show();
        $('#systempay_oney' + suffix + '_options_table').hide();
        $('#systempay_oney' + suffix + '_options_table').append("<input type=\"hidden\" id=\"SYSTEMPAY_ONEY" + suffix + "_OPTIONS\" name=\"SYSTEMPAY_ONEY" + suffix + "_OPTIONS\" value=\"\">");
    }
}

function systempayAdditionalOptionsToggle(legend) {
    var fieldset = $(legend).parent();

    $(legend).children('span').toggleClass('ui-icon-triangle-1-e ui-icon-triangle-1-s');
    fieldset.find('section').slideToggle();
}

function systempayCategoryTableVisibility() {
    var category = $('select#SYSTEMPAY_COMMON_CATEGORY option:selected').val();

    if (category === 'CUSTOM_MAPPING') {
        $('.systempay_category_mapping').show();
        $('.systempay_category_mapping select').removeAttr('disabled');
    } else {
        $('.systempay_category_mapping').hide();
        $('.systempay_category_mapping select').attr('disabled', 'disabled');
    }
}

function systempayDeliveryTypeChanged(key) {
    var type = $('#SYSTEMPAY_ONEY_SHIP_OPTIONS_' + key + '_type').val();

    if (type === 'RECLAIM_IN_SHOP') {
        $('#SYSTEMPAY_ONEY_SHIP_OPTIONS_' + key + '_address').show();
        $('#SYSTEMPAY_ONEY_SHIP_OPTIONS_' + key + '_zip').show();
        $('#SYSTEMPAY_ONEY_SHIP_OPTIONS_' + key + '_city').show();
    } else {
        $('#SYSTEMPAY_ONEY_SHIP_OPTIONS_' + key + '_address').val('');
        $('#SYSTEMPAY_ONEY_SHIP_OPTIONS_' + key + '_zip').val('');
        $('#SYSTEMPAY_ONEY_SHIP_OPTIONS_' + key + '_city').val('');

        $('#SYSTEMPAY_ONEY_SHIP_OPTIONS_' + key + '_address').hide();
        $('#SYSTEMPAY_ONEY_SHIP_OPTIONS_' + key + '_zip').hide();
        $('#SYSTEMPAY_ONEY_SHIP_OPTIONS_' + key + '_city').hide();
    }

    var speed = $('#SYSTEMPAY_ONEY_SHIP_OPTIONS_' + key + '_speed').val();
    if (speed === 'PRIORITY') {
        $('#SYSTEMPAY_ONEY_SHIP_OPTIONS_' + key + '_delay').show();
    } else {
        $('#SYSTEMPAY_ONEY_SHIP_OPTIONS_' + key + '_delay').hide();
    }
}

function systempayDeliverySpeedChanged(key) {
    var speed = $('#SYSTEMPAY_ONEY_SHIP_OPTIONS_' + key + '_speed').val();
    var type = $('#SYSTEMPAY_ONEY_SHIP_OPTIONS_' + key + '_type').val();

    if (speed === 'PRIORITY') {
        $('#SYSTEMPAY_ONEY_SHIP_OPTIONS_' + key + '_delay').show();
    } else {
        $('#SYSTEMPAY_ONEY_SHIP_OPTIONS_' + key + '_delay').hide();
    }
}

function systempayRedirectChanged() {
    var redirect = $('select#SYSTEMPAY_REDIRECT_ENABLED option:selected').val();

    if (redirect === 'True') {
        $('#systempay_redirect_settings').show();
        $('#systempay_redirect_settings select, #systempay_redirect_settings input').removeAttr('disabled');
    } else {
        $('#systempay_redirect_settings').hide();
        $('#systempay_redirect_settings select, #systempay_redirect_settings input').attr('disabled', 'disabled');
    }
}

function systempayOneyEnableOptionsChanged() {
    var enable = $('select#SYSTEMPAY_ONEY_ENABLE_OPTIONS option:selected').val();

    if (enable === 'True') {
        $('#systempay_oney_options_settings').show();
        $('#systempay_oney_options_settings select, #systempay_oney_options_settings input').removeAttr('disabled');
    } else {
        $('#systempay_oney_options_settings').hide();
        $('#systempay_oney_options_settings select, #systempay_oney_options_settings input').attr('disabled', 'disabled');
    }
}

function systempayFullcbEnableOptionsChanged() {
    var enable = $('select#SYSTEMPAY_FULLCB_ENABLE_OPTS option:selected').val();

    if (enable === 'True') {
        $('#systempay_fullcb_options_settings').show();
        $('#systempay_fullcb_options_settings select, #systempay_fullcb_options_settings input').removeAttr('disabled');
    } else {
        $('#systempay_fullcb_options_settings').hide();
        $('#systempay_fullcb_options_settings select, #systempay_fullcb_options_settings input').attr('disabled', 'disabled');
    }
}

function systempayHideOtherLanguage(id, name) {
    $('.translatable-field').hide();
    $('.lang-' + id).css('display', 'inline');

    $('.translation-btn button span').text(name);

    var id_old_language = id_language;
    id_language = id;

    if (id_old_language !== id) {
        changeEmployeeLanguage();
    }
}

function systempayAddOtherPaymentMeansOption(first) {
    if (first) {
        $('#systempay_other_payment_means_options_btn').hide();
        $('#systempay_other_payment_means_options_table').show();
        $('#SYSTEMPAY_OTHER_PAYMENT_MEANS').remove();
    }

    var timestamp = new Date().getTime();

    var rowTpl = $('#systempay_other_payment_means_row_option').html();
    rowTpl = rowTpl.replace(/SYSTEMPAY_OTHER_PAYMENT_SCRIPT_MEANS_KEY/g, '' + timestamp);

    $(rowTpl).insertBefore('#systempay_other_payment_means_option_add');
}

function systempayDeleteOtherPaymentMeansOption(key) {
    $('#systempay_other_payment_means_option_' + key).remove();

    if ($('#systempay_other_payment_means_options_table tbody tr').length === 1) {
        $('#systempay_other_payment_means_options_btn').show();
        $('#systempay_other_payment_means_options_table').hide();
        $('#systempay_other_payment_means_options_table').append("<input type=\"hidden\" id=\"SYSTEMPAY_OTHER_PAYMENT_MEANS\" name=\"SYSTEMPAY_OTHER_PAYMENT_MEANS\" value=\"\">");
    }
}

function systempayCountriesRestrictMenuDisplay(retrictCountriesPaymentId) {
    var countryRestrict = $('#' + retrictCountriesPaymentId).val();
    if (countryRestrict === '2') {
        $('#' + retrictCountriesPaymentId + '_MENU').show();
    } else {
        $('#' + retrictCountriesPaymentId + '_MENU').hide();
    }
}

function systempayOneClickMenuDisplay() {
    var oneClickPayment = $('#SYSTEMPAY_STD_1_CLICK_PAYMENT').val();
    if (oneClickPayment == 'True') {
        $('#SYSTEMPAY_STD_1_CLICK_MENU').show();
    } else {
        $('#SYSTEMPAY_STD_1_CLICK_MENU').hide();
    }
}

function systempayDisplayMultiSelect(selectId) {
    $('#' + selectId).show();
    $('#' + selectId).focus();
    $('#LABEL_' + selectId).hide();
}

function systempayDisplayLabel(selectId, clickMessage) {
    $('#' + selectId).hide();
    $('#LABEL_' + selectId).show();
    $('#LABEL_' + selectId).text(systempayGetLabelText(selectId, clickMessage));
}

function systempayGetLabelText(selectId, clickMessage) {
    var select = document.getElementById(selectId);
    var labelText = '', option;

    for (var i = 0, len = select.options.length; i < len; i++) {
        option = select.options[i];

        if (option.selected) {
            labelText += option.text + ', ';
        }
    }

    labelText = labelText.substring(0, labelText.length - 2);
    if (!labelText) {
        labelText = clickMessage;
    }

    return labelText;
}