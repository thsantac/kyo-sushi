{**
 * Copyright © Lyra Network.
 * This file is part of Systempay plugin for PrestaShop. See COPYING.md for license details.
 *
 * @author    Lyra Network (https://www.lyra.com/)
 * @copyright Lyra Network
 * @license   https://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 *}

<!-- This meta tag is mandatory to avoid encoding problems caused by \PrestaShop\PrestaShop\Core\Payment\PaymentOptionFormDecorator -->
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

<section style="margin-bottom: 2rem;">
<div id="systempay_oneclick_payment_description">
  <ul id="systempay_oneclick_payment_description_1">
    <li>
      <span>{l s='You will pay with your registered means of payment' mod='systempay'}<b> {$systempay_saved_payment_mean|escape:'html':'UTF-8'}. </b>{l s='No data entry is needed.' mod='systempay'}</span>
    </li>

    <li style="margin: 8px 0px 8px;">
      <span>{l s='OR' mod='systempay'}</span>
    </li>

    <li>
      <a href="javascript: void(0);" onclick="systempayOneclickPaymentSelect(0)">{l s='Click here to pay with another means of payment.' mod='systempay'}</a>
    </li>
  </ul>
{if ($systempay_std_card_data_mode == '2')}
  </div>
    <script type="text/javascript">
      function systempayOneclickPaymentSelect(paymentByIdentifier) {
        if (paymentByIdentifier) {
          $('#systempay_oneclick_payment_description_1').show();
          $('#systempay_standard').hide();
          $('#systempay_payment_by_identifier').val('1');
        } else {
          $('#systempay_oneclick_payment_description_1').hide();
          $('#systempay_standard').show();
          $('#systempay_payment_by_identifier').val('0');
         }
       }
     </script>
{else}
    <ul id="systempay_oneclick_payment_description_2" style="display: none;">
      {if ($systempay_std_card_data_mode != '5') || $systempay_rest_popin}
        <li>{l s='You will enter payment data after order confirmation.' mod='systempay'}</li>
      {/if}

      <li style="margin: 8px 0px 8px;">
        <span>{l s='OR' mod='systempay'}</span>
      </li>
      <li>
        <a href="javascript: void(0);" onclick="systempayOneclickPaymentSelect(1)">{l s='Click here to pay with your registered means of payment.' mod='systempay'}</a>
      </li>
    </ul>
  </div>

  <script type="text/javascript">
    function systempayOneclickPaymentSelect(paymentByIdentifier) {
      if (paymentByIdentifier) {
        $('#systempay_oneclick_payment_description_1').show();
        $('#systempay_oneclick_payment_description_2').hide()
        $('#systempay_payment_by_identifier').val('1');
      } else {
        $('#systempay_oneclick_payment_description_1').hide();
        $('#systempay_oneclick_payment_description_2').show();
        $('#systempay_payment_by_identifier').val('0');
      }

      {if ($systempay_std_card_data_mode == '5')}
        $('.systempay .kr-form-error').html('');

        if ($('#systempay_payment_by_identifier').val() == '1') {
          var token = "{$systempay_rest_identifier_token|escape:'html':'UTF-8'}";
        } else {
          var token = "{$systempay_rest_form_token|escape:'html':'UTF-8'}";
        }

        KR.setFormConfig({ formToken: token, language: SYSTEMPAY_LANGUAGE });
      {/if}
    }
  </script>
{/if}
</section>

<script type="text/javascript">
$(document).ready(function() {
  $("input[data-module-name=systempay]").change(function() {
    if ($(this).is(':checked')) {
      systempayOneclickPaymentSelect(1);
    }
  });
});
</script>