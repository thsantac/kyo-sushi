{**
 * Copyright © Lyra Network.
 * This file is part of Systempay plugin for PrestaShop. See COPYING.md for license details.
 *
 * @author    Lyra Network (https://www.lyra.com/)
 * @copyright Lyra Network
 * @license   https://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 *}

{if version_compare($smarty.const._PS_VERSION_, '1.6', '>=')}
<div class="row"><div class="col-xs-12{if version_compare($smarty.const._PS_VERSION_, '1.6.0.11', '<')} col-md-6{/if}">
{/if}

<div class="payment_module systempay {$systempay_tag|escape:'html':'UTF-8'}">
{if $systempay_saved_identifier}
  <a class="unclickable systempay-standard-link" title="{l s='Choose pay with registred means of payment or enter payment information and click « Pay » button' mod='systempay'}">
    <img class="logo" src="{$systempay_logo|escape:'html':'UTF-8'}" alt="Systempay" />{$systempay_title|escape:'html':'UTF-8'}
{else}
  <a href="javascript: void(0);" title="{l s='Click here to pay by credit card' mod='systempay'}" id="systempay_standard_link" class="systempay-standard-link">
    <img class="logo" src="{$systempay_logo|escape:'html':'UTF-8'}" alt="Systempay" />{$systempay_title|escape:'html':'UTF-8'}
    <br />
{/if}

    {if $systempay_saved_identifier}
      <br /><br />
      {include file="./payment_std_oneclick.tpl"}
      <input id="systempay_payment_by_identifier" type="hidden" name="systempay_payment_by_identifier" value="1" />
    {/if}

    <iframe class="systempay-iframe" id="systempay_iframe" src="{$link->getModuleLink('systempay', 'iframe', ['content_only' => 1], true)|escape:'html':'UTF-8'}" style="display: none;">
    </iframe>

    {if $systempay_can_cancel_iframe}
        <button class="systempay-iframe" id="systempay_cancel_iframe" style="display: none;">{l s='< Cancel and return to payment choice' mod='systempay'}</button>
    {/if}
  </a>

  <script type="text/javascript">
    var done = false;
    function systempayShowIframe() {
      if (done) {
        return;
      }

      done = true;

      {if !$systempay_saved_identifier}
        $('#systempay_iframe').parent().addClass('unclickable');
      {/if}

      $('.systempay-iframe').show();
      $('#systempay_oneclick_payment_description').hide();

      var url = "{$link->getModuleLink('systempay', 'redirect', ['content_only' => 1], true)|escape:'url':'UTF-8'}";
      {if $systempay_saved_identifier}
            url = url + '&systempay_payment_by_identifier=' + $('#systempay_payment_by_identifier').val();
      {/if}

      $('#systempay_iframe').attr('src', decodeURIComponent(url) + '&' + Date.now());
    }

    function systempayHideIframe() {
      if (!done) {
        return;
      }

      done = false;

      {if !$systempay_saved_identifier}
        $('#systempay_iframe').parent().removeClass('unclickable');
      {/if}

      $('.systempay-iframe').hide();
      $('#systempay_oneclick_payment_description').show();

      var url = "{$link->getModuleLink('systempay', 'iframe', ['content_only' => 1], true)|escape:'url':'UTF-8'}";
      $('#systempay_iframe').attr('src', decodeURIComponent(url) + '&' + Date.now());
    }

    $(function() {
      $('#systempay_standard_link').click(systempayShowIframe);
      $('#systempay_cancel_iframe').click(function() {
        systempayHideIframe();
        return false;
      });

      $('.payment_module a:not(.systempay-standard-link)').click(systempayHideIframe);
    });
  </script>
</div>

{if version_compare($smarty.const._PS_VERSION_, '1.6', '>=')}
</div></div>
{/if}