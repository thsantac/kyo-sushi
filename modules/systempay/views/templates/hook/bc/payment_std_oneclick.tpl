{**
 * Copyright © Lyra Network.
 * This file is part of Systempay plugin for PrestaShop. See COPYING.md for license details.
 *
 * @author    Lyra Network (https://www.lyra.com/)
 * @copyright Lyra Network
 * @license   https://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 *}

<div style="padding-left: 40px;" id="systempay_oneclick_payment_description">
  <ul id="systempay_oneclick_payment_description_1">
    <li>
      <span class="systempay_span">{l s='You will pay with your registered means of payment' mod='systempay'}<b> {$systempay_saved_payment_mean|escape:'html':'UTF-8'}. </b>{l s='No data entry is needed.' mod='systempay'}</span>
    </li>

    <li style="margin: 8px 0px 8px;">
      <span class="systempay_span">{l s='OR' mod='systempay'}</span>
    </li>

    <li>
      <p class="systempay_link" onclick="systempayOneclickPaymentSelect(0)">{l s='Click here to pay with another means of payment.' mod='systempay'}</p>
    </li>
  </ul>
{if ($systempay_std_card_data_mode == '2')}
  <script type="text/javascript">
    function systempayOneclickPaymentSelect(paymentByIdentifier) {
      if (paymentByIdentifier) {
        $('#systempay_oneclick_payment_description').show();
        $('#systempay_standard').hide();
        $('#systempay_payment_by_identifier').val('1');
      } else {
        $('#systempay_oneclick_payment_description').hide();
        $('#systempay_standard').show();
        $('#systempay_payment_by_identifier').val('0');
      }
    }
  </script>
{else}
  <ul id="systempay_oneclick_payment_description_2" style="display: none;">
    {if ($systempay_std_card_data_mode != '5') || $systempay_rest_popin}
      <li>{l s='You will enter payment data after order confirmation.' mod='systempay'}</li>
    {/if}

      <li style="margin: 8px 0px 8px;">
        <span class="systempay_span">{l s='OR' mod='systempay'}</span>
      </li>
      <li>
        <p class="systempay_link" onclick="systempayOneclickPaymentSelect(1)">{l s='Click here to pay with your registered means of payment.' mod='systempay'}</p>
      </li>
  </ul>

  <script type="text/javascript">
    $(document).ready(function() {
       sessionStorage.setItem('systempayIdentifierToken', "{$systempay_rest_identifier_token|escape:'html':'UTF-8'}");
       sessionStorage.setItem('systempayToken', "{$systempay_rest_form_token|escape:'html':'UTF-8'}");
    });

    function systempayOneclickPaymentSelect(paymentByIdentifier) {
      if (paymentByIdentifier) {
        $('#systempay_oneclick_payment_description_1').show();
        $('#systempay_oneclick_payment_description_2').hide()
        $('#systempay_payment_by_identifier').val('1');
      } else {
        $('#systempay_oneclick_payment_description_1').hide();
        $('#systempay_oneclick_payment_description_2').show();
        $('#systempay_payment_by_identifier').val('0');
      }

      {if ($systempay_std_card_data_mode == '5')}
        $('.systempay .kr-form-error').html('');

        var token;
        if ($('#systempay_payment_by_identifier').val() == '1') {
          token = sessionStorage.getItem('systempayIdentifierToken');
        } else {
          token = sessionStorage.getItem('systempayToken');
        }

        KR.setFormConfig({ formToken: token, language: SYSTEMPAY_LANGUAGE });
      {/if}
    }
    </script>
{/if}

{if ($systempay_std_card_data_mode != '5')}
    {if version_compare($smarty.const._PS_VERSION_, '1.6', '<')}
      <input id="systempay_standard_link" value="{l s='Pay' mod='systempay'}" class="button" />
    {else}
      <button id="systempay_standard_link" class="button btn btn-default standard-checkout button-medium">
        <span>{l s='Pay' mod='systempay'}</span>
      </button>
    {/if}
{/if}
</div>