{**
 * Copyright © Lyra Network.
 * This file is part of Systempay plugin for PrestaShop. See COPYING.md for license details.
 *
 * @author    Lyra Network (https://www.lyra.com/)
 * @copyright Lyra Network
 * @license   https://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 *}

<!-- This meta tag is mandatory to avoid encoding problems caused by \PrestaShop\PrestaShop\Core\Payment\PaymentOptionFormDecorator -->
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

<section style="margin-top: -12px;">
  <iframe class="systempay-iframe" id="systempay_iframe" src="{$link->getModuleLink('systempay', 'iframe', array(), true)|escape:'html':'UTF-8'}" style="display: none;">
  </iframe>

   {if $systempay_can_cancel_iframe}
       <a id="systempay_cancel_iframe" class="systempay-iframe" style="margin-bottom: 8px; display: none;" href="javascript:systempayInit();">
           {l s='< Cancel and return to payment choice' mod='systempay'}
       </a>
   {/if}
</section>
<br />

<script type="text/javascript">
  var systempaySubmit = function(e) {
    e.preventDefault();

    if (!$('#systempay_standard').data('submitted')) {
      $('#systempay_standard').data('submitted', true);
      $('#payment-confirmation button').attr('disabled', 'disabled');
      $('.systempay-iframe').show();
      $('#systempay_oneclick_payment_description').hide();

      var url = decodeURIComponent("{$link->getModuleLink('systempay', 'redirect', ['content_only' => 1], true)|escape:'url':'UTF-8'}") + '&' + Date.now();
      {if $systempay_saved_identifier}
        url = url + '&systempay_payment_by_identifier=' + $('#systempay_payment_by_identifier').val();
      {/if}

      $('#systempay_iframe').attr('src', url);
    }

    return false;
  }

  setTimeout(function() {
    $('input[type="radio"][name="payment-option"]').change(function() {
      systempayInit();
    });
  }, 0);

  function systempayInit() {
    if (!$('#systempay_standard').data('submitted')) {
      return;
    }

    $('#systempay_standard').data('submitted', false);
    $('#payment-confirmation button').removeAttr('disabled');
    $('.systempay-iframe').hide();
    $('#systempay_oneclick_payment_description').show();

    var url = decodeURIComponent("{$link->getModuleLink('systempay', 'iframe', ['content_only' => 1], true)|escape:'url':'UTF-8'}") + '&' + Date.now();
    $('#systempay_iframe').attr('src', url);
  }
</script>