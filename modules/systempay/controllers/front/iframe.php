<?php
/**
 * Copyright © Lyra Network.
 * This file is part of Systempay plugin for PrestaShop. See COPYING.md for license details.
 *
 * @author    Lyra Network (https://www.lyra.com/)
 * @copyright Lyra Network
 * @license   https://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */

class SystempayIframeModuleFrontController extends ModuleFrontController
{
    public $ssl = true;

    /**
     * @see FrontController::postProcess()
     */
    public function postProcess()
    {
        if (Configuration::get('SYSTEMPAY_CART_MANAGEMENT') !== SystempayTools::KEEP_CART) {
            if ($this->context->cart->id) {
                $this->context->cookie->systempayCartId = (int) $this->context->cart->id;
            }

            if (isset($this->context->cookie->systempayCartId)) {
                $this->context->cookie->id_cart = $this->context->cookie->systempayCartId;
            }
        }

        $this->setTemplate(SystempayTools::getTemplatePath('iframe/loader.tpl'));
    }
}
