<?php
/*
* 2007-2014 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2014 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

if (!defined('_PS_VERSION_'))
	exit;

use PrestaShop\PrestaShop\Core\Module\WidgetInterface;

class bestkit_shippingmatrix extends Module implements WidgetInterface
{
	const PREFIX = 'bestkit_sm';

	protected $_hooks = array();

	protected static $_configs = array(
		'add_handling_cost' => false,
		'add_tax' => false,
	);

    protected $_tabs = array(
        array(
            'class_name' => 'AdminBestkitShippingMatrix',
            'parent' => 'AdminParentShipping',
            'name' => 'Frais de livraison par zone'
        ),
    );

	public function __construct()
	{
		$this->name = 'bestkit_shippingmatrix';
		$this->tab = 'shipping_logistics';
		$this->version = '1.1.0';
		$this->author = 'best-kit.com';
		$this->module_key = '4724430ac457cf28742ac9ddd87d5053';
        $this->need_instance = 0;

        $this->bootstrap = true;
        parent::__construct();

		$this->displayName = $this->l('Frais de livraison par zone');
		$this->description = $this->l('Ce module permet de définir des frais de livraison par zones kilométriques.');
	}

	public static function getConfig($name, $value = null)
	{
		$result = false;
		if (array_key_exists($name, self::$_configs)) {
			$key = self::PREFIX . $name;
			if (is_null($value)) {
				$result = Configuration::get($key);
				if (is_array(self::$_configs[$name])) {
					$result = explode(',', $result);
				}
			} else {
				if (is_array(self::$_configs[$name])) {
					$value = implode(',', $value);
				} else {
					$value = pSQL($value);
				}

				$result = Configuration::updateValue($key, $value);
			}
		}
		
		return $result;
	}

    public function install()
    {
        if (parent::install()) {
			foreach (self::$_configs as $name => $value) {
				self::getConfig($name, $value);
			}

            $sql = array();

            /*
            	Prestashop Carrier Name
            	Country ISO
            	State (Name or ISO)
            	City
            	ZIP/Postal Code
            	Zip To
            	Price >=
            	Price <=
            	Weight >=
            	Weight <-
            	Shipping Price
            */

            $sql[] = 'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'bestkit_shippingmatrix` (
			            `id_bestkit_shippingmatrix` INT( 11 ) UNSIGNED NOT NULL AUTO_INCREMENT,
			            `id_carrier` int(11) NOT NULL,
			            `country_iso` varchar(3) DEFAULT "FR",
			            `state` varchar(255) DEFAULT "*",
			            `city` varchar(255) DEFAULT "*",
			            `zone_name` varchar(255) DEFAULT "Zone 1",
			            `km_from` int(4) DEFAULT "0",
			            `km_to` int(4) DEFAULT "0",
			            `price_from` decimal(20,6) DEFAULT "0",
			            `price_to` decimal(20,6) DEFAULT "0",
			            `shipping_price` varchar(20) NOT NULL,
			            `mt_min_order` decimal(20,6) DEFAULT "0",
			            PRIMARY KEY (`id_bestkit_shippingmatrix`)
			        ) ENGINE = ' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8';

            foreach ($sql as $_sql) {
                Db::getInstance()->Execute($_sql);
            }

            foreach ($this->_hooks as $hook) {
                if (!$this->registerHook($hook)) {
                    return FALSE;
                }
            }

            $languages = Language::getLanguages();
            foreach ($this->_tabs as $tab) {
                $_tab = new Tab();
                $_tab->class_name = $tab['class_name'];
                $_tab->id_parent = Tab::getIdFromClassName($tab['parent']);
                if (empty($_tab->id_parent)) {
                    $_tab->id_parent = 0;
                }

                $_tab->module = $this->name;
                foreach ($languages as $language) {
                    $_tab->name[$language['id_lang']] = $this->l($tab['name']);
                }

                $_tab->add();
            }

            return TRUE;
        }

        return FALSE;
    }

	public function addFeesToShippingCost($shipping_cost, $products, Carrier $carrier, Address $address, $id_currency)
	{
		foreach ($products as $product) {
			$shipping_cost += $product['additional_shipping_cost'];
		}

		if (self::getConfig('add_handling_cost')) {
			$shipping_cost += (float)Configuration::get('PS_SHIPPING_HANDLING');
		}

		if (self::getConfig('add_tax')) {
			$carrier_tax = $carrier->getTaxesRate($address);
			$shipping_cost *= 1 + ($carrier_tax / 100);
		}

		$shipping_cost = Tools::convertPrice($shipping_cost, Currency::getCurrencyInstance($id_currency));

		return (float)Tools::ps_round((float)$shipping_cost, 2);
	}

    public function uninstall()
    {
        if (parent::uninstall()) {
            $sql = array();
			$sql[] = 'DROP TABLE IF EXISTS `' . _DB_PREFIX_ . 'bestkit_shippingmatrix`';

            foreach ($sql as $_sql) {
                Db::getInstance()->Execute($_sql);
            }

            foreach ($this->_tabs as $tab) {
                $_tab_id = Tab::getIdFromClassName($tab['class_name']);
                $_tab = new Tab($_tab_id);
                $_tab->delete();
            }
        }

        return TRUE;
    }

	public function getImportForm()
	{
		return $this->display(__FILE__, 'import_form.tpl');
	}

	/**
	 * Load the configuration form
	 */
	public function getContent()
	{
		/**
		 * If values have been submitted in the form, process.
		 */
		
		if (Tools::getIsset($this->name)) {
			$this->_postProcess();
		}

		$this->bootstrap = true;
		return $this->renderForm();
	}

	/**
	 * Create the form that will be displayed in the configuration of your module.
	 */
	protected function renderForm()
	{
		$helper = new HelperForm();

		$helper->show_toolbar = false;
		$helper->module = $this;
		$helper->submit_action = $this->name;
		$helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false)
			.'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
		$helper->token = Tools::getAdminTokenLite('AdminModules');
		foreach (array_keys(self::$_configs) as $key) {
			$helper->tpl_vars['fields_value'][$key] = self::getConfig($key);
		}

		return $helper->generateForm(array($this->getConfigForm()));
	}

	/**
	 * Create the structure of your form.
	 */
	protected function getConfigForm()
	{
		$form = array(
			'form' => array(
				'legend' => array(
					'title' => $this->l('Frais de livraison par code postal - Réglages'),
					'icon' => 'icon-cogs',
				),
				'input' => array(
					array(
		                'type' => 'switch',
		                'label' => $this->l('Frais de manutention :'),
		                'name' => 'add_handling_cost',
		                'class' => 't',
		                'is_bool' => TRUE,
		                'values' => array(array(
			                'id' => 'add_handling_cost_on',
			                'value' => 1), array(
			                'id' => 'add_handling_cost_off',
			                'value' => 0)
			            ),
			            'desc' => $this->l('Inclut les frais de manutention (paramétrés dans Transport > Préférences) dans le prix final.'),
		            ),
					array(
		                'type' => 'switch',
		                'label' => $this->l('Taxe sur les frais de livraison :'),
		                'name' => 'add_tax',
		                'class' => 't',
		                'is_bool' => TRUE,
		                'values' => array(array(
			                'id' => 'add_tax_on',
			                'value' => 1), array(
			                'id' => 'add_tax_off',
			                'value' => 0)
			            ),
			            'desc' => $this->l('Ajoute la taxe (paramétrée dans Transporteurs > Destinations d\'expédition et coûts > Taxe) au prix final.'),
		            ),
				),
				'submit' => array(
					'title' => $this->l('Sauvegarder'),
				),
			),
		);

		return $form;
	}

	public function getConfigureUrl($conf = false)
	{
		return $this->context->link->getAdminLink('AdminModules')
			.'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name
			.($conf ? '&conf=4' : '');
	}

	/**
	 * Save form data.
	 */
	protected function _postProcess()
	{
		foreach (array_keys(self::$_configs) as $key) {
			self::getConfig($key, Tools::getValue($key));
		}
		
		Tools::redirectAdmin($this->getConfigureUrl(true));
	}
	
    public function renderWidget($hookName = null, array $params = [])
    {
		if($hookName == "displayDeliveryZonesButton") {
            $this->smarty->assign($this->getWidgetVariables($hookName, $params));
			return $this->fetch('module:bestkit_shippingmatrix/views/templates/hook/delivery_zones_button.tpl');
		}
	    if($hookName == "displayDeliveryZonesModal") {
			if($data = $this->getWidgetVariables($hookName, $params)) {
	            $this->smarty->assign($data);
				return $this->fetch('module:bestkit_shippingmatrix/views/templates/hook/delivery_zones_modal.tpl');
			}
		}
		
        return false;
    }

    public function getWidgetVariables($hookName = null, array $params = [])
    {
	    if($hookName == "displayDeliveryZonesModal") {
	
			if($this->context->shop->frais_par_zone) {
				$sql 						= '
					SELECT bss.*, bs.*, gl.name as group_name FROM `' . _DB_PREFIX_ . 'bestkit_shippingmatrix_shop` bss
					LEFT JOIN `' . _DB_PREFIX_ . 'bestkit_shippingmatrix` bs ON bss.`id_bestkit_shippingmatrix` = bs.`id_bestkit_shippingmatrix`
					LEFT JOIN `' . _DB_PREFIX_ . 'bestkit_shippingmatrix_group` bsg ON bsg.id_bestkit_shippingmatrix = bs.id_bestkit_shippingmatrix
					LEFT JOIN `' . _DB_PREFIX_ . 'group_lang` gl ON gl.id_group = bsg.id_group
					WHERE bss.`id_shop` =' . $this->context->shop->id . '
					AND bsg.id_group = 1
					ORDER BY bs.`km_from` ASC, bs.`price_from` ASC
					';
			}
			else {
				$sql 						= '
					SELECT bss.*, bs.*, gl.name as group_name FROM `' . _DB_PREFIX_ . 'bestkit_shippingmatrix_shop` bss
					LEFT JOIN `' . _DB_PREFIX_ . 'bestkit_shippingmatrix` bs ON bss.`id_bestkit_shippingmatrix` = bs.`id_bestkit_shippingmatrix`
					LEFT JOIN `' . _DB_PREFIX_ . 'bestkit_shippingmatrix_group` bsg ON bsg.id_bestkit_shippingmatrix = bs.id_bestkit_shippingmatrix
					LEFT JOIN `' . _DB_PREFIX_ . 'group_lang` gl ON gl.id_group = bsg.id_group
					WHERE bss.`id_shop` =' . $this->context->shop->id . '
					AND bsg.id_group = 1
					ORDER BY bs.`zone_name` ASC, bs.`price_from` ASC
					';
			}
			$shipping_rates 				= Db::getInstance()->executeS($sql);

			if (count($shipping_rates)) {
				foreach($shipping_rates as &$shipping_rate) {
					if(strpos($shipping_rate['shipping_price'], "%")===false) 
						$shipping_rate['shipping_price']	= number_format($shipping_rate['shipping_price'], 2, ",", ".")." €";
				}
				return [
	                'shipping_rates' => $shipping_rates,
	                'frais_par_zone' => (int)$this->context->shop->frais_par_zone,
	            ];
	        }

	        return false;
	    }
    }
}
