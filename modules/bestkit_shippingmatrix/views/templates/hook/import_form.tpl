<div class="panel">
	<div class="panel-heading">
			{l s='Import' mod='bestkit_shippingmatrix'}
	</div>
	<form action="" method="post" enctype="multipart/form-data">
		<div class="row">
            <label class="control-label col-lg-5" for="bestkit_delimiter" style="text-align:right">
                <span data-toggle="tooltip" title="">
                     {l s='Délimiteur :' mod='bestkit_shippingmatrix'}
                </span>
            </label>
            <div class="col-lg-5">
                <input type="text" id="bestkit_delimiter" name="bestkit_delimiter" value=";">
            </div>
        </div><br/>
        <div class="row">
            <label class="control-label col-lg-5" for="bestkit_enclosure" style="text-align:right">
                <span data-toggle="tooltip" title="">
                     {l s='Champs entourés par :' mod='bestkit_shippingmatrix'}
                </span>
            </label>
            <div class="col-lg-5">
                <input type="text" id="bestkit_enclosure" name="bestkit_enclosure" value='"'>
            </div>
        </div><br/>
        <div class="row">
            <label class="control-label col-lg-5" for="bestkit_import" style="text-align:right">
                <span data-toggle="tooltip" title="">
                     {l s='Importer un fichier CSV' mod='bestkit_shippingmatrix'} (<a href="../modules/bestkit_shippingmatrix/example.csv">{l s='Example' mod='bestkit_shippingmatrix'}</a>):
                </span>
            </label>
            <div class="col-lg-5">
                <input type="file" id="bestkit_import" name="bestkit_import" value="">
            </div>
        </div><br/>
        <div class="row">
            <label class="control-label col-lg-5">
            </label>
            <div class="col-lg-5">
                <input type="submit" class="btn" value="{l s='Télécharger' mod='bestkit_shippingmatrix'}">
            </div>
        </div>
	</form>
</div>