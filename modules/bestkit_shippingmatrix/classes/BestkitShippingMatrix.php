<?php
/**
 * 2007-2014 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 *         DISCLAIMER   *
 * *************************************** */
/* Do not edit or add to this file if you wish to upgrade Prestashop to newer
* versions in the future.
* ****************************************************
*
*  @author     BEST-KIT.COM (contact@best-kit.com)
*  @copyright  http://best-kit.com
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

class BestkitShippingMatrix extends ObjectModel
{			            
    public $id;
    public $id_bestkit_shippingmatrix;
    public $id_carrier;
    public $zone_name;
    public $country_iso;
    public $state;
    public $city;
    public $km_to;
    public $km_from;
    public $price_from;
    public $price_to;
    public $shipping_price;
    public $mt_min_order;

    public static $definition = array(
        'table' => 'bestkit_shippingmatrix',
        'primary' => 'id_bestkit_shippingmatrix',
        'multilang' => FALSE,
        'fields' => array(
            'id_carrier' 	=>	array('type' => self::TYPE_INT),
            'country_iso' =>	array('type' => self::TYPE_STRING),
            'state' =>			array('type' => self::TYPE_STRING),
            'city' =>			array('type' => self::TYPE_STRING),
            'zone_name' =>		array('type' => self::TYPE_STRING),
            'km_to' =>			array('type' => self::TYPE_INT),
            'km_from' =>		array('type' => self::TYPE_INT),
            'price_from' =>		array('type' => self::TYPE_FLOAT),
            'price_to' =>		array('type' => self::TYPE_FLOAT),
            'shipping_price' =>	array('type' => self::TYPE_STRING),
            'mt_min_order' =>	array('type' => self::TYPE_FLOAT),
        ),
    );

    public function __construct($id_bestkit_shippingmatrix = null, $id_lang = null, $id_shop = null)
    {
        parent::__construct($id_bestkit_shippingmatrix, $id_lang, $id_shop);
    }
}