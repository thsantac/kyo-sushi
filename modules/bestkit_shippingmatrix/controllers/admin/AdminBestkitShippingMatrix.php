<?php
/**
 * 2007-2013 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 *         DISCLAIMER   *
 * *************************************** */
 /* Do not edit or add to this file if you wish to upgrade Prestashop to newer
 * versions in the future.
 * ****************************************************
 *
 *  @author     BEST-KIT.COM (contact@best-kit.com)
 *  @copyright  http://best-kit.com
 *  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 *  International Registered Trademark & Property of PrestaShop SA
 */

require_once (_PS_MODULE_DIR_ . 'bestkit_shippingmatrix/classes/BestkitShippingMatrix.php');
require_once (_PS_MODULE_DIR_ . 'bestkit_shippingmatrix/lib/Csv.php');

class AdminBestkitShippingMatrixController extends ModuleAdminController
{
    protected $_module = NULL;

	protected $position_identifier = 'id_bestkit_shippingmatrix';
	
	protected $allow_export = true;

    public $groupBox;
    public $id;
    public $id_bestkit_shippingmatrix;

	private $_html = '';
  	private $_postErrors = array();
  
    public function __construct($id_bestkit_shippingmatrix = null, $id_lang = null, $id_shop = null)
    {
    	$this->bootstrap = true;
        $this->context = Context::getContext();
        if($id_shop==null)
        	$id_shop = $this->context->shop->id;
        $this->table = 'bestkit_shippingmatrix';
        $this->identifier = 'id_bestkit_shippingmatrix';
        $this->className = 'BestkitShippingMatrix';
        $this->lang = FALSE;

		$this->id_bestkit_shippingmatrix = $id_bestkit_shippingmatrix;

		if (Shop::isFeatureActive())
			Shop::setContext(Shop::CONTEXT_ALL);

        $this->context = Context::getContext();

        $this->addRowAction('edit');
        $this->addRowAction('delete');

		$this->fields_list = array(
			'id_bestkit_shippingmatrix' => array('title' => 'ID', 'width' => 25),
			'zone_name' => array('title' => 'Zone/secteur'),
			'id_shop' => array('title' => 'Boutique', 'callback' => 'showShopName'),
			'id_group' => array('title' => 'Groupes', 'callback' => 'showGroupName'),
			'km_from' => array('title' => "Km mini"),
			'km_to' => array('title' => "Km maxi"),
			'price_from' => array('title' => 'A partir de', 'type' => 'price'),
			'price_to' => array('title' => "Jusque", 'type' => 'price'),
			'mt_min_order' => array('title' => "Mt mini", 'type' => 'price'),
			'shipping_price' => array('title' => 'Frais', 'type' => 'text'),
		);

        parent::__construct($id_bestkit_shippingmatrix, $id_lang, $id_shop);
    }

    public function setMedia($isNewTheme = false)
    {
        parent::setMedia();
        $this->addJs(_PS_JS_DIR_.'admin/bestkit_shippingmatrix.js');
    }

	public function renderList()
	{
        $this->_select = 'bksms.*, bksmg.*';
        $this->_join = 'INNER JOIN `'._DB_PREFIX_.'bestkit_shippingmatrix_shop` bksms ON a.id_bestkit_shippingmatrix = bksms.id_bestkit_shippingmatrix
        				JOIN `'._DB_PREFIX_.'bestkit_shippingmatrix_group` bksmg ON a.id_bestkit_shippingmatrix = bksmg.id_bestkit_shippingmatrix
        				';
   		$this->_where = "AND bksms.id_shop=".$this->context->shop->id;
   		$this->_orderBy = "a.zone_name";
        $this->_orderWay = "ASC";

		return parent::renderList();
	}

    public function showShopName($id_shop, $tr)
    {
	    return Shop::getShopName($id_shop);
    }

    public function showGroupName($id_group, $tr)
    {
	    return Group::getGroupName($id_group);
    }

    public function renderForm()
    {
        $this->display = 'edit';
        $this->initToolbar();
        if (!$this->loadObject(TRUE)) {
            return;
        }

        $this->fields_form = array(
            'tinymce' => false,
            'legend' => array('title' => $this->l('Règles de livraison'), 'icon' =>
                    'icon-truck'),
            'input' => array(),
            'submit' => array('title' => $this->l('Enregistrer'))
        );

		unset($this->fields_list['id_bestkit_shippingmatrix']);

		$frais_par_zone = '';
        $this->fields_value = [];

        foreach ($this->fields_list as $key => $field) {
        	if ($key == 'id_carrier') {
		        $this->fields_form['input'][] = array(
					'type' => 'select',
					'label' => $field['title'],
	                'required' => TRUE,
	                'lang' => false,
					'name' => $key,
	                'options' => array(
	                    'query' => Carrier::getCarriers($this->context->language->id),
	                    'id' => 'id_carrier',
	                    'name' => 'name',
	                )
	            );

	        	continue;
        	}

        	if ($key == 'country_iso') {
		        $this->fields_form['input'][] = array(
					'type' => 'select',
					'label' => $field['title'],
	                'required' => TRUE,
	                'lang' => false,
					'name' => $key,
	                'options' => array(
	                    'query' => array_merge(
	                    	array(array('name' => '*', 'iso_code' => '*')), 
	                    	Country::getCountries($this->context->language->id)
	                    ),
	                    'id' => 'iso_code',
	                    'name' => 'name',
	                )
	            );

	        	continue;
        	}

        	if ($key == 'id_shop') {
		        $this->fields_form['input'][] = array(
                        'type' => 'shop',
                        'label' => $this->l('Boutiques concernées'),
                        'name' => 'checkBoxShopAsso',
                );

		        $id 					= Tools::getValue('id_bestkit_shippingmatrix');
		        if($id)
					$frais_par_zone 	= $this->getFraisParZone($id);
				else
					$frais_par_zone 	= Shop::getFraisParZone($this->context->shop->id);

		        $this->fields_form['input'][] = array(
                        'type' => 'hidden',
                        'name' => 'frais_par_zone',
                );
				
				$this->fields_value['frais_par_zone'] = $frais_par_zone;
				
	        	continue;
            }

        	if ($key == 'id_group') {
		        $this->fields_form['input'][] = array(
                        'type' => 'group',
                        'label' => $this->l('Groupes concernés'),
                        'name' => 'groupBox',
                        'values' => Group::getGroups(Context::getContext()->language->id),
                        'hint' => $this->l('Sélectionnez les groupes qui seront concernés par cette zone ou secteur.')
                );

		
		        // Added values of object Group
		        $carrier_groups = $this->getGroups();
		        $carrier_groups_ids = array();
		        if (is_array($carrier_groups)) {
		            foreach ($carrier_groups as $carrier_group) {
		                $carrier_groups_ids[] = $carrier_group['id_group'];
		            }
		        }

		        $groups = Group::getGroups($this->context->language->id);
		        $id = Tools::getValue('id_bestkit_shippingmatrix');
		
		        foreach ($groups as $group) {
		            $this->fields_value['groupBox_'.$group['id_group']] = Tools::getValue('groupBox_'.$group['id_group'], (in_array($group['id_group'], $carrier_groups_ids) || empty($carrier_groups_ids) && !$id));
		        }

	        	continue;
            }

        	if ($key == 'mt_min_order') {
		        $this->fields_form['input'][] = array(
					'type' => 'text',
					'label' => $field['title'],
	                'required' => FALSE,
	                'lang' => false,
					'name' => $key,
                );

	        	continue;
            }

	        $this->fields_form['input'][] = array(
				'type' => 'text',
				'label' => $field['title'],
                'required' => TRUE,
                'lang' => false,
				'name' => $key,
            );
        }

        return parent::renderForm();
    }

	public function processExport($text_delimiter = '"')
	{
		unset($this->fields_list['id_bestkit_shippingmatrix']);
		parent::processExport($text_delimiter);
	}

    public function processDuplicate()
    {
        if (Validate::isLoadedObject($bestkit = new BestkitShippingMatrix((int)Tools::getValue('id_bestkit_shippingmatrix')))) {
            unset($bestkit->id_bestkit_shippingmatrix);
            if (!$bestkit->add()) {
                $this->errors[] = Tools::displayError('An error occurred while creating an object.');
            }
        }
    }

	public function getFraisParZone($id)
	{
		$sql = '
			SELECT `id_shop`
			FROM `'._DB_PREFIX_.'bestkit_shippingmatrix_shop`
			WHERE `id_bestkit_shippingmatrix` = '.$id
		;
		$id_shop = Db::getInstance()->getValue($sql);
		return Shop::getFraisParZone($id_shop);
	}

    /**
     * Gets a specific group
     *
     * @since 1.5.0
     * @return array Group
     */
    public function getGroups()
    {
        return Db::getInstance()->executeS('
			SELECT id_group
			FROM '._DB_PREFIX_.'bestkit_shippingmatrix_group
			WHERE id_bestkit_shippingmatrix='.(int)Tools::getValue('id_bestkit_shippingmatrix'));
    }

    /**
     * Update customer groups associated to the object
     *
     * @param array $list groups
     */
    public function updateGroup($list)
    {
        $this->cleanGroups();
        if (!empty($list)) {
	        $this->addGroups($list);
        }
    }

    public function cleanGroups()
    {
    	return Db::getInstance()->delete('bestkit_shippingmatrix_group', 'id_bestkit_shippingmatrix = '.(int)Tools::getValue('id_bestkit_shippingmatrix'));
    }

    public function addGroups($groups)
    {
        foreach ($groups as $group) {
            if ($group !== false) {
                Db::getInstance()->insert('bestkit_shippingmatrix_group', array('id_bestkit_shippingmatrix' => (int)Tools::getValue('id_bestkit_shippingmatrix'), 'id_group' => (int)$group));
            }
        }
    }

	public function getContent()
	{
	  if (Tools::isSubmit('submitAddbestkit_shippingmatrix'))
	  {
    	if(count(Tools::getValue("checkBoxShopAsso_bestkit_shippingmatrix")) > 1) {
	    	$this->_postErrors[] = 'Vous ne pouvez sélectionner plus d\'une boutique !';
    	}
	    if (sizeof($this->_postErrors))
	    {
	      foreach ($this->_postErrors AS $err)
	      {
	        $this->_html .= '<div class="alert error">'.$err.'</div>';
	      }
	    }
	  }
	  $this->_displayForm();
	  return $this->_html;
	}

    public function postProcess()
    {
    	if (isset($_FILES['bestkit_import'])
    		//&& $_FILES['bestkit_import']['type'] == 'text/csv'
    		&& $_FILES['bestkit_import']['size'] > 0
    	) {
	    	$csv = new Varien_File_Csv();
	    	$csv->setDelimiter(Tools::getValue('bestkit_delimiter', ';'));
	    	$csv->setEnclosure(Tools::getValue('bestkit_enclosure', '"'));

	    	$data = $csv->getData($_FILES['bestkit_import']['tmp_name']);
	    	if (count($data) > 1) {
		    	unset($data[0]);
				Db::getInstance()->Execute('TRUNCATE TABLE `' . _DB_PREFIX_ . 'bestkit_shippingmatrix`');

		    	foreach ($data as $key => $line) {
			    	if (count($line) >= 11) {
				    	$rate = new BestkitShippingMatrix();
				    	$rate->id_carrier = $line[0];
				    	$rate->id_shop = $line[1];
				    	$rate->country_iso = 'FR';
				    	$rate->state = "*";
				    	$rate->city = " ";
				    	$rate->zip = $line[4];
				    	$rate->zip_to = $line[5];
				    	$rate->price_from = (float)$line[6];
				    	$rate->price_to = (float)$line[7];
				    	$rate->weight_from = (float)$line[8];
				    	$rate->weight_to = (float)$line[9];
				    	$rate->shipping_price = (float)$line[10];
				    	$rate->save();
			    	} else {
				    	$this->errors[] = 'Bad line #' . ($key + 1) . ' in the CSV file!';
			    	}
		    	}
		    	
		    	
	    	} else {
		    	$this->errors[] = 'Empty CSV file!';
	    	}
			return parent::postProcess();
    	}
    	else {
	    	if(isset($_GET['deletebestkit_shippingmatrix'])) {
		        $this->cleanGroups();
				return parent::postProcess();
	    	}
	    	else
	    	if(Tools::isSubmit('submitAddbestkit_shippingmatrix')) {
		    	if(count(Tools::getValue("checkBoxShopAsso_bestkit_shippingmatrix")) > 1) {
			    	$this->errors[] = 'Vous ne pouvez sélectionner plus d\'une boutique !';
		    	}
		    	else {
					parent::postProcess();
			        // Update group selection
			        $this->updateGroup(Tools::getValue('groupBox'));
		    	}
		    }
    	}

    }
}
