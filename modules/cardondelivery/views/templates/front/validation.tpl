{*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

{capture name=path}
	<a href="{$link->getPageLink('order', true, NULL, "step=3")|escape:'html':'UTF-8'}" rel="nofollow" title="">{l s='Commander' mod='cardondelivery'}</a><span class="navigation-pipe">{$navigationPipe}</span>{l s='Paiement par carte bancaire à la livraison' mod='cardondelivery'}
{/capture}

{include file="$tpl_dir./breadcrumb.tpl"}

<h2>{l s='Récapitulatif de la commande' mod='cardondelivery'}</h2>

{assign var='current_step' value='payment'}
{include file="$tpl_dir./order-steps.tpl"}

<h3>{l s='Paiement par carte bancaire à la livraison' mod='cardondelivery'}</h3>

<form action="{$link->getModuleLink('cardondelivery', 'validation', [], true)|escape:'html'}" method="post">
	<input type="hidden" name="confirm" value="1" />
	<p>
		<img src="{$this_path_cod}cardondelivery.jpg" alt="{l s='Paiement par carte bancaire à la livraison' mod='cardondelivery'}" style="float:left; margin: 0px 10px 5px 0px;" />
		{l s='Vous avez choisi de régler par carte bancaire à la livraison.' mod='cardondelivery'}
		<br/><br />
		{l s='Le montant total de votre commande est ' mod='cardondelivery'}
		<span id="amount_{$currencies.0.id_currency}" class="price">{convertPrice price=$total}</span>
		{if $use_taxes == 1}
		    {l s='(TTC)' mod='cardondelivery'}
		{/if}
	</p>
	<p>
		<br /><br />
		<br /><br />
		<b>{l s='Merci de confirmer votre commande en cliquant \'Je confirme ma commande\'.' mod='cardondelivery'}</b>
	</p>
	<p class="cart_navigation" id="cart_navigation">
		<a href="{$link->getPageLink('order', true)}?step=3" class="button_large">{l s='Autres moyens de paiement' mod='cardondelivery'}</a>
		<input type="submit" value="{l s='Je confirme ma commande' mod='cardondelivery'}" class="exclusive_large" />
	</p>
</form>
