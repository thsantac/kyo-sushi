<?php
/*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

if (!defined('_PS_VERSION_')) {
    exit;
}

use PrestaShop\PrestaShop\Core\Module\WidgetInterface;

class Llw_shopInfo extends Module implements WidgetInterface
{
    private $templates = array (
        'default' => 'llw_shopinfo.tpl',
    );

    public function __construct()
    {
        $this->name = 'llw_shopinfo';
        $this->author = 'La Ligne Web';
        $this->version = '1.0.0';

        $this->bootstrap = true;
        parent::__construct();

        $this->displayName = $this->getTranslator()->trans('Horaires boutique', array(), 'Modules.Contactinfo.Admin');
        $this->description = $this->getTranslator()->trans('Affichage heures d\'ouverture et de livraison.', array(), 'Modules.Contactinfo.Admin');
        $this->ps_versions_compliancy = array('min' => '1.7.2.0', 'max' => _PS_VERSION_);
    }

    public function install()
    {
        return parent::install()
            && $this->registerHook([
                'displayFooterInfos',
                'displayContactHours'
            ]);
    }

    public function renderWidget($hookName = null, array $configuration = [])
    {
        $template_file = $this->templates['default'];

        $this->smarty->assign($this->getWidgetVariables($hookName, $configuration));

		if($hookName == 'displayFooterInfos')
	        return $this->fetch('module:'.$this->name.'/'.$template_file);
	    else
	        return $this->fetch('module:'.$this->name.'/contact_hours.tpl');
    }

    public function getWidgetVariables($hookName = null, array $configuration = [])
    {
        return [
            'open_hours' => Configuration::get('LLW_SHOP_OPEN_HOURS_TEXT'),
            'delivery_hours' => Configuration::get('LLW_SHOP_DELIVERY_HOURS_TEXT'),
        ];
    }

    public function getContent()
    {
        $output = [];

        if (Tools::isSubmit('submitInfo')) {
            Configuration::updateValue('LLW_SHOP_OPEN_HOURS_TEXT', Tools::getValue('LLW_SHOP_OPEN_HOURS_TEXT'));
            Configuration::updateValue('LLW_SHOP_DELIVERY_HOURS_TEXT', Tools::getValue('LLW_SHOP_DELIVERY_HOURS_TEXT'));

            foreach ($this->templates as $template) {
                $this->_clearCache($template);
            }

            $output[] = $this->displayConfirmation($this->trans('Settings updated.', array(), 'Admin.Notifications.Success'));

            Tools::redirectAdmin($this->context->link->getAdminLink('AdminModules', true).'&conf=6&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name);
        }

        $helper = new HelperForm();
        $helper->submit_action = 'submitInfo';

        $fields = array(
            array(
                'type' => 'textarea',
                'cols' => 36,
                'rows' => 4,
                'label' 	=> $this->trans('Horaires d\'ouverture', array(), 'Modules.Mailalerts.Admin'),
                'name' 		=> 'LLW_SHOP_OPEN_HOURS_TEXT',
                'desc' 		=> '',
            ),
            array(
                'type' => 'textarea',
                'cols' => 36,
                'rows' => 4,
                'label' 	=> $this->trans('Horaires de livraison', array(), 'Modules.Mailalerts.Admin'),
                'name' 		=> 'LLW_SHOP_DELIVERY_HOURS_TEXT',
                'desc' 		=> '',
            ),
        );

        $helper->fields_value['LLW_SHOP_OPEN_HOURS_TEXT'] = Configuration::get('LLW_SHOP_OPEN_HOURS_TEXT');
        $helper->fields_value['LLW_SHOP_DELIVERY_HOURS_TEXT'] = Configuration::get('LLW_SHOP_DELIVERY_HOURS_TEXT');

        $output[] = $helper->generateForm(array(
            array(
                'form' => array(
                    'legend' => array(
                        'title' => $this->displayName,
                        'icon' => 'icon-clock-o'
                    ),
                    'input' => $fields,
                    'submit' => array(
                        'title' => $this->trans('Save', array(), 'Admin.Actions')
                    )
                )
            )
        ));

        return implode($output);
    }
}
