
{if class_exists('JsComposer') && Module::isInstalled('jscomposer') && Module::isEnabled('jscomposer') && JsComposer::condition()}

    {$jscomposer=Module::getInstanceByName('jscomposer')}
    {if $jscomposer instanceof JsComposer}        
        {$jscomposer->renderEditor()}        
    {/if}
{/if}


{$content}