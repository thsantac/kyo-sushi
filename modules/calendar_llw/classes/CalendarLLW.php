<?php
/**
 * 2007-2014 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 *         DISCLAIMER   *
 * *************************************** */
/* Do not edit or add to this file if you wish to upgrade Prestashop to newer
* versions in the future.
* ****************************************************
*
*  @author     LA-LIGNE-WEB.COM (contact@la-ligne-web.com)
*  @copyright  http://www.la-ligne-web.com
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

class CalendarLLW extends ObjectModel
{			            
    public $id;
    public $id_calendar;
    public $day_name;
    public $day;
    public $year;
    public $day_from;
    public $day_to;
    public $hours;
    public $hours_delivery;

    public static $definition = array(
        'table' 				=> 'calendar_llw',
        'primary' 				=> 'id_calendar',
        'multilang' 			=> false,
        'multishop' 			=> true,
        'fields' 				=> array(
            'day_name' 			=> array('type' => self::TYPE_STRING),
            'year' 				=> array('type' => self::TYPE_STRING),
            'day' 				=> array('type' => self::TYPE_STRING),
            'hours' 			=> array('type' => self::TYPE_STRING),
            'hours_delivery' 	=> array('type' => self::TYPE_STRING),
            'day_from'			=> array('type' => self::TYPE_STRING),
            'day_to'			=> array('type' => self::TYPE_STRING),
        ),
    );

    public function __construct($id_calendar = null, $id_lang = null, $id_shop = null)
    {
        parent::__construct($id_calendar, $id_lang, $id_shop);
    }

	public static function isFerie($day) {
		$context = Context::getContext();
		$id_shop = $context->shop->id;

		$sql = '
        	SELECT c.`id_calendar` 
        	FROM '._DB_PREFIX_.'calendar_llw c 
        	JOIN '._DB_PREFIX_.'calendar_llw_shop cs ON cs.`id_calendar` = c.`id_calendar` AND cs.`id_shop` = '.(int)$id_shop.' 
        	WHERE c.`day` = \''.pSQL($day).'\' OR (c.`day_from` <= \''.pSQL($day).'\' AND c.`day_to` >= \''.pSQL($day).'\')
            AND c.hours = "" AND c.hours_delivery = ""
        	';
        $id_calendar = Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue($sql);
        return (bool)$id_calendar;
	}

	public static function getIdByDay($day) {
		$context = Context::getContext();
		$id_shop = $context->shop->id;

        $query = new DbQuery();
        $query->select('c.id_calendar');
        $query->from('calendar_llw', 'c');
        $query->leftJoin('calendar_llw_shop', 'cls', 'cls.id_calendar = c.id_calendar AND cls.id_shop='.(int)$id_shop);
        $query->where('c.`day` = \''.pSQL($day).'\'');

        return Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue($query);
	}

	public static function getOpenHour($day) {

		$context = Context::getContext();
		$id_shop = $context->shop->id;

        $query = new DbQuery();
        $query->select('c.hours, c.hours_delivery');
        $query->from('calendar_llw', 'c');
        $query->leftJoin('calendar_llw_shop', 'cls', 'cls.id_calendar = c.id_calendar AND cls.id_shop='.(int)$id_shop);
        $query->where('c.`day` = \''.pSQL($day).'\'');

        return Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue($query);
	}
}

