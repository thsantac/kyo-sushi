<?php
/*
* 2007-2014 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2014 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

if (!defined('_PS_VERSION_'))
	exit;

class calendar_llw extends Module
{
	const PREFIX = 'llw';

	protected $_hooks = array();

    protected $_tabs = array(
        array(
            'class_name' => 'AdminCalendarLLW',
            'parent' => 'AdminParentPreferences',
            'name' => 'Jours fériés'
        ),
    );

	public function __construct()
	{
		$this->name = 'calendar_llw';
		$this->tab = 'front_office_features';
		$this->version = '1.0';
		$this->author = 'la-ligne-web.com';
		$this->module_key = '';
		$this->bootstrap = true;

		parent::__construct();

		$this->displayName = $this->l('Jours fériés');
		$this->description = $this->l('Ce module permet de définir les jours fériés et les horaires d\'ouverture.');
	}

	public static function getConfig($name, $value = null)
	{
		$result = false;
		if (array_key_exists($name, self::$_configs)) {
			$key = self::PREFIX . $name;
			if (is_null($value)) {
				$result = Configuration::get($key);
				if (is_array(self::$_configs[$name])) {
					$result = explode(',', $result);
				}
			} else {
				if (is_array(self::$_configs[$name])) {
					$value = implode(',', $value);
				} else {
					$value = pSQL($value);
				}

				$result = Configuration::updateValue($key, $value);
			}
		}
		
		return $result;
	}

    public function install()
    {
        // SQL
        $sqlQueries = [];
        $sqlQueries[] = ' CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'calendar_llw` (
			`id_calendar` int(11) unsigned NOT NULL AUTO_INCREMENT,
			`day_name` varchar(30) DEFAULT NULL,
			`day` varchar(10) DEFAULT NULL,
			`hours` varchar(254) DEFAULT NULL,
			`hours_delivery` varchar(254) DEFAULT NULL,
			`day_from` varchar(10) DEFAULT NULL,
			`day_to` varchar(10) DEFAULT NULL,
            PRIMARY KEY (`id_calendar`)
        ) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=UTF8;';
        $sqlQueries[] = 'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'calendar_llw_shop` (
			`id_calendar` int(11) unsigned NOT NULL AUTO_INCREMENT,
            `id_shop` int(10) unsigned NOT NULL,
            PRIMARY KEY (`id_calendar`, `id_shop`)
        ) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=UTF8;';


        foreach ($sqlQueries as $query) {
            if (Db::getInstance()->execute($query) == false) {
                return false;
            }
        }

        if (parent::install()
            && $this->installTab()
        ) {
            return true;
        }

        $this->_errors[] = $this->trans('Erreur lors de l\'installation.', [], 'Modules.calendar_llw.Admin');

        return false;
    }

    public function uninstall()
    {
        if (parent::uninstall()
            && $this->uninstallTab()
        ) {
            $sql = array();
			$sql[] = 'DROP TABLE IF EXISTS `' . _DB_PREFIX_ . 'calendar_llw`';
			$sql[] = 'DROP TABLE IF EXISTS `' . _DB_PREFIX_ . 'calendar_llw_shop`';

            foreach ($sql as $_sql) {
                Db::getInstance()->Execute($_sql);
            }
        }

        return TRUE;
    }

    public function installTab()
    {
        $tab = new Tab();
        $tab->active = 1;
        $tab->class_name = "AdminCalendarLLW";
        $tab->name = array();
        foreach (Language::getLanguages(true) as $lang) {
            $tab->name[$lang['id_lang']] = "Jours fériés";
        }
        $tab->id_parent = (int)Tab::getIdFromClassName('AdminParentShipping');
        $tab->module = $this->name;
        return $tab->add();
    }

    public function uninstallTab()
    {
        $id_tab = (int)Tab::getIdFromClassName('AdminCalendarLLW');
        $tab = new Tab($id_tab);
        return $tab->delete();
    }

	/**
	 * Load the configuration form
	 */
	public function getContent()
	{
		/**
		 * If values have been submitted in the form, process.
		 */
		
		if (Tools::getIsset($this->name)) {
			$this->_postProcess();
		}

		$this->bootstrap = true;
		return $this->renderForm();
	}

	/**
	 * Create the form that will be displayed in the configuration of your module.
	 */
	protected function renderForm()
	{
		$helper = new HelperForm();

		$helper->show_toolbar = false;
		$helper->module = $this;
		$helper->submit_action = $this->name;
		$helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false)
			.'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
		$helper->token = Tools::getAdminTokenLite('AdminModules');

		return $helper->generateForm(array($this->getConfigForm()));
	}

	/**
	 * Create the structure of your form.
	 */
	protected function getConfigForm()
	{
		$form = array(
		);

		return $form;
	}

	public function getConfigureUrl($conf = false)
	{
		return $this->context->link->getAdminLink('AdminModules')
			.'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name
			.($conf ? '&conf=4' : '');
	}

	/**
	 * Save form data.
	 */
	protected function _postProcess()
	{
		foreach (array_keys(self::$_configs) as $key) {
			self::getConfig($key, Tools::getValue($key));
		}
		
		Tools::redirectAdmin($this->getConfigureUrl(true));
	}
}
