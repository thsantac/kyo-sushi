<?php
/**
 * 2007-2013 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 *         DISCLAIMER   *
 * *************************************** */
 /* Do not edit or add to this file if you wish to upgrade Prestashop to newer
 * versions in the future.
 * ****************************************************
 *
 *  @author     LA-LIGNE-WEB.COM (contact@la-ligne-web.com)
 *  @copyright  http://www.la-ligne-web.com
 *  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 *  International Registered Trademark & Property of PrestaShop SA
 */

require_once (_PS_MODULE_DIR_ . 'calendar_llw/classes/CalendarLLW.php');

class AdminCalendarLLWController extends ModuleAdminController
{
    protected $_module = NULL;

	protected $position_identifier = 'id_calendar';
	
	protected $allow_export = false;

    public $id;
    public $id_calendar;

	private $_html = '';
  	private $_postErrors = array();
  
    public function __construct($id_calendar = null, $id_lang = null, $id_shop = null)
    {
    	$this->bootstrap = true;
        $this->context = Context::getContext();
        $this->table = 'calendar_llw';
        $this->identifier = 'id_calendar';
        $this->className = 'CalendarLLW';
        $this->lang = false;

		$this->id_calendar = $id_calendar;

		if (Shop::isFeatureActive())
			Shop::setContext(Shop::CONTEXT_ALL);

        $this->addRowAction('edit');
        $this->addRowAction('delete');

        parent::__construct($id_calendar, $id_lang, $id_shop);

		$this->fields_list = array(
			'id_calendar' => array('title' => $this->l('ID'), 'width' => 11),
			'day_name' => array('title' => $this->l('Nom du jour / période')),
			'day' => array('title' => $this->l("Jour"), 'type' => 'date', 'class' => 'fixed-width-lg'),
			'day_from' => array('title' => $this->l('Du'), 'type' => 'date', 'class' => 'fixed-width-lg'),
			'day_to' => array('title' => $this->l('Au'), 'type' => 'date', 'class' => 'fixed-width-lg'),
			'id_shop' => array('title' => $this->l('Boutique'), 'callback' => 'showShopName'),
			'hours' => array('title' => $this->l("Hres emport.")),
			'hours_delivery' => array('title' => $this->l("Hres livr.")),
		);

    }

    public function setMedia($isNewTheme = false)
    {
        parent::setMedia();
    }

	public function renderList()
	{
        $this->_select = 'cls.*';
        $this->_join = 'INNER JOIN `'._DB_PREFIX_.'calendar_llw_shop` cls ON a.id_calendar = cls.id_calendar';
        $this->_where = ' AND cls.id_shop='.$this->context->shop->id;
		return parent::renderList();
	}

    public function showShopName($id_shop, $tr)
    {
	    return Shop::getShopName($id_shop);
    }

    public function renderForm()
    {
        $this->display = 'edit';
        $this->initToolbar();
        if (!$this->loadObject(TRUE)) {
            return;
        }

        $this->fields_form = array(
            'tinymce' => false,
            'legend' => array(
                'title' => $this->l('Jour férié'),
                'icon' => 'icon-calendar'
            ),
            'input' => array(),
            'submit' => array('title' => $this->l('Enregistrer'))
        );

		unset($this->fields_list['id_calendar']);

        foreach ($this->fields_list as $key => $field) {
        	if ($key == 'id_shop') {
		        $this->fields_form['input'][] = array(
                        'type' => 'shop',
                        'label' => $this->l('Boutiques concernées'),
                        'name' => 'checkBoxShopAsso',
                );

		        $id 					= Tools::getValue('id_calendar');
				$obj 					= new CalendarLLW($id);
	        	continue;
            }

        	if ($key == 'hours') {
		        $this->fields_form['input'][] = array(
					'type' => 'text',
					'label' => $field['title'],
	                'required' => FALSE,
	                'lang' => false,
					'name' => $key,
                    'hint' => 'Exemple : 08:00-13:00',
                    'validation' => 'isOpenHours',
                );

	        	continue;
            }

        	if ($key == 'hours_delivery') {
		        $this->fields_form['input'][] = array(
					'type' => 'text',
					'label' => $field['title'],
	                'required' => FALSE,
	                'lang' => false,
					'name' => $key,
                    'hint' => 'Exemple : 08:00-13:00',
                    'validation' => 'isOpenHours',
                );

	        	continue;
            }

        	if ($key == 'day') {
		        $this->fields_form['input'][] = array(
					'type' => 'date',
					'label' => $field['title'],
	                'required' => FALSE,
	                'lang' => false,
					'name' => $key,
                    'hint' => 'Exemple : 25/12/2020',
                    'validation' => 'isDate',
                );

	        	continue;
            }

        	if ($key == 'day_from') {
		        $this->fields_form['input'][] = array(
					'type' => 'date',
					'label' => $field['title'],
	                'required' => false,
	                'lang' => false,
					'name' => $key,
                    'hint' => 'Exemple : 25/12/2020',
                    'validation' => 'isDate',
                );

	        	continue;
            }

        	if ($key == 'day_to') {
		        $this->fields_form['input'][] = array(
					'type' => 'date',
					'label' => $field['title'],
	                'required' => false,
	                'lang' => false,
					'name' => $key,
                    'hint' => 'Exemple : 31/12/2020',
                    'validation' => 'isDate',
                );

	        	continue;
            }

	        $this->fields_form['input'][] = array(
				'type' => 'text',
				'label' => $field['title'],
                'required' => TRUE,
                'lang' => false,
				'name' => $key,
            );
        }

        /*$day = $this->getFieldValue($obj, 'day');
        if (!empty($day)) {
            $day = substr($day, 2, 2)."/".substr($day, 0, 2);
        }*/

        $this->fields_value = array(
            'day_name' 			=> (isset($obj->day_name) 		? $obj->day_name : ""),
            'day' 				=> (isset($obj->day) 			? $obj->day : ""),
            'day_from'			=> (isset($obj->day_from)		? $obj->day_from : ""),
            'day_to'			=> (isset($obj->day_to)			? $obj->day_to : ""),
            'hours' 			=> (isset($obj->hours) 			? $obj->hours : false),
            'hours_delivery' 	=> (isset($obj->hours_delivery) ? $obj->hours_delivery : false),
        );

        return parent::renderForm();
    }

	public function getContent()
	{
	  if (Tools::isSubmit('submitAddcalendar_llw'))
	  {
    	//if(count(Tools::getValue("checkBoxShopAsso_calendar_llw")) > 1) {
	    //	$this->_postErrors[] = 'Vous ne pouvez sélectionner plus d\'une boutique !';
    	//}
		if(Tools::getValue("day") && Tools::getValue("day_from") && Tools::getValue("day_to")) {
			$this->_postErrors[] = 'Veuillez saisir soit un jour férié, soit une période !';
		}
    	if(Tools::getValue("hours") && strlen(Tools::getValue("hours")) != 11 ) {
	    	$this->_postErrors[] = 'Veuillez saisir les horaires d\'ouverture à emporter sous la forme hh-mn:hh-mn !';
    	}
    	if(Tools::getValue("hours_delivery") && strlen(Tools::getValue("hours_delivery")) != 11 ) {
	    	$this->_postErrors[] = 'Veuillez saisir les horaires d\'ouverture de livraison sous la forme hh-mn:hh-mn !';
    	}

	    if (sizeof($this->_postErrors))
	    {
	      foreach ($this->_postErrors AS $err)
	      {
	        $this->_html .= '<div class="alert error">'.$err.'</div>';
	      }
	    }
	  }
	  $this->_displayForm();
	  return $this->_html;
	}

    public function postProcess()
    {
    	if(isset($_GET['deletecalendar_llw'])) {
			return parent::postProcess();
    	}
    	else
    	if(Tools::isSubmit('submitAddcalendar_llw')) {

			if(!Tools::getValue("year")) {
				$_POST['year']	= substr($_POST['day'], 0, 4);
			}

	    	if(count(Tools::getValue("checkBoxShopAsso_calendar_llw")) > 1) {
		    	$this->errors[] 		= 'Vous ne pouvez sélectionner plus d\'une boutique !';
	    	}
	    	else {
				parent::postProcess();
	    	}
	    }
		//die("---postProcess");
    }
}
