{*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
<div class="shop-selector">
	<div class="btn-group">
		<button type="button" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" id="dropdownShopSelector">
			<div class="left">
				<span class="label">Vous êtes actuellement sur :</span>
				<span class="shop_name"><span class="shop_label">La boutique</span> {$current_shop.name}</span>
				<span class="shop_address">{$current_shop.store.address1} - {$current_shop.store.postcode} {$current_shop.store.city}</span> 
				<span class="hidden-md-up">{hook h='displayTopContact'}</span>
			</div>
			{if $canChangeShop}
			<span class="caret withToolTip" title="Voir les autres boutiques"><i class="fa fa-angle-down animate__animated animate__slideDown"></i></span>
			<span class="location withToolTip" title="Voir la carte"><img src="{$urls.img_url}picto_location_gold.png" id="headerCurrentLocator" class="animate__animated animate__pulse" alt="Locator" /></span>
			{/if}
		</button>
	</div>
</div>
<script type="text/javascript">
	{ldelim}var shopLat = {$current_shop.store.latitude}; {rdelim}	
	{ldelim}var shopLong = {$current_shop.store.longitude}; {rdelim}
	{ldelim}var shopName = "{$current_shop.store.name}"; {rdelim}
	{ldelim}var shopAddress = "{$current_shop.store.address1} {$current_shop.store.postcode} {$current_shop.store.city}"; {rdelim}
</script>
