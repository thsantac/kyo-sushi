{*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
<div id="shopSelectorList">
	<div class="container">
		<ul class="dropdown-menu">
			{foreach from=$shops item=shop}
			<li class="col-xs-6 col-md-3">
				<a href="https://{$shop.url}">
					<div class="shop-item{if $shop.id_shop == $current_shop.id_shop} current{/if}">
						<span class="shop_name"><span class="shop_label">{$shop.name}</span></span>
						<span class="shop_address">{$shop.store.address1} - {$shop.store.postcode} {$shop.store.city}</span> 
					</div>
				</a>
			</li>
			{/foreach}
		</ul>
	</div>
</div>