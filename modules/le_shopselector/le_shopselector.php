<?php
/*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

if (!defined('_PS_VERSION_')) {
    exit;
}

use PrestaShop\PrestaShop\Core\Module\WidgetInterface;

class Le_Shopselector extends Module implements WidgetInterface
{
    private $templateFile;

    public function __construct()
    {
        $this->name = 'le_shopselector';
        $this->author = 'Lueur Externe';
        $this->version = '1.0.0';
        $this->need_instance = 0;

        parent::__construct();

        $this->displayName = $this->trans('Shop selector block', array(), 'Modules.Languageselector.Admin');
        $this->description = $this->trans('Ajoute un bloc permettant à vos clients de sélectionner une boutique.', array(), 'Modules.Languageselector.Admin');

        $this->ps_versions_compliancy = array('min' => '1.7.1.0', 'max' => _PS_VERSION_);

        $this->templateFile = 'module:le_shopselector/views/template/hook/le_shopselector.tpl';
    }

    public function renderWidget($hookName = null, array $params = [])
    {
	    if($hookName=="displayNav1") {
	        $shops = Shop::getShops();
	
	        if (1 < count($shops)) {
	            $this->smarty->assign($this->getWidgetVariables($hookName, $params));
				$this->templateFile = 'module:le_shopselector/views/template/hook/le_shopselector.tpl';
	
	            return $this->fetch($this->templateFile);
	        }
		}

	    if($hookName=="displayShopList") {
	        $shops = Shop::getShops();
	
	        if (1 < count($shops)) {
	            $this->smarty->assign($this->getWidgetVariables($hookName, $params));
				$this->templateFile = 'module:le_shopselector/views/template/hook/le_shopselector_list.tpl';
	
	            return $this->fetch($this->templateFile);
	        }
		}

	    if($hookName=="displayTopContact") {
            $this->smarty->assign($this->getWidgetVariables($hookName, $params));
			$this->templateFile = 'module:le_shopselector/views/template/hook/le_shopphone.tpl';
            return $this->fetch($this->templateFile);
		}
		
        return false;
    }

    public function getWidgetVariables($hookName = null, array $params = [])
    {
	    if($hookName=="displayNav1") {
	        $shops = Shop::getShops(true);
	
	        foreach ($shops as &$shop) {
	            $shop['url'] = ShopUrl::getMainShopDomain($shop['id_shop']);
	            $shop['store'] = Store::getStoreAddress($shop['id_shop']);
	            if($shop['id_shop']==$this->context->shop->id) {
		            $current_store = $shop['store'];
	            }
	        }
	
	        return array(
	            'shops' => $shops,
	            'current_shop' => array(
	                'id_shop' => $this->context->shop->id,
	                'name' => $this->context->shop->name,
	                'store' => $current_store
	            ),
	            'canChangeShop' => $params['canChangeShop']
	        );
	    }

	    if($hookName=="displayTopContact") {
	        $shops = Shop::getShops(true);

	        foreach ($shops as $shop) {
	            $store = Store::getStoreAddress($shop['id_shop']);
	            if($shop['id_shop']==$this->context->shop->id) {
		            $current_phone = $store['phone'];
	            }
	        }
	
	        return array(
	            'shop_phone' => $current_phone
	        );
	    }

    }
}
