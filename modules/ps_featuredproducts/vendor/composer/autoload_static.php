<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit15568113e2f99e349096d24fd506c905
{
    public static $classMap = array (
        'Ps_FeaturedProducts' => __DIR__ . '/../..' . '/ps_featuredproducts.php',
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->classMap = ComposerStaticInit15568113e2f99e349096d24fd506c905::$classMap;

        }, null, ClassLoader::class);
    }
}
