<?php
/**
 * Page Cache Ultimate, Page Cache standard and Speed pack are powered by Jpresta (jpresta . com)
 *
 * @author    Jpresta
 * @copyright Jpresta
 * @license   You are just allowed to modify this copy for your own use. You must not redistribute it. License
 *               is permitted for one Prestashop instance only but you can install it on your test instances.
 */

include_once(dirname(__FILE__) . '/../../pagecache.php');

class pagecacheCacheWarmerModuleFrontController extends ModuleFrontController
{
    public function initContent()
    {
        parent::initContent();

        if (Module::isEnabled("pagecache")) {
            $token = Tools::getValue('token', '');
            $goodToken = Configuration::get('pagecache_cron_token');
            if (!$goodToken || strcmp($goodToken, $token) === 0) {
                $action = Tools::getValue('action');
                if ($action && $action === 'GetShopInfos') {
                    self::processGetShopInfos(Tools::getValue('shopId'));
                }
            } else {
                header("HTTP/1.0 403 Bad token");
                die('Bad token ' . $token);
            }
        } else {
            header("HTTP/1.0 503 Module not enabled");
            die('Module not enabled');
        }

        header("HTTP/1.0 404 Not found");
        die('Not found');
    }

    private function processGetShopInfos($shopId)
    {

        $shop = Shop::getShop((int)$shopId);
        $link = new Link();

        if (!$shop) {
            header("HTTP/1.0 404 Shop not found");
            die('Shop not found #' . $shopId);
        }

        $shop = new Shop($shopId);

        // All enabled languages for this shop
        $languages = array();
        foreach (Language::getLanguages(true, $shopId) as $language) {
            $languages[$language['id_lang']] = $language['iso_code'];
        }

        // All enabled currencies for this shop
        $currencies = array();
        foreach (Currency::getCurrenciesByIdShop($shopId) as $currency) {
            $currencies[$currency['id_currency']] = $currency['iso_code'];
        }

        // Take all fake users already created (don't generate all combinations)
        $groups = array();
        // TODO check if customer are shared among shops
        $sql = 'SELECT * FROM `' . _DB_PREFIX_ . 'customer`
            WHERE `id_shop` =' . (int)$shopId .
            ' AND `firstname` = \'fake-user-for-pagecache\'';
        $fakeUsers = DB::getInstance()->executeS($sql);
        foreach ($fakeUsers as $fakeUser) {
            $groups[] = $fakeUser['email'];
        }

        // Take default country and the 4 most used countries
        $countries = array();
        $defaultCountryIso = Country::getIsoById((int)Configuration::get('PS_COUNTRY_DEFAULT'));
        $countries[(int)Configuration::get('PS_COUNTRY_DEFAULT')] = $defaultCountryIso;
        $mostUsedCountriesRows = PageCacheDAO::getMostUsedCountries(4);
        foreach ($mostUsedCountriesRows as $mostUsedCountriesRow) {
            $countries[(int) $mostUsedCountriesRow['id_country']] = Country::getIsoById((int) $mostUsedCountriesRow['id_country']);
        }

        // Take the 4 most used country/state/zipcode for taxes
        $taxes = array();
        $mostUsedCountriesRows = PageCacheDAO::getMostUsedCountryStateZipcode(4);
        foreach ($mostUsedCountriesRows as $mostUsedCszRow) {
            $taxes[] = $mostUsedCszRow['tax_csz'];
        }

        // Take the 2 most used specifics
        $specifics = array();
        $mostUsedSpecificsRows = PageCacheDAO::getMostUsedSpecifics(2);
        foreach ($mostUsedSpecificsRows as $mostUsedSpecificsRow) {
            $specifics[] = $mostUsedSpecificsRow['specifics'];
        }

        $devices = array('desktop');
        if ((int) Configuration::get('pagecache_depend_on_device_auto')) {
            $devices[] = 'mobile';
        }

        ob_end_clean();
        header('Content-Type: application/json');
        $jsonOptions = JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_NUMERIC_CHECK;
        if (Tools::getIsset('pretty')) {
            $jsonOptions |= JSON_PRETTY_PRINT;
        }
        die(json_encode([
            'moduleVersion' => $this->module->version,
            'prestashopVersion' => _PS_VERSION_,
            'shopId' => $shopId,
            'shopName' => $shop->name,
            'shopBaseUrl' => $link->getBaseLink($shopId),
            'languages' => $languages,
            'currencies' => $currencies,
            'groups' => $groups,
            'countries' => $countries,
            'taxes' => $taxes,
            'specifics' => $specifics,
            'devices' => $devices,
            'urls' => $this->getShopUrls($shopId, self::getMultiplicator($currencies, $groups, $countries, $taxes, $specifics, $devices))
        ], $jsonOptions));
    }

    /**
     * @param $shopId integer Shop ID
     * @param $multiplator integer Number of contexts (currency, devices, groups, etc.)
     * @return array Array of URLs
     * @throws PrestaShopDatabaseException
     * @throws PrestaShopException
     */
    private function getShopUrls($shopId, $multiplator)
    {
        $link = new Link();
        $baseUrl = $link->getBaseLink($shopId);
        $urls = array();

        foreach (Language::getLanguages(true, $shopId) as $language) {
            //
            // GENERIC PAGES
            //
            if (Configuration::get('pagecache_index')) {
                self::addUrl(
                    $urls,
                    $link->getPageLink('index', null, $language['id_lang'], null, false, $shopId),
                    (int)Configuration::get('pagecache_index_timeout'),
                    $baseUrl, $multiplator
                );
            }
            if (Configuration::get('pagecache_newproducts')) {
                self::addUrl(
                    $urls,
                    $link->getPageLink('new-products', null, $language['id_lang'], null, false, $shopId),
                    (int)Configuration::get('pagecache_newproducts_timeout'),
                    $baseUrl, $multiplator
                );
            }
            if (Configuration::get('pagecache_pricesdrop')) {
                self::addUrl(
                    $urls,
                    $link->getPageLink('prices-drop', null, $language['id_lang'], null, false, $shopId),
                    (int)Configuration::get('pagecache_pricesdrop_timeout'),
                    $baseUrl, $multiplator
                );
            }
            if (Configuration::get('pagecache_contact')) {
                self::addUrl(
                    $urls,
                    $link->getPageLink('contact', null, $language['id_lang'], null, false, $shopId),
                    (int)Configuration::get('pagecache_contact_timeout'),
                    $baseUrl, $multiplator
                );
            }
            if (Configuration::get('pagecache_sitemap')) {
                self::addUrl(
                    $urls,
                    $link->getPageLink('sitemap', null, $language['id_lang'], null, false, $shopId),
                    (int)Configuration::get('pagecache_sitemap_timeout'),
                    $baseUrl, $multiplator
                );
            }
            if (Configuration::get('PS_DISPLAY_BEST_SELLERS') && (int)Configuration::get('pagecache_bestsales')) {
                self::addUrl(
                    $urls,
                    $link->getPageLink('best-sales', null, $language['id_lang'], null, false, $shopId),
                    (int)Configuration::get('pagecache_bestsales_timeout'),
                    $baseUrl, $multiplator
                );
            }
            if (Configuration::get('PS_DISPLAY_SUPPLIERS')) {
                //
                // MANUFACTURERS
                //
                if (Configuration::get('pagecache_manufacturer')) {
                    // List of manufacturers
                    self::addUrl(
                        $urls,
                        $link->getPageLink('manufacturer', null, $language['id_lang'], null, false, $shopId),
                        (int)Configuration::get('pagecache_manufacturer_timeout'),
                        $baseUrl, $multiplator
                    );
                    // Each manufacturers
                    $sql = 'SELECT c.id_manufacturer
                    FROM `' . _DB_PREFIX_ . 'manufacturer` c' . Shop::addSqlAssociation('manufacturer', 'c') . '
                    WHERE c.`active` = 1 AND manufacturer_shop.id_shop = ' . (int) $shopId;
                    $id_manufacturer_rows = DB::getInstance()->executeS($sql);
                    foreach ($id_manufacturer_rows as $id_manufacturer_row) {
                        self::addUrl(
                            $urls,
                            $link->getManufacturerLink((int) $id_manufacturer_row['id_manufacturer'], null, $language['id_lang'], $shopId),
                            (int)Configuration::get('pagecache_manufacturer_timeout'),
                            $baseUrl, $multiplator);
                    }
                }
                //
                // SUPPLIERS
                //
                if (Configuration::get('pagecache_supplier')) {
                    // List of suppliers
                    self::addUrl(
                        $urls,
                        $link->getPageLink('supplier', null, $language['id_lang'], null, false, $shopId),
                        (int)Configuration::get('pagecache_supplier_timeout'),
                        $baseUrl, $multiplator
                    );
                    // Each suppliers
                    $sql = 'SELECT c.id_supplier
                    FROM `' . _DB_PREFIX_ . 'supplier` c' . Shop::addSqlAssociation('supplier', 'c') . '
                    WHERE c.`active` = 1 AND supplier_shop.id_shop = ' . (int) $shopId;
                    $id_supplier_rows = DB::getInstance()->executeS($sql);
                    foreach ($id_supplier_rows as $id_supplier_row) {
                        self::addUrl(
                            $urls,
                            $link->getSupplierLink((int) $id_supplier_row['id_supplier'], null, $language['id_lang'], $shopId),
                            (int)Configuration::get('pagecache_supplier_timeout'),
                            $baseUrl, $multiplator);
                    }
                }
            }
            //
            // PRODUCTS
            //
            if (Configuration::get('pagecache_product')) {
                $sql = 'SELECT p.id_product 
                    FROM `' . _DB_PREFIX_ . 'product` p' . Shop::addSqlAssociation('product', 'p') . '
                    WHERE product_shop.`active` = 1 AND product_shop.id_shop = ' . (int) $shopId;
                $id_product_rows = DB::getInstance()->executeS($sql);
                foreach ($id_product_rows as $id_product_row) {
                    // Check that product is not customizable
                    $customizationFieldCount = (int) DB::getInstance()->getValue('SELECT COUNT(*) FROM `'._DB_PREFIX_.'customization_field` WHERE `id_product` = '.(int)$id_product_row['id_product']);
                    if ($customizationFieldCount) {
                        // Skip this product
                        continue;
                    }

                    // Simple product (even products with combinations have a simple URL)
                    self::addUrl(
                        $urls,
                        $link->getProductLink((int) $id_product_row['id_product'], null, null, null, $language['id_lang'], $shopId),
                        (int)Configuration::get('pagecache_product_timeout'),
                        $baseUrl, $multiplator);

                    // Check if it is a product with combinations
                    $sql = 'SELECT pa.id_product_attribute
                    FROM `' . _DB_PREFIX_ . 'product_attribute` pa' . Shop::addSqlAssociation('product_attribute', 'pa') . '
                    WHERE product_attribute_shop.id_shop = ' . (int) $shopId . '
                        AND pa.id_product = ' . (int) $id_product_row['id_product'];
                    $ipa_rows = DB::getInstance()->executeS($sql);
                    if ($ipa_rows && count($ipa_rows) > 0) {
                        // Product with combinations
                        foreach ($ipa_rows as $ipa_row) {
                            // Add URL for all combinations
                            self::addUrl(
                                $urls,
                                $link->getProductLink((int) $id_product_row['id_product'], null, null, null, $language['id_lang'], $shopId, $ipa_row['id_product_attribute']),
                                (int)Configuration::get('pagecache_product_timeout'),
                                $baseUrl, $multiplator);
                        }
                    }
                }
            }
            //
            // CATEGORIES
            //
            if (Configuration::get('pagecache_category')) {
                $sql = 'SELECT c.id_category
                    FROM `' . _DB_PREFIX_ . 'category` c' . Shop::addSqlAssociation('category', 'c') . '
                    WHERE c.`active` = 1 AND c.id_parent > 0 AND category_shop.id_shop = ' . (int) $shopId;
                $id_category_rows = DB::getInstance()->executeS($sql);
                foreach ($id_category_rows as $id_category_row) {
                    self::addUrl(
                        $urls,
                        $link->getCategoryLink((int) $id_category_row['id_category'], null, $language['id_lang'], null, $shopId),
                        (int)Configuration::get('pagecache_category_timeout'),
                        $baseUrl, $multiplator);
                }
            }
            //
            // CMS
            //
            if (Configuration::get('pagecache_cms')) {
                $sql = 'SELECT c.id_cms
                    FROM `' . _DB_PREFIX_ . 'cms` c' . Shop::addSqlAssociation('cms', 'c') . '
                    WHERE c.`active` = 1 AND cms_shop.id_shop = ' . (int)$shopId;
                $id_cms_rows = DB::getInstance()->executeS($sql);
                foreach ($id_cms_rows as $id_cms_row) {
                    self::addUrl(
                        $urls,
                        $link->getCMSLink((int)$id_cms_row['id_cms'], null, null, $language['id_lang'], $shopId),
                        (int)Configuration::get('pagecache_cms_timeout'),
                        $baseUrl, $multiplator);
                }

                //
                // CMS CATEGORIES
                //
                $sql = 'SELECT c.id_cms_category
                    FROM `' . _DB_PREFIX_ . 'cms_category` c' . Shop::addSqlAssociation('cms_category', 'c') . '
                    WHERE c.`active` = 1 AND cms_category_shop.id_shop = ' . (int)$shopId;
                $id_cms_category_rows = DB::getInstance()->executeS($sql);
                foreach ($id_cms_category_rows as $id_cms_category_row) {
                    self::addUrl(
                        $urls,
                        $link->getCMSCategoryLink((int)$id_cms_category_row['id_cms_category'], null, $language['id_lang'], $shopId),
                        (int)Configuration::get('pagecache_cms_timeout'),
                        $baseUrl, $multiplator);
                }
            }
        }
        return $urls;
    }

    private static function getMultiplicator($currencies, $groups, $countries, $taxes, $specifics, $devices) {
        // Compute the number of URLs to warm-up
        $multiplicator = max(1, count($currencies));
        $multiplicator *= max(1, count($countries));
        $multiplicator *= max(1, count($devices));
        $multiplicator *= max(1, count($groups) + 1);
        $multiplicator *= max(1, count($taxes) + 1);
        $multiplicator *= max(1, count($specifics) + 1);
        return $multiplicator;
    }

    /**
     * @param $urls array Array of URLs to feed
     * @param $url string URL to add
     * @param $timeout_minutes integer Maximum timeout in minutes
     * @param $baseUrl string Base URL of the shop
     * @param $multiplator integer Number of contexts (currency, devices, groups, etc.)
     */
    private function addUrl(&$urls, $url, $timeout_minutes, $baseUrl, $multiplator) {
        $url_no_anchor = strtok($url, "#");
        // Replace spaces if needed
        $stats = PageCacheDAO::getStatsByUrl(self::normalizeUrl('', $url_no_anchor));
        if ($stats['count'] == 0) {
            $urls[] = array(
                'u' => self::normalizeUrl($baseUrl, $url),
                't' => 0,
                'p' => 1000,
            );
        }
        else {
            $ttl = max(0, $timeout_minutes - $stats['max_age_minutes']);
            if ($ttl < (24 * 60) || $stats['count'] < $multiplator) {
                // Do not add URL if minimum time to live is more than 24H AND if number of cached URL is greater or
                // equal to the multiplicator. It does not garanty that all context of this URL are cached: it's a guess.
                $urls[] = array(
                    'u' => self::normalizeUrl($baseUrl, $url),
                    't' => $ttl,
                    'p' => $stats['sum_hit'],
                );
            }
        }
    }

    private static function normalizeUrl($baseUrl, $url) {
        $normalizedUrl = str_replace($baseUrl, '', $url);
        // Sometimes, URL contains spaces :-(
        $normalizedUrl = str_replace(' ', '%20', $normalizedUrl);
        return $normalizedUrl;
    }
}
