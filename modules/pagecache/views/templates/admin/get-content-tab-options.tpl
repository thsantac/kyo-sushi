{*
* Page Cache Ultimate, Page Cache standard and Speed pack are powered by Jpresta (jpresta . com)
* 
*    @author    Jpresta
*    @copyright Jpresta
*    @license   You are just allowed to modify this copy for your own use. You must not redistribute it. License
*               is permitted for one Prestashop instance only but you can install it on your test instances.
*}
<div class="panel">
<h3>{if $avec_bootstrap}<i class="icon-gear"></i>{else}<img width="16" height="16" src="../img/admin/AdminPreferences.gif" alt=""/>{/if}&nbsp;{l s='Options' mod='pagecache'}</h3>
<form id="pagecache_form_options" action="{$request_uri|escape:'html':'UTF-8'}" method="post">
    <input type="hidden" name="submitModule" value="true"/>
    <input type="hidden" name="pctab" value="options"/>
    <fieldset>
        <div style="clear: both;">
            <div class="form-group">
                <div id="pagecache_skiplogged">
                    <label class="control-label col-lg-3">
                        {l s='No cache for logged in users' mod='pagecache'}
                    </label>
                    <div class="col-lg-9">
                        <span class="switch prestashop-switch fixed-width-lg">
                            <input type="radio" name="pagecache_skiplogged" id="pagecache_skiplogged_on" value="1" {if $pagecache_skiplogged}checked{/if}>
                            <label for="pagecache_skiplogged_on" class="radioCheck">{l s='Yes' mod='pagecache'}</label>
                            <input type="radio" name="pagecache_skiplogged" id="pagecache_skiplogged_off" value="0" {if !$pagecache_skiplogged}checked{/if}>
                            <label for="pagecache_skiplogged_off" class="radioCheck">{l s='No' mod='pagecache'}</label>
                            <a class="slide-button btn"></a>
                        </span>
                    </div>
                    <div class="col-lg-9 col-lg-offset-3">
                        <div class="help-block">
                            {l s='Disable cache for visitors that are logged in' mod='pagecache'}
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div id="pagecache_normalize_urls">
                    <label class="control-label col-lg-3">
                        {l s='Normalize URLs' mod='pagecache'}
                    </label>
                    <div class="col-lg-9">
                        <span class="switch prestashop-switch fixed-width-lg">
                            <input type="radio" name="pagecache_normalize_urls" id="pagecache_normalize_urls_on" value="1" {if $pagecache_normalize_urls}checked{/if}>
                            <label for="pagecache_normalize_urls_on" class="radioCheck">{l s='Yes' mod='pagecache'}</label>
                            <input type="radio" name="pagecache_normalize_urls" id="pagecache_normalize_urls_off" value="0" {if !$pagecache_normalize_urls}checked{/if}>
                            <label for="pagecache_normalize_urls_off" class="radioCheck">{l s='No' mod='pagecache'}</label>
                            <a class="slide-button btn"></a>
                        </span>
                    </div>
                    <div class="col-lg-9 col-lg-offset-3">
                        <div class="help-block">
                            {l s='Avoid same page linked with different URLs to use different cache. Should only be disabled when you have a lot of links in a page (> 500).' mod='pagecache'}
                        </div>
                    </div>
                </div>
            </div>


            <div class="form-group">
                <div id="pagecache_logs_debug">
                    <label class="control-label col-lg-3">
                        {l s='Enable logs' mod='pagecache'}
                    </label>
                    <div class="col-lg-9">
                        <span class="switch prestashop-switch fixed-width-lg">
                            <input type="radio" name="pagecache_logs" id="pagecache_logs_debug_2" value="2" {if $pagecache_logs > 0}checked{/if}>
                            <label for="pagecache_logs_debug_2" class="radioCheck">{l s='Yes' mod='pagecache'}</label>
                            {*<input type="radio" name="pagecache_logs" id="pagecache_logs_debug_1" value="1" {if $pagecache_logs == 1}checked{/if}>
                            <label for="pagecache_logs_debug_1" class="radioCheck">{l s='Info' mod='pagecache'}</label>*}
                            <input type="radio" name="pagecache_logs" id="pagecache_logs_debug_0" value="0" {if $pagecache_logs == 0}checked{/if}>
                            <label for="pagecache_logs_debug_0" class="radioCheck">{l s='No' mod='pagecache'}</label>
                            <a class="slide-button btn"></a>
                        </span>
                    </div>
                    <div class="col-lg-9 col-lg-offset-3">
                        <div class="help-block">
                            {l s='Logs informations into the Prestashop logger. You should only enable it to debug or understand how the cache works.' mod='pagecache'}
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div id="pagecache_logs_debug">
                    <label class="control-label col-lg-3">
                        {l s='Ignored URL parameters' mod='pagecache'}
                    </label>
                    <div class="col-lg-9">
                        <input type="text" name="pagecache_ignored_params" id="pagecache_ignored_params" value="{$pagecache_ignored_params|escape:'html':'UTF-8'}" size="100">
                    </div>
                    <div class="col-lg-9 col-lg-offset-3">
                        <div class="help-block">
                            {l s='URL parameters are used to identify a unique page content. Some URL parameters do not affect page content like tracking parameters for analytics (utm_source, utm_campaign, etc.) so we can ignore them. You can set a comma separated list of these parameters here.' mod='pagecache'}
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div id="pagecache_always_infosbox">
                    <label class="control-label col-lg-3">
                        {l s='Always display infos box' mod='pagecache'}
                    </label>
                    <div class="col-lg-9">
                        <span class="switch prestashop-switch fixed-width-lg">
                            <input type="radio" name="pagecache_always_infosbox" id="pagecache_always_infosbox_on" value="1" {if $pagecache_always_infosbox}checked{/if}>
                            <label for="pagecache_always_infosbox_on" class="radioCheck">{l s='Yes' mod='pagecache'}</label>
                            <input type="radio" name="pagecache_always_infosbox" id="pagecache_always_infosbox_off" value="0" {if !$pagecache_always_infosbox}checked{/if}>
                            <label for="pagecache_always_infosbox_off" class="radioCheck">{l s='No' mod='pagecache'}</label>
                            <a class="slide-button btn"></a>
                        </span>
                    </div>
                    <div class="col-lg-9 col-lg-offset-3">
                        <div class="help-block">
                            {l s='Only used for demo' mod='pagecache'}
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div id="pagecache_depend_on_device_auto">
                    <label class="control-label col-lg-3">
                        {l s='Create separate cache for desktop and mobile' mod='pagecache'}
                    </label>
                    <div class="col-lg-9">
                        <span class="switch prestashop-switch fixed-width-lg">
                            <input type="radio" name="pagecache_depend_on_device_auto" id="pagecache_depend_on_device_auto_on" value="1" {if $pagecache_depend_on_device_auto}checked{/if}>
                            <label for="pagecache_depend_on_device_auto_on" class="radioCheck">{l s='Yes' mod='pagecache'}</label>
                            <input type="radio" name="pagecache_depend_on_device_auto" id="pagecache_depend_on_device_auto_off" value="0" {if !$pagecache_depend_on_device_auto}checked{/if}>
                            <label for="pagecache_depend_on_device_auto_off" class="radioCheck">{l s='No' mod='pagecache'}</label>
                            <a class="slide-button btn"></a>
                        </span>
                    </div>
                    <div class="col-lg-9 col-lg-offset-3">
                        <div class="help-block">
                            {l s='If you know that your mobile version is the same as the desktop version then you can disable this option' mod='pagecache'}
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div id="pagecache_exec_header_hook">
                    <label class="control-label col-lg-3">
                        {l s='Executes "header" hook in dynamic modules request' mod='pagecache'}
                    </label>
                    <div class="col-lg-9">
                        <span class="switch prestashop-switch fixed-width-lg">
                            <input type="radio" name="pagecache_exec_header_hook" id="pagecache_exec_header_hook_on" value="1" {if $pagecache_exec_header_hook}checked{/if}>
                            <label for="pagecache_exec_header_hook_on" class="radioCheck">{l s='Yes' mod='pagecache'}</label>
                            <input type="radio" name="pagecache_exec_header_hook" id="pagecache_exec_header_hook_off" value="0" {if !$pagecache_exec_header_hook}checked{/if}>
                            <label for="pagecache_exec_header_hook_off" class="radioCheck">{l s='No' mod='pagecache'}</label>
                            <a class="slide-button btn"></a>
                        </span>
                    </div>
                    <div class="col-lg-9 col-lg-offset-3">
                        <div class="help-block">
                            {l s='If checked, the header hook will be executed so javascript variables added in this hook by other modules will be refreshed' mod='pagecache'}
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div id="pagecache_product_refreshEveryX">
                    <label class="control-label col-lg-3">
                        {l s='Refresh product page every X sales' mod='pagecache'}
                    </label>
                    <div class="col-lg-9">
                        {l s='Every' mod='pagecache'}
                        <select style="display: inline-block; width: fit-content;" name="pagecache_product_refreshEveryX" class="form-control">
                            <option value="1" {if $pagecache_product_refreshEveryX == 1} selected{/if}>1</option>
                            <option value="5" {if $pagecache_product_refreshEveryX == 5} selected{/if}>5</option>
                            <option value="10" {if $pagecache_product_refreshEveryX == 10} selected{/if}>10</option>
                            <option value="50" {if $pagecache_product_refreshEveryX == 50} selected{/if}>50</option>
                            <option value="100" {if $pagecache_product_refreshEveryX == 100} selected{/if}>100</option>
                        </select>
                        {l s='sales' mod='pagecache'}
                    </div>
                    <div class="col-lg-9 col-lg-offset-3">
                        <div class="help-block">
                            {l s='When stock is not displayed on product page then you can set how often the cache of the product page should be refreshed when the quantity is greater than the quantity that displays a "last items..."' mod='pagecache'}
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div id="pagecache_key_tax">
                    <label class="control-label col-lg-3">
                        {l s='Cache key for taxes' mod='pagecache'}
                    </label>
                    <div class="col-lg-9">
                        {l s='Country' mod='pagecache'}
                        <span class="switch prestashop-switch fixed-width-lg">
                            <input type="radio" name="pagecache_key_tax_country" id="pagecache_key_tax_country_on" value="1" {if $pagecache_key_tax_country}checked{/if}>
                            <label for="pagecache_key_tax_country_on" class="radioCheck">{l s='Yes' mod='pagecache'}</label>
                            <input type="radio" name="pagecache_key_tax_country" id="pagecache_key_tax_country_off" value="0" {if !$pagecache_key_tax_country}checked{/if}>
                            <label for="pagecache_key_tax_country_off" class="radioCheck">{l s='No' mod='pagecache'}</label>
                            <a class="slide-button btn"></a>
                        </span>

                        {l s='State' mod='pagecache'}
                        <span class="switch prestashop-switch fixed-width-lg">
                            <input type="radio" name="pagecache_key_tax_state" id="pagecache_key_tax_state_on" value="1" {if $pagecache_key_tax_state}checked{/if}>
                            <label for="pagecache_key_tax_state_on" class="radioCheck">{l s='Yes' mod='pagecache'}</label>
                            <input type="radio" name="pagecache_key_tax_state" id="pagecache_key_tax_state_off" value="0" {if !$pagecache_key_tax_state}checked{/if}>
                            <label for="pagecache_key_tax_state_off" class="radioCheck">{l s='No' mod='pagecache'}</label>
                            <a class="slide-button btn"></a>
                        </span>

                        {l s='Zip code' mod='pagecache'}
                        <span class="switch prestashop-switch fixed-width-lg">
                            <input type="radio" name="pagecache_key_tax_postcode" id="pagecache_key_tax_postcode_on" value="1" {if $pagecache_key_tax_postcode}checked{/if}>
                            <label for="pagecache_key_tax_postcode_on" class="radioCheck">{l s='Yes' mod='pagecache'}</label>
                            <input type="radio" name="pagecache_key_tax_postcode" id="pagecache_key_tax_postcode_off" value="0" {if !$pagecache_key_tax_postcode}checked{/if}>
                            <label for="pagecache_key_tax_postcode_off" class="radioCheck">{l s='No' mod='pagecache'}</label>
                            <a class="slide-button btn"></a>
                        </span>
                    </div>
                    <div class="col-lg-9 col-lg-offset-3">
                        <div class="help-block">
                            {l s='Check here if a country, a state or a zipcode can change the amount of taxes. This is normally automatically set at installation but you can force value if you know what you are doing.' mod='pagecache'}
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="bootstrap">
            <button type="submit" value="1" id="submitModuleOptions" name="submitModuleOptions" class="btn btn-default pull-right">
                <i class="process-icon-save"></i> {l s='Save' mod='pagecache'}
            </button>
        </div>
    </fieldset>
</form>
</div>