{*
* Page Cache Ultimate, Page Cache standard and Speed pack are powered by Jpresta (jpresta . com)
*
*    @author    Jpresta
*    @copyright Jpresta
*    @license   You are just allowed to modify this copy for your own use. You must not redistribute it. License
*               is permitted for one Prestashop instance only but you can install it on your test instances.
*}
<script type="text/javascript">
function addNoCacheParam() {
    let links = document.querySelectorAll("a");
    for (let i = 0, len = links.length; i < len; i++) {
        let e = links[i].href;
        let n = "_pcnocache=" + (new Date().getTime());
        let r = (typeof baseDir !== 'undefined' ? baseDir : prestashop.urls.base_url).replace("https", "http");
        if (typeof e != "undefined" && e.substr(0, 1) != "#" && (e.replace("https", "http").substr(0, r.length) == r || e.indexOf('://') == -1) && e.indexOf('javascript:') == -1) {
            if (e.indexOf('?') >= 0) links[i].href += '_' + n;
            else links[i].href += '?' + n;
        }
    }
}
setTimeout('addNoCacheParam();', 200);
</script>
