# Changelogs #
## TODOs
- Enable cache for modules like blog, layered navigation, etc.
- Allow to skip configurations steps in advanced mode
- Add possibility to move the info box on the screen
- Detect wrong overrides done with Core class (with index_class.php)
- Avoid customizable products in speed analysis
- Use https://devdocs.prestashop.com/1.7/modules/concepts/commands/ ?

## Versions ##
### 7.0.2 ###
- Fix version number
### 7.0.1 ###
- Page Cache: Improve JPresta Cache Warmer service informations
- Page Cache: Preserve token when resetting the module
- Page Cache: Ignored parameters are now case insensitive
### 7.0.0 ###
- Page Cache: Possibility to subscribe to JPresta Cache Warmer service
### 6.5.4 ###
- DB cleaner: Fix table optimization
### 6.5.3 ###
- Page Cache: Compatibility with autolanguagecurrency module
### 6.5.2 ###
- Page Cache: Fix issue in isCustomerWithSpecificPricesOrPermissions()
### 6.5.1 ###
- WEBP: Override Module::getCacheId() so modules with internal cache can handle '.webp' extension
- WEBP: Force image ratio in configuration
### 6.5.2 ###
- Page Cache: Fix issue in isCustomerWithSpecificPricesOrPermissions()
### 6.5.1 ###
- WEBP: Override Module::getCacheId() so modules with internal cache can handle '.webp' extension
- WEBP: Force image ratio in configuration
### 6.5.0 ###
- WEBP: New option to make it compatible with Cloudflare and servers that do not handle HTTP header "Vary: Accept".
- WEBP: Fix image ratio in configuration
- Page Cache: Fix validator warnings
- Page Cache: Make the dynamic request safer
### 6.4.6 ###
- WEBP: Delete WEBP image when it is modified
### 6.4.5 ###
- Page Cache: Compatibility with last version of autolanguagecurrency module and PS >= 1.7.4
### 6.4.4 ###
- WEBP: Respect image ratio and real size in image quality comparison slider 
- Page Cache: Fix bug 'Undefined index: url_crc32'
### 6.4.3 ###
- Fix multiple entries in menu (bis)
- Page Cache: Don't refresh dynamic modules for bots
- Page Cache: Use text/html content-type for dynamic requests so the content is compressed by most configurations
- Page Cache: Compatibility with Amazon Pay module by patworx
### 6.4.2 ###
- Fix multiple entries in menu
### 6.4.1 ###
- Page Cache: Disable tokens automatically
- Page Cache: Compatibility with apcpopup module by Idnovate
### 6.4.0 ###
- Page Cache: New advanced feature to browse the cache
- Page Cache: Add CSS and JS version in cache key
### 6.3.18 ###
- Page Cache: Fix (bis) cache refreshment when a product becomes available for order
### 6.3.17 ###
- Page Cache: Fix cache refreshment when a product becomes available for order
- Page Cache: Fix an error when uploading images with StoreCommander
### 6.3.16 ###
- Fix Addons validator warnings
### 6.3.15 ###
- WEBP: Security fix (sanitize image path)
### 6.3.14 ###
- Page Cache: Handle array parameters in URLs
- Page Cache: Detect modification of the selected behavior when a product is out of stock 
### 6.3.13 ###
- Page Cache: add confirmation on auto-conf button when there is already a configuration
- Page Cache: Fix an issue with automatic detection of language
- WEBP: Add vary header for Plesk users (and people using nginx as a proxy)
- Page Cache: Fix pagecache_product_refresh_every_x length name
- Page Cache: Fix a warning in PHP logs
### 6.3.12 ###
- Lazy loading: Fix an issue when template contains special chars
### 6.3.11 ###
- Page Cache: Compatibility with shaim_gdpr module by Dominik Shaim
- Lazy loading: small fix for URL of loader image
### 6.3.10 ###
- Page Cache: Fix cache refreshment when using shared stocks on multistore 
### 6.3.9 ###
- WEBP: Fix issue with vendor directory for CSS and JS
- Page Cache: Disable cache for customers having specific prices (native and with groupinc module)
- Page Cache: Disable cache for employees so superuser module by MassonVincent can work
- Page Cache: Compatibility with atssuperuser module by ATSinfosystem Sotwares
- WEBP: Make sure permissions are correctly set on binaries
- Lazy loading: Improve installation for multi-store
- Lazy loading: Don't set height of images (conflict with some themes)
### 6.3.8 ###
- Page Cache: Fix price reduction for groupinc module developped by Idnovate
- Page Cache: Fix errors occuring for few modules set as dynamic in product list
### 6.3.7 ###
- Page Cache: Fix 'logged' redirection
### 6.3.6 ###
- WEBP: Enable WEBP for all images when using media servers pointing to same server
- DB cleaner: Add recommentations and forbid 0 day for abandonned carts and logs.
- Page Cache: Fix 'is_logged' state in cache
### 6.3.5 ###
- Page Cache: Fix bug with low_stock_threshold for Prestashop < 1.7.3.0
- Page Cache: compatibility with webpgenerator module by PrestaChamps
- Page Cache: Fix case of class JprestaCustomer
### 6.3.4 ###
- Page Cache: Fix build 6.3.3
- Page Cache: Cache management for zonehomeblocks module
### 6.3.3 ###
- Page Cache: Handle customer with extra required fields
- Page Cache: compatibility with deluxecookies module by innovadeluxe
### 6.3.2 ###
- Page Cache: Compatibility with last version of autolanguagecurrency module
- Page Cache: Module key for Addons
### 6.3.1 ###
- Page Cache: Improvment of localization to determine taxes and country of the visitor
### 6.2.2 ###
- WEBP: Fix htaccess for special chars in URLs of images
- WEBP: Improve 'Vary' header detection
- Page Cache: Fix an issue for some modules set as dynamic using id_product_attribute
### 6.2.1 ###
- WEBP: Test if the HTTP header 'Vary' is correctly added
- WEBP: Add cache directive for webp images
- WEBP: Fix double slash // in URL of the converter
### 6.2.0 ###
- Page Cache: Fix a bug with HTMLPRO module
- Page Cache: Make automatic refreshment more accurate, specially on stock modifications
### 6.1.1 ###
- Forbid modifictions in DEMO mode
### 6.1.0 ###
- WEBP: Don't load Prestashop engine when converting images: it's faster and avoid max_user_connections errors when a lot of images are converted.
- WEBP: Fix enable/disable of WEBP feature
- Lazy loading: fix directory creation for child theme
- Page Cache: Fix illimited timeout for memcache and memcached servers
- Page Cache: Catch javascript error in dynamic modules so it does not block the remaining code to execute
### 6.0.7 ###
- Page Cache: Optimize specific prices refreshment
- Validation on Addons
- Block install if Page Cache is already installed
### 6.0.6 ###
- Lazy loading: new parser for templates, better handle errors, add logs
- Page Cache: Make IDs thread safe
### 6.0.5 ###
- Add Prestashop Addons key
### 6.0.4 ###
- A new way to anonymize the cache has been developped so it needs less overrides
- The configuration of the module is now available in menu "Advanced parameters"
- First version of Speed Pack module (that shares this Readme file with Page Cache Ultimate)
    - Lazy loading of images
    - WEBP compression of images
    - Database cleaning and optimisation
### 5.2.0 ###
- Add an entry in Prestashop menu for the configuration of the module
- Fix compatibility with authorizedclientsonly module by PrestaHeroes
### 5.1.19 ###
- Handle widgets in ProductExtraContent
### 5.1.18 ###
- Fix an issue with dynamic modules request
- Improve BR and PT translations
### 5.1.17 ###
- Upgrade ajax detection
- Fix compatibility with PS < 1.6.0.4
- Add advanced option "Executes header hook in dynamic modules request"
### 5.1.16 ###
- Fix for compatibility with pm_advancedcookiebanner module by Presta-Module
### 5.1.15 ###
- Fix for compatibility with pm_advancedcookiebanner module by Presta-Module
### 5.1.14 ###
- Compatibility with pm_advancedcookiebanner module by Presta-Module with option "Enable module categorization"
### 5.1.12 ###
- Update Dispatcher override (resolves some 404 issues)
### 5.1.11 ###
- Fix a bug introduced in 5.1.8 (loose language selection and some other cookies) 
### 5.1.10 ###
- Compatibility with pm_advancedcookiebanner module by Presta-Module 
### 5.1.9 ###
- Fix upgrade script 5.1.8
- Fix validation warnings
### 5.1.8 ###
- Move some ModuleFrontController as ModuleAdminController as they should be
- Improve new products and home featured products refreshment
- Compatibility with ageverifyer module by Simon Agostini (need to uncomment a line)
- Compatibility with autolanguagecurrency module
- Improve PT translations
### 5.1.7 ###
- Avoid browser cache to be used in back office during performance analysis
### 5.1.6 ###
- Fix dynamic refreshment on pages that display all manufacturers, all suppliers, etc. (listing without filter) on PS 1.7
- Fix HTML in Hook.php for Addons
### 5.1.5 ###
- Fix a language selection issue in home page
### 5.1.4 ###
- Handle classes inherited from AbstractLazyArray (bug with estimateddelivery module)
### 5.1.3 ###
- Fix a bug with Media override (sometimes the parsing was failing)
- Compatibility with module validatevatnumber module by ActiveDesign
- Compatibility with MySQL 5.5
### 5.1.2 ###
- Fix Geolocalisation
- Spanish translation
- Fix a bug with isRestrictedCountry()
- Allow hook 'actionproductoutofstock' to be dynamic
### 5.1.1 ###
- The profiling feature to detect slow modules is only available from PS1.7
- Improve styles
### 5.1.0 ###
- New profiling feature to detect slow modules
- Logo updated
- French translations
### 5.0.11 ###
- Don't change value type in execHook
- Instanciate Link in context if not set
- Rename page_cache_dynamics_modules to page_cache_dynamics_mods for compatibility with prettyurls module
### 5.0.10 ###
- Fix a bug with PS 1.7.5.0 with Webservices
- Compatibility with new version of 'Deluxe Private Shop' module from Innovadeluxe
- Fix a bug in Page Cache Standard
### 5.0.9 ###
- Keep URL parameters in dynamic module request URL
- Fix a bug with Memcached
- Add header Access-Control-Allow-Origin for Speed Analysis tools
### 5.0.8 ###
- Fix a bug with PS 1.7.5.0 with ProductListingFrontController
### 5.0.7 ###
- Fix getCurrentUrl()
### 5.0.6 ###
- Compatibility with jQuery 3
- Upgrade Bootstrap Slider to version 10.6.0
- Do not display HTML comments around hook 'displayoverridetemplate'
### 5.0.5 ###
- Fix 5.0.4 released too fast :-(
### 5.0.4 ###
- Only display HTML comments around hooks starting with 'display'
### 5.0.3 ###
- Refresh all ProductExtraContent tabs when set as dynamic
- Add a filter on dynamic modules configuration
### 5.0.2 ###
- Report bug fixes from versions 4.58
- Better handle special cases in speed analysis tools
### 5.0.1 ###
- Report bug fixes from versions 4.55, 4.56, 4.57
- Disable form fields in dynamic modules tab when the module is in production mode
- Do not add HTML comment in pages that cannot be cached 
### 5.0.0 ###
- PS1.7 look and feel
- Group dynamic hook by modules, hide action/forbidden hooks 
- Add cache directive to ajax request to avoid PageSpeed warning (Specify a cache validator)
- Add the possibility to use Memcached server
- Fix parameters propagation for URL like mailto:, phone:, skype: etc.
- Add HTML comments in debug mode to identify modules faster
- Do not execute javascript in test mode when cache is disabled
- Disable speed test in maintenance mode
- Remove some overrides replaced by a generic one
- Do not display warning message for autoconf if first time
### 4.58 ###
- Call 'displayHeader' hook instead of 'header' in dynamic modules request
### 4.57 ###
- Fix a bug in default visitor group definition
- Fix a bug in speed analysis (thousands separator)
### 4.56 ###
- Fix a bug in getCache() function for multistores
### 4.55 ###
- Fix negative value in speed analysis
- Fix a bug with hook DisplayProductExtraContent added in 4.54
### 4.54 ###
- Fix a bug with hok DisplayProductExtraContent when then returned value is not a string (cannot be dynamic)
### 4.53 ###
- Fix a bug with 1.7.5.0 (file upload)
### 4.52 ###
- Fix a Javascript issue with Internet Explorer
- Compatibility with module "Deluxe Cookies Warning" by Innovadeluxe
- Fix URL of test mode that was wrong in some cases
### 4.51 ###
- Fix a bug in CRON URLs list
- Fix a bug in cache refreshment
### 4.50 ###
- Improvement of speed analysis to make it more precise
- Better refresh cache for packs of products
### 4.49 ###
- Compatibility with module "European Union Cookies Law" by MyPresta.eu
### 4.48 ###
- Compatibility with module "Customer Ratings and Reviews Pro + Google Rich Snippets" by Business Tech
### 4.47 ###
- Fix a bug introduced in 4.46
### 4.46 ###
- Handle DisplayProductExtraContent hook in PS1.7
- Fix for default group
### 4.45 ###
- Compatibility with module Knowband GDPR module by Knowband
- Fix Hook.php for a specific case
- Better refresh cache for module "IQIT Content Creator - Unique Homepage Generator"
### 4.44 ###
- Fix version number
### 4.43 ###
- Fix validation warnings
### 4.42 ###
- Cache management for new modules name
- Compatibility with module Age Verify module by Musaffar Patel
### 4.41 ###
- Handle 'false' and 'null' returned values in coreRenderWidget and coreCallHook (PS1.7)
### 4.40 ###
- Better compatibility with module GDPR - General Data Protection Regulation (by Active Design)
### 4.39 ###
- Fix v4.38 upgrade (for Hook)
### 4.38 ###
- Restrict dynamic request treatments to avoid conflicts
### 4.37 ###
- Compatibility with module systemina_employeefilter module by Systemina
- Fix returned value in coreRenderWidget (PS1.7)
- Add quantity_all_versions in product properties in dynamic requests
### 4.36 ###
- Improve widget_block management
- Fix a bug in dynamic modules refreshment (since 4.33)
### 4.35 ###
 (released failed)
### 4.34 ###
- Handle hook aliases
- Fix dynamic modules refreshment in products list pages (since 4.33)
- Fix a bug when PHP scripts are directly called without controller
### 4.33 ###
- Dynamic modules are now refreshed in a POST request
### 4.32 ###
- Fix language selection issue (since 4.27)
### 4.31 ###
- Fix a mobile detection problem (since 4.21)
- Fix a bug in pcnocache parameter propagation (when cache is disabled for logged in visitors)
### 4.30 ###
- Handle old theme
### 4.29 ###
- Fix a bug in widget_block management (since 4.24)
- Handle int/string/boolean attributes on hooks for dynamic modules
### 4.28 ###
- Fix a bug in upgrade 4.27
### 4.27 ###
- Compatibility with module GDPR Compliance Pro (by PrestaChamps) 
- Compatibility with module EU Cookie Advise + Cookies blocker + Iubenda (by Tanzo)
- Compatibility with module GDPR - General Data Protection Regulation (by Active Design)
- Compatibility with module EU Cookie Law (Notification Banner + Cookie Blocker) (by Línea Gráfica)
- Fix a bug for PS 1.7 since 4.25
- Fix a bug in dynamic refreshment for PS 1.7
### 4.26 ###
- Fix installation issues since 4.25
### 4.25 ###
- Allow dynamic modules on product list
### 4.24 ###
- Refresh javascript definition even when there is no dynamic module
- Handle widget_block smarty tag
- Fix translation encoding
### 4.23 ###
- Improve cache refreshment for multi-stores
- Complete translations
- Fix a bug (since 4.20) in hookActionProductUpdate
### 4.22 ###
- Fix property name 'pagecache_depend_on_device_auto' length
### 4.21 ###
- Fix a conflict with module "Go Reviews (by Lineven)" and maybe other modules using internal cache
- Fix a conflict with module "Theme editor (by SUNNYTOO.COM)" to display correct number of products per line
- Detects if desktop and mobile site are the same to use the same cache or not
- Clear the cache when CSS and JS cache are cleared 
### 4.20 ###
- Avoid dynamic modules request to be cached by browser
- Avoid dynamic modules request to be indexed by search engines
- Avoid testing inactive category in speed analysis
- Improve cache refreshment when a product is deleted/disabled
### 4.19 ###
- Enable Prestatrust
### 4.18 ###
- Improve performance analysis feature
- Fix restricted countries detection with geolocalisation by IP
- Fix "debug mode" parameter propagation
- Fix "no cache for logged in users" parameter propagation
- Handle the private shop feature of "Extended Registration for Prestashop" module by modulesmarket.com
- Handle 'widget' tag in templates for PS 1.7
### 4.17 ###
- Little fix on URL of test mode
- Don't cache redirected pages (improve detection)
- Fix a bug in diagnostic tab template (admin)
### 4.16 ###
- Improve cache refreshment for suppliers and manufacturers
- Handle bad JSON (skip characters before first '{')
### 4.15 ###
- Fix a bug in CRON job that was cleaning the whole cache when using '&index'
- Improve error message during installation
- Improve "Page timeouts" tab for PS 1.5
- Prepare for widgets management
### 4.14 ###
- Use class instead of id to identify dynamic block for PS 1.7 (smarty cache must be cleared)
- Fix a bug with SSL redirect detection
- Fix a bug with AdminTab
### 4.13 ###
- Take care of 'prestashop' javascript variable for PS 1.7
### 4.12 ###
- Assign smarty variables for dynamic modules for PS 1.7
### 4.11 ###
- Fix a bug in automatic refreshment when modifying images of products
### 4.10 ###
- Fix a bug in automatic refreshment for multistore
- Fix protocol in speed analysis
- Fix timeout too short for file_get_content in speed analysis
### 4.09 ###
- Fix a retro-compatibility issue with old hooks
- Swedish translation improvement
### 4.08 ###
- Use base 64 encoded images for front stats to not degrade YSlow or PageSpeed score
- Improve speed analysis
- Fix CRON task
### 4.07 ###
- Fix some log trace in ZipArchive cache system
### 4.06 ###
- Add details on log when directory cannot be created
### 4.05 ###
- Do not add CSS file in production mode
- Update spanish and swedish translations
- Remove Media.php override which is obsolete
- Add possibility to ignore backlinks from HTML parts (specific developpement)
### 4.04 ###
- Compatibility with PHP 5.3
- Move all HTML into templates files
- Fix a bug with 1.5
- Improve automatic refreshment
### 4.02 ###
- Module key for standard edition
### 4.01 ###
- Compatibility with HHVM
- Compatibility with 'Deluxe Private Shop' module from Innovadeluxe
- Fix an issue with translations
- Fix a cache issue in speed analysis
- Update of the swedish translation
### 4.00 ###
- Compatibility with Prestashop 1.7
- Add auto-configuration system
- Add speed test on home, product and category pages
- Fix debug URL for multistores
- Add prefix to CSS rules to avoid conflict in infos box
- Reduce directory creations
- Improve algorithms to be faster
- Ultimate edition: add caching system 'ZIP archives'
- Ultimate edition: add diagnosic feature to help you to configure your prestashop for speed
- Media override replaced by hook 'actionAdminPerformanceControllerAfter'
- Add header 'application/json' on Ajax requests to avoid search engines to index it
- Fix: Preserve baseDir and baseUrl on ajax request
### 3.17 ###
- Fix cache refreshment when modifying specific prices
### 3.16 ###
- Avoid loosing the session when redirected to HTTPS protocole
### 3.15 ###
- Fix a conflict with PopExit module
### 3.14 ###
- Add hooks for advanced stock management
- Add a workaround for a bug in LiteSpeed server
### 3.12 ###
- Fix permissions of files and directories in '/cache/pagecache' directory
### 3.11 ###
- Update Media override to avoid the cache to be cleared to many times
### 3.10 ###
- Fix images URL in info box
- Update cccCss prototype to latest one
### 3.09 ###
- Better check parameters for security reasons (avoid XSS)
### 3.08 ###
- Remove module's parameters in production mode to avoid them to be referenced in search engines
### 3.07 ###
- Improvement: do not insert DIV around dynamic modules if current page will not be cached
- Fix: native customization is not a module and therefore cannot be refreshed. The workaround is to disable cache for these products.
### 3.06 ###
- Merge footer and header hook for better compatibility
- Fix an SQL problem in hookActionFeature*
### 3.05 ###
- Fix problem with restricted countries
- Fix the issue that was allowing the display of infos box even in production mode
### 3.04 ###
- Refresh cache when CSS or JS have been changed
### 3.03 ###
- Now PageCache will not fail on PHP command line call
### 3.02 ###
- Improve CSS to avoid style problems
- Add workaround in case of bad datas in cart rules
### 3.01 ###
- Fix CSS styles for Prestashop 1.5
- Fix cache refreshment for single language stores
### 3.00 ###
- Improve infos box
- Redesign of configuration page
- Step by step configuration feature
### 2.71 ###
- Fix specific price detection (problem of seconds)
### 2.70 ###
- Fix country detection (when detected with browser settings)
### 2.69 ###
- Handle countries with multiple names (Array)
### 2.68 ###
- Handle modules that have controller named like standard controllers
### 2.67 ###
- Fix issue with some overrides with version >= 1.6.1.0
### 2.66 ###
- Improve dynamic modules feature to avoid CSS issues
- Fix groups detection with Advanced Top Menu module
### 2.65 ###
- Fix default group detection
### 2.64 ###
- Fix SQL query in hookActionProductAttributeUpdate()
- Fix the check of overrides activation
### 2.63 ###
- Add ';' to fix javascript
- Prepare for Prestashop v1.6.1
### 2.62 ###
- Fix ProductController hook call
### 2.61 ###
- Update Hook override
- Fix javascript error
### 2.60 ###
- Fix 'logged out' after SSL redirection
- Fix CSS in back office
### 2.59 ###
- Fix maintenance detection
### 2.58 ###
- Drop useless column modules
### 2.57 ###
- Fix javascript order.
### 2.56 ###
- Fix carriage return in front controllers overrides
### 2.55 ###
- Fix warning with 1.6.0.1
### 2.54 ###
- Fix Workaround for a bug in Prestashop (multiple same JS script when CCC is activated)
- Compatibility with 1.6.0.1
- Fix a problem with 1.6.0.14
- Cast variable for addons validation
### 2.53 ###
- Check for Express Cache installation
- Improve speed and robustness of dynamic modules display
- Fix URL with '|' character
### 2.52 ###
- Fix clear cache for multistore
### 2.51 ###
- Add Readme.md, move js, css and img to views directory
- Avoid bug PSCSX-4773
- Check bug PSCSX-4794
### 2.50 ###
- Improve mobile version
- Fix products comparison
- Fix missing modules when clearing cache
### 2.49 ###
- Fix PSCSX-4507 (clear cache of null product id)
### 2.48 ###
- Remove useless StoresController override
- Avoid "Invalid argument supplied for foreach()" warning
- It's now possible to delete specific pages with CRON URL (controller=comma separated list of ID)
- Now handle country when it's based on users's address (delivery or invoice)
- Fix a bug when geolocalisation is enabled
### 2.47 ###
- Handle maintenance mode
- Fix problem with blockwishlist (and more generally with Media::addJsDef feature)
### 2.46 ###
- Fix recursive problem in Hook
- Improve forwarding of dbgpagecache parameter
### 2.45 ###
- Handle cookie encryption
- Execute javascript even if there is no dynamic modules (to refresh cart)
### 2.44 ###
- New functionnality to let dynamic modules bloc empty in cached pages
### 2.43 ###
- Add option to disable cache for logged in users
### 2.42 ###
- Handle cookies set by dynamic modules
- Fix jquery-cooki(e)-plugins insertion
### 2.41 ###
- Fix CRC32 column value
### 2.40 ###
- Workaround for PSCFV-10168 (before 1.5.6.0)
- Fix jquery-cookie insertion
### 2.39 ###
- Reduce database size by using CRC32 instead of MD5 hashes
- Reduce database size by storing modules's id instead of modules's name
- Reduce backlinks count
- Forward dbgpagecache parameter so testing is easier, no need to add it on every page
- Controls known compatibility issues
- Improve infos block: real cache type is displayed (no cache, server cache or browser cache)
- Move CSS and JS into pagecache.css and pagecache.js
### 2.38 ###
- Fix a bug with advanced configuration (javascript executed after dynamic modules)
- Fix a bug with Prestashop <= 1.6.0.7 (PS_JS_DEFER)
### 2.37 ###
- Fix a bug with multi-store
- German translation
### 2.36 ###
- Fix Prestashop addons valitator issues
- Optimize isLogged() override
- Handle access denied pages for dynamic modules
- Do not block store if module is not well uninstalled
- Improve uninstallation (removeOverride)
### 2.35 ###
- Prevent AJAX requests to be cached
### 2.34 ###
- Fix delpagecache feature
- Fix product update refreshment (on price)
### 2.33 ###
- Make browser cache private
### 2.32 ###
- Replace intval() by cast
- Fix browser cache min value
### 2.31 ###
- Add a delpagecache parameter to force the cache be reffreshed
- Add link to documentation
- A workaround in case overrides have not been well uninstalled
### 2.30 ###
- Fix country detection
### 2.29 ###
- Handle enabled modules per device 
### 2.28 ###
- Fix cookie problem when changing language (found with ajaxfilter module)
### 2.27 ###
- Workaround for a bug in Prestashop (multiple same JS script when CCC is activated)
### 2.26 ###
- Now compatible with Duplicate URL Redirect
### 2.25 ###
- Fix for $context->link not being initialized
### 2.24 ###
- Handle specific price changes (for flash sales modules)
### 2.23 ###
- Add 1.6.0.5 compatibility
### 2.22 ###
- Fix a problem with Mailalerts
### 2.21 ###
- Fix a problem with BlockWishlist
- Avoid javascript error if the user click before the cart is refreshed.
### 2.20 ###
- Use of Module::isEnabled('pagecache')
### 2.19 ###
- Handle specific prices feature
- Handle module restrictions on groups
- Fix problem with URL fragments
### 2.18 ###
- Fix for user groups dependency
### 2.17 ###
- Replace _GET and _POST by Tools::getValue()
- Fix a bug that will improve cache statistics
### 2.16 ###
- Add CRON URL
### 2.15 ###
- Add user groups dependency
### 2.14 ###
- Add compatibility with ClearURL module
- Add compatibility with 1.4 themes
### 2.13 ###
- Fix auto detect language feature in cookie
### 2.12 ###
- Multiple shop compatibility
### 2.11 ###
- Ignore ad tracking parameters to be more efficient
- Add log levels to debug faster
- Add link to addons forum
### 2.10 ###
- Add possibility to add javascript to be called after dynamic module have been displayed (to solve specific theme issues)
### 2.9 ###
- Disable pagecache footer in debug mode and when it's not needed
### 2.8 ###
- Improve cache refreshment speed
### 2.7 ###
- Add browser cache feature
- Add possibility to disable statistics and save some more milliseconds
- Check product quantity to know how we should refresh the cache on order validation
- Fix a bug with _PS_BASE_URL_ which is not defined in some cases
### 2.6 ###
- SQL optimisations
### 2.5 ###
- Fix random number at bottom of cached pages
### 2.4 ###
- Fix HTTPs issue
- Fix mobile version
### 2.3 ###
- Add a debug mode to be able to test on production site
- Add logs possibilities
- Fix currency selection
### 2.2 ###
- Fix a "white page" issue when module is disabled
### 2.1 ###
- Fix logout feature
### 2.0 ###
- Hit statistics to know how the cache is efficient
- Improve cache management by deleting cache files only when necessary (you can modify how cache is refreshed)
- Now compatible with CloudCache (do not override same methods anymore)
- Save cache files in multiple directories to avoid filesystems to slow down
- Enable auto-update so you can update your module from your backoffice
- New logo
- Fix an issue with prices-drop, new-products and best-sales controllers
- Fix an issue with default empty cookie name which is now a random name and expires immediately
### 1.7 ###
- Add default configuration for dynamic modules and a button to go back to this default configuration
### 1.6 ###
- Hook class updated to merge with version 1.5.4.1
- Disable cache and display an error if tokens are enabled
### 1.5 ###
- Keep 'id_lang' cookie to be compatible with some modules

