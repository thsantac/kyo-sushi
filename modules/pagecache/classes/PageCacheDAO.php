<?php
/**
 * Page Cache Ultimate, Page Cache standard and Speed pack are powered by Jpresta (jpresta . com)
 *
 *    @author    Jpresta
 *    @copyright Jpresta
 *    @license   You are just allowed to modify this copy for your own use. You must not redistribute it. License
 *               is permitted for one Prestashop instance only but you can install it on your test instances.
 */

class PageCacheDAO
{
    /**
     * Not using InnoDb will disable foreign key constraint and cascade delete
     */
    const MYSQL_ENGINE = 'InnoDb';

    const TABLE = 'jm_pagecache';
    const TABLE_DETAILS = 'jm_pagecache_details';
    const TABLE_BACKLINK = 'jm_pagecache_bl';
    const TABLE_MODULE = 'jm_pagecache_mods';

    // This table is created to store state of the cache refreshment concerning a specific price
    const TABLE_SPECIFIC_PRICES = 'jm_pagecache_sp';

    // Store date for profiling
    const TABLE_PROFILING = 'jm_pagecache_prof';

    /**
     * @throws PrestaShopDatabaseException
     */
    public static function createTables()
    {
        $db = Db::getInstance();

        JprestaUtils::dbExecuteSQL('CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.self::TABLE_DETAILS.'`(
            `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
            `details` VARCHAR(2000) NOT NULL,
            PRIMARY KEY (`id`),
            INDEX (`details`)
            ) ENGINE='.PageCacheDAO::MYSQL_ENGINE.' DEFAULT CHARSET=utf8', true, true);


        JprestaUtils::dbExecuteSQL('CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.self::TABLE.'`(
            `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
            `id_shop` TINYINT UNSIGNED NOT NULL DEFAULT 1,
            `cache_key` INT UNSIGNED NOT NULL,
            `url` VARCHAR(1000) NOT NULL,
            `controller` VARCHAR(15) NOT NULL,
            `id_object` MEDIUMINT UNSIGNED,
            `id_currency` INT(10) UNSIGNED,
            `id_lang` INT(10) UNSIGNED,
            `id_fake_customer` INT(10) UNSIGNED DEFAULT NULL,
            `id_device` TINYINT(1) UNSIGNED,
            `id_country` INT(10) UNSIGNED DEFAULT NULL,
            `mask_country` BINARY(4) DEFAULT NULL,
            `id_tax_csz` INT(11) UNSIGNED DEFAULT NULL,
            `id_specifics` INT(11) UNSIGNED DEFAULT NULL,
            `v_css` SMALLINT UNSIGNED DEFAULT NULL,
            `v_js` SMALLINT UNSIGNED DEFAULT NULL,
            `count_missed` MEDIUMINT UNSIGNED NOT NULL DEFAULT 0,
            `count_hit` MEDIUMINT UNSIGNED NOT NULL DEFAULT 0,
            `last_gen` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
            `deleted` TINYINT UNSIGNED NOT NULL DEFAULT 0,
            PRIMARY KEY (`id`),
            KEY (`controller`,`id_object`),
            KEY (`id_shop`),
            KEY (`id_country`),
            KEY (`last_gen`),
            KEY (`deleted`),
            KEY (`url`),
            KEY (`v_css`),
            KEY (`v_js`),
            UNIQUE (`cache_key`),
            FOREIGN KEY (id_tax_csz) REFERENCES '._DB_PREFIX_.self::TABLE_DETAILS.'(id) ON DELETE RESTRICT,
            FOREIGN KEY (id_specifics) REFERENCES '._DB_PREFIX_.self::TABLE_DETAILS.'(id) ON DELETE RESTRICT
            ) ENGINE='.PageCacheDAO::MYSQL_ENGINE.' DEFAULT CHARSET=utf8', true, true);

        JprestaUtils::dbExecuteSQL('CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.self::TABLE_BACKLINK.'`(
            `id` int(11) UNSIGNED NOT NULL,
            `backlink_key` INT UNSIGNED NOT NULL,
            KEY (`id`),
            KEY (`backlink_key`),
            FOREIGN KEY (id) REFERENCES '._DB_PREFIX_.self::TABLE.'(id) ON DELETE CASCADE
            ) ENGINE='.PageCacheDAO::MYSQL_ENGINE.' DEFAULT CHARSET=utf8', true, true);

        JprestaUtils::dbExecuteSQL('CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.self::TABLE_MODULE.'`(
            `id` int(11) UNSIGNED NOT NULL,
            `id_module` int(11) UNSIGNED NOT NULL,
            PRIMARY KEY (`id`,`id_module`),
            KEY (`id_module`),
            FOREIGN KEY (id) REFERENCES '._DB_PREFIX_.self::TABLE.'(id) ON DELETE CASCADE
            ) ENGINE='.PageCacheDAO::MYSQL_ENGINE.' DEFAULT CHARSET=utf8', true, true);

        JprestaUtils::dbExecuteSQL('CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.self::TABLE_SPECIFIC_PRICES.'`(
            `id_specific_price` int(10) unsigned NOT NULL,
            `id_product` int(10) unsigned NOT NULL,
            `date_from` datetime,
            `date_to` datetime,
            PRIMARY KEY (`id_specific_price`),
            KEY `idxfrom` (`date_from`),
            KEY `idxto` (`date_to`)
            ) ENGINE='.PageCacheDAO::MYSQL_ENGINE.' DEFAULT CHARSET=utf8', true, true);

        JprestaUtils::dbExecuteSQL('CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.self::TABLE_PROFILING.'`(
            `id_profiling` int(10) unsigned NOT NULL AUTO_INCREMENT,
            `id_module` int(10) unsigned NOT NULL,
            `description` varchar(255) NOT NULL,
            `date_exec` timestamp DEFAULT CURRENT_TIMESTAMP,
            `duration_ms` mediumint unsigned NOT NULL,
            PRIMARY KEY (`id_profiling`)
            ) ENGINE='.PageCacheDAO::MYSQL_ENGINE.' DEFAULT CHARSET=utf8', true, true);

        // Feed TABLE_SPECIFIC_PRICES to trigger cache reffreshment when a reduction starts or ends
        $now = date('Y-m-d H:i:00');
        $inserts = 'INSERT INTO `'._DB_PREFIX_.self::TABLE_SPECIFIC_PRICES.'` (`id_specific_price`,`id_product`,`date_from`,`date_to`) VALUES ';
        $select_existing = 'SELECT * FROM `'._DB_PREFIX_.'specific_price` WHERE `from`>\''.pSQL($now).'\' OR `to`>\''.pSQL($now).'\'';
        $rows = $db->executeS($select_existing);
        $index = 0;
        if (JprestaUtils::isIterable($rows)) {
            foreach ($rows as $row) {
                $inserts .= '('.(int)$row['id_specific_price'].','.(int)$row['id_product'].',\''.pSQL($row['from']).'\',\''.pSQL($row['to']).'\')';
                $index++;
                if ($index < count($rows)) {
                    $inserts .= ',';
                }
            }
        }
        if ($index > 0) {
            JprestaUtils::dbExecuteSQL($inserts . ';', true, true);
        }
    }

    public static function optimizeHash2_39()
    {
        // Make it reentrant, try to delete before creating
        try {
            Db::getInstance()->execute('ALTER TABLE `'._DB_PREFIX_.self::TABLE.'` DROP COLUMN `url_crc32`;');
        } catch (PrestaShopDatabaseException $e) {
            // Just ignore it
        }
        try {
            Db::getInstance()->execute('ALTER TABLE `'._DB_PREFIX_.self::TABLE_BACKLINK.'` DROP COLUMN `backlink_key`;');
        } catch (PrestaShopDatabaseException $e) {
            // Just ignore it
        }
        try {
            Db::getInstance()->execute('ALTER TABLE `'._DB_PREFIX_.self::TABLE_MODULE.'` DROP COLUMN `id_module`;');
        } catch (PrestaShopDatabaseException $e) {
            // Just ignore it
        }

        // Creates new columns
        $result = Db::getInstance()->execute('ALTER TABLE `'._DB_PREFIX_.self::TABLE.'` ADD `url_crc32` INT NOT NULL;');
        $result = $result && Db::getInstance()->execute('CREATE UNIQUE INDEX `url_crc32` ON `'._DB_PREFIX_.self::TABLE.'` (`url_crc32`);');

        $result = $result && Db::getInstance()->execute('ALTER TABLE `'._DB_PREFIX_.self::TABLE_BACKLINK.'` ADD `backlink_crc32` INT NOT NULL;');
        $result = $result && Db::getInstance()->execute('CREATE INDEX `backlink_crc32` ON `'._DB_PREFIX_.self::TABLE_BACKLINK.'` (`backlink_crc32`);');

        $result = $result && Db::getInstance()->execute('ALTER TABLE `'._DB_PREFIX_.self::TABLE_MODULE.'` ADD `id_module` int(11) UNSIGNED NOT NULL;');
        $result = $result && Db::getInstance()->execute('CREATE INDEX `id_module` ON `'._DB_PREFIX_.self::TABLE_MODULE.'` (`id_module`);');
        try {
            Db::getInstance()->execute('ALTER TABLE `'._DB_PREFIX_.self::TABLE_MODULE.'` DROP PRIMARY KEY;');
        } catch (PrestaShopDatabaseException $e) {
            // Just ignore it
        }
        $result = $result && Db::getInstance()->execute('ALTER TABLE `'._DB_PREFIX_.self::TABLE_MODULE.'` ADD PRIMARY KEY (`id`,`id_module`);');

        // Delete old columns.
        // Be tolerent, do not check result here since it will not block the module
        try {
            Db::getInstance()->execute('ALTER TABLE `'._DB_PREFIX_.self::TABLE.'` DROP COLUMN `url_hash`;');
        } catch (PrestaShopDatabaseException $e) {
            // Just ignore it
        }
        try {
            Db::getInstance()->execute('ALTER TABLE `'._DB_PREFIX_.self::TABLE_BACKLINK.'` DROP COLUMN `backlink_hash`;');
        } catch (PrestaShopDatabaseException $e) {
            // Just ignore it
        }
        try {
            Db::getInstance()->execute('ALTER TABLE `'._DB_PREFIX_.self::TABLE_MODULE.'` DROP COLUMN `module`;');
        } catch (PrestaShopDatabaseException $e) {
            // Just ignore it
        }

        return $result;
    }

    public static function insertSpecificPrice($id, $id_product, $from, $to)
    {
        $query = 'INSERT INTO `'._DB_PREFIX_.self::TABLE_SPECIFIC_PRICES.'` (id_specific_price,id_product,date_from,date_to) VALUES ';
        $query .= '('.(int)$id.','.(int)$id_product.',\''.pSQL($from).'\',\''.pSQL($to).'\');';
        JprestaUtils::dbExecuteSQL($query);
    }

    public static function updateSpecificPrice($id, $id_product, $from, $to)
    {
        $query = 'UPDATE `'._DB_PREFIX_.self::TABLE_SPECIFIC_PRICES.'` SET id_product='.(int)$id_product.', date_from=\''.pSQL($from).'\', date_to=\''.pSQL($to).'\'
            WHERE id_specific_price='.(int)$id.';';
        JprestaUtils::dbExecuteSQL($query);
    }

    public static function deleteSpecificPrice($id)
    {
        $query = 'DELETE FROM `'._DB_PREFIX_.self::TABLE_SPECIFIC_PRICES.'` WHERE id_specific_price='.(int)$id.';';
        JprestaUtils::dbExecuteSQL($query);
    }

    /**
     * Reffresh cache if any specific sprice started or ended since last check
     */
    public static function triggerReffreshment()
    {
        $now = date('Y-m-d H:i:00');
        $query = 'SELECT DISTINCT id_product FROM `'._DB_PREFIX_.self::TABLE_SPECIFIC_PRICES.'` WHERE date_from<=\''.pSQL($now).'\' OR date_to<\''.pSQL($now).'\';';
        $rows = Db::getInstance()->executeS($query);
        if (JprestaUtils::isIterable($rows)) {
            if (count($rows) > 0) {
                // Change date now to avoid other visitors to trigger refreshment
                $query = 'UPDATE `'._DB_PREFIX_.self::TABLE_SPECIFIC_PRICES.'` SET date_from=\'6666-01-01 00:00:00\' WHERE date_from<=\''.pSQL($now).'\';';
                JprestaUtils::dbExecuteSQL($query);
                $query = 'UPDATE `'._DB_PREFIX_.self::TABLE_SPECIFIC_PRICES.'` SET date_to=\'6666-01-01 00:00:00\' WHERE date_to<\''.pSQL($now).'\';';
                JprestaUtils::dbExecuteSQL($query);
                // Clean useless rows
                $query = 'DELETE FROM `'._DB_PREFIX_.self::TABLE_SPECIFIC_PRICES.'` WHERE date_from=\'6666-01-01 00:00:00\' AND date_to=\'6666-01-01 00:00:00\';';
                JprestaUtils::dbExecuteSQL($query);
                foreach ($rows as $row) {
                    // Clear product cache and linking pages because price has changed
                    if ((int)$row['id_product']) {
                        self::clearCacheOfObject('product', $row['id_product'], true);
                    }
                }
            }
        }
    }

    public static function deleteCacheWithObsoleteCssAndJsVersions() {
        $current_css = (int) Configuration::get('PS_CCCCSS_VERSION');
        $current_js = (int) Configuration::get('PS_CCCJS_VERSION');
        if ($current_css > 0 || $current_js > 0) {
            $query = 'SELECT * FROM `' . _DB_PREFIX_ . self::TABLE . '` 
                WHERE v_css < ' . $current_css . ' OR v_js < ' . $current_js;
            if ($current_css === 1) {
                $query .= ' OR v_css is NULL';
            }
            if ($current_js === 1) {
                $query .= ' OR v_js is NULL';
            }
            $rows = Db::getInstance()->executeS($query);
            self::deleteCachedPages($rows, true);
        }
    }

    /**
     * @param $nbHourExpired Number of hour since the cache is expired, can be negative to pages that are about to expire
     * @param bool $maxRows Max number of returned rows
     * @param bool $deleted false if you want pages that have available cache, true for pages where the cache has been deleted, null if it does not matter
     * @return array cached pages
     */
    public static function getCachedPages($nbHourExpired, $maxRows = false, $deleted = false) {
        $rowsToReturn = array();
        foreach (PageCache::$managed_controllers as $controller) {
            $configuredMaxAge = 60 * ((int)Configuration::get('pagecache_'.$controller.'_timeout'));
            if ($configuredMaxAge == 0) {
                $minAgeToReturn = 0;
            }
            else {
                $minAgeToReturn = max(0, $configuredMaxAge + ((int)$nbHourExpired * 60 * 60));
            }
            $query = 'SELECT * FROM `' . _DB_PREFIX_ . self::TABLE . '` 
                WHERE controller = \'' . $controller . '\' 
                    AND last_gen < (NOW() - INTERVAL ' . (int)$minAgeToReturn . ' SECOND)';
            if ($deleted !== null) {
                $query .= ' AND `deleted`=' . ($deleted ? 1 : 0);
            }
            $query .= ' ORDER BY last_gen ASC';
            if ($maxRows !== false) {
                $query .= 'LIMIT ' . ((int) $maxRows - count($rowsToReturn));
            }
            $rows = Db::getInstance()->executeS($query);
            if (is_array($rows)) {
                $rowsToReturn = array_merge($rowsToReturn, $rows);
                if ($maxRows !== false && count($rowsToReturn) >= $maxRows) {
                    // We got all necessary rows
                    break;
                }
            }
        }
        return $rowsToReturn;
    }

    /**
     * @param $rows array Rows returned by self::getCachedPages()
     * @param bool $deleteStats
     */
    public static function deleteCachedPages($rows, $deleteStats = false) {
        if (JprestaUtils::isIterable($rows) && count($rows) > 0) {
            $url_query_deleted = '(';
            foreach ($rows as $row) {
                PageCache::getCache()->delete(JprestaCacheKey::intToString($row['cache_key']));
                $url_query_deleted .= pSQL($row['cache_key']) . ',';
            }
            if ($deleteStats) {
                // Delete all rows
                $query_deleted = 'DELETE FROM `' . _DB_PREFIX_ . self::TABLE . '` WHERE `cache_key` IN ' . $url_query_deleted . '0)';
            }
            else {
                // Mark deleted cache contents as deleted in DB
                $query_deleted = 'UPDATE `' . _DB_PREFIX_ . self::TABLE . '` SET `deleted`=1 WHERE `cache_key` IN ' . $url_query_deleted . '0)';
            }
            JprestaUtils::dbExecuteSQL($query_deleted);
        }
    }

    public static function hasTriggerIn2H() {
        $now = date('Y-m-d H:i:00');
        $now_plus_2h = new DateTime();
        $now_plus_2h->modify('+2 hour');
        $now_plus_2h = $now_plus_2h->format('Y-m-d H:i:00');
        $query = 'SELECT * FROM `'._DB_PREFIX_.self::TABLE_SPECIFIC_PRICES.'` WHERE (date_from >= \''.pSQL($now).'\' AND date_from <= \''.pSQL($now_plus_2h).'\') OR (date_to >= \''.pSQL($now).'\'   AND date_to <= \''.pSQL($now_plus_2h).'\');';
        $rows = Db::getInstance()->executeS($query);
        if (JprestaUtils::isIterable($rows)) {
            return (count($rows) > 0);
        } else {
            return false;
        }
    }

    public static function dropTables()
    {
        JprestaUtils::dbExecuteSQL('DROP TABLE IF EXISTS `'._DB_PREFIX_.self::TABLE_MODULE.'`;');
        JprestaUtils::dbExecuteSQL('DROP TABLE IF EXISTS `'._DB_PREFIX_.self::TABLE_BACKLINK.'`;');
        JprestaUtils::dbExecuteSQL('DROP TABLE IF EXISTS `'._DB_PREFIX_.self::TABLE.'`;');
        JprestaUtils::dbExecuteSQL('DROP TABLE IF EXISTS `'._DB_PREFIX_.self::TABLE_DETAILS.'`;');
        JprestaUtils::dbExecuteSQL('DROP TABLE IF EXISTS `'._DB_PREFIX_.self::TABLE_SPECIFIC_PRICES.'`;');
        JprestaUtils::dbExecuteSQL('DROP TABLE IF EXISTS `'._DB_PREFIX_.self::TABLE_PROFILING.'`;');
    }

    /**
     * @param $jprestaCacheKey JprestaCacheKey
     */
    public static function incrementCountHit($jprestaCacheKey)
    {
        JprestaUtils::dbExecuteSQL('UPDATE `' . _DB_PREFIX_ . self::TABLE . '` SET count_hit=count_hit+1 WHERE `cache_key`=' . JprestaUtils::dbToInt($jprestaCacheKey->toInt()) . ';');
    }

    /**
     * @param $id_country integer
     * @return false|null|string
     */
    public static function getCountryMask($id_country)
    {
        $query = 'SELECT `mask_country` FROM `' . _DB_PREFIX_ . self::TABLE . '` WHERE `id_country`=' . (int)$id_country . ';';
        return Db::getInstance()->getValue($query);
    }

    public static function getMostUsedCountryStateZipcode($limit = 4)
    {
        $query = 'SELECT `details` as `tax_csz`, sum(1) AS `count`
          FROM `' . _DB_PREFIX_ . self::TABLE . '` AS cc
          INNER JOIN `' . _DB_PREFIX_ . self::TABLE_DETAILS . '` AS cd
          ON cc.id_tax_csz = cd.id
          GROUP BY cd.`id`
          ORDER BY 2 DESC
          LIMIT ' . (int) $limit;
        return Db::getInstance()->executeS($query);
    }

    public static function getMostUsedSpecifics($limit = 4)
    {
        $query = 'SELECT `details` as `specifics`, sum(1) AS `count`
          FROM `' . _DB_PREFIX_ . self::TABLE . '` AS cc
          INNER JOIN `' . _DB_PREFIX_ . self::TABLE_DETAILS . '` AS cd
          ON cc.id_specifics = cd.id
          GROUP BY cd.`id`
          ORDER BY 2 DESC
          LIMIT ' . (int) $limit;
        return Db::getInstance()->executeS($query);
    }

    public static function getMostUsedCountries($limit = 4)
    {
        $query = 'SELECT `id_country`, sum(1) AS `count`
          FROM `' . _DB_PREFIX_ . self::TABLE . '` AS cc
          GROUP BY cc.`id_country`
          HAVING cc.id_country <> '. (int) Configuration::get('PS_COUNTRY_DEFAULT').'
          ORDER BY 2 DESC
          LIMIT ' . (int) $limit;
        return Db::getInstance()->executeS($query);
    }

    /**
     * @param $id_details
     * @return string|
     */
    public static function getDetailsById($id_details) {
        $query = 'SELECT `details` FROM `' . _DB_PREFIX_ . self::TABLE_DETAILS . '` AS cd WHERE cd.id = ' . (int) $id_details;
        return Db::getInstance()->getValue($query);
    }

    /**
     * @param $jprestaCacheKey JprestaCacheKey
     * @return array Array with 2 keys: 'hit' and 'missed'
     */
    public static function getStats($jprestaCacheKey)
    {
        $db = Db::getInstance();
        $query = 'SELECT count_hit, count_missed FROM `' . _DB_PREFIX_ . self::TABLE . '` WHERE `cache_key`=' . JprestaUtils::dbToInt($jprestaCacheKey->toInt()) . ';';
        $result = $db->executeS($query);
        if (JprestaUtils::isIterable($result) && count($result) == 1) {
            return array('hit' => (int)$result[0]['count_hit'], 'missed' => (int)$result[0]['count_missed']);
        }
        return array('hit' => -1, 'missed' => -1);
    }

    /**
     * @param $jprestaCacheKey JprestaCacheKey Cache key
     * @param $cache_ttl integer configured timeout in minutes
     * @return integer Number of minutes the page will leave in cache
     */
    public static function getTtl($jprestaCacheKey, $cache_ttl_minutes)
    {
        $db = Db::getInstance();
        $query = 'SELECT `deleted`, TIMESTAMPDIFF(MINUTE, last_gen, NOW()) as age  FROM `' . _DB_PREFIX_ . self::TABLE . '` WHERE `cache_key`=' . JprestaUtils::dbToInt($jprestaCacheKey->toInt()) . ';';
        $result = $db->executeS($query);
        if (JprestaUtils::isIterable($result) && count($result) == 1) {
            if ($result[0]['deleted']) {
                return 0;
            } else {
                return max(0, $cache_ttl_minutes - $result[0]['age']);
            }
        }
        return 0;
    }

    public static function getStatsByUrl($url)
    {
        $db = Db::getInstance();
        $query = 'SELECT sum(1) as `count`, sum(count_hit) as sum_hit, sum(count_missed) as sum_missed, max(TIMESTAMPDIFF(MINUTE, last_gen, NOW())) as max_age_minutes FROM `' . _DB_PREFIX_ . self::TABLE . '` GROUP BY url HAVING url ='.JprestaUtils::dbToString($db, $url).';';
        $result = $db->executeS($query);
        if (JprestaUtils::isIterable($result) && count($result) == 1) {
            return $result[0];
        }
        return array('count' => 0, 'sum_hit' => 0, 'sum_missed' => 0, 'max_age_minutes' => PHP_INT_MAX);
    }

    public static function getPerformances($ids_shop = null)
    {
        $db = Db::getInstance();
        if (empty($ids_shop)) {
            $query = 'SELECT sum(1) as `count`, sum(count_hit) as sum_hit, sum(count_missed) as sum_missed
            FROM `'._DB_PREFIX_.self::TABLE.'`';
        } else {
            $query = 'SELECT sum(1) as `count`, sum(count_hit) as sum_hit, sum(count_missed) as sum_missed
            FROM `'._DB_PREFIX_.self::TABLE.'` WHERE id_shop IN ('.pSQL(implode(',', $ids_shop)).')';
        }
        $result = $db->executeS($query)[0];
        return $result;
    }

    public static function getHitCount($controller, $id_object, $shopId)
    {
        $sql = 'SELECT SUM(count_missed) + SUM(count_hit) as count_total FROM `' . _DB_PREFIX_ . self::TABLE . '` WHERE `id_shop` =' . (int)$shopId . ' AND `controller` = \'' . pSQL($controller) . '\'';
        if ($id_object) {
            $sql .= ' AND `id_object` = ' . (int)$id_object;
        }
        $totalCount = DB::getInstance()->getValue($sql);
        if ($totalCount) {
            return (int) $totalCount;
        }
        return 0;
    }

    /**
     * @param $details string
     * @return int|null
     */
    private static function getOrCreateDetailsId($details) {
        $id_details = null;
        if ($details) {
            $db = Db::getInstance();
            $query = 'SELECT id FROM `'._DB_PREFIX_.self::TABLE_DETAILS.'` WHERE `details`='.JprestaUtils::dbToString($db, $details).';';
            $id_details = $db->getValue($query);
            if (!$id_details) {
                $query = 'INSERT INTO `'._DB_PREFIX_.self::TABLE_DETAILS.'` (`details`) VALUES ('.JprestaUtils::dbToString($db, $details).');';
                $db->execute($query);
                $id_details = (int)$db->Insert_ID();
            }
        }
        return $id_details;
    }

    /**
     * @param $jprestaCacheKey JprestaCacheKey Cache key with informations
     * @param $controller string Controller that manage the URL
     * @param $id_shop integer
     * @param $id_object integer ID of the object (product, category, supplier, etc.) if any
     * @param $module_ids array IDs of called module on this page
     * @param $backlinks_cache_keys int[] List of cache keys present in this page
     * @param int $log_level
     * @param bool $stats_it
     * @throws PrestaShopDatabaseException
     */
    public static function insert($jprestaCacheKey, $controller, $id_shop, $id_object = null, $module_ids, $backlinks_cache_keys, $log_level=0, $stats_it=true)
    {
        $startTime1 = microtime(true);

        $db = Db::getInstance();
        $query = 'SELECT id FROM `'._DB_PREFIX_.self::TABLE.'` WHERE `cache_key`='.JprestaUtils::dbToInt($jprestaCacheKey->toInt()).';';
        $existing_rows = $db->executeS($query); // getValue() will trigger error 2014 if used
        $startTime2 = microtime(true);

        if (JprestaUtils::isIterable($existing_rows) && count($existing_rows) > 0) {
            //
            // ALREADY CACHED PAGE (but content is not in cache anymore)
            //
            $id_pagecache = (int)$existing_rows[0]['id'];
            if ($stats_it) {
                $query = 'UPDATE `'._DB_PREFIX_.self::TABLE.'` SET
                    `count_missed`=`count_missed` + 1, last_gen = CURRENT_TIMESTAMP, `deleted` = 0
                    WHERE id='.(int)$id_pagecache.';';
            }
            else {
                $query = 'UPDATE `'._DB_PREFIX_.self::TABLE.'` SET
                    last_gen = CURRENT_TIMESTAMP, `deleted` = 0
                    WHERE id='.(int)$id_pagecache.';';
            }
            JprestaUtils::dbExecuteSQL($query);
        } else {
            //
            // NEW PAGE
            //
            $id_tax_csz = self::getOrCreateDetailsId($jprestaCacheKey->get('tax_csz'));
            $id_specifics = self::getOrCreateDetailsId($jprestaCacheKey->get('specifics'));
            $query = 'INSERT INTO `'._DB_PREFIX_.self::TABLE.'` (`cache_key`, `url`, `controller`, `id_shop`, `id_object`, `id_currency`, `id_lang`, `id_fake_customer`, `id_device`, `id_country`, `mask_country`, `id_tax_csz`, `id_specifics`, `v_css`, `v_js`, `count_missed`, `count_hit`)
                VALUES (
                '.JprestaUtils::dbToInt($jprestaCacheKey->toInt()).',
                '.JprestaUtils::dbToString($db, $jprestaCacheKey->get('url')).',
                '.JprestaUtils::dbToString($db, $controller).',
                '.JprestaUtils::dbToInt($id_shop).',
                '.JprestaUtils::dbToInt($id_object).',
                '.JprestaUtils::dbToInt($jprestaCacheKey->get('id_currency')).',
                '.JprestaUtils::dbToInt($jprestaCacheKey->get('id_lang')).',
                '.JprestaUtils::dbToInt($jprestaCacheKey->get('id_fake_customer')).',
                '.JprestaUtils::dbToInt($jprestaCacheKey->get('id_device')).',
                '.JprestaUtils::dbToInt($jprestaCacheKey->get('id_country')).',
                '.JprestaUtils::dbToString($db, $jprestaCacheKey->get('mask_country')).',
                '.JprestaUtils::dbToString($db, $id_tax_csz).',
                '.JprestaUtils::dbToString($db, $id_specifics).',
                '.JprestaUtils::dbToInt($jprestaCacheKey->get('css_version')).',
                '.JprestaUtils::dbToInt($jprestaCacheKey->get('js_version')).',
                '.($stats_it ? '1' : '0').', 
                0);';
            JprestaUtils::dbExecuteSQL($query);
            $id_pagecache = (int)$db->Insert_ID();
        }
        //
        // MODULES
        //
        $startTime3 = microtime(true);
        $startTime4 = microtime(true);
        JprestaUtils::dbExecuteSQL('DELETE FROM `'._DB_PREFIX_.self::TABLE_MODULE.'` WHERE `id`='.(int)$id_pagecache.';');
        if (count($module_ids) > 0) {
            $module_query = 'INSERT INTO `'._DB_PREFIX_.self::TABLE_MODULE.'` (`id`, `id_module`) VALUES ';
            $idx = 0;
            foreach($module_ids as $id_module) {
                $module_query .= '('.$id_pagecache.',' . $id_module . ')';
                $idx++;
                if ($idx < count($module_ids)) {
                    $module_query .= ',';
                }
            }
            $startTime4 = microtime(true);
            JprestaUtils::dbExecuteSQL($module_query);
        }
        //
        // BACKLINKS
        //
        $startTime5 = microtime(true);
        $startTime6 = microtime(true);
        JprestaUtils::dbExecuteSQL('DELETE FROM `'._DB_PREFIX_.self::TABLE_BACKLINK.'` WHERE `id`='.(int)$id_pagecache.';');
        if (count($backlinks_cache_keys) > 0) {
            $backlink_query = 'INSERT INTO `'._DB_PREFIX_.self::TABLE_BACKLINK.'` (`id`, `backlink_key`) VALUES ';
            $idx = 0;
            foreach($backlinks_cache_keys as $backlink_cache_key) {
                $backlink_query .= '('.(int)$id_pagecache.',' . JprestaUtils::dbToInt($backlink_cache_key) . ')';
                $idx++;
                if ($idx < count($backlinks_cache_keys)) {
                    $backlink_query .= ',';
                }
            }
            $startTime6 = microtime(true);
            JprestaUtils::dbExecuteSQL($backlink_query);
        }
        if (((int)$log_level)>0) {
            Logger::addLog("PageCache | insert | added cache for $controller#$id_object during "
                . number_format($startTime2 - $startTime1, 3) . '+'
                . number_format($startTime3 - $startTime2, 3) . '+'
                . number_format($startTime4 - $startTime3, 3) . '+'
                . number_format($startTime5 - $startTime4, 3) . '+'
                . number_format($startTime6 - $startTime5, 3) . '+'
                . number_format(microtime(true) - $startTime6, 3) . '='
                . number_format(microtime(true) - $startTime1, 3)
                . " second(s) with ".count($backlinks_cache_keys)." backlinks", 1, null, null, null, true);
        }
    }

    public static function clearCacheOfObject($controller, $id_object, $delete_linking_pages, $action_origin='', $log_level=0) {
        // Some code to avoid calling this method multiple times (can happen when saving a product for exemple)
        static $already_done = array();
        $key = $controller.'|'.$id_object.'|'.($delete_linking_pages ? '1' : '0');
        if (array_key_exists($key, $already_done)) {
            return;
        }
        $already_done[$key] = true;
        if ($delete_linking_pages) {
            // When called with option $delete_linking_pages we can skip call without the option...
            $already_done[$controller.'|'.$id_object.'|0'] = true;
        }

        $startTime1 = microtime(true);
        $db = Db::getInstance();
        if ($id_object != null) {
            $query = 'SELECT id, id_shop, cache_key FROM `'._DB_PREFIX_.self::TABLE.'`
                WHERE controller=\''.$db->escape($controller).'\' AND id_object='.((int)$id_object).';';
        } else {
            $query = 'SELECT id, id_shop, cache_key FROM `'._DB_PREFIX_.self::TABLE.'`
                WHERE controller=\''.$db->escape($controller).'\' AND id_object IS NULL;';
        }
        $results = $db->executeS($query);
        $startTime2 = microtime(true);

        $url_query = '(';
        $url_query_deleted = '(';
        $idx = 0;
        if (JprestaUtils::isIterable($results)) {
            foreach ($results as $result) {
                PageCache::getCache($result['id_shop'])->delete(JprestaCacheKey::intToString($result['cache_key']));
                $idx++;
                $url_query .=  pSQL($result['cache_key']);
                $url_query_deleted .=  pSQL($result['cache_key']) . ',';
                if ($idx < count($results)) {
                    $url_query .= ',';
                }
            }
        }
        if (((int)$log_level)>0) {
            Logger::addLog("PageCache | $action_origin | reffreshed $idx pages from $controller#$id_object during "
                . number_format($startTime2 - $startTime1, 3) . '+'
                . number_format(microtime(true) - $startTime2, 3) . '='
                . number_format(microtime(true) - $startTime1, 3)
                . " second(s)", 1, null, null, null, true);
            $startTime1 = microtime(true);
        }
        if ($delete_linking_pages) {
            // Also add the default link of the object in case that the page has never been cached
            $default_links = self::_getDefaultLinks($controller, $id_object);
            if (count($default_links) > 0) {
                if (count($results) > 0) {
                    $url_query .= ',';
                }
                $remainingLinks = count($default_links);
                foreach ($default_links as $default_link) {
                    $url_query .= JprestaUtils::dbToInt(PageCache::getCacheKeyForBacklink($default_link));
                    $remainingLinks--;
                    if ($remainingLinks > 0) {
                        $url_query .= ',';
                    }
                }
            }
            $startTime2 = microtime(true);
            // Delete pages that link to these pages
            if (Tools::strlen($url_query) > 1) {
                $idx = 0;
                $query = 'SELECT DISTINCT pc.id, pc.cache_key FROM `'._DB_PREFIX_.self::TABLE.'` AS pc
                    LEFT JOIN `'._DB_PREFIX_.self::TABLE_BACKLINK.'` AS bl ON (bl.id = pc.id)
                    WHERE `backlink_key` IN ' . $url_query . ')';
                $results = $db->executeS($query);
                $startTime3 = microtime(true);
                if (JprestaUtils::isIterable($results)) {
                    foreach ($results as $result) {
                        PageCache::getCache()->delete(JprestaCacheKey::intToString($result['cache_key']));
                        $idx++;
                        $url_query_deleted .=  pSQL($result['cache_key']) . ',';
                    }
                }
            }
            if (((int)$log_level)>0) {
                Logger::addLog("PageCache | $action_origin | reffreshed $idx pages that were linking to $controller#$id_object during "
                    . number_format($startTime2 - $startTime1, 3) . '+'
                    . number_format($startTime3 - $startTime2, 3) . '+'
                    . number_format(microtime(true) - $startTime3, 3) . '='
                    . number_format(microtime(true) - $startTime1, 3)
                    . " second(s)", 1, null, null, null, true);
            }
        }
        if (Tools::strlen($url_query_deleted) > 1) {
            // Mark deleted cache contents as deleted in DB
            $query_deleted = 'UPDATE `' . _DB_PREFIX_ . self::TABLE . '` SET `deleted`=1 WHERE `cache_key` IN ' . $url_query_deleted . '0)';
            JprestaUtils::dbExecuteSQL($query_deleted);
        }
    }

    private static function _getDefaultLinks($controller, $id_object) {
        $links = array();
        if ($id_object != null) {
            $context = Context::getContext();
            if (!isset($context->link)) {
                /* Link should be initialized in the context but sometimes it is not */
                $https_link = (Tools::usingSecureMode() && Configuration::get('PS_SSL_ENABLED')) ? 'https://' : 'http://';
                $context->link = new Link($https_link, $https_link);
            }
            switch ($controller) {
                case 'cms':
                    $links[] = $context->link->getCMSLink((int)($id_object), null, null, null, null, true);
                    break;
                case 'product':
                    $idLang = $context->language->id;
                    $idShop = Shop::getContextShopID();
                    if (!is_object($context->cart)) {
                        $context->cart = new Cart();
                    }
                    $product = new Product((int) $id_object, false, $idLang, $idShop);
                    $ipass = Product::getProductAttributesIds((int) $id_object);
                    if (is_array($ipass)) {
                        foreach ($ipass as $ipas) {
                            foreach ($ipas as $ipa) {
                                $links[] = $context->link->getProductLink($product, null, null, null, $idLang, $idShop, $ipa, false, true);
                            }
                        }
                    }
                    $links[] = $context->link->getProductLink((int)($id_object), null, null, null, null, null, 0, false, true);
                    break;
                case 'category':
                    $links[] = $context->link->getCategoryLink((int)($id_object), null, null, null, null, true);
                    break;
                case 'manufacturer':
                    $links[] = $context->link->getManufacturerLink((int)($id_object), null, null, null, true);
                    break;
                case 'supplier':
                    $links[] = $context->link->getSupplierLink((int)($id_object), null, null, null, true);
                    break;
            }
        }
        return $links;
    }

    public static function clearCacheOfModule($module_name, $action_origin='', $log_level=0) {
        $startTime1 = microtime(true);
        $module = Module::getInstanceByName($module_name);
        if ($module instanceof Module) {
            $id_module = $module->id;
            if (!empty($id_module)) {
                $query = 'SELECT pc.id, pc.cache_key FROM `'._DB_PREFIX_.self::TABLE.'` AS pc
                    LEFT JOIN `'._DB_PREFIX_.self::TABLE_MODULE.'` AS mods ON (mods.id = pc.id)
                    WHERE `id_module`='.((int)$id_module);
                $db = Db::getInstance();
                $results = $db->executeS($query);

                $startTime2 = microtime(true);
                $idx = 0;
                $url_query_deleted = '(';
                if (JprestaUtils::isIterable($results)) {
                    foreach ($results as $result) {
                        PageCache::getCache()->delete(JprestaCacheKey::intToString($result['cache_key']));
                        $idx++;
                        $url_query_deleted .=  pSQL($result['cache_key']) . ',';
                    }
                }
                if (((int)$log_level)>0) {
                    Logger::addLog("PageCache | $action_origin | reffreshed $idx pages containing module $module_name during "
                        . number_format($startTime2 - $startTime1, 3) . '+'
                        . number_format(microtime(true) - $startTime2, 3) . '='
                        . number_format(microtime(true) - $startTime1, 3)
                        . " second(s)", 1, null, null, null, true);
                }

                if (Tools::strlen($url_query_deleted) > 1) {
                    // Mark deleted cache contents as deleted in DB
                    $query_deleted = 'UPDATE `' . _DB_PREFIX_ . self::TABLE . '` SET `deleted`=1 WHERE `cache_key` IN ' . $url_query_deleted . '0)';
                    JprestaUtils::dbExecuteSQL($query_deleted);
                }
            }
        }
    }

    public static function resetCache($ids_shop = null) {
        if (empty($ids_shop)) {
            JprestaUtils::dbExecuteSQL('DELETE FROM `'._DB_PREFIX_.self::TABLE.'`;');
            JprestaUtils::dbExecuteSQL('DELETE FROM `'._DB_PREFIX_.self::TABLE_DETAILS.'`;');
            JprestaUtils::dbExecuteSQL('DELETE FROM `'._DB_PREFIX_.self::TABLE_BACKLINK.'`;');
            JprestaUtils::dbExecuteSQL('DELETE FROM `'._DB_PREFIX_.self::TABLE_MODULE.'`;');
        } else {
            JprestaUtils::dbExecuteSQL('DELETE bl, mods, pc FROM `'._DB_PREFIX_.self::TABLE.'` AS pc
                LEFT JOIN `'._DB_PREFIX_.self::TABLE_BACKLINK.'` AS bl ON pc.id=bl.id
                LEFT JOIN `'._DB_PREFIX_.self::TABLE_MODULE.'` AS mods ON pc.id=mods.id
                WHERE pc.id_shop IN ('.pSQL(implode(',', $ids_shop)).');');
        }
    }

    public static function clearAllCache() {
        try {
            JprestaUtils::dbExecuteSQL('UPDATE `'._DB_PREFIX_.self::TABLE.'` SET `deleted`=1;');
            JprestaUtils::dbExecuteSQL('DELETE FROM `'._DB_PREFIX_.self::TABLE_BACKLINK.'`;');
            JprestaUtils::dbExecuteSQL('DELETE FROM `'._DB_PREFIX_.self::TABLE_MODULE.'`;');
        } catch (Exception $e) {
            error_log('Warning, cannot delete cache backlinks ' . $e->getMessage());
        }
    }

    /**
     * @param $id_module
     * @param $description
     * @param $duration
     * @param integer $max_records Maximum number of records
     * @return bool true if the number of records is less than $max_records
     */
    public static function addProfiling($id_module, $description, $duration, $max_records = 1000) {
        try {
            $db = Db::getInstance();
            $query = 'INSERT INTO `'._DB_PREFIX_.self::TABLE_PROFILING.'` (`id_module`, `description`, `duration_ms`) VALUES ('.(int)$id_module.', \''.$db->escape($description).'\', '.(int)$duration.');';
            JprestaUtils::dbExecuteSQL($query);
            return $db->getValue('SELECT COUNT(*) FROM '._DB_PREFIX_.self::TABLE_PROFILING) < $max_records;
        } catch (Exception $e) {
            error_log('Warning, cannot insert profiling datas ' . $e->getMessage());
        }
        return true;
    }

    public static function clearProfiling($minMs = 0) {
        try {
            if ($minMs === 0) {
                JprestaUtils::dbExecuteSQL('TRUNCATE TABLE `' . _DB_PREFIX_ . self::TABLE_PROFILING . '`;');
            }
            else {
                JprestaUtils::dbExecuteSQL('DELETE FROM `' . _DB_PREFIX_ . self::TABLE_PROFILING . '` WHERE `duration_ms` < ' . (int)$minMs . ';');
            }
        } catch (Exception $e) {
            error_log('Warning, cannot clear profiling datas ' . $e->getMessage());
        }
    }

}