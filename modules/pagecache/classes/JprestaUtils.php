<?php
/**
 * Page Cache Ultimate, Page Cache standard and Speed pack are powered by Jpresta (jpresta . com)
 *
 *    @author    Jpresta
 *    @copyright Jpresta
 *    @license   You are just allowed to modify this copy for your own use. You must not redistribute it. License
 *               is permitted for one Prestashop instance only but you can install it on your test instances.
 */

if (! class_exists('JprestaUtils')) {

    require_once 'JprestaUtilsDispatcher.php';

    class JprestaUtils
    {
        /**
         * Original PHP code by Chirp Internet: www.chirp.com.au, Please acknowledge use of this code by including this header.
         *
         * @param unknown $html
         */
        public static function parseLinks($html, $base, $managedControllers, $tagIgnoreStart = false, $tagIgnoreEnd = false) {
            $startPos = false;
            if ($tagIgnoreStart !== false) {
                if (method_exists('Tools', 'strpos')) {
                    $startPos = Tools::strpos($html, $tagIgnoreStart);
                }
                else {
                    $startPos = strpos($html, $tagIgnoreStart);
                }
            }
            if ($startPos !== false) {
                $linksAfter = array();
                if (method_exists('Tools', 'strpos')) {
                    $endPos = Tools::strpos($html, $tagIgnoreEnd, min(Tools::strlen($html), $startPos + 4));
                }
                else {
                    $endPos = strpos($html, $tagIgnoreEnd, min(Tools::strlen($html), $startPos + 4));
                }
                $linksBefore = self::parseLinks(Tools::substr($html, 0, $startPos), $base, $managedControllers, $tagIgnoreStart, $tagIgnoreEnd);
                if ($endPos !== false) {
                    $linksAfter = self::parseLinks(Tools::substr($html, $endPos + 4), $base, $managedControllers, $tagIgnoreStart, $tagIgnoreEnd);
                }
                return array_merge($linksBefore, $linksAfter);
            }
            else {
                $links = array();

                $base_relative = preg_replace('/https?:\/\//', '//', $base);
                $base_exp = preg_replace('/([^a-zA-Z0-9])/', '\\\\$1', $base);
                $base_exp = preg_replace('/https?/', 'http[s]?', $base_exp);
                $regexp = '<a\s[^>]*href=(\"??)' . $base_exp . '([^\" >]*?)\\1[^>]*>(.*)<\/a>';
                $isMultiLanguageActivated = Language::isMultiLanguageActivated();

                if(preg_match_all("/$regexp/siU", $html, $matches, PREG_SET_ORDER)) {

                    // The links array will help us to remove duplicates
                    foreach($matches as $match) {
                        // $match[2] = link address
                        // $match[3] = link text
                        // Insert backlinks that correspond to a possibily cached page into the database

                        $url = $match[2];
                        // Add leading /
                        if (strpos($url, "/") > 0 || strpos($url, "/") === false) {
                            $url = "/" . $url;
                        }

                        // Remove language part if any
                        $url_without_lang = $url;
                        if ($isMultiLanguageActivated && preg_match('#^/([a-z]{2})(?:/.*)?$#', $url, $m)) {
                            $url_without_lang = Tools::substr($url, 3);
                        }
                        $anchorPos = strpos($url_without_lang, '#');
                        if ($anchorPos !== false) {
                            $url_without_lang = Tools::substr($url_without_lang, 0, $anchorPos);
                        }

                        $bl_controller = JprestaUtilsDispatcher::getPageCacheInstance()->getControllerFromURL($url_without_lang);
                        if ($bl_controller === false) {
                            // To avoid re-installation of override we have this workaround
                            $bl_controller = JprestaUtilsDispatcher::getPageCacheInstance()->getControllerFromURL('en'. $url_without_lang);
                        }
                        if (in_array($bl_controller, $managedControllers)) {
                            $links[$match[2]] = $base_relative . $match[2];
                        }
                    }
                }
                return $links;
            }
        }

        public static function parseCSS($html, $base) {
            $links = array();
            $base_exp = preg_replace('/([^a-zA-Z0-9])/', '\\\\$1', $base);
            $regexp = '<link\s[^>]*href=(\"??)[^\" >]*' . $base_exp . '([^\" >]*?)\\1[^>]*>';
            if(preg_match_all("/$regexp/siU", $html, $matches, PREG_SET_ORDER)) {
                foreach($matches as $match) {
                    $links[] = $match[2];
                }
            }
            return $links;
        }

        public static function parseJS($html, $base) {
            $links = array();
            $base_exp = preg_replace('/([^a-zA-Z0-9])/', '\\\\$1', $base);
            $regexp = '<script\s[^>]*src=(\"??)[^\" >]*' . $base_exp . '([^\" >]*?)\\1[^>]*>';
            if(preg_match_all("/$regexp/siU", $html, $matches, PREG_SET_ORDER)) {
                foreach($matches as $match) {
                    $links[] = $match[2];
                }
            }
            return $links;
        }

        public static function deleteFile($file) {
            if (is_file($file)) {
                unlink($file);
            }
        }

        /**
         * Creates a backup file, then search and replace in it
         *
         * @param $file string File to modify
         * @param mixed $search <p>
         * The value being searched for, otherwise known as the needle.
         * An array may be used to designate multiple needles.
         * </p>
         * @param mixed $replace <p>
         * The replacement value that replaces found search
         * values. An array may be used to designate multiple replacements.
         * </p>
         */
        public static function replaceInFile($file, $search, $replace) {
            if (is_file($file)) {
                $i = 1;
                $suffix = '-backup-' . date('Ymd');
                while(file_exists($file . $suffix)) {
                    $suffix = '-backup-' . date('Ymd') . '-' . $i;
                    $i++;
                }
                Tools::copy($file, $file . $suffix);
                $content = Tools::file_get_contents($file);
                $content = str_replace($search, $replace, $content);
                file_put_contents($file, $content);
            }
        }

        public static function isAjax() {
            // Usage of ajax parameter is deprecated
            $isAjax = Tools::getValue('ajax') || Tools::isSubmit('ajax');

            if (isset($_SERVER['HTTP_ACCEPT'])) {
                $isAjax = $isAjax || preg_match(
                        '#\bapplication/json\b#',
                        $_SERVER['HTTP_ACCEPT']
                    );
            }

            return $isAjax;
        }

        // Does not support flag GLOB_BRACE
        public static function glob_recursive($pattern, $flags = 0)
        {
            $files = glob($pattern, $flags);
            foreach (glob(dirname($pattern).'/*', GLOB_ONLYDIR|GLOB_NOSORT) as $dir)
            {
                $files = array_merge($files, self::glob_recursive($dir.'/'.basename($pattern), $flags));
            }
            return $files;
        }

        public static function startsWith($haystack, $needle)
        {
            $length = Tools::strlen($needle);
            return (Tools::substr($haystack, 0, $length) === $needle);
        }

        public static function endsWith($haystack, $needle)
        {
            $length = Tools::strlen($needle);
            if ($length == 0) {
                return true;
            }
            return (Tools::substr($haystack, -$length) === $needle);
        }

        public static function trimTo($string, $default) {
            if (!$string) {
                return $default;
            }
            $ret = trim($string);
            if (Tools::strlen($ret === 0)) {
                return $default;
            }
            return $ret;
        }

        /**
         * Determine if a variable is iterable. i.e. can be used to loop over.
         *
         * @return bool
         */
        public static function isIterable($var)
        {
            return $var !== null && (is_array($var) || $var instanceof Traversable);
        }

        public static function getDomains()
        {
            if (method_exists('Tools', 'getDomains')) {
                return Tools::getDomains();
            }
            $domains = [];
            foreach (ShopUrl::getShopUrls() as $shop_url) {
                /** @var ShopUrl $shop_url */
                if (!isset($domains[$shop_url->domain])) {
                    $domains[$shop_url->domain] = [];
                }

                $domains[$shop_url->domain][] = [
                    'physical' => $shop_url->physical_uri,
                    'virtual' => $shop_url->virtual_uri,
                    'id_shop' => $shop_url->id_shop,
                ];

                if ($shop_url->domain == $shop_url->domain_ssl) {
                    continue;
                }

                if (!isset($domains[$shop_url->domain_ssl])) {
                    $domains[$shop_url->domain_ssl] = [];
                }

                $domains[$shop_url->domain_ssl][] = [
                    'physical' => $shop_url->physical_uri,
                    'virtual' => $shop_url->virtual_uri,
                    'id_shop' => $shop_url->id_shop,
                ];
            }

            return $domains;
        }

        public static function valuesAreIdentical($v1, $v2)
        {
            $type1 = gettype($v1);
            $type2 = gettype($v2);

            switch (true) {
                case ($type1 === 'boolean'):
                    if ($type2 === 'string') {
                        if (($v1 && ((int)$v2) !== 1) || (!$v1 && ((int)$v2) !== 0)) {
                            // Can be string "1" or "0"
                            return false;
                        }
                    } // Else do strict comparison here.
                    else {
                        if ($v1 !== $v2) {
                            return false;
                        }
                    }
                    break;

                case ($type1 === 'integer'):
                    if ($type2 === 'string') {
                        if ($v1 !== (int)$v2) {
                            return false;
                        }
                    } // Else do strict comparison here.
                    else {
                        if ($v1 !== $v2) {
                            return false;
                        }
                    }
                    break;

                case ($type1 === 'double'):
                    if ($type2 === 'string') {
                        if ($v1 !== (float)$v2) {
                            return false;
                        }
                    } // Else do strict comparison here.
                    else {
                        if ($v1 !== $v2) {
                            return false;
                        }
                    }
                    break;

                case ($type1 === 'string'):
                    //Do strict comparison here.
                    if ($v1 !== $v2) {
                        return false;
                    }
                    break;

                case ($type1 === 'array'):
                    $bool = self::arraysAreIdentical($v1, $v2);
                    if ($bool === false) {
                        return false;
                    }
                    break;

                case ($type1 === 'object'):
                    $diffs = self::getObjectDifferences($v1, $v2);
                    if (count($diffs) > 0) {
                        return false;
                    }
                    break;

                case ($type1 === 'NULL'):
                    //Since both types were of type NULL, consider their "values" equal.
                    break;

                case ($type1 === 'resource'):
                    //How to compare if at all?
                    break;

                case ($type1 === 'unknown type'):
                    //How to compare if at all?
                    break;
            } //end switch

            //All tests passed.
            return true;
        }

        public static function getObjectDifferences($o1, $o2)
        {
            $differences = array();
            // Now do strict(er) comparison.
            $reflectionObject = new ReflectionObject($o1);

            $properties = $reflectionObject->getProperties(ReflectionProperty::IS_PUBLIC);

            foreach ($properties as $property) {
                if (in_array($property->name, ['date_upd', 'indexed']) || $property->isStatic()) {
                    continue;
                }
                if (!property_exists($o2, $property->name)) {
                    $differences[$property->name] = self::toString($o1->{$property->name}) . ' <> (not set)';
                }
                else {
                    $bool = self::valuesAreIdentical($o1->{$property->name}, $o2->{$property->name});
                    if ($bool === false) {
                        $differences[$property->name] = self::toString($o1->{$property->name}) . ' <> ' . self::toString($o2->{$property->name});
                    }
                }
            }

            // All tests passed.
            return $differences;
        }

        public static function arraysAreIdentical(array $arr1, array $arr2)
        {
            $count = count($arr1);

            // Require that they have the same size.
            if (count($arr2) !== $count) {
                return false;
            }

            // Require that they have the same keys.
            $arrKeysInCommon = array_intersect_key($arr1, $arr2);
            if (count($arrKeysInCommon) !== $count) {
                return false;
            }

            // Require that they have the same value for same key.
            foreach ($arr1 as $key => $val) {
                $bool = self::valuesAreIdentical($val, $arr2[$key]);
                if ($bool === false) {
                    return false;
                }
            }

            // All tests passed.
            return true;
        }

        public static function toString($val) {
            $type = gettype($val);
            switch (true) {
                case ($type === 'boolean'):
                    return $val ? 'true' : 'false';

                case ($type === 'array'):
                    return 'array[' . count($val) . ']';

                case ($type === 'NULL'):
                    return '(null)';

                case ($type === 'unknown type'):
                    return '(unknown type)';

                default:
                    return (string) $val;
            }
        }

        public static function getDatabaseName() {
            if (Tools::version_compare(_PS_VERSION_, '1.7', '>')) {
                $config = require dirname(__FILE__) . '/../../../app/config/parameters.php';
                return $config['parameters']['database_name'];
            }
            else {
                return _DB_NAME_;
            }
        }

        /**
         * @param string $sql SQL query to execute
         * @param bool $logOnError true if you want errors to be logged
         * @param bool $throwOnError true if you want errors to throw PrestaShopDatabaseException
         * @return bool true if OK
         * @throws PrestaShopDatabaseException
         */
        public static function dbExecuteSQL($sql, $logOnError = true, $throwOnError = false) {
            $db = DB::getInstance();
            $result = $db->execute($sql);
            if (!$result) {
                $msg = 'SQL Error #' . $db->getNumberError(). ': "' .$db->getMsgError().'" in ' . self::getCallerInfos();
                $msg .= ' - SQL query was: "' . $sql . '"';
                if ($logOnError) {
                    Logger::addLog($msg, 4);
                }
                if ($throwOnError) {
                    throw new PrestaShopDatabaseException($msg);
                }
            }
            return $result;
        }

        /**
         * @return string Caller information s a string : file:line::function()
         */
        private function getCallerInfos()
        {
            $traces = debug_backtrace();
            if (isset($traces[2])) {
                return $traces[1]['file'] . ':' . $traces[1]['line'] . '::' . $traces[2]['function'] . '()';
            }
            return '?';
        }

        public static function getRequestHeaderValue($headerName) {
            $headerNameLower = Tools::strtolower($headerName);
            $headers = self::getAllHeaders();
            if (array_key_exists($headerName, $headers)) {
                return $headers[$headerNameLower];
            }
            return null;
        }

        public static function getAllHeaders() {
            static $headers = null;
            if ($headers === null) {
                $headers = [];
                foreach ($_SERVER as $name => $value) {
                    if (Tools::substr($name, 0, 5) == 'HTTP_') {
                        $headers[str_replace(' ', '-',
                            Tools::strtolower(str_replace('_', ' ', Tools::substr($name, 5))))] = $value;
                    }
                }
            }
            return $headers;
        }

        public static function dbToInt($val) {
            if ($val && !empty($val)) {
                if (is_numeric($val)) {
                    // Preserve unsigned integers for 32bit systems
                    return $val;
                }
            }
            else {
                return 'NULL';
            }
        }

        /**
         * @param $db Db
         * @param $val
         * @return string
         */
        public static function dbToString($db, $val) {
            if ($val && !empty($val)) {
                return '\'' . $db->escape($val) . '\'';
            }
            else {
                return 'NULL';
            }
        }

    }
}