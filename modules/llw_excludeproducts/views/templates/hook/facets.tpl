{**
 * Copyright since 2007 PrestaShop SA and Contributors
 * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.md.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright Since 2007 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 *}
{if $features_to_exclude|count}
  	<div id="search_filters">
	  	
	  	{if $from != 'cart' && $from != 'order'}
	  	
		    {block name='facets_clearall_button'}
	
	        <div id="_desktop_search_filters_clear_all" class="clear-all-wrapper" style="display: none">
	          	<button class="btn btn-tertiary search-filters-clear-all">
	            	<i class="material-icons">&#xE14C;</i>
	            	{l s='Clear all' d='Shop.Theme.Actions'}
				</button>
	        </div>
	
		    {/block}

		{/if}
	
		<div class="row">
			<section class="facet clearfix">
				<ul>
					{foreach from=$features_to_exclude key="filter_key" item="filter"}
					{if ($from == 'order' && $filter.selected) || $from != 'order'}
					<li class="{if $from == 'cart'}col-lg-3 col-md-4 col-sm-4 col-xs-6{else}col-xs-12{/if}">
						<label class="facet-label" for="facet_input_{$filter_key}">
							{if $from != 'order'}
							<span class="custom-checkbox">
								<input
								  id="facet_input_{$filter_key}"
								  type="checkbox"
								  data-feature="{$filter.value}"
								  class="feature_value_to_exclude"
								  {if $filter.selected}checked{/if}
								>
								<span><i class="material-icons rtl-no-flip checkbox-checked">&#xE5CA;</i></span>
							</span>
							{/if}
							<div class="filter-label">{$filter.label}</div>
						</label>
					</li>
					{/if}
					{/foreach}
				</ul>
	      	</section>
		</div>
	</div>
{/if}
