<?php
/**
 * 2007-2020 PrestaShop.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2020 PrestaShop SA
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 * International Registered Trademark & Property of PrestaShop SA
 */

if (!defined('_CAN_LOAD_FILES_')) {
    exit;
}

use PrestaShop\PrestaShop\Core\Module\WidgetInterface;

class Llw_excludeproducts extends Module implements WidgetInterface
{
    public function __construct()
    {
        $this->name = 'llw_excludeproducts';
        $this->tab = 'administration';
        $this->version = '1.0.0';
        $this->author = 'La Ligne Web';
        $this->need_instance = 0;

        $this->bootstrap = true;
        parent::__construct();

        $this->init();

        $this->displayName = $this->trans('Exclusion de produits', array(), 'Modules.Mailalerts.Admin');
        $this->description = $this->trans('Exclusion de produits dans la page catégorie', array(), 'Modules.Mailalerts.Admin');
        $this->ps_versions_compliancy = [
            'min' => '1.7.1.0',
            'max' => _PS_VERSION_,
        ];
    }

    protected function init()
    {

    }

    public function install($delete_params = true)
    {
        if (!parent::install() ||
            !$this->registerHook('displayExclusionFilters') ||
            !$this->registerHook('actionProductSearchAfter') ||
            !$this->registerHook('displayHeader')) {
            return false;
        }

        if ($delete_params) {
            Configuration::updateValue('LLW_KYO_EXCLUDE_FEATURE', '');
        }

        return true;
    }

    public function installTabs()
    {
        if (version_compare(_PS_VERSION_, '1.7.1', '>=')) {
            return true;
        }
        return true;
    }

    public function uninstall($delete_params = true)
    {
        if ($delete_params) {
            Configuration::deleteByName('LLW_KYO_EXCLUDE_FEATURE');
        }

        return parent::uninstall();
    }

    public function reset()
    {
        if (!$this->uninstall(false)) {
            return false;
        }
        if (!$this->install(false)) {
            return false;
        }

        return true;
    }

    public function getContent()
    {
        $this->html = '';

        $this->postProcess();

        $this->html .= $this->renderForm();

        return $this->html;
    }

    protected function postProcess()
    {
        $errors = array();

        if (Tools::isSubmit('submitData')) {
            if (!Configuration::updateValue('LLW_KYO_EXCLUDE_FEATURE', (string) Tools::getValue('LLW_KYO_EXCLUDE_FEATURE'))
            ) {
                $errors[] = $this->trans('Cannot update settings', array(), 'Modules.Mailalerts.Admin');
            } 
        }

        if (count($errors) > 0) {
            $this->html .= $this->displayError(implode('<br />', $errors));
        } else if (Tools::isSubmit('submitData')) {
            $this->html .= $this->displayConfirmation($this->trans('Settings updated successfully', array(), 'Modules.Mailalerts.Admin'));
        }

        $this->init();
    }

    public function renderForm()
    {
        $fields_form_1 = array(
            'form' => array(
                'legend' => array(
                    'title' 		=> $this->trans('Caractéristique pour l\'exclusion', array(), 'Modules.Mailalerts.Admin'),
                    'icon' 			=> 'icon-cog',
                ),
                'input' => array(
		            array(
		                'type' 		=> 'text',
		                'label' 	=> $this->trans('ID de la caractéristique à exclure', array(), 'Modules.Mailalerts.Admin'),
		                'name' 		=> 'LLW_KYO_EXCLUDE_FEATURE',
		            ),
                ),
                'submit' => array(
                    'title' 		=> $this->trans('Save', array(), 'Admin.Actions'),
                    'class' 		=> 'btn btn-default pull-right',
                    'name' 			=> 'submitData',
                ),
            ),
        );
        
        $helper = new HelperForm();
        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $lang = new Language((int) Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language = $lang->id;
        $helper->module = $this;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submitKyoParamConfiguration';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false)
            .'&configure='.$this->name
            .'&tab_module='.$this->tab
            .'&module_name='.$this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars = array(
            'fields_value' => $this->getConfigFieldsValues(),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id,
        );

        return $helper->generateForm(array($fields_form_1));
    }

    public function getConfigFieldsValues()
    {
        return array(
            'LLW_KYO_EXCLUDE_FEATURE' => Tools::getValue('LLW_KYO_EXCLUDE_FEATURE', Configuration::get('LLW_KYO_EXCLUDE_FEATURE')),
        );
    }

    public function renderWidget($hookName = null, array $params = [])
    {
		if($hookName == "displayExclusionFilters") {
            $this->smarty->assign($this->getWidgetVariables($hookName, $params));
			return $this->fetch('module:llw_excludeproducts/views/templates/hook/facets.tpl');
		}

    }

    public function getWidgetVariables($hookName = null, array $params = [])
    {
		if($hookName == "displayExclusionFilters") {

			$features_to_exclude			= [];
			if(Configuration::get("LLW_KYO_EXCLUDE_FEATURE")) {
				$excluded_feature 			= Configuration::get("LLW_KYO_EXCLUDE_FEATURE");
				$feature_values 			= FeatureValue::getFeatureValuesWithLang($this->context->language->id, $excluded_feature);
				$features_to_exclude		= [];
		        foreach($feature_values as $feature_value) {
			        $features_to_exclude[]	= [
			        	'label' 			=> $feature_value['value'],
						'value'				=> $feature_value['id_feature_value'],
						'selected' 			=> isset($params['elements']) ? in_array($feature_value['id_feature_value'], $params['elements']) : false
					];
		        }
	        }

	        return [
		     	'features_to_exclude' 	=> $features_to_exclude,
				'from' 					=> isset($params['from']) ? $params['from'] : ''
	        ];
		}

    }

}
