<?php
/**
 * 2007-2020 PrestaShop.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2020 PrestaShop SA
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 * International Registered Trademark & Property of PrestaShop SA
 */

if (!defined('_CAN_LOAD_FILES_')) {
    exit;
}

use PrestaShop\PrestaShop\Core\Module\WidgetInterface;

include_once dirname(__FILE__).'/classes/LlwKyoparam.php';
include_once dirname(__FILE__).'/classes/PointLocation.php';

class Llw_kyoparam extends Module implements WidgetInterface
{
	
	protected $google_map_api_key;

	protected $slider_or_video;
	protected $video_source;
	protected $booking_link;

	protected $delai_mini_livraison;
	protected $delai_mini_emporter;
	protected $nb_minutes_tranche;
	
    public function __construct()
    {
        $this->name = 'llw_kyoparam';
        $this->tab = 'administration';
        $this->version = '1.0.0';
        $this->author = 'La Ligne Web';
        $this->need_instance = 0;

        $this->controllers = array('account');

        $this->bootstrap = true;
        parent::__construct();

        $this->init();

        $this->displayName = $this->trans('Paramètres Kyo Sushi', array(), 'Modules.Mailalerts.Admin');
        $this->description = $this->trans('Gestion des paramètres spécifiques Kyo Sushi.', array(), 'Modules.Mailalerts.Admin');
        $this->ps_versions_compliancy = [
            'min' => '1.7.1.0',
            'max' => _PS_VERSION_,
        ];
    }

    protected function init()
    {
        $this->google_map_api_key = (string) Configuration::get('LLW_KYO_GOOGLE_MAP_API_KEY');
        $this->delai_mini_livraison = Configuration::get('LLW_KYO_DELAI_MINI_LIVRAISON');
        $this->delai_mini_emporter = Configuration::get('LLW_KYO_DELAI_MINI_EMPORTER');
        $this->slider_or_video = Configuration::get('LLW_KYO_SLIDER_OR_VIDEO');
        $this->video_source = Configuration::get('LLW_KYO_VIDEO_SOURCE');
        $this->nb_minutes_tranche = Configuration::get('LLW_KYO_NB_MINUTES_TRANCHE');
        $this->booking_link = Configuration::get('LLW_KYO_BOOKING_LINK');
    }

    public function install($delete_params = true)
    {
        if (!parent::install() ||
            !$this->registerHook('actionValidateOrder') ||
            !$this->registerHook('actionSetDeliveryAddressBefore') ||
            !$this->registerHook('displayHeader')) {
            return false;
        }

        if ($delete_params) {
            Configuration::updateValue('LLW_KYO_GOOGLE_MAP_API_KEY', '');
            Configuration::updateValue('LLW_KYO_DELAI_MINI_LIVRAISON', '');
            Configuration::updateValue('LLW_KYO_DELAI_MINI_EMPORTER', '');
            Configuration::updateValue('LLW_KYO_SLIDER_OR_VIDEO', '');
            Configuration::updateValue('LLW_KYO_VIDEO_SOURCE', '');
            Configuration::updateValue('LLW_KYO_NB_MINUTES_TRANCHE', '');
            Configuration::updateValue('LLW_KYO_BOOKING_LINK', '');
        }

        return true;
    }

    public function installTabs()
    {
        if (version_compare(_PS_VERSION_, '1.7.1', '>=')) {
            return true;
        }
        return true;
    }

    public function uninstall($delete_params = true)
    {
        if ($delete_params) {
            Configuration::deleteByName('LLW_KYO_GOOGLE_MAP_API_KEY');
            Configuration::deleteByName('LLW_KYO_DELAI_MINI_LIVRAISON');
            Configuration::deleteByName('LLW_KYO_DELAI_MINI_EMPORTER');
            Configuration::deleteByName('LLW_KYO_SLIDER_OR_VIDEO', '');
            Configuration::deleteByName('LLW_KYO_VIDEO_SOURCE', '');
            Configuration::deleteByName('LLW_KYO_NB_MINUTES_TRANCHE', '');
            Configuration::deleteByName('LLW_KYO_BOOKING_LINK', '');
        }

        return parent::uninstall();
    }

    public function reset()
    {
        if (!$this->uninstall(false)) {
            return false;
        }
        if (!$this->install(false)) {
            return false;
        }

        return true;
    }

    public function getContent()
    {
        $this->html = '';

        $this->postProcess();

        $this->html .= $this->renderForm();

        return $this->html;
    }

    protected function postProcess()
    {
        $errors = array();

        if (Tools::isSubmit('submitGoogleMapData')) {
            if (!Configuration::updateValue('LLW_KYO_GOOGLE_MAP_API_KEY', (string) Tools::getValue('LLW_KYO_GOOGLE_MAP_API_KEY'))
            ) {
                $errors[] = $this->trans('Cannot update settings', array(), 'Modules.Mailalerts.Admin');
            } 
        } elseif (Tools::isSubmit('submitHoraires')) {
            if (!Configuration::updateValue('LLW_KYO_DELAI_MINI_LIVRAISON', (int) Tools::getValue('LLW_KYO_DELAI_MINI_LIVRAISON'))) {
                $errors[] = $this->trans('Cannot update settings', array(), 'Modules.Mailalerts.Admin');
            } elseif (!Configuration::updateValue('LLW_KYO_DELAI_MINI_EMPORTER', (int) Tools::getValue('LLW_KYO_DELAI_MINI_EMPORTER'))) {
                $errors[] = $this->trans('Cannot update settings', array(), 'Modules.Mailalerts.Admin');
            } elseif (!Configuration::updateValue('LLW_KYO_NB_MINUTES_TRANCHE', (int) Tools::getValue('LLW_KYO_NB_MINUTES_TRANCHE'))) {
                $errors[] = $this->trans('Cannot update settings', array(), 'Modules.Mailalerts.Admin');
            }
        } elseif (Tools::isSubmit('submitHome')) {
            if (!Configuration::updateValue('LLW_KYO_SLIDER_OR_VIDEO', (int) Tools::getValue('LLW_KYO_SLIDER_OR_VIDEO'))) {
                $errors[] = $this->trans('Cannot update settings', array(), 'Modules.Mailalerts.Admin');
            } elseif (!Configuration::updateValue('LLW_KYO_VIDEO_SOURCE', (string) Tools::getValue('LLW_KYO_VIDEO_SOURCE'))) {
                $errors[] = $this->trans('Cannot update settings', array(), 'Modules.Mailalerts.Admin');
            } elseif (!Configuration::updateValue('LLW_KYO_BOOKING_LINK', (string) Tools::getValue('LLW_KYO_BOOKING_LINK'))) {
                $errors[] = $this->trans('Cannot update settings', array(), 'Modules.Mailalerts.Admin');
            }
        }

        if (count($errors) > 0) {
            $this->html .= $this->displayError(implode('<br />', $errors));
        } else if (Tools::isSubmit('submitGoogleMapData') || Tools::isSubmit('submitHoraires')) {
            $this->html .= $this->displayConfirmation($this->trans('Settings updated successfully', array(), 'Modules.Mailalerts.Admin'));
        }

        $this->init();
    }

    public function renderForm()
    {
        $fields_form_1 = array(
            'form' => array(
                'legend' => array(
                    'title' 		=> $this->trans('Données Google', array(), 'Modules.Mailalerts.Admin'),
                    'icon' 			=> 'icon-cog',
                ),
                'input' => array(
		            array(
		                'type' 		=> 'text',
		                'label' 	=> $this->trans('Clé API Google', array(), 'Modules.Mailalerts.Admin'),
		                'name' 		=> 'LLW_KYO_GOOGLE_MAP_API_KEY',
		                'desc' 		=> 'Cette clé est générée sur <a href="https://console.cloud.google.com" target="_blank">console.cloud.google.com</a>, site sur lequel les API suivantes doivent être activées : Geocoding API, Maps JavaScript API et Places API.',
		            ),
                ),
                'submit' => array(
                    'title' 		=> $this->trans('Save', array(), 'Admin.Actions'),
                    'class' 		=> 'btn btn-default pull-right',
                    'name' 			=> 'submitGoogleMapData',
                ),
            ),
        );
        
        $fields_form_2 = array(
            'form' => array(
                'legend' => array(
                    'title' => $this->trans('Délais et horaires', array(), 'Modules.Mailalerts.Admin'),
                    'icon' => 'icon-clock-o',
                ),
                'input' => array(
		            array(
		                'type' 		=> 'text',
		                'label' 	=> $this->trans('Délai minimum pour une livraison', array(), 'Modules.Mailalerts.Admin'),
		                'name' 		=> 'LLW_KYO_DELAI_MINI_LIVRAISON',
		                'desc' 		=> '',
		            ),
		            array(
		                'type' 		=> 'text',
		                'label' 	=> $this->trans('Délai minimum pour une emportée', array(), 'Modules.Mailalerts.Admin'),
		                'name' 		=> 'LLW_KYO_DELAI_MINI_EMPORTER',
		                'desc' 		=> '',
		            ),
		            array(
		                'type' 		=> 'text',
		                'label' 	=> $this->trans('Minutes par tranche horaire proposée', array(), 'Modules.Mailalerts.Admin'),
		                'name' 		=> 'LLW_KYO_NB_MINUTES_TRANCHE',
		                'desc' 		=> '',
		            ),
                ),
                'submit' => array(
                    'title' => $this->trans('Save', array(), 'Admin.Actions'),
                    'class' => 'btn btn-default pull-right',
                    'name' => 'submitHoraires',
                ),
            ),
        );

        $fields_form_3 = array(
            'form' => array(
                'legend' => array(
                    'title' => $this->trans('Page d\'accueil', array(), 'Modules.Mailalerts.Admin'),
                    'icon' => 'icon-home',
                ),
                'input' => array(
                    array(
                        'type' => 'switch',
                        'is_bool' => false, //retro compat 1.5
                        'label' => $this->trans('Carrousel ou vidéo ?', array(), 'Modules.Mailalerts.Admin'),
                        'name' => 'LLW_KYO_SLIDER_OR_VIDEO',
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => 'Carrousel',
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 2,
                                'label' => 'Vidéo',
                            ),
                        ),
                    ),
		            array(
		                'type' => 'textarea',
		                'cols' => 36,
		                'rows' => 4,
		                'label' 	=> $this->trans('Source de la vidéo', array(), 'Modules.Mailalerts.Admin'),
		                'name' 		=> 'LLW_KYO_VIDEO_SOURCE',
		                'desc' 		=> '',
		            ),
		            array(
		                'type' => 'textarea',
		                'cols' => 36,
		                'rows' => 2,
		                'label' 	=> $this->trans('Lien de réservation', array(), 'Modules.Mailalerts.Admin'),
		                'name' 		=> 'LLW_KYO_BOOKING_LINK',
		                'desc' 		=> '',
		            ),
                ),
                'submit' => array(
                    'title' => $this->trans('Save', array(), 'Admin.Actions'),
                    'class' => 'btn btn-default pull-right',
                    'name' => 'submitHome',
                ),
            ),
        );

        $helper = new HelperForm();
        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $lang = new Language((int) Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language = $lang->id;
        $helper->module = $this;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submitBastaParamConfiguration';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false)
            .'&configure='.$this->name
            .'&tab_module='.$this->tab
            .'&module_name='.$this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars = array(
            'fields_value' => $this->getConfigFieldsValues(),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id,
        );

        return $helper->generateForm(array($fields_form_1, $fields_form_2, $fields_form_3));
    }

    public function getConfigFieldsValues()
    {
        return array(
            'LLW_KYO_GOOGLE_MAP_API_KEY' => Tools::getValue('LLW_KYO_GOOGLE_MAP_API_KEY', Configuration::get('LLW_KYO_GOOGLE_MAP_API_KEY')),
            'LLW_KYO_DELAI_MINI_LIVRAISON' => Tools::getValue('LLW_KYO_DELAI_MINI_LIVRAISON', Configuration::get('LLW_KYO_DELAI_MINI_LIVRAISON')),
            'LLW_KYO_DELAI_MINI_EMPORTER' => Tools::getValue('LLW_KYO_DELAI_MINI_EMPORTER', Configuration::get('LLW_KYO_DELAI_MINI_EMPORTER')),
            'LLW_KYO_SLIDER_OR_VIDEO' => Tools::getValue('LLW_KYO_SLIDER_OR_VIDEO', Configuration::get('LLW_KYO_SLIDER_OR_VIDEO')),
            'LLW_KYO_VIDEO_SOURCE' => Tools::getValue('LLW_KYO_VIDEO_SOURCE', Configuration::get('LLW_KYO_VIDEO_SOURCE')),
            'LLW_KYO_NB_MINUTES_TRANCHE' => Tools::getValue('LLW_KYO_NB_MINUTES_TRANCHE', Configuration::get('LLW_KYO_NB_MINUTES_TRANCHE')),
            'LLW_KYO_BOOKING_LINK' => Tools::getValue('LLW_KYO_BOOKING_LINK', Configuration::get('LLW_KYO_BOOKING_LINK')),
        );
    }

    public function hookActionSetDeliveryAddressBefore($param)
    {
	    return $this->renderWidget("actionSetDeliveryAddressBefore", $param);
	}
	
	public function hookDisplayCheckoutAddressesTop($param) {
	    return $this->renderWidget("displayCheckoutAddressesTop", $param);
	}
	
	public function hookDisplayGoogleMapModal($param) {
	    return $this->renderWidget("displayGoogleMapModal", $param);
	}
	
	public function hookDisplayNav3($param) {
	    return $this->renderWidget("displayNav3", $param);
	}
	
	public function hookDisplayCurrentDiscount($param) {
	    return $this->renderWidget("displayCurrentDiscount", $param);
	}
	
    public function renderWidget($hookName = null, array $params = [])
    {
		//echo "renderWidget hookName=$hookName<br>";
		if($hookName == "actionSetDeliveryAddressBefore") {
	        return $this->isAddressInsideDeliveryZone($params);
		}

		if($hookName == "displayCheckoutAddressesTop") {
            $this->smarty->assign($this->getWidgetVariables($hookName, $params));
			return $this->fetch('module:llw_kyoparam/views/templates/hook/googleMapButton.tpl');
		}

        if (!$this->isCached($this->templateFile, $this->getCacheId('llw_kyoparam'))) {
            $this->smarty->assign($this->getWidgetVariables($hookName, $params));
        }

        return $this->fetch($this->templateFile, $this->getCacheId('llw_kyoparam'));
    }

    public function getWidgetVariables($hookName = null, array $params = [])
    {


        return array(

        );
    }

    public function isAddressInsideDeliveryZone($data) {

        $context 							= Context::getContext();
		$id_shop							= $data['shop']->id;
		$id_address  						= $data['id_address_delivery'];

		$shop 								= $data['shop'];
		$shopAddress 						= $shop->getAddress();
		$userAddress						= new Address($id_address);
		$default_lang 						= Configuration::get('PS_LANG_DEFAULT');
        $shopCountry 						= Country::getNameById($default_lang, $shopAddress->id_country);
        $userCountry 						= Country::getNameById($default_lang, $userAddress->id_country);
        $dest_address 						= $userAddress->address1.' '.$userAddress->address2.' '.$userAddress->postcode.' '.$userAddress->city.' '.$userCountry;

		return true;
    }

}
