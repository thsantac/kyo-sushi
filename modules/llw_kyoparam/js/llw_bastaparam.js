/**
 * 2007-2020 PrestaShop.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2020 PrestaShop SA
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 * International Registered Trademark & Property of PrestaShop SA
 */

var area	= [];
var shopId 	= 1;

$('document').ready(function()
{
	var request = new XMLHttpRequest();
	request.open("GET", "/modules/llw_bastaparam/kml/shop_"+shopId+".kml", false); // filename = kml_filename
	xmlDocObj = $($.parseXML(request.responseText));  
	var coordinates = xmlDocObj.find("Placemark.Polygon.coordinates");
	coordinates.each(function (index) {
		var latLong = this.split(',');
		var coord = {
			lat: latLong [0],
			lng: latLong [1]
		}
	
		area.push(coord);
	}
});

function isMarkerInPolygon(marker, polygon){
    var message = google.maps.geometry.poly.containsLocation(marker.latLng, polygon) ?
              'Inside Polygon' :
              'Outside Polygon';
    return message;
}

var map;
var marker;
var polygon;
var bounds;
window.onload = initMap;
function initMap() {
	map = new google.maps.Map(document.getElementById('map'), {
		center: center,
		zoom: 14,
		scaleControl: true
	});
	bounds = new google.maps.LatLngBounds();
	google.maps.event.addListenerOnce(map, 'tilesloaded', function(evt) { 
		bounds = map.getBounds();
	});

	marker = new google.maps.Marker({
		position: center
	});
	polygon = new google.maps.Polygon({
		path: area,
		geodesic: true,
	});

	polygon.setMap(map);	
	  
	var input = /** @type {!HTMLInputElement} */(
		document.getElementById('pac-input'));
        var types = document.getElementById('type-selector');
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(types);

        var autocomplete = new google.maps.places.Autocomplete(input);
        autocomplete.addListener('place_changed', function() {
		marker.setMap(null);
		var place = autocomplete.getPlace();
		var newBounds = new google.maps.LatLngBounds();
		newBounds = bounds;
          if (!place.geometry) {
            window.alert("Autocomplete's returned place contains no geometry");
            return;
          };
		  marker.setPosition(place.geometry.location);
		  marker.setMap(map);
		  newBounds.extend(place.geometry.location);
		  map.fitBounds(newBounds);
		  if (google.maps.geometry.poly.containsLocation(place.geometry.location, polygon)){
			alert('The area contains the address');  
		  } else {
			alert('The address is outside of the area.');  
		  };
	   });
}

var center 	= new google.maps.LatLng(41.3899621, 2.1469796);
