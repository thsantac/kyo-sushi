{**
* 2018 Anvanto
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author Anvanto (anvantoco@gmail.com)
*  @copyright  2018 anvanto.com

*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

.notification_cookie {
    background:{$an_modal_cookie_background};
    opacity:{$an_modal_cookie_opacity}%;
    {if $an_modal_cookie_width}width:{$an_modal_cookie_width}px;{/if}
    {if $an_modal_cookie_height}height:{$an_modal_cookie_height}px;{/if}
    {if $an_modal_cookie_position == 'bl' || !$an_modal_cookie_position}
    bottom: 30px;
    left: 30px;
    {/if}
    {if $an_modal_cookie_position == 'br'}
    bottom: 30px;
    right: 30px;
    {/if}
    {if $an_modal_cookie_position == 'tl'}
    top: 30px;
    left: 30px;
    {/if}
    {if $an_modal_cookie_position == 'tr'}
    top: 30px;
    right: 30px;
    {/if}
}

.notification_cookie a, .notification_cookie span {
    color: {$an_modal_cookie_links_color};
}
.notification_cookie p,
.notification_cookie {
    color: {$an_modal_cookie_text_color};
}
