{*
* 2007-2014 PrestaShop
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
* @author    PrestaShop SA <contact@prestashop.com>
* @copyright 2007-2014 PrestaShop SA
* @license   http://addons.prestashop.com/en/content/12-terms-and-conditions-of-use
* International Registered Trademark & Property of PrestaShop SA
*}

<h3><i class="icon-book"></i> {l s='Documentation' mod='atos'} <small>{$module_display|escape:'htmlall':'UTF-8'}</small></h3>
<p>{l s='The module Sips (Atos Worldline) is a remote payment solution to allow customers to use all of its functionalities with all distance selling channels.' mod='atos'}</p>
<ul>
	<li><b>{l s='Designed to provide a very high level of security.' mod='atos'}</b></li>
	<li><b>{l s='PayLib and 3D Secure included.' mod='atos'}</b></li>
</ul>
<p>&nbsp;</p>
<div class="media">
	<a class="pull-left" target="_blank" href="{$guide_link3|escape:'htmlall':'UTF-8'}">
		<img heigh="64" width="64" class="media-object" src="{$module_dir|escape:'htmlall':'UTF-8'}/views/img/pdf.png" alt="" title=""/>
	</a>
	<div class="media-body">
		<h4 class="media-heading"><p>{l s='Attached you will find the documentation for your module. Please consult it to properly configure the module.' mod='atos'}</p></h4>
		<p><a href="{$module_dir|escape:'htmlall':'UTF-8'}{$guide_link3|escape:'htmlall':'UTF-8'}" target="_blank">{l s='Documentation Utilisateur (Only in French)' mod='atos'}</a></p>
		<p><a href="{$module_dir|escape:'htmlall':'UTF-8'}{$guide_link1|escape:'htmlall':'UTF-8'}" target="_blank">{l s='Atos integration guide (Only in French)' mod='atos'}</a></p>
		<p><a href="{$module_dir|escape:'htmlall':'UTF-8'}{$guide_link2|escape:'htmlall':'UTF-8'}" target="_blank">{l s='Atos customization guide (Only in French)' mod='atos'}</a></p>
		<p>{l s='Access to Prestashop free documentation: ' mod='atos'} <a href="http://doc.prestashop.com/" target="_blank">{l s='Click here' mod='atos'}</a></p>
	</div>
</div>

<p>&nbsp;</p>
<p>{l s='Need help? You will find it on' mod='atos'} <a href="#contacts" class="contactus" data-toggle="tab" data-original-title="" title="{l s='Need help?' mod='atos'}">{l s='Contact tab.' mod='atos'}</a></p>