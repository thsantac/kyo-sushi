<h1 class="text-center">{l s='Your order is being processed, please wait...' mod='atos'}</h1>
{literal}
<script type="text/javascript">
setTimeout(function() {
	var newLoc = "{/literal}{$orderConfirmationUrl nofilter}{literal}";
	console.log(newLoc);
	location.href = newLoc;
}, 3000);
</script>
{/literal}
