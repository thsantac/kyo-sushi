<?php

/**
 * 2007-2016 PrestaShop
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 * @file        orderconfirmation.php
 * @author      PrestaShop SA <contact@prestashop.com>
 * @copyright   2007-2014 PrestaShop SA
 * @license     http://addons.prestashop.com/en/content/12-terms-and-conditions-of-use
 *
 * International Registered Trademark & Property of PrestaShop SA
 */
class AtosOrderconfirmationModuleFrontController extends ModuleFrontController
{
    /**
     * @throws PrestaShopDatabaseException
     */
    public function initContent()
    {
        $this->display_column_left = false;
        parent::initContent();
        $orderConfirmationUrl = $this->getOrderConfirmationUrl();
        $this->context->smarty->assign('orderConfirmationUrl', $orderConfirmationUrl);
        $this->template = _PS_MODULE_DIR_.'atos/views/templates/front/orderconfirmation.tpl';
    }

    public function getOrderConfirmationUrl()
    {
        $id_cart = Tools::getValue('id_cart');
        $id_customer = Tools::getValue('id_customer');
        $secure_key = Tools::getValue('secure_key');
        $id_order = Order::getIdByCartId((int) ($id_cart));
        $order = new Order($id_order);
        $secure_key = $order->secure_key;

        $orderConfirmationUrl = (Configuration::get('PS_SSL_ENABLED') ? 'https://' : 'http://').htmlspecialchars($_SERVER['HTTP_HOST'], ENT_COMPAT, 'UTF-8').__PS_BASE_URI__.'index.php?controller=order-confirmation&key='.$secure_key.'&id_cart='.(int)$id_cart.'&id_module='.(int)$this->module->id.'&id_order='.(int)$id_order;

        return $orderConfirmationUrl;
    }
}
?>