<?php
/**
* 2007-2014 PrestaShop
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
* @author    PrestaShop SA <contact@prestashop.com>
* @copyright 2007-2014 PrestaShop SA
* @license   http://addons.prestashop.com/en/content/12-terms-and-conditions-of-use
* International Registered Trademark & Property of PrestaShop SA
*/

include(dirname(__FILE__).'/../../config/config.inc.php');
if (version_compare(_PS_VERSION_, '1.5', '<'))
	include(dirname(__FILE__).'/../../init.php');
include(dirname(__FILE__).'/atos.php');

$datas = Tools::getValue('DATA');
if (empty($datas))
	Tools::redirect('index.php');
else
{
	$atos = new Atos();
	$is_win = (Tools::strtoupper(Tools::substr(PHP_OS, 0, 3)) === 'WIN');
	$exec = $atos->bin_dir.'response'.(((int)$is_win === 1) ? '.exe' : '');
	$result = exec($exec.' pathfile='.dirname(__FILE__).'/pathfile message='.preg_replace('#[^a-z0-9]#Ui', '', $datas));
	$result_array = explode('!', $result);
	if (!empty($result_array))
	{
		$cart = new Cart((int)$result_array[22]);
		$customer = new Customer((int)$cart->id_customer);

		if (version_compare(_PS_VERSION_, '1.5', '>'))
		{
			$back_url = 'index.php?controller=order&step=3';
			$return_page = 'index.php?controller=order-confirmation&id_cart='.$cart->id.'&id_module='.$atos->id.'&key='.$customer->secure_key;
		}
		else
		{
			$back_url = 'order.php?step=3';
			$return_page = 'order-confirmation.php?id_cart='.$cart->id.'&id_module='.$atos->id.'&key='.$customer->secure_key;
		}
		$cancel_page = (Configuration::get('ATOS_ERROR_BEHAVIOR') ? $back_url : $return_page);

		Tools::redirect($cancel_page);
	}
	else
		Tools::redirect('index.php');
}