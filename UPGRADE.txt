admin13/themes/default/public/theme.css
---------------------------------------
1:
::-webkit-scrollbar-track
{
	-webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
	background-color: #F5F5F5;
}

::-webkit-scrollbar
{
	width: 10px;
	background-color: #F5F5F5;
}

::-webkit-scrollbar-thumb
{
	background-color: #c11334;
}

6634 :
.bootstrap .form-horizontal.llw_categoryproducts .form-group {
    margin-bottom: 5px;
}
.bootstrap .form-horizontal.llw_categoryproducts .form-group .form-group {
    display: none;
}
6689 :
.bootstrap .btn.active:focus,
.bootstrap .btn:active:focus,
.bootstrap .btn:focus {
    outline: none;
}
10075 :
.bootstrap .tooltip-inner p {
	color: #FFF;
	font-size: 12px;
	line-height: 16px;
}
11113 :
#content.bootstrap .panel-heading,
#content.bootstrap h3:not(.modal-title) {
    border: none;
    border-bottom: 1px solid #eee;
    font-size: 1.2em;
    height: 2.2em;
    line-height: 2.2em;
    margin: -16px -9px 10px !important;
    padding: 0 0 0 5px;
    text-transform: uppercase
}

#header_infos : box-shadow: 0px 3px 7px 0px rgba(0,0,0,0.1);

11407 :
#header_infos #header_logo {
    background: url(fbc2a379fdad6d5a1b10c881644bc5dd.png) no-repeat 0;
    height: auto;
    width: 202px;
    background-size: contain;
    background-position-x: left;
}

... :
#header_infos #shop_version {
    color: #887e74;
    font-size: 14px;
    left: 9rem;
    position: absolute;
    top: 11px;
    font-weight: 700;
}

#header_infos #quick_select : color: #fff; padding: 7px 15px 5px;

11873 :
#header_quick {
    font-size: 13px;
    background-color: #a5102d;
}

.bootstrap #header_quick .dropdown-menu {
	border-radius: 0px;
	margin-top: 0px;
}

11894 :
#header_quick .dropdown>a:hover {
    color: #fff !important
    background-color: #a5102d;
}
#header_quick .dropdown-menu>li a {
    padding: 3px 15px;
    font-size: 13px;
}

14497 :
.bootstrap .page-head .page-title : font-weight: 600;
.bootstrap .page-head h4.page-subtitle {
    float: left;
    font-family: Open Sans;
    left: 226px;
    margin-top: 66px;
    position: absolute;
    font-size: 13px;
}

15905 :
.bootstrap #login {
    height: 100%;
    background-image: url(../img/bg_login.jpg);
    background-repeat: no-repeat;
    background-size: cover;
}

.bootstrap #content {
    height: 100%;
}

.bootstrap #shop-img : width: 198px;

15937 :
.bootstrap #login-panel {
    margin: 0 auto;
    width: 560px;
    padding: 0 2rem;
    background: rgba(0,0,0,0.6);
    height: 100%;
}

.bootstrap #login-panel h4 {
	display: none;
}

.bootstrap #login-panel .flip-container : margin-top: 145px;

16010 :
.bootstrap #login-panel .back,
.bootstrap #login-panel .front {
    -webkit-backface-visibility: hidden;
    -webkit-transform-style: preserve-3d;
    -webkit-transition: .6s;
    backface-visibility: hidden;
    left: 0;
    padding: 40px;
    position: absolute;
    top: 0;
    transform-style: preserve-3d;
    transition: .6s;
    width: 100%;
    border-radius: 10px;
    background-color: rgba(136,125,116,.8);
}

.bootstrap #login-panel label.control-label,
.bootstrap #login-panel label {
    color: #fff;
}

.bootstrap #login-panel a,
.bootstrap #login-panel a:hover {
    color: #fff;
}

21058 :
.ps__rail-x {
    bottom: 0;
    height: 15px;
    display: none;
}
.ps__rail-y {
    right: 0;
    width: 15px;
    display: none;
}

CHANGE : 
#00aff0 to #a5102d
#2eacce to #a5102d
#555 to #a5102d
#008abd to #92001c
#31708f to #a5102d
#0077a4 to #8e001d
#c7d6db to #dac1c6
#66afe9 to #e46d84
#363a41 to #887d74
#23829c to #92001c
#25b9d7 to #c52544
#4e6167 to #887e74
#202226 to #29211b
#1e94ab to #8d132b

admin13/themes/default/public/fbc2a379fdad6d5a1b10c881644bc5dd.png ==> dans Sites/Kyo-Sushi/sushi-marseille
admin13/themes/default/template/controllers/login/content.tpl ==> dans Sites/Kyo-Sushi/sushi-marseille/login-content.tpl
admin13/themes/default/template/controllers/shop/content.tpl ==> dans Sites/Kyo-Sushi/sushi-marseille/shop-content.tpl
admin13/themes/default/template/controllers/stores/helpers/form/form.tpl ==> dans Sites/Kyo-Sushi/sushi-marseille/form-1.tpl
admin13/themes/default/template/helpers/form/form.tpl ==> dans Sites/Kyo-Sushi/sushi-marseille/form-2.tpl
admin13/themes/new-theme/public/theme.css
-----------------------------------------
216:
::-webkit-scrollbar-track
{
	-webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
	background-color: #F5F5F5;
}

::-webkit-scrollbar
{
	width: 10px;
	background-color: #F5F5F5;
}

::-webkit-scrollbar-thumb
{
	background-color: #c11334;
}

4807 :
.btn-outline-primary:not(:disabled):not(.disabled).active:focus,
.btn-outline-primary:not(:disabled):not(.disabled):active:focus,
.show>.btn-outline-primary.dropdown-toggle:focus {

}

5087 :
.fade.show,
.fade.in {
    opacity: 1
}

5294 :
.dropdown-item.active,
.dropdown-item:active {
    color: #fff !important;
    text-decoration: none;
    background-color: #de3253
}

14283 :
.table tr.a_emporter {
	background-color: #f4f0eb;
}

.table tr.a_livrer {
	background-color: #e0dad3;
}

15426 :
.main-header : box-shadow: 0px 3px 7px 0px rgba(0,0,0,0.1);

15477 :
.main-header>.logo {
    background: url(fbc2a379fdad6d5a1b10c881644bc5dd.png) no-repeat 0;
    height: auto;
    width: 202px;
    background-size: contain;
    background-position-x: left;
}

.main-header #shop_version {
    color: #887e74;
    font-size: 14px;
    left: 9rem;
    position: absolute;
    top: 11px;
    font-weight: 700;
}

.main-header #quick-access-container {
	font-size: 13px;
    background-color: #a5102d;
}

.main-header #quick-access-container #quick_select,
.main-header #quick-access-container #quick_select:hover {
    color: #fff;
    height: 100%;
    font-weight: 400;
    font-size: 13px;
    font-size: .8125rem;
    text-decoration: none;
    letter-spacing: normal
}

.main-header #quick-access-container #quick_select:hover {
    color: #fff
}

.main-header #header-search-container .dropdown-menu>a,
.main-header #quick-access-container .dropdown-menu>a {
    color: inherit;
    border-bottom: 0;
    padding: 3px 15px;
}

.nav-bar {
    -webkit-transition: all .5s ease-out;
    background: #42372e;
    bottom: 0;
    height: 100%;
    margin-top: 2.5rem;
    overflow-y: auto;
    position: fixed;
    top: 0;
    transition: all .5s ease-out;
    width: 210.08px;
    width: 13.13rem;
    z-index: 502
}

19044 :
#categoriesShowcaseCard {
	display: none;
}
body#admincategories h1 {
	font-family: "Open Sans";
	color: #9c9287;
	font-size: 40px;
	font-weight: 100;
}
body#admincategories h1 strong {
	font-weight: 600;
}

.product-page .category-tree-overflow ul.category-tree label : font-size: 12px;

.module-list .module-column-select : width: 108px;

.ps--active-x>.ps__rail-x,
.ps--active-y>.ps__rail-y {
    display: none;
    background-color: transparent;
}

#00aff0 to #a5102d
#2eacce to #a5102d
#555 to #a5102d
#008abd to #92001c
#31708f to #a5102d
#0077a4 to #8e001d
#c7d6db to #dac1c6
#66afe9 to #e46d84
#363a41 to #887d74
#23829c to #92001c
#25b9d7 to #de3253
#4e6167 to #887e74
#202226 to #29211b
#6c868e to #9e757d
#3ed2f0 to #f94e6f


admin13/themes/new-theme/public/fbc2a379fdad6d5a1b10c881644bc5dd.png ==> dans Sites/Kyo-Sushi/sushi-marseille

config/smartyfront.config.inc.php :
-----------------------------------
Line 56 : 
if (Module::isEnabled('pagecache')) {
	require_once _PS_MODULE_DIR_ . 'pagecache/pagecache.php';
	smartyRegisterFunction($smarty, 'block', 'widget_block', array('PageCache', 'smartyWidgetBlockPageCache'));
	$smarty->registerFilter('pre', array('PageCache', 'smartyWidgetBlockPageCachePrefilter'));
} else {
	smartyRegisterFunction($smarty, 'block', 'widget_block', 'smartyWidgetBlock');
}

controllers/admin/AdminStoresController.php
-------------------------------------------
Line 227 (latitude) :    'maxlength' => 18,
postProcess() : 
347 :
//$_POST['latitude'] = number_format((float) $_POST['latitude'], 18);
//$_POST['longitude'] = number_format((float) $_POST['longitude'], 18);
364 :
$latitude = Tools::getValue('latitude');
$longitude = Tools::getValue('longitude');

js/tiny_mce/skins/prestashop/content.min.css

src/Adapter/Image/ImageRetriever.php
------------------------------------
$extPath = $imageFolderPath . DIRECTORY_SEPARATOR . 'fileType';
// #TS --------------------------------------------------------
//$ext = @file_get_contents($extPath) ?: 'jpg';
$ext = 'jpg';

foreach ($image_types as $image_type) {
    $resizedImagePath = implode(DIRECTORY_SEPARATOR, [
        $imageFolderPath,
        $id_image . '-' . $image_type['name'] . '.' . $ext,
    ]);

    // #TS --------------------------------------------------------
    /*if (!file_exists($resizedImagePath)) {
        ImageManager::resize(
            $mainImagePath,
            $resizedImagePath,
            (int) $image_type['width'],
            (int) $image_type['height']
        );
    }*/

src/Adapter/Order/CommandHandler/AddProductToOrderHandler.php
-------------------------------------------------------------
// #TS -----------------------------------------------------------------------------
$total_shipping_tax_excl = (float) $cart->getTotalShippingCost(null, false);
$invoice->total_shipping_tax_excl = (float) $total_shipping_tax_excl['value'];
$total_shipping_tax_incl = (float) $cart->getTotalShippingCost();
$invoice->total_shipping_tax_incl = $total_shipping_tax_incl['value'];

src/Adapter/Order/QueryHandler/GetOrderForViewingHandler.php
------------------------------------------------------------
            $this->getLinkedOrders($order),
            $order->delivery_when

src/Core/Domain/Order/QueryResult/OrderForViewing.php
-----------------------------------------------------
185:
    private $delivery_when;
250 :
        $delivery_when
    ) {
282 :
       $this->delivery_when = $delivery_when;
    }
322 :
    /**
     * @return string
     */
    public function getReference(): string
    {
        return $this->reference;
    }

    /**
     * @return string
     */
    public function getdelivery_when(): string
    {
        return $this->delivery_when;
    }

src/Adapter/Presenter/Order/OrderDetailLazyArray.php
----------------------------------------------------
211 :
    public function getDelivery()
    {
        if($this->order->to_be_delivered)
        	$delivery_when		= "Commande à livrer ";
        else
        	$delivery_when		= "Commande à emporter ";
		$delivery_when		   .= str_replace("Aujourd'hui", "le ".date("d/m/Y"), $this->order->delivery_when);
        return $delivery_when;
    }


src/Adapter/Presenter/Cart/CartPresenter.php
--------------------------------------------
369 :
        if (!$cart->isVirtualCart()) {
            $shippingCost = $cart->getTotalShippingCost(null, $this->includeTaxes());
        } else {
            $shippingCost = ['label' => 'OK', 'value' => 0];
        }

         // #TS - 20160315 -------------------------------------------------------------------------------
        $context 								= Context::getContext();
        if($context->cart->id_address_delivery) {
			$return 							= Cart::getDeliveryByZoneReturn($context->cart->getDeliveryByZone($context->cart->id_address_delivery));

	        if($return['delivery_ok']) {
		        $subtotals['shipping'] = [
		            'type' 		=> 'shipping',
		            'label' 	=> $this->translator->trans('Shipping', [], 'Shop.Theme.Checkout'),
		            'amount'	=> $shippingCost['value'],
		            'value' 	=> $this->getShippingDisplayValue($cart, $shippingCost['value']),
		        ];
	        }
	        else
		        $subtotals['shipping'] = [
		            'type' 		=> 'shipping',
		            'label' 	=> $this->translator->trans('Shipping', [], 'Shop.Theme.Checkout'),
		            'amount' 	=> -1,
		            'value' 	=> $return['error_code'],
		        ];
		}
		else {
	        if($shippingCost['label']=='OK') {
		        $subtotals['shipping'] = [
		            'type' => 'shipping',
		            'label' => $this->translator->trans('Shipping', [], 'Shop.Theme.Checkout'),
		            'amount' => $shippingCost['value'],
		            'value' => $this->getShippingDisplayValue($cart, $shippingCost['value']),
		        ];
	        }
			else {
		        $subtotals['shipping'] = [
		            'type' => 'shipping',
		            'label' => $this->translator->trans('Shipping', [], 'Shop.Theme.Checkout'),
		            'amount' => 0,
		            'value' => $shippingCost['label'],
		        ];
	        }
		}
		

src/Adapter/Presenter/Product/ProductLazyArray.php
--------------------------------------------------
use Context;
use Order;
use Cart;
...
		// #TS ---------------------------------------------------------------
        $this->addExtraInformation(
            $settings,
            $product,
            $language
        );

        parent::__construct();
        $this->appendArray($this->product);
...
    public function addExtraInformation(
        ProductPresentationSettings $settings,
        array $product,
        Language $language
    ) {
		$context 										= Context::getContext();
		$this->product['previously_ordered']			= false;
		if(isset($context->ordered_products) && is_array($context->ordered_products) && count($context->ordered_products)) {
			if(in_array($this->product['id_product'], $context->ordered_products)) {
				$this->product['previously_ordered']	= true;
			}
		}

		$this->product['in_cart']						= false;
		if(isset($context->cart_products) && is_array($context->cart_products) && count($context->cart_products)) {
			if(in_array($this->product['id_product'], $context->cart_products)) {
				$this->product['in_cart']				= true;
			}
		}
	}
	
src/Adapter/Product/PriceCalculator.php
---------------------------------------
156 :
        $idCustomization = 0,
        $to_be_delivered = null
180 :
            $idCustomization,
			$to_be_delivered

src/Core/Cart/Fees.php
----------------------
use Carrier;
...
    public function processCalculation(
        Cart $cart,
        CartRowCollection $cartRowCollection,
        $computePrecision,
        $id_carrier = null
    ) {
        if(empty($id_carrier))
        	$id_carrier						= Carrier::getCarrierForDeliveryMode();


src/Core/Cart/CartRow.php
-------------------------
334 :
        // The $null variable below is not used,
        // but it is necessary to pass it to getProductPrice because
        // it expects a reference.
        $specificPriceOutput = null;

        if($cart->delivery_mode==1) {
        	$to_be_delivered 			= 1;
        }
        else {
        	$to_be_delivered 			= 0;
        }
366 :
            $cartQuantity,
            (int) $rowData['id_customization'],
            $to_be_delivered
391 :
            (int) $rowData['id_customization'],
            $to_be_delivered


src/Core/Cart/AmountImmutable.php
---------------------------------
    public function __construct($taxIncluded = 0.0, $taxExcluded = 0.0)
    {
	    // #TS -------------------------------------------------------
	    // Les deux variables ne sont pas forcément numérique si elles proviennent 
	    // de la méthode getPackageShippingCost qui a été surclassée
		if(is_array($taxIncluded))
	        $this->setTaxIncluded(0.0);
		else
	        $this->setTaxIncluded($taxIncluded);
		if(is_array($taxExcluded))
	        $this->setTaxExcluded(0.0);
		else
	        $this->setTaxExcluded($taxExcluded);
    }


src/Core/Grid/Definition/Factory/OrderGridDefinitionFactory.php
---------------------------------------------------------------
256 :
        $columns->addAfter('new', (new DataColumn('delivery_mode'))
            ->setName($this->trans('Delivery', [], 'Admin.Global'))
            ->setOptions([
                'field' => 'delivery_mode',
            ])
        );
370 :
        //$orderCountriesChoices = $this->orderCountriesChoiceProvider->getChoices();
		$deliveryModeChoices = [
			'À livrer' => 1,
			'À emporter' => 0
		];
        if (!empty($deliveryModeChoices)) {
            $filters->add((new Filter('delivery_mode', ChoiceType::class))
                ->setTypeOptions([
                    'required' => false,
                    'choices' => $deliveryModeChoices,
                ])
                ->setAssociatedColumn('delivery_mode')
            );
        }

        if ($this->configuration->get('PS_B2B_ENABLE')) {
            $filters->add((new Filter('company', TextType::class))
                ->setTypeOptions([
                    'required' => false,
                    'attr' => [
                        'placeholder' => $this->trans('Search company', [], 'Admin.Actions'),
                    ],
                ])
                ->setAssociatedColumn('company')
            );
        }


src/Core/Grid/Query/OrderQueryBuilder.php
-----------------------------------------
89 :
        $qb = $this
            ->getBaseQueryBuilder($searchCriteria->getFilters())
            ->addSelect($this->getCustomerField() . ' AS `customer`')
            ->addSelect('o.id_order, o.reference, o.total_paid_tax_incl, os.paid, osl.name AS osname, o.to_be_delivered')
            ->addSelect('o.id_currency, cur.iso_code')
            ->addSelect('o.current_state, o.id_customer')
            ->addSelect('cu.`id_customer` IS NULL as `deleted_customer`')
            ->addSelect('os.color, o.payment, s.name AS shop_name')
            ->addSelect('o.date_add, cu.company, cl.name AS country_name, dm.name as delivery_mode, o.invoice_number, o.delivery_number')
        ;
137 :
        $qb = $this->connection
            ->createQueryBuilder()
            ->from($this->dbPrefix . 'orders', 'o')
            ->leftJoin('o', $this->dbPrefix . 'customer', 'cu', 'o.id_customer = cu.id_customer')
            ->leftJoin('o', $this->dbPrefix . 'currency', 'cur', 'o.id_currency = cur.id_currency')
            ->innerJoin('o', $this->dbPrefix . 'address', 'a', 'o.id_address_delivery = a.id_address')
            ->innerJoin('a', $this->dbPrefix . 'country', 'c', 'a.id_country = c.id_country')
            ->innerJoin('o', $this->dbPrefix . 'delivery_mode', 'dm', 'o.to_be_delivered = dm.id_delivery_mode')
            ->innerJoin(
                'c',
                $this->dbPrefix . 'country_lang',
                'cl',
                'c.id_country = cl.id_country AND cl.id_lang = :context_lang_id'
            )
            ->leftJoin('o', $this->dbPrefix . 'order_state', 'os', 'o.current_state = os.id_order_state')
            ->leftJoin(
                'os',
                $this->dbPrefix . 'order_state_lang',
                'osl',
                'os.id_order_state = osl.id_order_state AND osl.id_lang = :context_lang_id'
            )
            ->leftJoin('o', $this->dbPrefix . 'shop', 's', 'o.id_shop = s.id_shop')
            ->andWhere('o.`id_shop` IN (:context_shop_ids)')
            ->setParameter('context_lang_id', $this->contextLangId, PDO::PARAM_INT)
            ->setParameter('context_shop_ids', $this->contextShopIds, Connection::PARAM_INT_ARRAY)
        ;
164 :
        $strictComparisonFilters = [
            'id_order' => 'o.id_order',
            'country_name' => 'c.id_country',
            'delivery_mode' => 'dm.id_delivery_mode',
            'total_paid_tax_incl' => 'o.total_paid_tax_incl',
            'osname' => 'os.id_order_state',
        ];

src/Core/Product/Search/SortOrdersCollection.php
------------------------------------------------
53 :
        return [
            (new SortOrder('category', 'position', 'asc'))->setLabel(
                $this->translator->trans('Category', [], 'Shop.Theme.Catalog')
            ),
            (new SortOrder('product', 'position', 'desc'))->setLabel(
                $this->translator->trans('Relevance', [], 'Shop.Theme.Catalog')
            ),


src/PrestaShopBundle/Controller/Admin/Sell/Order/OrderController.php
--------------------------------------------------------------------
use Cart;
use Order;
...
504 :
        sort($paginationNumOptions);

		$order 								= new Order($orderId);
	    $cart 								= new Cart($order->id_cart);
        $complements						= $cart->getCartComplements($order->id_cart);
        $elements							= $cart->getUnwantedEements();

        return $this->render('@PrestaShop/Admin/Sell/Order/Order/view.html.twig', [
538 :
	        'complements' 	=> $complements,
	        'unwanted' 		=> $elements
        ]);
    }

src/PrestaShopBundle/Resources/views/Admin/Common/pagination.html.twig
----------------------------------------------------------------------
58 :
        <select name="paginator_select_page_limit" id="paginator_select_page_limit" psurl="{{ changeLimitUrl }}" class="pagination-link custom-select">
          {% if limit not in limit_choices %}
            <option value="{{ limit }}" selected="selected">{{ limit }}</option>
          {% endif %}
          {% for limit_choice in limit_choices %}
            <option value="{{ limit_choice }}" {% if limit==limit_choice %}selected="selected"{% endif %}>{{ limit_choice }}</option>
          {% endfor %}
            <option value="1000"{% if limit == 1000 %} selected{% endif %}>1000</option>
        </select>

src/PrestaShopBundle/Resources/views/Admin/Common/Grid/Blocks/table.html.twig
-----------------------------------------------------------------------------
41 :
        <tr{% for column in grid.columns %}{% if column.id == 'delivery_mode' %} class="{{ column_content(record, column, grid)|trim|replace({'À': 'a', ' ': "_"}) }}"{% endif %}{% endfor %}>
 
src/PrestaShopBundle/Resources/views/Admin/Improve/Design/Cms/edit.html.twig
----------------------------------------------------------------------------
28 :
{% block jscomposer %}{% endblock %}

src/PrestaShopBundle/Resources/views/Admin/Improve/Design/Cms/add.html.twig
---------------------------------------------------------------------------
28 :
{% block jscomposer %}{% endblock %}

src/PrestaShopBundle/Resources/views/Admin/Sell/Order/Order/index.html.twig
---------------------------------------------------------------------------
46 :
  {% block order_grid_row %}
    <div class="row">
      <div class="col-12 grid_panel">
        {% include '@PrestaShop/Admin/Common/Grid/grid_panel.html.twig' with {'grid': orderGrid} %}
      </div>
    </div>
  {% endblock %}

src/PrestaShopBundle/Resources/views/Admin/Sell/Order/Order/Blocks/View/products.html.twig
------------------------------------------------------------------------------------------
116 :
            <select id="orderProductsTablePaginationNumberSelector" class="pagination-link custom-select">
              {% for numPageOption in paginationNumOptions %}
                <option value="{{ numPageOption }}"{% if numPageOption == paginationNum %} selected{% endif %}>{{ numPageOption }}</option>
              {% endfor %}
                <option value="1000">1000</option>
            </select>

224 : avant 'For this customer...' 
      <div class="col-md-12">
        <div class="info-block">
          <div class="row">
            <div class="col-md-12">
              <p class="text-muted mb-0"><strong>Compléments</strong></p>
			  	<div style="display: flex; align-items: center; justify-content: space-between;">
				    <div class="complement">
						<label>Baguettes</label> : {{ complements.nb_baguettes }}
					</div>
					<div class="complement">
						<label>Sauce salée</label> : {{ complements.nb_sauce_salee }}
					</div>
					<div class="complement">
						<label>Sauce sucrée</label> : {{ complements.nb_sauce_sucree }}
					</div>
					<div class="complement">
						<label>Gingembre</label> : {{ complements.nb_gingembre }}
					</div>
					<div class="complement">
						<label>Wasabi</label> : {{ complements.nb_wasabi }}
					</div>
				</div>
            </div>

            <div class="col-md-12">
              <p class="text-muted mb-0"><strong>Aliments indésirables</strong></p>
				<div class="col-md-12">
					<div class="row">
						<div class="col-md-12">
						</div>
					</div>
				</div>
            </div>

          </div>
        </div>
      </div>


src/PrestaShopBundle/Resources/views/Admin/Sell/Order/Order/Blocks/View/shipping.html.twig
------------------------------------------------------------------------------------------
39 :
<table class="table">
    <thead>
      <tr>
        <th>{{ 'Date'|trans({}, 'Admin.Global') }}</th>
        <th>&nbsp;</th>
        <th>{{ 'Carrier'|trans({}, 'Admin.Shipping.Feature') }}</th>
        <th>{{ 'Weight'|trans({}, 'Admin.Global') }}</th>
        <th>{{ 'Shipping cost'|trans({}, 'Admin.Shipping.Feature') }}</th>
        <th>{{ 'Date et heure souhaitée'|trans({}, 'Admin.Shipping.Feature') }}</th>
        <th></th>
      </tr>
    </thead>

52 :
    {% for carrier in orderForViewing.shipping.carriers %}
        <tr>
          <td>{{ carrier.date|date_format_lite }}</td>
          <td>&nbsp;</td>
          <td>{{ carrier.name }}</td>
          <td>{{ carrier.weight }}</td>
          <td>{{ carrier.price }}</td>
          <td>
                {{ orderForViewing.delivery_when }}
          </td>
          <td class="text-right">
            {% if carrier.canEdit %}
              <a href="#"
                 class="js-update-shipping-btn d-print-none"
                 data-toggle="modal"
                 data-target="#updateOrderShippingModal"
                 data-order-carrier-id="{{ carrier.orderCarrierId }}"
                 data-order-tracking-number="{{ carrier.trackingNumber }}"
              >
                {{ 'Edit'|trans({}, 'Admin.Actions') }}
              </a>
            {% endif %}
          </td>
        </tr>
      {% endfor %}